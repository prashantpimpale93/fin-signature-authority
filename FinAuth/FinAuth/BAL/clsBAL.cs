﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using FinAuth.DAL;
using FinAuth.BO;
using System.Data.SqlClient;
namespace FinAuth.BAL
{
    public class clsBAL
    {
        public DataTable getPlant()
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable("select ID,PlantName from dbo.AS_Mst_Plant where status='Y' order by PlantName");
            return dt;
        }
        public DataTable getRegion()
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable("select ID,RegionName from dbo.AS_Mst_Region where status='Y' order by RegionName");
            return dt;
        }
        public DataTable getAddDetails()
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable("select ID,ReasonDetails from dbo.AS_Mst_Reason where status='Y'");
            return dt;
        }
        public DataTable getAccountNos(string ReqFor, string Location)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable("select distinct AccountNo,AccountNo from dbo.AS_Mst_BankDetails where Location='" + Location + "' and RequiredFor='" + ReqFor + "' and Status='Y'");
            return dt;
        }
        public DataTable getDeletionAccountNos(string Location)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable("select distinct AccountNo,AccountNo from dbo.AS_Mst_BankDetails where Location='" + Location + "' and Status='Y'");
            return dt;
        }
        /// <summary>
        /// akshay naik on 12-2-18 For CR
        /// </summary>
        
        /// <param name="Location"></param>
        /// <returns></returns>
        public DataTable getBanklist(string Location)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable("select distinct BankName from dbo.AS_Mst_BankDetails where Location='" + Location + "' and Status='Y' order by BankName ");
            return dt;
        }
        public DataTable getAccountDetails(string AccNo)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable("select distinct BankName,BankAddress from AS_Mst_BankDetails where AccountNo='" + AccNo + "'");
            return dt;
        }
        /// <summary>
        /// akshay naik on 9-2-18 For CR
        /// </summary>
        /// <param name="ReqFor"></param>
        /// <param name="Location"></param>
        /// <returns></returns>
        public DataTable getBanklist(string ReqFor, string Location)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable("select distinct BankName from dbo.AS_Mst_BankDetails where Location='" + Location + "' and RequiredFor='" + ReqFor + "' and Status='Y' order by BankName ");
            return dt;
        }
        public DataTable getAccountNos(string BankName)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable("select distinct AccountNo,AccountNo from dbo.AS_Mst_BankDetails where BankName='" + BankName + "' and Status='Y'");
            return dt;
        }
        public DataTable getExAuthSignDetails(string strLocation, string strReqFor, string strAccNo)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select A.Empid,B.EmpName,B.Designation,A.Type from AS_Mst_BankDetails as A inner join Mta_MstEmployee As B
                                               on A.Empid=B.EmpID where A.Location='" + strLocation + "' and A.RequiredFor='" + strReqFor + "' and A.AccountNo='" + strAccNo + "' and A.Status='Y'");
            return dt;
        }

        public DataTable getEmpDetails()
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select * from dbo.mta_mstemployee where EmpName <>'' and EmpActive=1 order by EmpName");
            return dt;
        }
        public DataTable getType()
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@" select * from dbo.AS_Mst_Type where Status='Y'");
            return dt;
        }
        public DataTable getDesignation(string empid)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select * from Mta_MstEmployee where EmpID='" + empid + "' ");
            return dt;
        }
        public DataTable getUniqueID()
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select isnull(Max(DeletionID),0) as DeletionID from AS_Trn_DeletionDetails");
            return dt;
        }
        public DataTable getDetailsAdd(string strDet)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select * from dbo.AS_Mst_Reason where ReasonDetails like '%" + strDet + "%'");
            return dt;
        }
        public DataTable getAddUniqueID()
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select isnull(Max(AdditionID),0) as AdditionID from AS_Trn_AdditionDetails");
            return dt;
        }
        public int insertAdditionDetails(clsBO ObjBO)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@AdditionID", ObjBO.AdditionID));
            db.Parameters.Add(new SqlParameter("@LocationType", ObjBO.LocationType));
            db.Parameters.Add(new SqlParameter("@LocationDetails", ObjBO.LocationDetails));
            db.Parameters.Add(new SqlParameter("@RequiredFor", ObjBO.RequiredFor));
            db.Parameters.Add(new SqlParameter("@AccountNO", ObjBO.AccountNO));
            db.Parameters.Add(new SqlParameter("@AdditionDetails", ObjBO.AdditionDetails));
            db.Parameters.Add(new SqlParameter("@Comments", ObjBO.Comments));
            db.Parameters.Add(new SqlParameter("@CreatedBy", ObjBO.CreatedBy));
            db.Parameters.Add(new SqlParameter("@SaveDraftFlag", ObjBO.SaveDraftFlag));
            int retrival = db.ExecuteNonQuery("AS_InsertAdditionDetails", CommandType.StoredProcedure);
            return retrival;
        }
        public int getCheckCount(string strEmpID, string strDelID)
        {
            int count = 0;
            clsDAL db = new clsDAL();
            count = Convert.ToInt32(db.ExecuteScalar("select count(*) as count from AS_Trn_DeletionMemberDetails where DeletionID='" + strDelID + "' and EmpID='" + strEmpID + "'"));
            return count;
        }
        public int insertDeletionDetails(clsBO ObjBO)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@DeletionID", ObjBO.AdditionID));
            db.Parameters.Add(new SqlParameter("@LocationType", ObjBO.LocationType));
            db.Parameters.Add(new SqlParameter("@LocationDetails", ObjBO.LocationDetails));
            db.Parameters.Add(new SqlParameter("@AccountNO", ObjBO.AccountNO));
            db.Parameters.Add(new SqlParameter("@DeletionDetails", ObjBO.AdditionDetails));
            db.Parameters.Add(new SqlParameter("@Comments", ObjBO.Comments));
            db.Parameters.Add(new SqlParameter("@CreatedBy", ObjBO.CreatedBy));
            db.Parameters.Add(new SqlParameter("@SaveDraftFlag", ObjBO.SaveDraftFlag));
            int retrival = db.ExecuteNonQuery("AS_InsertDeletionDetails", CommandType.StoredProcedure);
            return retrival;
        }
        public int UpdateAdditionDetails(clsBO ObjBO)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@AdditionID", ObjBO.AdditionID));
            db.Parameters.Add(new SqlParameter("@LocationType", ObjBO.LocationType));
            db.Parameters.Add(new SqlParameter("@LocationDetails", ObjBO.LocationDetails));
            db.Parameters.Add(new SqlParameter("@RequiredFor", ObjBO.RequiredFor));
            db.Parameters.Add(new SqlParameter("@AccountNO", ObjBO.AccountNO));
            db.Parameters.Add(new SqlParameter("@AdditionDetails", ObjBO.AdditionDetails));
            db.Parameters.Add(new SqlParameter("@Comments", ObjBO.Comments));
            db.Parameters.Add(new SqlParameter("@CreatedBy", ObjBO.CreatedBy));
            db.Parameters.Add(new SqlParameter("@SaveDraftFlag", ObjBO.SaveDraftFlag));
            int retrival = db.ExecuteNonQuery("AS_UpdateAdditionDetails", CommandType.StoredProcedure);
            return retrival;
        }
        public int UpdateDeletionDetails(clsBO ObjBO)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@DeletionID", ObjBO.AdditionID));
            db.Parameters.Add(new SqlParameter("@LocationType", ObjBO.LocationType));
            db.Parameters.Add(new SqlParameter("@LocationDetails", ObjBO.LocationDetails));
            db.Parameters.Add(new SqlParameter("@AccountNO", ObjBO.AccountNO));
            db.Parameters.Add(new SqlParameter("@DeletionDetails", ObjBO.AdditionDetails));
            db.Parameters.Add(new SqlParameter("@Comments", ObjBO.Comments));
            db.Parameters.Add(new SqlParameter("@CreatedBy", ObjBO.CreatedBy));
            db.Parameters.Add(new SqlParameter("@SaveDraftFlag", ObjBO.SaveDraftFlag));
            int retrival = db.ExecuteNonQuery("AS_UpdateDeletionDetails", CommandType.StoredProcedure);
            return retrival;
        }
        public int InsertAddMemberDetails(clsBO ObjBO)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@AdditionID", ObjBO.AdditionID));
            db.Parameters.Add(new SqlParameter("@EmpID", ObjBO.EmpID));
            db.Parameters.Add(new SqlParameter("@Type", ObjBO.Type));
            db.Parameters.Add(new SqlParameter("@Aadhar", ObjBO.Aadhar));
            db.Parameters.Add(new SqlParameter("@Otherdoc", ObjBO.Otherdoc));
            int retrival = db.ExecuteNonQuery("AS_InsertAddMemberDetails", CommandType.StoredProcedure);
            return retrival;
        }
        public int InsertDeletionMemberDetails(clsBO ObjBO)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@DeletionID", ObjBO.DeletionID));
            db.Parameters.Add(new SqlParameter("@RequiredFor", ObjBO.RequiredFor));
            db.Parameters.Add(new SqlParameter("@EmpID", ObjBO.EmpID));
            db.Parameters.Add(new SqlParameter("@EmpName", ObjBO.EmpName));
            db.Parameters.Add(new SqlParameter("@Designation", ObjBO.Designation));
            db.Parameters.Add(new SqlParameter("@Type", ObjBO.Type));
            int retrival = db.ExecuteNonQuery("AS_InsertDeletionMemberDetails", CommandType.StoredProcedure);
            return retrival;
        }
        public DataTable getDraftDetails()
        {
            clsDAL db = new clsDAL();
            DataTable dt = new DataTable();
            dt = db.FillDataTable("AS_getDraftDetails");
            return dt;
        }
        public DataTable getDeletionDraftDetails()
        {
            clsDAL db = new clsDAL();
            DataTable dt = new DataTable();
            dt = db.FillDataTable("AS_getDeletionDraftDetails");
            return dt;
        }
        public DataTable getAdditionReportDetails()
        {
            clsDAL db = new clsDAL();
            DataTable dt = new DataTable();
            dt = db.FillDataTable("AS_getAdditionReportDetails");
            return dt;
        }
        public DataTable getDeletionReportDetails()
        {
            clsDAL db = new clsDAL();
            DataTable dt = new DataTable();
            dt = db.FillDataTable("AS_getDeletionReportDetails");
            return dt;
        }

        public DataTable getAllDetails(string AdditionID)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select * from AS_Trn_AdditionDetails where AdditionID='" + AdditionID + "' and Status='A'");
            return dt;
        }
        public DataTable getAllDeletionDetails(string DeletionID)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select * from dbo.AS_Trn_DeletionDetails where DeletionID='" + DeletionID + "' and Status='A'");
            return dt;
        }
        public DataTable getNewMembDetails(string AdditionID)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select  A.EmpID,B.EmpName,B.Designation,A.Type,A.Aadhar,A.OtherDoc from AS_Trn_AdditionMemberDetails A inner join Mta_MstEmployee as B on A.EmpID=B.EmpID where A.AdditionID='" + AdditionID + "'");
            return dt;
        }
        public int DeleteMemberDetails(string AdditionID)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("delete from AS_Trn_AdditionMemberDetails where AdditionID='" + AdditionID + "'", CommandType.Text);
            return retrival;
        }
        public int removeDeletionMemberDetails(string DeletionID)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("delete from AS_Trn_DeletionMemberDetails where DeletionID='" + DeletionID + "'", CommandType.Text);
            return retrival;
        }
        public int getMemberCount(string AdditionID, string strEmpID)
        {
            clsDAL db = new clsDAL();
            int retrival = Convert.ToInt32(db.ExecuteScalar("select count(*) as count from AS_Trn_AdditionMemberDetails where AdditionID='" + AdditionID + "' and EmpID='" + strEmpID + "'"));
            return retrival;
        }
        public DataTable returnAllDetails(string AdditionID)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select A.AdditionID,case A.LocationType when 'P' then 'Plant' when 'H' then 'HO' when 'R' then 'Region' else '' 
                                               end as Location,A.LocationDetails,A.RequiredFor,A.CreatedBy,A.Date,A.Name,A.Treasury,B.EmpName,A.AccountNO,C.ReasonDetails,A.Comments from dbo.AS_Trn_AdditionDetails as A
                                               inner join Mta_MstEmployee as B on A.CreatedBy=B.EmpID 
                                               left join dbo.AS_Mst_Reason as C on C.ID=A.AdditionDetails
                                               where   A.Status='A' and A.AdditionID='" + AdditionID + "'");
            return dt;
        }
        public DataTable returnAllDeletionDetails(string DeletionID)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select A.DeletionID,case A.LocationType when 'P' then 'Plant' when 'H' then 'HO' when 'R' then 'Region' else '' 
                                                end as Location,A.LocationDetails,B.EmpName,A.CreatedBy,A.Date,A.Name,A.Treasury,A.AccountNO,C.ReasonDetails,A.Comments from dbo.AS_Trn_DeletionDetails as A
                                                inner join Mta_MstEmployee as B on A.CreatedBy=B.EmpID 
                                                left join dbo.AS_Mst_Reason as C on C.ID=A.DeletionDetails
                                                where   A.Status='A' and A.DeletionID='" + DeletionID + "'");
            return dt;
        }
        public DataTable getExdetails(string DeletionID)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@DeletionID", DeletionID));
            DataTable dt = db.FillDataTable("AS_AdditionSignatoryPrint");
            return dt;
        }
        public DataTable getExdetailsDeletion(string AccountNo)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@AccountNo", AccountNo));
            DataTable dt = db.FillDataTable("AS_getDeleExistDetails");
            return dt;
        }
        public DataTable getApproverComments(string RequestID)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@RequestID", RequestID));
            DataTable dt = db.FillDataTable("AS_getApproverComments");
            return dt;
        }
        public DataTable getMainApprovalTrackDetails()
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.FillDataTable("AS_getMainApprovalTrackDetails");
            return dt;
        }
        public DataTable getDetailApprovalTrackDetails(string strTrnID)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@TrnID", strTrnID));
            DataTable dt = db.FillDataTable("AS_getDetailApprovalTrackDetails");
            return dt;
        }
        public DataTable getInitiatorSupervisor(string Initiator)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select *from mta_mstemployee where EmpID in (select RepEmpID from mta_mstemployee where EmpID='" + Initiator + "')");
            return dt;
        }
        public DataTable getNextApprover(string Location, string LocatioDetails, string Flag)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"Select EmailID  from dbo.mta_mstemployee where EmpID=(select ApproverID from dbo.AS_Mst_ApprovalFlowTemplate where Locationtype='" + Location + "' and LocationName='" + LocatioDetails + "' and ApproverType='" + Flag + "' and Status='A')");
            return dt;
        }

        public DataTable getEmpDetails(string EmpID)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select *from mta_mstemployee where EmpID='" + EmpID + "'");
            return dt;
        }
        public DataTable getAppFirstAddition(string Initiator)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select *from mta_mstemployee where EmpID in (select RepEmpID from mta_mstemployee where EmpID='" + Initiator + "')");
            return dt;
        }
        public int insertApprovalFlowAddition(string AdditionID, string InitiatorID, string comments, string type)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("insert into dbo.AS_Trn_ApprovalFlow(TrnID,SeqNo,ApproverID,InitiatorID,Comments,ApprovalDatetime,Status,Authority)values('" + AdditionID + "',0,'" + InitiatorID + "','" + InitiatorID + "','" + comments + "',getdate(),'S','" + type + "')", CommandType.Text);
            return retrival;
        }
        public int updateApprovalFlowAdditionResubmit(string AdditionID, string InitiatorID, string comments, string type, int SeNo)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("update AS_Trn_ApprovalFlow set Status='S',ApprovalDatetime=getdate(),Comments='" + comments + "' where Trnid='" + AdditionID + "' and ApproverId='" + InitiatorID + "' and Status='P' and Seqno=" + SeNo + "", CommandType.Text);
            return retrival;
        }
        public int insertApprovalFlowAdditionNext(string AdditionID, string ApprovarID, string InitiatorID, string type)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("insert into dbo.AS_Trn_ApprovalFlow(TrnID,SeqNo,ApproverID,InitiatorID,Status,Authority)values('" + AdditionID + "',1,'" + ApprovarID + "','" + InitiatorID + "','P','" + type + "')", CommandType.Text);
            return retrival;
        }
        public int insertApprovalFlowAdditionNextResubmit(string AdditionID, string ApprovarID, string InitiatorID, string type, int SeqNo)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("insert into dbo.AS_Trn_ApprovalFlow(TrnID,SeqNo,ApproverID,InitiatorID,Status,Authority)values('" + AdditionID + "'," + SeqNo + ",'" + ApprovarID + "','" + InitiatorID + "','P','" + type + "')", CommandType.Text);
            return retrival;
        }
        public DataTable getPendingAdditionRequests(string ApproverID)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@ApproverID", ApproverID));
            DataTable dt = db.FillDataTable("AS_getPendingAddition");
            return dt;
        }
        public DataTable getPendingDeletionRequests(string ApproverID)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@ApproverID", ApproverID));
            DataTable dt = db.FillDataTable("AS_getPendingDeletion");
            return dt;
        }
        public DataTable getRejectedRequestsAddition(string InitiatorID)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@InitiatorID", InitiatorID));
            DataTable dt = db.FillDataTable("AS_getRejectedRequestsAddition");
            return dt;
        }
        public DataTable getRejectedRequestsDeletion(string InitiatorID)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@InitiatorID", InitiatorID));
            DataTable dt = db.FillDataTable("AS_getRejectedRequestsDeletion");
            return dt;
        }
        public int updateAppFlowAddition(string AdditionID, string ApproverID, string comments, int seqno, string type)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("update AS_Trn_ApprovalFlow set Status='A',ApprovalDatetime=getdate(),Comments='" + comments + "' where Trnid='" + AdditionID + "' and ApproverId='" + ApproverID + "'  and Status='P' and Seqno=" + seqno + "", CommandType.Text);  //
            return retrival;
        }
              //update for tr approval
        public int updateAppFlowAdditionTRHO(string AdditionID, string ApproverID, string comments, int seqno, string type)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("update AS_Trn_ApprovalFlow set Status='A',ApprovalDatetime=getdate(),Comments='" + comments + "', ApproverId='" + ApproverID + "' where Trnid='" + AdditionID + "' and Status='P' and Seqno=" + seqno + "", CommandType.Text);
            return retrival;
        }
        public int updateAppFlowAdditionTROther(string AdditionID, string ApproverID, string comments, int seqno, string type)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("update AS_Trn_ApprovalFlow set Status='A',ApprovalDatetime=getdate(),Comments='" + comments + "', ApproverId='" + ApproverID + "' where Trnid='" + AdditionID + "' and Status='P' and Seqno=" + seqno + "", CommandType.Text);
            return retrival;
        }
        public int updateSupervisorStatus(string AdditionID, string Flag)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("update AS_Trn_AdditionDetails set Supervisor='" + Flag + "' where AdditionID='" + AdditionID + "'", CommandType.Text);
            return retrival;
        }
        public int updateSupervisorStatusDeletion(string DeletionID, string Flag)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("update dbo.AS_Trn_DeletionDetails set Supervisor='" + Flag + "' where DeletionID='" + DeletionID + "'", CommandType.Text);
            return retrival;
        }
        public int updateTreasuryStatus(string AdditionID)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("update  AS_Trn_AdditionDetails set Treasury='F' where AdditionID='" + AdditionID + "'", CommandType.Text);
            return retrival;
        }
        public int updateTreasuryStatusDeletion(string DeletionID)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("update  AS_Trn_DeletionDetails set Treasury='F' where DeletionID='" + DeletionID + "'", CommandType.Text);
            return retrival;
        }
        public DataTable getNextApproverAddition(string strLocation, string strLocationtype, string ApproverID)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select * from dbo.AS_Mst_ApprovalFlowTemplate where  LocationType='" + strLocation + "' and LocationName='" + strLocationtype + "' and Status='A' and Sequence=(select Sequence+1 from dbo.AS_Mst_ApprovalFlowTemplate where ApproverID='" + ApproverID + "' and  LocationType='" + strLocation + "' and LocationName='" + strLocationtype + "' and Status='A')");
            return dt;
        }
        public DataTable getHOApprover(string strLocation)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select * from dbo.AS_Mst_ApprovalFlowTemplate where  LocationType='" + strLocation + "' and Status='A'");
            return dt;
        }
        public DataTable getSupStatusAddition(string AdditionID)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select Supervisor from dbo.AS_Trn_AdditionDetails where  AdditionID='" + AdditionID + "'");
            return dt;
        }
        public DataTable getSupStatusDeletion(string DeletionID)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select Supervisor from AS_Trn_DeletionDetails where  DeletionID='" + DeletionID + "'");
            return dt;
        }
        public DataTable getFirstAppAddition(string AdditionID, string strLocation, string strLocationtype)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select * from dbo.AS_Mst_ApprovalFlowTemplate where  LocationType='" + strLocation + "' and LocationName='" + strLocationtype + "' and Status='A' and Sequence=1");
            return dt;
        }
        public int insertNextApprovalFlowAddition(string AdditionID, string ApprovarID, string InitiatorID, string type, int seqno)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("insert into dbo.AS_Trn_ApprovalFlow(TrnID,SeqNo,ApproverID,InitiatorID,Status,Authority)values('" + AdditionID + "'," + seqno + ",'" + ApprovarID + "','" + InitiatorID + "','P','" + type + "')", CommandType.Text);
            return retrival;
        }
        public DataTable getApproverType(string strLocation, string strLocationtype, string ApproverID)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select * from dbo.AS_Mst_ApprovalFlowTemplate where  ApproverID='" + ApproverID + "' and LocationType='" + strLocation + "' and LocationName='" + strLocationtype + "' and Status='A'");
            return dt;
        }
        public DataTable getApproverTypeHO(string strLocation, string ApproverID)
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(@"select * from dbo.AS_Mst_ApprovalFlowTemplate where  ApproverID='" + ApproverID + "' and LocationType='" + strLocation + "' and  Status='A'");
            return dt;
        }
        public int updateTreasuryFieldsFirst(string AdditionID, string ApprovarID, string InitiatorID, string type, int seqno)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("insert into dbo.AS_Trn_ApprovalFlow(TrnID,SeqNo,ApproverID,InitiatorID,Status,Authority)values('" + AdditionID + "'," + seqno + ",'" + ApprovarID + "','" + InitiatorID + "','P','" + type + "')", CommandType.Text);
            return retrival;
        }
        public int UpdatetreasuryDetailsAddition(clsBO ObjBO)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@AdditionID", ObjBO.AdditionID));
            db.Parameters.Add(new SqlParameter("@Name", ObjBO.Name));
            db.Parameters.Add(new SqlParameter("@Comments", ObjBO.Comments));
            db.Parameters.Add(new SqlParameter("@Date", ObjBO.Date));
            db.Parameters.Add(new SqlParameter("@TreasuryType", ObjBO.TreasuryType));
            db.Parameters.Add(new SqlParameter("@ApproverID", ObjBO.ApproverID));
            db.Parameters.Add(new SqlParameter("@InitiatorID", ObjBO.InitiatorID));
            db.Parameters.Add(new SqlParameter("@NextSeq", ObjBO.NextSeq));
            db.Parameters.Add(new SqlParameter("@OriginalFname", ObjBO.OriginalFname));
            db.Parameters.Add(new SqlParameter("@ModifiedFname", ObjBO.ModifiedFname));
            int retrival = db.ExecuteNonQuery("AS_UpdatetreasuryDetailsAddition", CommandType.StoredProcedure);
            return retrival;
        }
        public int InsertFinalBankDetails(clsBO ObjBO)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@EmpID", ObjBO.EmpID));
            db.Parameters.Add(new SqlParameter("@RequiredFor", ObjBO.RequiredFor));
            db.Parameters.Add(new SqlParameter("@AccountNO", ObjBO.AccountNO));
            db.Parameters.Add(new SqlParameter("@Bank", ObjBO.Bank));
            db.Parameters.Add(new SqlParameter("@BankAddress", ObjBO.BankAddress));
            db.Parameters.Add(new SqlParameter("@LocationDetails", ObjBO.LocationDetails));
            db.Parameters.Add(new SqlParameter("@Type", ObjBO.Type));
            int retrival = db.ExecuteNonQuery("AS_InsertFinalBankDetails", CommandType.StoredProcedure);
            return retrival;
        }
        public int DeleteFinalBankDetails(clsBO ObjBO)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@DeletionID", ObjBO.DeletionID));
            db.Parameters.Add(new SqlParameter("@EmpID", ObjBO.EmpID));
            db.Parameters.Add(new SqlParameter("@RequiredFor", ObjBO.RequiredFor));
            db.Parameters.Add(new SqlParameter("@AccountNO", ObjBO.AccountNO));
            db.Parameters.Add(new SqlParameter("@Bank", ObjBO.Bank));
            db.Parameters.Add(new SqlParameter("@BankAddress", ObjBO.BankAddress));
            db.Parameters.Add(new SqlParameter("@LocationDetails", ObjBO.LocationDetails));
            db.Parameters.Add(new SqlParameter("@Type", ObjBO.Type));
            int retrival = db.ExecuteNonQuery("AS_DeleteFinalBankDetails", CommandType.StoredProcedure);
            return retrival;
        }
        public int UpdatetreasuryDetailsDeletion(clsBO ObjBO)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@AdditionID", ObjBO.AdditionID));
            db.Parameters.Add(new SqlParameter("@Name", ObjBO.Name));
            db.Parameters.Add(new SqlParameter("@Comments", ObjBO.Comments));
            db.Parameters.Add(new SqlParameter("@Date", ObjBO.Date));
            db.Parameters.Add(new SqlParameter("@TreasuryType", ObjBO.TreasuryType));
            db.Parameters.Add(new SqlParameter("@ApproverID", ObjBO.ApproverID));
            db.Parameters.Add(new SqlParameter("@InitiatorID", ObjBO.InitiatorID));
            db.Parameters.Add(new SqlParameter("@NextSeq", ObjBO.NextSeq));
            db.Parameters.Add(new SqlParameter("@OriginalFname", ObjBO.OriginalFname));
            db.Parameters.Add(new SqlParameter("@ModifiedFname", ObjBO.ModifiedFname));
            int retrival = db.ExecuteNonQuery("AS_UpdatetreasuryDetailsDeletion", CommandType.StoredProcedure);
            return retrival;
        }
        public int updateRejAppFlowAddition(string AdditionID, string ApproverID, string comments, int seqno, string type)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("update AS_Trn_ApprovalFlow set Status='R',ApprovalDatetime=getdate(),Comments='" + comments + "' where Trnid='" + AdditionID + "' and ApproverId='" + ApproverID + "' and Status='P' and Seqno=" + seqno + "", CommandType.Text);
            return retrival;
        }
        public int InsertRejAppFlowAddition(string AdditionID, string InitiatorID, string type, int seqno)
        {
            clsDAL db = new clsDAL();
            int retrival = db.ExecuteNonQuery("insert into dbo.AS_Trn_ApprovalFlow(TrnID,SeqNo,ApproverID,InitiatorID,Status,Authority)values('" + AdditionID + "'," + seqno + ",'" + InitiatorID + "','" + InitiatorID + "','P','" + type + "')", CommandType.Text);
            return retrival;
        }
                                //Akshay naik on 20-2-18 For CR 
        
        public DataTable GetReportBanklist()
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable("select distinct BankName from dbo.AS_Mst_BankDetails where Status='Y' order by BankName ");
            return dt;
        }
        public DataTable GetReportMemberlist()
        {
            clsDAL db = new clsDAL();
            DataTable dt = db.ReturnDataTable(" SELECT EmpID,EmpName  FROM mta_mstemployee WHERE EmpID in (SELECT  DISTINCT EmpID  from AS_Mst_BankDetails where Status='Y')  and EmpName<>'' and EmpActive=1  order by EmpName");
            return dt;
        }
        public DataTable GetMemberReport(string Empid,string Bank,string Account,string Type)
        {
            clsDAL db = new clsDAL();
            db.Parameters.Add(new SqlParameter("@EmpId", Empid));
            db.Parameters.Add(new SqlParameter("@Bank", Bank));
            db.Parameters.Add(new SqlParameter("@Accountno", Account));
            db.Parameters.Add(new SqlParameter("@Type", Type));
            DataTable dt = db.FillDataTable("AS_getMemberReportDetails");
            return dt;
        }
        public DataTable GetViewReport(string Bank, string Account, string Type, string ReqType)
        {
            clsDAL db = new clsDAL();
           
            db.Parameters.Add(new SqlParameter("@Bank", Bank));
            db.Parameters.Add(new SqlParameter("@Accountno", Account));
            db.Parameters.Add(new SqlParameter("@Type", Type));
            db.Parameters.Add(new SqlParameter("@Reqtype", ReqType));
            DataTable dt = db.FillDataTable("AS_getViewReportDetails");
            return dt;
        }
    }
}