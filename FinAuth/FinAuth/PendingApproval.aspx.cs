﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Net.Mail;
using System.IO;
using FinAuth.BLL;
using System.Data;
namespace FinAuth
{
    public partial class PendingApproval : System.Web.UI.Page
    {
        POA objpoa = new POA();
        FinAuthEntities1 fm = new FinAuthEntities1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["EmpID"] + "" == "" || Session["UserRole_Id"] + "" == "")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    n1.Text = "POA Approved Successfully";
                   // n1.Show();
                    int EmpID = Convert.ToInt32(Session["EmpID"]);
                    if (Request.QueryString["R"].ToString() == "S")
                    {
                        Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                from s in fm.Mststatus1
                                                from emp in fm.mta_mstemployee
                                                from d in fm.Trn_Approvalflow
                                              
                        .Where(c => c.POANO == m.PoaId && s.statusid==m.status && c.status == "P" && c.ApproverId.Value == EmpID && (m.status== 2 || m.status==5 ) && emp.EmpID==m.Initiator_id)
                                                select new { POANo = m.POANo, s.Status, m.PoaId, Initiator_Name = m.Initiator_Name,emp.Function, m.Comments, m.RequestReson, m.requesteddate }).OrderByDescending(s => s.requesteddate).Distinct().ToList();

                    }
                    else {
                        Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                from s in fm.Mststatus1
                                                from emp in fm.mta_mstemployee
                                                from d in fm.Trn_Approvalflow
                        .Where(c => c.POANO == m.PoaId && s.statusid == m.status && c.status == "P" && c.ApproverId.Value == EmpID && m.status == 1 && emp.EmpID == m.Initiator_id)
                                                select new { POANo = m.POANo, m.PoaId, s.Status, Initiator_Name = m.Initiator_Name, emp.Function, m.Comments, m.RequestReson, m.requesteddate }).OrderByDescending(s => s.requesteddate).Distinct().ToList();

                   
                    }                                   
                    
                    Rgrequest.DataBind();
                }
            }

        }

        protected void Rgrequest_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            int EmpID = Convert.ToInt32(Session["EmpID"]);
            if (Request.QueryString["R"].ToString() == "S")
            {
                Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                        from s in fm.Mststatus1
                                        from emp in fm.mta_mstemployee
                                        from d in fm.Trn_Approvalflow

                .Where(c => c.POANO == m.PoaId && s.statusid == m.status && c.status == "P" && c.ApproverId.Value == EmpID && (m.status == 2 || m.status == 5) && emp.EmpID == m.Initiator_id)
                                        select new { POANo = m.POANo, s.Status, m.PoaId, Initiator_Name = m.Initiator_Name, emp.Function, m.Comments,  m.RequestReson, m.requesteddate }).OrderByDescending(s => s.requesteddate).Distinct().ToList();

            }
            else
            {
                Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                        from s in fm.Mststatus1
                                        from emp in fm.mta_mstemployee
                                        from d in fm.Trn_Approvalflow
                .Where(c => c.POANO == m.PoaId && s.statusid == m.status && c.status == "P" && c.ApproverId.Value == EmpID && m.status == 1 && emp.EmpID == m.Initiator_id)
                                        select new { POANo = m.POANo, m.PoaId, s.Status, Initiator_Name = m.Initiator_Name, emp.Function, m.Comments,  m.RequestReson, m.requesteddate }).OrderByDescending(s => s.requesteddate).Distinct().ToList();


            } 
        }
    }
}