﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.Master" AutoEventWireup="true" CodeBehind="POASubmit.aspx.cs" Inherits="FinAuth.POASubmit" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheading" runat="server">
    Track POA
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Web20">
    </telerik:RadAjaxManager>

    <table style="width: 100%" align="center">

        <tr>
            <td colspan="2">
                <telerik:RadFormDecorator Skin="Web20" ID="FormDecorator1" runat="server" DecoratedControls="All"
                    EnableEmbeddedScripts="true" ControlsToSkip="Zone" />
                <telerik:RadGrid ID="Rgrequest" runat="server" Width="100%" AllowPaging="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="Rgrequest_NeedDataSource">

                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <PagerStyle PageSizes="2,5,10" AlwaysVisible="true" />
                    <MasterTableView PagerStyle-Mode="NextPrevAndNumeric" NoMasterRecordsText="No POA Request Exists on your Name">
                        <RowIndicatorColumn Visible="False">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Created="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                <ItemTemplate>
                                    <%# Container.DataSetIndex+1 %>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="POA Request No" UniqueName="POANo" SortExpression="POANo">
                                <ItemTemplate>
                                    <a href='<%#"POArequest.aspx?POANo="+Eval("PoaId") %>'>
                                        <asp:Label ID="Label1" runat="server" Text='<%#Bind("POANo") %>'></asp:Label></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <%--  <telerik:GridBoundColumn DataField="POANo" UniqueName="POANo" SortExpression="POANo"
                                HeaderText="POANo">
                            </telerik:GridBoundColumn>--%>
                            <telerik:GridBoundColumn DataField="requesteddate" UniqueName="requesteddate" SortExpression="requesteddate"
                                HeaderText="Date of Request" DataFormatString="{0:dd-MMM-yyyy}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Initiator_Name" UniqueName="Initiator_Name" SortExpression="Initiator_Name"
                                HeaderText="Employee Name">
                            </telerik:GridBoundColumn>
                            <%--<telerik:GridBoundColumn DataField="IONo" UniqueName="IONo" SortExpression="IONo" Visible="false"
                                HeaderText="IO No">
                            </telerik:GridBoundColumn>--%>

                            <telerik:GridBoundColumn DataField="RequestReson" UniqueName="RequestReson" SortExpression="RequestReson" Visible="false"
                                HeaderText="Request Reson">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Comments" UniqueName="Comments" SortExpression="Comments" Visible="false"
                                HeaderText="Purpose/Powers Conferred">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Status" UniqueName="Status" SortExpression="Status"
                                HeaderText="Status">
                            </telerik:GridBoundColumn>

                        </Columns>

                        <PagerStyle AlwaysVisible="True"></PagerStyle>
                    </MasterTableView>
                </telerik:RadGrid>

            </td>
        </tr>
    </table>
</asp:Content>
