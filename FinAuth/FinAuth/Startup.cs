﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FinAuth.Startup))]
namespace FinAuth
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
