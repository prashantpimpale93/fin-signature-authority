﻿using FinAuth.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinAuth
{
    public partial class Menu : System.Web.UI.MasterPage
    {
        FinAuthEntities1 fm = new FinAuthEntities1();
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["EmpID"] + "" == "" && Session["UserRole_Id"] + "" == "")
            {
                Response.Redirect("~/login.aspx", true);

            }
            else { lblUserName.Text = Session["UserName"].ToString(); }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["EmpID"] + "" == "" && Session["UserRole_Id"] + "" == "")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                lblUserName.Text = Session["UserName"].ToString();
                //if (fm.Mst_TreasTeam.ToList().Where(c => c.EmpId.ToString() == Session["EmpID"].ToString()).Count() == 1)
                //{
                //    shwTreasPendingSec.Visible = true;
                //    shwTreasrequest.Visible = true;
                //    shwTreasstatus.Visible = true;
                //    shwTreasRevoke_app.Visible = true;
                //    shwTreasrequest_revoke.Visible = true;
                //}
              //  else 
                if (fm.Mst_SecTeam.ToList().Where(c => c.EmpId.ToString() == Session["EmpID"].ToString()).Count() == 1)
                {
                    a.Visible = true;
                    liAdd.Visible = true;
                    b.Visible = false;  
                  /*  shwTreasPendingSec.Visible = true;
                    shwOtherPendingSec.Visible = true;
                    shwTreasPendingMgr.Visible = true;
                    shwOtherPendingMgr.Visible = true;
                    shwTreasRevoke_app.Visible = true;
                    shwTreasrequest_revoke.Visible = true;*/
                }
                else
                {
                    a.Visible = false;
                    liAdd.Visible = false;
                    b.Visible = true;

                    if (Session["URole"] + "" == "INI")
                    {
                        b.Visible = false;
                    }
                }
                if (Session["EmpID"] + "" == "16307")
                { shwsearch.Visible = true; }

            }

        }
        protected void imgBtnHom_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["EmpID"] != null)
            {
                Response.Redirect("Home.aspx", true);
            }
        }
        protected void imglogout_Click(object sender, ImageClickEventArgs e)
        {
            Session.Abandon();

            Response.Redirect("login.aspx", true);
        }



    }
}