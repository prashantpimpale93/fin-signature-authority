﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Net.Mail;
using System.IO;
using FinAuth.BLL;
using System.Data;


namespace FinAuth
{
    public partial class POASubmit : System.Web.UI.Page
    {



        POA objpoa = new POA();
        FinAuthEntities1 fm = new FinAuthEntities1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["EmpID"] + "" == "" || Session["UserRole_Id"] + "" == "")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    int EmpID = Convert.ToInt32(Session["EmpID"]);
                    Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                           from d in fm.Mststatus1
                   .Where(c => c.statusid == m.status && m.requestedby == EmpID && (m.status != 8 && m.status != 9))//&& (m.status == 0 || m.status == 3 || m.status == 4 || m.status == 7))
                                            select new { m.PoaId, m.POANo, m.Initiator_Name, m.IONo, m.requesteddate, m.RequestReson, m.Comments, d.statusid,d.Status }).OrderBy(s => s.statusid).ToList();
                   
                    Rgrequest.DataBind();
                }
            }

        }

        protected void Rgrequest_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            int EmpID = Convert.ToInt32(Session["EmpID"]);
            Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                    from d in fm.Mststatus1
            .Where(c => c.statusid == m.status && m.requestedby == EmpID && (m.status != 8 && m.status != 9))//&& (m.status == 0 || m.status == 3 || m.status == 4 || m.status == 7))
                                    select new { m.PoaId, m.POANo, m.Initiator_Name, m.IONo, m.requesteddate, m.RequestReson, m.Comments, d.statusid, d.Status }).OrderBy(s => s.statusid).ToList();

          
        }
    }
}
