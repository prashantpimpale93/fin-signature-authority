﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Net.Mail;
using System.IO;
using FinAuth.BLL;
using System.Data;
using System.Data.Entity;

namespace FinAuth
{
    public partial class ApprovePOA : System.Web.UI.Page
    {
        POA objpoa = new POA();
        FinAuthEntities1 fm = new FinAuthEntities1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["EmpID"] + "" == "" || Session["UserRole_Id"] + "" == "")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    var dtemp = fm.view_poa_details(Convert.ToInt32(Request.QueryString["POANo"])).ToList();

                    if (dtemp.Count > 0)
                    {
                        lblSubType.Text = dtemp[0].str_poaType.ToString();
                        if (dtemp[0].POATYPE.ToString() == "0")
                        {
                            tblMainRole.Visible = true;
                        }
                        else
                        {
                            tblMainRole.Visible = false;
                        }
                        lblReqFor.Text = dtemp[0].str_requestfor.ToString();
                        if (lblReqFor.Text == "On Behalf")
                        {
                            trreqid.Visible = true;
                            trreqname.Visible = true;
                        }
                        else
                        {
                            trreqid.Visible = false;
                            trreqname.Visible = false;
                        }
                        rcbempId.DataSource = fm.mta_mstemployee.ToList().Where(a => a.EmpID == dtemp[0].Initiator_id);
                        rcbempId.DataTextField = "EmpID";
                        rcbempId.DataValueField = "EmpID";
                        rcbempId.DataBind();
                        LabelempId.Text = rcbempId.SelectedItem.Text;
                        Session["RequestorId"] = dtemp[0].Initiator_id;
                        rcbEmpname.DataSource = fm.mta_mstemployee.ToList().Where(a => a.EmpID == dtemp[0].Initiator_id);
                        rcbEmpname.DataTextField = "EmpName";
                        rcbEmpname.DataValueField = "EmpID";
                        rcbEmpname.DataBind();
                        LabelEmpname.Text = rcbEmpname.SelectedItem.Text;
                        rcbRole.DataSource = fm.MstRoles.ToList().Where(a => a.RoleId == dtemp[0].RoleId);
                        rcbRole.DataTextField = "role";
                        rcbRole.DataValueField = "roleid";
                        rcbRole.DataBind();
                        rcbRole.Visible = false;
                        if (rcbRole.SelectedItem != null)
                        {
                            LabelRole.Text = Convert.ToString(rcbRole.SelectedItem.Text);
                        }
                        else { LabelRole.Text = ""; }
                        LabelRequestorname.Text = dtemp[0].requestorname.ToString();
                        LabelRequestorid.Text = dtemp[0].requestedby.ToString();
                        Rgrequest.DataSource = objpoa.usp_view_poa_details(Convert.ToInt32(Request.QueryString["POANo"])).Tables[1];
                        Rgrequest.DataBind();
                        rgRequestAdditional_selected.DataSource = objpoa.usp_view_poa_details(Convert.ToInt32(Request.QueryString["POANo"])).Tables[2];
                        rgRequestAdditional_selected.DataBind();
                        if (objpoa.usp_view_poa_details(Convert.ToInt32(Request.QueryString["POANo"])).Tables[2].Rows.Count > 0)
                        { tradditonal_pwr.Visible = true; }
                        else
                        {
                            tradditonal_pwr.Visible = false;
                        }
                        if (dtemp[0].Phisical_copy_required.ToString().ToLower() == "true")
                        {
                            trshowreason.Visible = true;
                        }
                        else
                        {
                            trshowreason.Visible = false;
                        }
                        txtPhisical_copy_request_reason.Text = Convert.ToString(dtemp[0].Phisical_copy_request_reason);
                        txtAddress.Text = dtemp[0].Address.ToString();
                        txtFatherName.Text = dtemp[0].Fname.ToString();
                        txtLocation.Text = dtemp[0].Location.ToString();
                        txtDesignation.Text = dtemp[0].Designation.ToString();
                        txtFunction.Text = dtemp[0].Function.ToString();
                        txtComments.Text = dtemp[0].Comments.ToString();
                        //    txtIOnumber.Text = dtemp[0].IONo.ToString();
                        txtPOAReason.Text = dtemp[0].RequestReson.ToString();
                        lblPOAholder.Text = dtemp[0].str_POAExist.ToString();
                        lblIsResolution.Text = dtemp[0].str_ISPOA_or_Resolutioncopy.ToString();
                        lblPhysicalCopyRequired.Text = dtemp[0].str_Phisical_copy_required.ToString();

                        lblisScanned.Text = dtemp[0].str_ResolutionCopy_format_isScanned.ToString();

                        Session["str_POANo"] = dtemp[0].POANo.ToString();
                        Session["requestedby"] = dtemp[0].requestedby.ToString();
                        TrNewComment_header.Visible = false;

                        switch (dtemp[0].status)
                        {
                            case 1:
                                {
                                    Session["Role"] = "HOD";
                                    if (fm.bindPriviousComments(Convert.ToInt32(Request.QueryString["POANo"])).ToList().Count > 0)
                                    {
                                        TrComments.Visible = true;
                                    }
                                    else
                                    {
                                        TrComments.Visible = false;
                                    }
                                    TrNewComment.Visible = true;
                                    TrNewComment_header.Visible = true;
                                    TrButton.Visible = true;
                                    TrProcess.Visible = false;
                                    break;
                                }
                            case 2:
                                {
                                    Session["Role"] = "SEC";
                                    TrNewComment.Visible = true;
                                    TrButton.Visible = true;
                                    TrProcess.Visible = false;
                                    TrNewComment_header.Visible = true;
                                    break;
                                }
                            case 5:
                                {
                                    Session["Role"] = "SEC";
                                    TrNewComment.Visible = false;
                                    TrButton.Visible = false;
                                    TrProcess.Visible = true;
                                    var dtemp1 = fm.generate_POASequenceNO().ToList();
                                    string POASequenceNO = "";
                                    int POAseqMaxId;
                                    if (dtemp1.Count > 0)
                                    {
                                        POASequenceNO = dtemp1[0].POASequenceNO.ToString();
                                        POAseqMaxId = Convert.ToInt32(dtemp1[0].POAseqMaxId);
                                        lblSeqNo.Text = POASequenceNO;

                                    }
                                    break;

                                }
                        }
                    }
                }
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {

            fm.POAapprovalflow("Reject", txtApprComments.Text, Convert.ToInt32(Request.QueryString["POANo"]), Convert.ToInt32(Session["EmpID"]), Session["Role"].ToString());
            //send mail

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Request Rejected Successfully.');window.location ='PendingApproval.aspx?R=" + Session["Role"].ToString().ElementAt(0) + "';",
true);
            //mail
            var dthod = fm.usp_getHOD_forApproval(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
            var dthodEmail = fm.getEmpDetails(Convert.ToInt32(dthod[0].hod.ToString())).ToList();
            string hodName = "", hodemailid = "", ccmail = "", iniName = "", OnBehalfEmail = "";
            if (dthodEmail.Count > 0)
            {
                hodName = dthodEmail[0].EmpName.ToString();
                hodemailid = dthodEmail[0].EmailID.ToString();
            }
            var dtiniEmail = fm.getEmpDetails(Convert.ToInt32(Session["requestedby"].ToString())).ToList();
            if (dtiniEmail.Count > 0)
            {
                ccmail = dtiniEmail[0].EmailID.ToString();
                iniName = dtiniEmail[0].EmpName.ToString();
            }
            var dtOnBehalfEmail = fm.getEmpDetails(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
            if (dtOnBehalfEmail.Count > 0)
            {
                OnBehalfEmail = dtOnBehalfEmail[0].EmailID.ToString();

            }
            if (Session["Role"] + "" == "HOD")
            {

                //send mail
                objpoa.sendMail_HOD_Rejected(Session["str_POANo"].ToString(), hodName, ccmail, hodemailid, txtApprComments.Text, iniName, OnBehalfEmail);
            }
            else
            {

                //send mail
                objpoa.sendMail_SEC_Rejected(Session["str_POANo"].ToString(), iniName, ccmail, hodemailid, txtApprComments.Text, OnBehalfEmail);
            }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            fm.POAapprovalflow("Approve", txtApprComments.Text, Convert.ToInt32(Request.QueryString["POANo"]), Convert.ToInt32(Session["EmpID"]), Session["Role"].ToString());

            //mail
            var dthod = fm.usp_getHOD_forApproval(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
            var dthodEmail = fm.getEmpDetails(Convert.ToInt32(dthod[0].hod.ToString())).ToList();
            string hodName = "", hodemailid = "", ccmail = "", iniName = "", OnBehalfEmail = "";
            if (dthodEmail.Count > 0)
            {
                hodName = dthodEmail[0].EmpName.ToString();
                hodemailid = dthodEmail[0].EmailID.ToString();
            }
            var dtiniEmail = fm.getEmpDetails(Convert.ToInt32(Session["requestedby"].ToString())).ToList();
            if (dtiniEmail.Count > 0)
            {
                ccmail = dtiniEmail[0].EmailID.ToString();
                iniName = dtiniEmail[0].EmpName.ToString();
            }
            var dtOnBehalfEmail = fm.getEmpDetails(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
            if (dtOnBehalfEmail.Count > 0)
            {
                OnBehalfEmail = dtOnBehalfEmail[0].EmailID.ToString();

            }
            //send mail
            if (Convert.ToString(Session["Role"]) == "HOD")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Request Approved Successfully.');window.location ='PendingApproval.aspx?R=" + Session["Role"].ToString().ElementAt(0) + "';", true);
                //send mail
                objpoa.sendMail_HOD_approval(Session["str_POANo"].ToString(), hodName, ccmail, hodemailid, txtApprComments.Text, OnBehalfEmail);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Request Approved Successfully.');window.location ='ApprovePOA.aspx?POANo=" + Convert.ToInt32(Request.QueryString["POANo"]) + "';",
    true);
                //send mail
                objpoa.sendMail_SEC_approval(Session["str_POANo"].ToString(), iniName, ccmail, hodemailid, OnBehalfEmail);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "window.location ='PendingApproval.aspx?R=" + Session["Role"].ToString().ElementAt(0) + "';", true);
        }

        protected void rgRequestAdditional_selected_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            rgRequestAdditional_selected.DataSource = objpoa.usp_view_poa_details(Convert.ToInt32(Request.QueryString["POANo"])).Tables[2];
        }

        protected void Rgrequest_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            Rgrequest.DataSource = objpoa.usp_view_poa_details(Convert.ToInt32(Request.QueryString["POANo"])).Tables[1];
        }

        protected void rgComments_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            rgComments.DataSource = fm.bindPriviousComments(Convert.ToInt32(Request.QueryString["POANo"]));

        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            UploadedFile file = RadAsyncUpload1.UploadedFiles[0];
            string path = file.FileName;
            string s = System.IO.Path.GetFileName(path);
            string strFileName = (Server.MapPath("~/UploadedFiles/") + Session["str_POANo"].ToString() + "_" + DateTime.Now.ToString("dd_MM_yy_hh_mm") + s);
            RadAsyncUpload1.UploadedFiles[0].SaveAs(strFileName);
            int POANo = Convert.ToInt32(Request.QueryString["POANo"]);
            string requestRefNo = Session["str_POANo"].ToString();

            //added to generate poa seq no

            var dtemp = fm.generate_POASequenceNO().ToList();
            string POASequenceNO = "";
            int POAseqMaxId;
            if (dtemp.Count > 0)
            {
                POASequenceNO = dtemp[0].POASequenceNO.ToString();
                POAseqMaxId = Convert.ToInt32(dtemp[0].POAseqMaxId);

                //-----------------------------------
                var reqPOA = (from o in fm.Mst_POArequest
                              where o.PoaId == POANo
                              select o).First();

                reqPOA.PoaId = Convert.ToInt32(Request.QueryString["POANo"]);
                reqPOA.POANo = requestRefNo;
                reqPOA.status = 7;
                reqPOA.POApdf = Session["str_POANo"].ToString() + "_" + DateTime.Now.ToString("dd_MM_yy_hh_mm") + s;
                reqPOA.POAtitle = txtTitle.Text;
                reqPOA.POAseqNo = POASequenceNO;
                reqPOA.POAseqMaxId = POAseqMaxId;
                using (var context = new FinAuthEntities1())
                {
                    context.Mst_POArequest.Attach(reqPOA);
                    context.Entry(reqPOA).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
            int empid = Convert.ToInt32(Session["EmpID"]);

            var reqflow = (from o in fm.Trn_Approvalflow
                           where o.status == "P" && o.POANO == POANo
                           select o).ToList();
            reqflow.ForEach(a =>
                     {
                         a.POANO = Convert.ToInt32(Request.QueryString["POANo"]);
                         a.status = "E";
                         a.remarks = txtProComment.Text;
                         a.Approvaldate = DateTime.Now;


                         using (var context = new FinAuthEntities1())
                         {
                             context.Trn_Approvalflow.Attach(a);
                             context.Entry(a).State = EntityState.Modified;
                             context.SaveChanges();
                         }
                     });
            //mail
            var dthod = fm.usp_getHOD_forApproval(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
            var dthodEmail = fm.getEmpDetails(Convert.ToInt32(dthod[0].hod.ToString())).ToList();
            string hodName = "", hodemailid = "", ccmail = "", iniName = "", OnBehalfEmail = "";
            if (dthodEmail.Count > 0)
            {
                hodName = dthodEmail[0].EmpName.ToString();
                hodemailid = dthodEmail[0].EmailID.ToString();
            }
            var dtiniEmail = fm.getEmpDetails(Convert.ToInt32(Session["requestedby"].ToString())).ToList();
            if (dtiniEmail.Count > 0)
            {
                ccmail = dtiniEmail[0].EmailID.ToString();
                iniName = dtiniEmail[0].EmpName.ToString();
            }
            var dtOnBehalfEmail = fm.getEmpDetails(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
            if (dtOnBehalfEmail.Count > 0)
            {
                OnBehalfEmail = dtOnBehalfEmail[0].EmailID.ToString();

            }
            objpoa.sendMail_SEC_EXECUTED(Session["str_POANo"].ToString(), iniName, ccmail, hodemailid, OnBehalfEmail);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Request Processed Successfully.');window.location ='PendingApproval.aspx?R=S';", true);
            // Response.Redirect("PendingApproval?R=S");
        }
    }
}