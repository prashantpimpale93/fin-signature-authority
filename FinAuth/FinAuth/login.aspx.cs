﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinAuth.BLL;
using System.Data.SqlClient;
using FinAuth.ADAuthentication;


public partial class login : System.Web.UI.Page
{
    DataSet ds;
    SqlConnection MssConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Conn"].ToString());
    login_cs objlogin = new login_cs();
    FinAuthEntities1 fm = new FinAuthEntities1();
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            //Session.Abandon();


           // txtPassword.Attributes.Add("onkeypress", "javascript:return filterInput(3,event)");
            txtPassword.Attributes.Add("onpaste", "javascript:return false;");
            txtPassword.Attributes.Add("oncopy", "javascript:return false;");
            //EMPId = Request.QueryString("ID")
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
             //   Session["Val"] = (Request.QueryString["val"]);
                LoginAccess(DecryptText(Request.QueryString["ID"]), "Milaap");
                Session["strUserID"] = Request.QueryString["ID"];
            }
            else
            {
                if (!string.IsNullOrEmpty(txtUserName.Text))
                {
                    LoginAccess(txtUserName.Text, "Outside");
                }
            }

        }
    }

    public void LoginAccess(string strEmpID, string strFrom)
    {
        try
        {
            if ((MssConn.State != ConnectionState.Open))
            {
                MssConn.Open();
            }
            string strSQL = null;
            string strSQL1 = null;
            string Mode = "";
            Mode = Request.QueryString["Flag"];
            string s = "";
            if (strFrom == "Outside")
            {
                //strSQL = " SELECT *,Convert(varchar,getDate(),107) As 'Today','User' as Access_By From Mta_mta_mta_mstemployee Where EmpID=@EmpID and Password=@password and EmpActive=1"
                strSQL = " SELECT *,Convert(varchar,getDate(),107) As 'Today','User' as Access_By From mta_mstemployee Where EmpID=@EmpID and Password=@password ";
            }
            else
            {
                //strSQL = " SELECT *,Convert(varchar,getDate(),107) As 'Today','User' as Access_By From Mta_mta_mstemployee Where EmpID=@EmpID and EmpActive=1"
                strSQL = " SELECT *,Convert(varchar,getDate(),107) As 'Today','User' as Access_By From mta_mstemployee Where EmpID=@EmpID ";
            }

            SqlCommand comlogin = new SqlCommand(strSQL, MssConn);
            comlogin.CommandType = CommandType.Text;

            Response.Write(strFrom + "<br><br><br>");
            Response.Write(comlogin.CommandText + "<br><br><br>");
            if (strFrom == "Outside")
            {
                comlogin.Parameters.AddWithValue("@EmpID", txtUserName.Text.Replace("'", "''"));
                comlogin.Parameters.AddWithValue("@password", txtPassword.Text.Replace("'", "''"));
            }
            else
            {
                comlogin.Parameters.AddWithValue("@EmpID", strEmpID.Replace("'", "''"));

            }


            SqlDataReader drlogin = default(SqlDataReader);
            drlogin = comlogin.ExecuteReader();
            //----------Addded by Vikas Gage on 08/12/2015---------------'
            string ID = strEmpID;
            ADAuthentication adAuth = new ADAuthentication();
            bool boolHRMS = adAuth.HRMSAuthentication(ID);
            bool boolAd = adAuth.IsAuthenticated(ID);
            //----------Ended by vikas Gage on 08/12/2015----------------'
             if (drlogin.HasRows)
             {
                drlogin.Read();
                if (ID.StartsWith("1"))
                {
                    if (boolHRMS && boolAd)
                    {
                        Session["Today"] = drlogin["Today"];
                        Session["EmpID"] = drlogin["EmpID"];
                        Session["EmpName"] = drlogin["EmpName"];
                        Session["Grade"] = drlogin["Grade"];
                        Session["SupID"] = drlogin["RepEmpID"];
                        Session["Designation"] = drlogin["Designation"].ToString();
                        Session["Department"] = drlogin["Department"];
                        Session["Access_by"] = drlogin["Access_By"];
                        Session["EmailIDTS"] = drlogin["EmailID"];
                        Session["Gender"] = drlogin["Gender"];
                        Session["FunctionLoger"] = drlogin["Gender"];
                        Session["Location"] = drlogin["Location"];
                        Session["Function"] = drlogin["Function"];
                        // Mahendra
                        Session["WorkingDays"] = drlogin["WorkingDays"];
                        string g = drlogin["Gender"].ToString();
                        Session["EmpID"] = drlogin["Empid"].ToString();
                        Session["UserRole_Id"] = drlogin["Grade"].ToString();
                        Session["UserName"] = drlogin["EmpName"].ToString();
                        if (drlogin["Grade"].ToString() == "Partner")
                        {
                            Session["URole"] = "HOD";
                        }
                        else if (fm.Mst_SecTeam.ToList().Where(c => c.EmpId.ToString() == strEmpID).Count() == 1)
                        {
                            Session["URole"] = "SEC";
                        }
                        else
                        {
                            Session["URole"] = "INI";
                        }
                        //Session.Timeout = 30
                        drlogin.Close();
                        //comlogin.Dispose()
                        //Response.End()
                        //Response.Redirect("Home.aspx")
                        if (strFrom == "Outside")
                        {
                            Session["From"] = "Outside";
                            //if (txtUserName.Text == loginMTA.Password)
                            //{
                            //    Response.Redirect("Mta_ChangePassword.aspx");
                            //}
                            // else
                            // {


                            Response.Redirect("Home.aspx");
                            // }
                            // }
                        }
                        else
                        {
                            Session["From"] = "Inside";

                            Response.Redirect("Home.aspx");

                            //End of Code
                            // }
                        }
                    }
                }
                else
                {
                    Session["Today"] = drlogin["Today"];
                    Session["EmpID"] = drlogin["EmpID"];
                    Session["EmpName"] = drlogin["EmpName"];
                    Session["Grade"] = drlogin["Grade"];
                    Session["SupID"] = drlogin["RepEmpID"];
                    Session["Designation"] = drlogin["Designation"].ToString();
                    Session["Department"] = drlogin["Department"];
                    Session["Access_by"] = drlogin["Access_By"];
                    Session["EmailIDTS"] = drlogin["EmailID"];
                    Session["Gender"] = drlogin["Gender"];
                    Session["FunctionLoger"] = drlogin["Gender"];
                    Session["Location"] = drlogin["Location"];
                    Session["Function"] = drlogin["Function"];
                    // Mahendra
                    Session["WorkingDays"] = drlogin["WorkingDays"];
                    string g = drlogin["Gender"].ToString();
                    Session["EmpID"] = drlogin["Empid"].ToString();
                    Session["UserRole_Id"] = drlogin["Grade"].ToString();
                    Session["UserName"] = drlogin["EmpName"].ToString();
                    if (drlogin["Grade"].ToString() == "Partner")
                    {
                        Session["URole"] = "HOD";
                    }
                    else if (fm.Mst_SecTeam.ToList().Where(c => c.EmpId.ToString() == strEmpID).Count() == 1)
                    {
                        Session["URole"] = "SEC";
                    }
                    else
                    {
                        Session["URole"] = "INI";
                    }
                    //Session.Timeout = 30
                    drlogin.Close();
                    //comlogin.Dispose()
                    //Response.End()
                    //Response.Redirect("Home.aspx")
                    if (strFrom == "Outside")
                    {
                        Session["From"] = "Outside";
                        //if (txtUserName.Text == loginMTA.Password)
                        //{
                        //    Response.Redirect("Mta_ChangePassword.aspx");
                        //}
                        // else
                        // {


                        Response.Redirect("Home.aspx");
                        // }
                        // }
                    }
                    else
                    {
                        Session["From"] = "Inside";

                        Response.Redirect("Home.aspx");

                        //End of Code
                        // }
                    }
                }
               
            }
            /*  else
              {
                  drlogin.Close();
                  strSQL1 = " SELECT *  FROM Mta_MstAdmins WHERE   OnlyAdmins =@OnlyAdmins and  adminID=@adminID and Password=@Password";

                  SqlCommand comlogin1 = new SqlCommand(strSQL1, MssConn);
                  comlogin1.CommandType = CommandType.Text;

                  comlogin1.Parameters.AddWithValue("@OnlyAdmins", 1);
                  comlogin1.Parameters.AddWithValue("@adminID", txtUserName.Text.Replace( "'", "''"));
                  comlogin1.Parameters.AddWithValue("@Password", txtPassword.Text.Replace( "'", "''"));

                  SqlDataReader drlogin1 = default(SqlDataReader);
                  drlogin1 = comlogin1.ExecuteReader();
                  Session["From"] = "Outside";
                  if (drlogin1.HasRows)
                  {
                      drlogin1.Read();
                      Session["EmpID"] = drlogin1["AdminID"];
                      Session["EmpName"] = drlogin1["EmpName"];
                      //Session["Grade"] = drlogin1["Grade"]
                      //Session["SupID"] = drlogin1["RepEmpID"]
                      Session["Access_by"] = drlogin1["Role"];
                      Session["EmailIDTS"] = drlogin1["EmailID"];
                      //Session.Timeout = 30
                      drlogin1.Close();
                      if (Mode == "Feedback")
                      {
                          Response.Redirect("MBR_Survey\\Survey.aspx");
                      }
                      else
                      {
                          Response.Redirect("Home.aspx", false);
                      }
                      //Response.Redirect("Home.aspx", False)
                  }
                  else
                  {
                      Response.Redirect("~/AccessDenied.aspx");
                  }
              }*/
        }
        catch (Exception ex)
        {

        }
        finally
        {
            MssConn.Close();
        }
    }




    private string DecryptText(string strEmpID)
    {
        int intSec = 0;
        string strDecEmpID = "";
        while ((intSec < strEmpID.Length))
        {
            strDecEmpID += strEmpID.Substring(intSec, 1);
            intSec += 6;
        }

        return strDecEmpID;

    }
    protected void imgbtnLogin_Click(object sender, ImageClickEventArgs e)
    {
        ds = objlogin.validatelogin(txtUserName.Text, txtPassword.Text);
      
        if (ds.Tables[0].Rows.Count > 0)
        {
            LoginAccess(txtUserName.Text, "Outside");
            Session["EmpID"] = ds.Tables[0].Rows[0]["Empid"].ToString();
            Session["UserRole_Id"] = ds.Tables[0].Rows[0]["Grade"].ToString();
            Session["UserName"] = ds.Tables[0].Rows[0]["EmpName"].ToString();
            if (ds.Tables[0].Rows[0]["Grade"].ToString() == "Partner")
            {
                Session["URole"] = "HOD";
            }
            else if (fm.Mst_SecTeam.ToList().Where(c => c.EmpId.ToString() == txtUserName.Text).Count() == 1)
            {
                Session["URole"] = "SEC";
            }
            else
            {
                Session["URole"] = "INI";
            }
            Response.Redirect("Home.aspx");
        }
        else
        {
            lblsms.Text = "Invalid UserName or Password. <br /> Passwords are case sensitive.";
        }

    }
}
