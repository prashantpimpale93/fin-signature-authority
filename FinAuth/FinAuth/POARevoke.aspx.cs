﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Telerik.Web.UI;

using System.Net.Mail;
using System.IO;
using FinAuth.BLL;
using System.Data;
using System.Data.Entity;

namespace FinAuth
{
    public partial class POARevoke : System.Web.UI.Page
    {
        POA objpoa = new POA();
        FinAuthEntities1 fm = new FinAuthEntities1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["EmpID"] + "" == "" || Session["UserRole_Id"] + "" == "")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (!IsPostBack)
                {

                    Rdpdate.MinDate = Convert.ToDateTime(DateTime.Now.ToString("dd-MMM-yyyy"));
                    rcbempId.DataSource = fm.bindEmpname();
                    rcbempId.DataTextField = "EmpID";
                    rcbempId.DataValueField = "EmpID";
                    rcbempId.DataBind();
                    rcbempId.Items.Insert(0, new RadComboBoxItem("-- All --", "0"));

                    rcbEmpname.DataSource = fm.bindEmpname();
                    rcbEmpname.DataTextField = "EmpName";
                    rcbEmpname.DataValueField = "EmpID";
                    rcbEmpname.DataBind();
                    rcbEmpname.Items.Insert(0, new RadComboBoxItem("-- All --", "0"));

                    if (Session["URole"] + "" == "SEC")
                    {
                        Rgrequest.Columns.FindByUniqueName("Initiator_Name").Visible = true;
                        pnlselEMP.Visible = true;
                        /* Rgrequest.DataSource = Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                                        from s in fm.Mststatus1
                                                                        from d in fm.Trn_Approvalflow
                                                .Where(c => c.POANO == m.PoaId && s.statusid == m.status && c.status == "E" && (m.status == 8))
                                                                        select new { POANo = m.POANo, s.Status, m.PoaId, d.Approvaldate, Initiator_Name = m.Initiator_Name, m.Comments,  m.RequestReson, m.requesteddate, m.POAtitle, m.POApdf }).OrderByDescending(s => s.requesteddate).Distinct().ToList();
                         * */
                        Rgrequest.DataSource = Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                                       from s in fm.Mststatus1
                                                                           // from d in fm.Trn_Approvalflow
                                               .Where(c => c.statusid == m.status && ((m.status == 8))
                                                   )
                                                                       select new { POANo = m.POANo, s.Status, m.PoaId, m.approveddate, Initiator_Name = m.Initiator_Name, m.Comments, m.RequestReson, m.requesteddate, m.POAtitle, m.POApdf, m.Initiator_id }).Distinct().ToList();

                    }
                    else
                    {
                        Rgrequest.Columns.FindByUniqueName("Initiator_Name").Visible = false;
                        pnlselEMP.Visible = false;
                        int Initiator_id = Convert.ToInt32(Session["EmpID"]);
                        /* Rgrequest.DataSource = Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                                        from s in fm.Mststatus1
                                                                        from d in fm.Trn_Approvalflow
                                                .Where(c => c.POANO == m.PoaId && s.statusid == m.status && c.status == "E" && m.Initiator_id == Initiator_id && (m.status == 8))
                                                                        select new { POANo = m.POANo, s.Status, m.PoaId, d.Approvaldate, Initiator_Name = m.Initiator_Name, m.Comments,  m.RequestReson, m.requesteddate, m.POAtitle, m.POApdf }).OrderByDescending(s => s.requesteddate).Distinct().ToList();
                         * */
                        Rgrequest.DataSource = Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                                       from s in fm.Mststatus1
                                                                           // from d in fm.Trn_Approvalflow
                                               .Where(c => c.statusid == m.status && m.Initiator_id == Initiator_id && ((m.status == 8))
                                                   )
                                                                       select new { POANo = m.POANo, s.Status, m.PoaId, m.approveddate, Initiator_Name = m.Initiator_Name, m.Comments, m.RequestReson, m.requesteddate, m.POAtitle, m.POApdf, m.Initiator_id }).Distinct().ToList();

                    }
                    Rgrequest.DataBind();
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "window.location ='POARevoke.aspx';", true);
            Response.Redirect("POARevoke.aspx");
        }

        protected void btnRevoke_Click(object sender, EventArgs e)
        {
            int count = 0;
            GridDataItemCollection dtCol = Rgrequest.Items;
            foreach (GridDataItem item in dtCol)
            {
                CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
                Label lblPOAid = (Label)item.FindControl("lblPOAid");
                Label lblInitiator_id = (Label)item.FindControl("lblInitiator_id");
                Label lblPOAno = (Label)item.FindControl("lblPOAno");
                
                if (chkselect.Checked == true)
                {
                    count++;

                    var reqPOA = (from o in fm.Mst_POArequest
                                  where o.PoaId + "" == lblPOAid.Text
                                  select o).First();

                    reqPOA.PoaId = Convert.ToInt32(lblPOAid.Text);

                    reqPOA.status = 9;
                    reqPOA.Revoke_by = Convert.ToInt32(Session["EmpId"]);
                    reqPOA.revoke_date = Convert.ToDateTime(DateTime.Now.ToString("dd-MMM-yyyy"));
                    reqPOA.revoke_reason = txtRevokeReason.Text;
                    reqPOA.revoke_eff_date = Convert.ToDateTime(Rdpdate.SelectedDate.Value.ToString("dd-MMM-yyyy"));
                    using (var context = new FinAuthEntities1())
                    {
                        context.Mst_POArequest.Attach(reqPOA);
                        context.Entry(reqPOA).State = EntityState.Modified;
                        context.SaveChanges();
                        //send mail
                        var dthod = fm.usp_getHOD_forApproval(Convert.ToInt32(lblInitiator_id.Text)).ToList();

                        var dthodEmail = fm.getEmpDetails(Convert.ToInt32(dthod[0].hod.ToString())).ToList();
                        string hodName = "", hodemailid = "", ccmail = "", iniName = "",onbehalfEmailId="";
                        if (dthodEmail.Count > 0)
                        {
                            hodName = dthodEmail[0].EmpName.ToString();
                            hodemailid = dthodEmail[0].EmailID.ToString();
                        }
                        var dtiniEmail = fm.getEmpDetails(Convert.ToInt32(lblInitiator_id.Text)).ToList();
                        if (dtiniEmail.Count > 0)
                        {
                            ccmail = dtiniEmail[0].EmailID.ToString();
                            iniName = dtiniEmail[0].EmpName.ToString();
                        }
                        objpoa.sendMail_POARevoked(lblPOAno.Text.ToString(), iniName, ccmail, hodemailid);

                    }
                }
            }
            if (count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your request for revocation of POA has been taken on record.');window.location ='POARevoke.aspx';",
    true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Please Select Atleast one POA to revoke rights.');window.location ='POARevoke.aspx';",
    true);
            }

        }

        protected void btnview_Click(object sender, EventArgs e)
        {
            int Initiator_id = Convert.ToInt32(rcbempId.SelectedValue);
            /* Rgrequest.DataSource = Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                            from s in fm.Mststatus1
                                                            from d in fm.Trn_Approvalflow
                                    .Where(c => c.POANO == m.PoaId && s.statusid == m.status && c.status == "E" && m.Initiator_id == Initiator_id && (m.status == 8))
                                                            select new { POANo = m.POANo, s.Status, m.PoaId, d.Approvaldate, Initiator_Name = m.Initiator_Name, m.Comments, m.RequestReson, m.requesteddate, m.POAtitle, m.POApdf }).Distinct().ToList();*/
            if (Initiator_id != 0)
            {
                Rgrequest.DataSource = Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                               from s in fm.Mststatus1

                                       .Where(c => c.statusid == m.status && m.Initiator_id == Initiator_id && m.Initiator_id == Initiator_id && ((m.status == 8))
                                           )
                                                               select new { POANo = m.POANo, s.Status, m.PoaId, m.approveddate, Initiator_Name = m.Initiator_Name, m.Comments, m.RequestReson, m.requesteddate, m.POAtitle, m.POApdf, m.Initiator_id }).Distinct().ToList();

            }
            else
            {
                Rgrequest.DataSource = Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                               from s in fm.Mststatus1

                                       .Where(c => c.statusid == m.status && m.Initiator_id == Initiator_id && ((m.status == 8))
                                           )
                                                               select new { POANo = m.POANo, s.Status, m.PoaId, m.approveddate, Initiator_Name = m.Initiator_Name, m.Comments, m.RequestReson, m.requesteddate, m.POAtitle, m.POApdf, m.Initiator_id }).Distinct().ToList();

            }
            Rgrequest.DataBind();
        }

        protected void Rgrequest_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (Session["URole"] + "" == "SEC")
            {
                Rgrequest.Columns.FindByUniqueName("Initiator_Name").Visible = true;
                pnlselEMP.Visible = true;
                if (rcbempId.SelectedValue == "0")
                {
                    Rgrequest.DataSource = Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                                   from s in fm.Mststatus1
                                                                       // from d in fm.Trn_Approvalflow
                                           .Where(c => c.statusid == m.status && ((m.status == 8))
                                               )
                                                                   select new { POANo = m.POANo, s.Status, m.PoaId, m.approveddate, Initiator_Name = m.Initiator_Name, m.Comments, m.RequestReson, m.requesteddate, m.POAtitle, m.POApdf, m.Initiator_id }).Distinct().ToList();

                }
                else
                {
                    int Initiator_id = Convert.ToInt32(rcbempId.SelectedValue);
                    Rgrequest.DataSource = Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                                   from s in fm.Mststatus1

                                           .Where(c => c.statusid == m.status && m.Initiator_id == Initiator_id && m.Initiator_id == Initiator_id && ((m.status == 8))
                                               )
                                                                   select new { POANo = m.POANo, s.Status, m.PoaId, m.approveddate, Initiator_Name = m.Initiator_Name, m.Comments, m.RequestReson, m.requesteddate, m.POAtitle, m.POApdf, m.Initiator_id }).Distinct().ToList();


                }
            }
            else
            {
                Rgrequest.Columns.FindByUniqueName("Initiator_Name").Visible = false;
                pnlselEMP.Visible = false;
                int Initiator_id = Convert.ToInt32(Session["EmpID"]);
                /* Rgrequest.DataSource = Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                                from s in fm.Mststatus1
                                                                from d in fm.Trn_Approvalflow
                                        .Where(c => c.POANO == m.PoaId && s.statusid == m.status && c.status == "E" && m.Initiator_id == Initiator_id && (m.status == 8))
                                                                select new { POANo = m.POANo, s.Status, m.PoaId, d.Approvaldate, Initiator_Name = m.Initiator_Name, m.Comments,  m.RequestReson, m.requesteddate, m.POAtitle, m.POApdf }).OrderByDescending(s => s.requesteddate).Distinct().ToList();
                 * */
                Rgrequest.DataSource = Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                               from s in fm.Mststatus1
                                                                   // from d in fm.Trn_Approvalflow
                                       .Where(c => c.statusid == m.status && m.Initiator_id == Initiator_id && ((m.status == 8))
                                           )
                                                               select new { POANo = m.POANo, s.Status, m.PoaId, m.approveddate, Initiator_Name = m.Initiator_Name, m.Comments, m.RequestReson, m.requesteddate, m.POAtitle, m.POApdf, m.Initiator_id }).Distinct().ToList();

            }
        }

        protected void rcbempId_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            rcbEmpname.SelectedValue = rcbempId.SelectedValue;
        }

        protected void rcbEmpname_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            rcbempId.SelectedValue = rcbEmpname.SelectedValue;
        }



    }

}