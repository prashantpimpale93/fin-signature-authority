<!--

/*
Configure menu styles below
NOTE: To edit the link colors, go to the STYLE tags and edit the ssm2Items colors
*/
YOffset=150; // no quotes!!
XOffset=0;
staticYOffset=30; // no quotes!!
slideSpeed=20 // no quotes!!
waitTime=100; // no quotes!! this sets the time the menu stays out for after the mouse goes off it.
menuBGColor="#672D91";
menuIsStatic="yes"; //this sets whether menu should stay static on the screen
menuWidth=200; // Must be a multiple of 10! no quotes!!
menuCols=2;
hdrFontFamily="verdana";
hdrFontSize="2";
hdrFontColor="white";
hdrBGColor="#672D91";
hdrAlign="left";
hdrVAlign="center";
hdrHeight="20";
linkFontFamily="Verdana";
linkFontSize="2";
linkBGColor="white";
linkOverBGColor="#F6E8FA";
linkTarget="_top";
linkAlign="Left";
barBGColor="#672D91";
barFontFamily="Verdana";
barFontSize="2";
barFontColor="white";
barVAlign="center";
barWidth=15; // no quotes!!
barText="PURPLE"; // <IMG> tag supported. Put exact html for an image to show.

///////////////////////////

// ssmItems[...]=[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
ssmItems[0]=["Menu"] //create header
ssmItems[1]=["DashBoard", "../DashBorad/frmCPDashBoard.aspx", ""]
//ssmItems[2]=["Channel Partner(s)", "../UI/frmChannelPartner.aspx",""]/// <reference path="../UI/frmSalesManager.aspx" />
//ssmItems[3]=["Customer Registration", "../UI/frmCustomer.aspx", ""]/// <reference path="../UI/frmManager.aspx" />
ssmItems[2]=["FAQ", "#", ""] // for new page use like this ssmItems[4]=["FAQ", "#", "_new"]
ssmItems[3]=["Feedback & Question", "#", ""]
ssmItems[4]=["#", "#", ""]
buildMenu();

//-->