﻿<%@ Page Title="POA Request" Language="C#" MasterPageFile="~/Menu.Master" AutoEventWireup="true" CodeBehind="POARequest.aspx.cs" Inherits="FinAuth.POARequest" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="Scripts/jquery-1.10.2.min.js"></script>


        <script type="text/javascript">
         
            function fu1() {
                if (window.document.getElementById("<%=btnSubmit.ClientID %>") != undefined && window.document.getElementById("<%=btnSubmit.ClientID %>") != null) {
                    window.document.getElementById("<%=btnSubmit.ClientID %>").disabled = true;
                }
                if (window.document.getElementById("<%=btnDraft.ClientID %>") != undefined && window.document.getElementById("<%=btnDraft.ClientID %>") != null) {
                    window.document.getElementById("<%=btnDraft.ClientID %>").disabled = true;
                }
                if (window.document.getElementById("<%=btnProcess.ClientID %>") != undefined && window.document.getElementById("<%=btnProcess.ClientID %>") != null) {
                    window.document.getElementById("<%=btnProcess.ClientID %>").disabled = true;
                }
            }
          <%--  function fu2() { window.document.getElementById("<%=btnSubmit.ClientID %>").disabled = false; }--%>
            function chkterms11(object, args) {
                if (window.document.getElementById("<%=chkTerms.ClientID %>") != undefined && window.document.getElementById("<%=chkTerms.ClientID %>") != null && window.document.getElementById("<%=chkTerms.ClientID %>").checked == false) {
                    //   alert("Please Accept Terms and Conditions");
                    //  object.errormessage ="Please Accept Terms and Conditions";
                    args.IsValid = false;
                    return false;
                }
            }
                function chkterms1(object, args) {
                    if (window.document.getElementById("ctl00_ContentPlaceHolder1_rcbAdditionalRole") != undefined
                        && window.document.getElementById("ctl00_ContentPlaceHolder1_rcbAdditionalRole") != null
                        && (window.document.getElementById("ctl00_ContentPlaceHolder1_rgRequestAdditional_selected") == undefined || window.document.getElementById("ctl00_ContentPlaceHolder1_rgRequestAdditional_selected") == null)) {
                        //  window.document.getElementById("ctl00_ContentPlaceHolder1_rgRequestAdditional_selected").focus();
                        //  alert("Additional Request can not be blank.");
                        //  object.errormessage = "Additional Request can not be blank.";
                        args.IsValid = false;
                        return false;
                    }

                    // args.IsValid = true;
                    return true;

                }
                ////function WebForm_OnSubmit() {
                ////    if (typeof (ValidatorOnSubmit) == "function" && ValidatorOnSubmit() == false) {
                ////        $("#validation_dialog").dialog({
                ////            title: "Validation Error!",
                ////            modal: true,
                ////            resizable: false,
                ////            enabled: true,
                ////            buttons: {
                ////                Close: function () {
                ////                    $(this).dialog('close');
                ////                }
                ////            }
                ////        });
                ////        return false;
                ////    }
                ////    return true;
                ////}

                <%-- function EnableVS()
            {
                debugger;
                document.getElementById("ContentPlaceHolder1_ValidationSummary2").enable = true;
                if (Page_IsValid != null && Page_IsValid == true) {
                    alert("valid");
                    
                }
                else {

                    alert("not valid");
                }

            }--%>
        </script>

    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Contentheading" runat="server">
    POA REQUEST
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Web20">

        <AjaxSettings>
            <%--  <telerik:AjaxSetting AjaxControlID="rbPhysicalCopyRequired">
                <UpdatedControls>

                    <telerik:AjaxUpdatedControl ControlID="trshowreason"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="txtPhisical_copy_request_reason"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RequiredFieldValidator14"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" ClientEvents-OnRequestStart="fu1" >
        <table style="width: 100%" align="center">
            <tr>
                <td colspan="2" style="align-items: center">
                    <h4 style="color: #3768AD; font-weight: bold">Select type</h4>
                    <%--  <fieldset>--%>

                    <table style="width: 50%; line-height: 15px;" align="center">
                        <tr>
                            <td></td>
                            <td>
                                <asp:RadioButtonList ID="rbSubType" runat="server" RepeatDirection="Horizontal" CssClass="RadButton RadButton_Metro rbToggleButton" AutoPostBack="True" OnSelectedIndexChanged="rbSubType_SelectedIndexChanged">
                                    <asp:ListItem Text="Role Based" Value="0" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Temporary" Value="1"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding: 7px">POA to be issued to :
                            </td>
                            <td style="vertical-align: top">
                                <asp:RadioButtonList ID="rbReqFor" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList2_SelectedIndexChanged" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Self" Value="0" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="On Behalf" Value="1"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table style="width: 100%" id="tblonbehalf" runat="server" visible="false" align="center">
                                    <tr>
                                        <td>Select employee name<strong style="color: red; font-size: smaller">*</strong>
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="rcbEmpname" runat="server" AutoPostBack="true" AllowCustomText="false" Filter="StartsWith" MaxHeight="250px" OnSelectedIndexChanged="rcbEmpname_SelectedIndexChanged"></telerik:RadComboBox>
                                            <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Select Employee Name"
                                                SetFocusOnError="true" Display="Dynamic" InitialValue="--Select--" ValidationGroup="Save" ControlToValidate="rcbEmpname">*</asp:RequiredFieldValidator>
                                        </td>
                                        <%--OnClientSelectedIndexChanged="OnClientSelectedIndexChanged1"--%>
                                    </tr>
                                    <tr>
                                        <td>Select employee id<strong style="color: red; font-size: smaller">*</strong>
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="rcbempId" runat="server" AutoPostBack="true" Filter="StartsWith" MaxHeight="250px"
                                                OnSelectedIndexChanged="rcbempId_SelectedIndexChanged">
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please select employee id"
                                                SetFocusOnError="true" Display="Dynamic" InitialValue="--Select--" ValidationGroup="Save" ControlToValidate="rcbempId">*</asp:RequiredFieldValidator>
                                        </td>
                                        <%--OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"--%>
                                    </tr>

                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:Button ID="btnview" runat="server" Text="View" SkinID="Web20" Width="50px" Visible="false"
                                                OnClientClick="return funview();" OnClick="btnview_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <%-- </fieldset>--%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <h4 style="color: #3768AD; font-weight: bold">Select power</h4>
                    <%--  <fieldset>--%>
                    <table style="width: 100%">
                        <tr>
                            <td colspan="2">

                                <table style="width: 100%" align="center" id="tblMainRole" runat="server" visible="true">
                                    <tr>
                                        <td nowarp="nowarp">Select role<strong style="color: red; font-size: smaller">*</strong>
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="rcbRole" runat="server" MaxHeight="250px" AutoPostBack="True" OnSelectedIndexChanged="rcbRole_SelectedIndexChanged"></telerik:RadComboBox>
                                            <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please select role"
                                                SetFocusOnError="true" Display="Dynamic" InitialValue="-- Select --" ValidationGroup="Save" ControlToValidate="rcbRole">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div style="height: 150px; overflow-y: auto; width: 100%">
                                                <telerik:RadGrid ID="Rgrequest" runat="server" Width="98%" AllowPaging="false" HeaderStyle-Font-Bold="true"
                                                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="Rgrequest_NeedDataSource">
                                                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                                                    <ItemStyle Font-Bold="false" />
                                                    <MasterTableView PagerStyle-Mode="NextPrevAndNumeric" NoMasterRecordsText="No powers exists for this role">
                                                        <RowIndicatorColumn Visible="False">
                                                        </RowIndicatorColumn>
                                                        <ExpandCollapseColumn Created="True">
                                                        </ExpandCollapseColumn>
                                                        <Columns>
                                                            <telerik:GridTemplateColumn HeaderText="Sr No." Visible="true">
                                                                <ItemTemplate>
                                                                    <%# Container.DataSetIndex+1 %>
                                                                    <asp:Label ID="lblPowerid2" runat="server" Text='<%#Bind("PowerId") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblRoleid2" runat="server" Text='<%#Bind("RoleId") %>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridBoundColumn DataField="PowerId" UniqueName="PowerId" SortExpression="PowerId" Visible="false"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="powerDesc" UniqueName="powerDesc" SortExpression="powerDesc"
                                                                HeaderText="Description of Power">
                                                            </telerik:GridBoundColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                </telerik:RadGrid>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr runat="server" id="tr_Template" visible="false">
                                        <td style="vertical-align: bottom">Template view &nbsp; &nbsp; &nbsp;   <a id="role_template" runat="server" target="_blank">
                                            <img src="images/pdf_Icon_16.png" height="45px" width="40px" /></a>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr runat="server" id="trAdditional_req">
                            <td style="vertical-align: top; width: 450px; padding: 7px">Are additional powers required?
                            </td>
                            <td style="text-align: left; vertical-align: top">
                                <asp:RadioButtonList ID="rblistAdded_power" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rblistAdded_power_SelectedIndexChanged">
                                    <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="false" Selected="True"></asp:ListItem>
                                </asp:RadioButtonList>

                            </td>


                        </tr>

                        <tr style="width: 100%" align="center" runat="server" id="trAdditional" visible="false">
                            <td colspan="2">
                                <%-- <fieldset>
                                <legend>Select Additional Powers</legend>--%>
                                <table style="width: 100%;" cellspacing="5" cellpadding="3">
                                    <tr>
                                        <td>Select role<strong style="color: red; font-size: smaller">*</strong>
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="rcbAdditionalRole" runat="server" MaxHeight="250px" AutoPostBack="true"
                                                OnSelectedIndexChanged="rcbAdditionalRole_SelectedIndexChanged">
                                            </telerik:RadComboBox>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div id="div_rgRequestAdditional" runat="server" style="height: 150px; overflow-y: auto; width: 100%">
                                                <telerik:RadGrid ID="rgRequestAdditional" runat="server" Width="98%" AllowPaging="false" HeaderStyle-Font-Bold="true"
                                                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnItemDataBound="rgRequestAdditional_ItemDataBound" OnNeedDataSource="rgRequestAdditional_NeedDataSource">
                                                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                                                    <MasterTableView PagerStyle-Mode="NextPrevAndNumeric" NoMasterRecordsText="No powers exists for this role">
                                                        <RowIndicatorColumn Visible="False">
                                                        </RowIndicatorColumn>
                                                        <ExpandCollapseColumn Created="True">
                                                        </ExpandCollapseColumn>
                                                        <Columns>
                                                            <telerik:GridTemplateColumn HeaderText="Sr No." Visible="false">
                                                                <ItemTemplate>
                                                                    <%# Container.DataSetIndex+1 %>
                                                                    <asp:Label ID="lblPowerid" runat="server" Text='<%#Bind("PowerId") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblRoleid" runat="server" Text='<%#Bind("RoleId") %>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>

                                                            <telerik:GridBoundColumn DataField="powerDesc" UniqueName="powerDesc" SortExpression="powerDesc"
                                                                HeaderText="Description of Additional Powers">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PowerId" UniqueName="PowerId" SortExpression="PowerId" Visible="false"></telerik:GridBoundColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Select">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                </telerik:RadGrid>

                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label3" runat="server" Text="List of additional powers to be added in your POA :"></asp:Label>
                                            <div style="height: 150px; overflow-y: auto; width: 100%">
                                                <telerik:RadGrid ID="rgRequestAdditional_selected" runat="server" Width="98%" AllowPaging="false" HeaderStyle-Font-Bold="true"
                                                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="rgRequestAdditional_selected_NeedDataSource" OnItemCommand="rgRequestAdditional_selected_ItemCommand">
                                                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                                                    <MasterTableView PagerStyle-Mode="NextPrevAndNumeric" DataKeyNames="PowerId" NoMasterRecordsText="No powers exists for this role">
                                                        <RowIndicatorColumn Visible="False">
                                                        </RowIndicatorColumn>
                                                        <ExpandCollapseColumn Created="True">
                                                        </ExpandCollapseColumn>
                                                        <Columns>
                                                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                                                <ItemTemplate>
                                                                    <%# Container.DataSetIndex+1 %>
                                                                    <asp:Label ID="lblPowerid1" runat="server" Text='<%#Bind("PowerId") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblRoleid1" runat="server" Text='<%#Bind("RoleId") %>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridBoundColumn DataField="Role" UniqueName="Role" SortExpression="Role"
                                                                HeaderText="Role">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="powerDesc" UniqueName="powerDesc" SortExpression="powerDesc"
                                                                HeaderText="Description of Additional Powers">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PowerId" UniqueName="PowerId" SortExpression="PowerId" Visible="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridButtonColumn Text="Delete" UniqueName="delete" CommandName="Delete" ButtonType="ImageButton" ItemStyle-Width="5px" ItemStyle-Height="5px" />
                                                        </Columns>
                                                    </MasterTableView>

                                                </telerik:RadGrid>
                                                <asp:CustomValidator ID="CustomValidator3" runat="server" ErrorMessage="Please add additional powers" ForeColor="Red"
                                                    ClientValidationFunction="chkterms1" SetFocusOnError="true"
                                                    Display="Dynamic" ValidationGroup="Save">*</asp:CustomValidator>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <%-- </fieldset>--%>
                            </td>
                        </tr>
                    </table>
                    <%--</fieldset>--%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <h4 style="color: #3768AD; font-weight: bold">Request powers</h4>
                </td>
            </tr>
            <tr>

                <td width="450px" style="padding: 7px; vertical-align: top">Do you need any other powers which are not listed above?
                </td>
                <td  style="text-align: left; vertical-align: top">
                    <asp:RadioButtonList ID="rcbRaiseNew" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rcbRaiseNew_SelectedIndexChanged">
                        <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                        <asp:ListItem Text="No" Value="false" Selected="True"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="width: 95%" align="center" runat="server" id="tblraisenew" visible="false">
                        <tr>
                            <td colspan="2" style="width: 100%">
                                <h4>Note : If you could not find the desired powers in the above section, please  enter the description of power in the field below. You will be notified when the request is approved by the Secretarial Team, you can then resume for raising the request for POA.</h4>

                            </td>
                        </tr>
                        <tr>
                            <td>Description of power :<strong style="color: red; font-size: smaller">*</strong></td>
                            <td>
                                <asp:TextBox ID="txtpowerDesc" runat="server" Width="500px" TextMode="MultiLine" Rows="5" Columns="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please Enter Power Description"
                                    SetFocusOnError="true" Display="Dynamic" ValidationGroup="additional" ControlToValidate="txtpowerDesc">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Reason for raising power :<strong style="color: red; font-size: smaller">*</strong></td>
                            <td>
                                <asp:TextBox ID="txtRaisepowerReason" runat="server" Width="500px" TextMode="MultiLine" Rows="5" Columns="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please Enter Power Description"
                                    SetFocusOnError="true" Display="Dynamic" ValidationGroup="additional" ControlToValidate="txtRaisepowerReason">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>

                            <td align="center" colspan="2">
                                <asp:Button ID="btnsendMail" runat="server" Text="Submit" SkinID="Web20" Width="100px"
                                    ValidationGroup="additional" OnClick="btnsendMail_Click" />

                            </td>
                        </tr>
                    </table>


                    <table style="width: 100%" align="center" runat="server" id="tbldesc" visible="false">
                        <tr>
                            <td colspan="3">
                                <h4 style="color: #3768AD; font-weight: bold">Provide POA details</h4>
                            </td>
                        </tr>

                        <tr>
                            <td>1.</td>
                            <td style="padding-left: 7px;">Designation<strong style="color: red; font-size: smaller">*</strong></td>
                            <td style="text-align: right">
                                <asp:TextBox ID="txtDesignation" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please Enter Designation" SetFocusOnError="true" Display="Dynamic"
                                    ValidationGroup="Save" ControlToValidate="txtDesignation">*</asp:RequiredFieldValidator>

                            </td>
                        </tr>

                        <tr>
                            <td>2.</td>
                            <td style="padding-left: 7px;white-space:nowrap">Father's name/Husband's name<strong style="color: red; font-size: smaller">*</strong></td>
                            <td style="text-align: right">
                                <asp:TextBox ID="txtFatherName" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please Enter Father's Name"
                                    SetFocusOnError="true" Display="Dynamic" ValidationGroup="Save" ControlToValidate="txtFatherName">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td style="padding-left: 7px;">Present address<strong style="color: red; font-size: smaller">*</strong></td>
                            <td style="text-align: right">
                                <asp:TextBox ID="txtAddress" runat="server" Width="300px" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please Enter Address" SetFocusOnError="true" Display="Dynamic"
                                    ValidationGroup="Save" ControlToValidate="txtAddress">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td style="padding-left: 7px;">Function<strong style="color: red; font-size: smaller">*</strong></td>
                            <td style="text-align: right">
                                <asp:TextBox ID="txtFunction" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please Enter Functions" SetFocusOnError="true" Display="Dynamic"
                                    ValidationGroup="Save" ControlToValidate="txtFunction">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>5.</td>
                            <td style="padding-left: 7px;">Location<strong style="color: red; font-size: smaller">*</strong></td>
                            <td style="text-align: right">
                                <asp:TextBox ID="txtLocation" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator11" runat="server" ErrorMessage="Please Enter Location" SetFocusOnError="true" Display="Dynamic"
                                    ValidationGroup="Save" ControlToValidate="txtLocation">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td>6.</td>
                            <td style="padding-left: 7px;">Comments </td>
                            <td style="text-align: right">
                                <asp:TextBox ID="txtComments" runat="server" Width="300px" MaxLength="50" TextMode="MultiLine" Rows="2"></asp:TextBox>
                             <%--   <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator12" runat="server" ErrorMessage="Please Enter Comments" SetFocusOnError="true" Display="Dynamic"
                                    ValidationGroup="Save" ControlToValidate="txtComments">*</asp:RequiredFieldValidator>--%>

                            </td>
                        </tr>
                        <%-- <tr>

                        <td>IO Number<strong style="color: red; font-size: smaller">*</strong></td>
                        <td style="text-align: right">
                            <asp:TextBox ID="txtIOnumber" runat="server" Width="300px" MaxLength="50"></asp:TextBox></td>
                    </tr>--%>
                        <tr>
                            <td>7.</td>
                            <td style="padding-left: 7px;">Reason for POA<strong style="color: red; font-size: smaller">*</strong></td>
                            <td style="text-align: right;">
                                <asp:TextBox ID="txtPOAReason" runat="server" Width="300px" MaxLength="500" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator13" runat="server" ErrorMessage="Please Enter Reason for POA"
                                    SetFocusOnError="true" Display="Dynamic"
                                    ValidationGroup="Save" ControlToValidate="txtPOAReason">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td style=" padding-top: 7px;vertical-align: top;">8.</td>
                            <td style="width: 450px; padding: 7px; vertical-align: top">Whether you have any active POA issued in your name?
                            </td>
                            <td style="text-align: right">
                                <asp:RadioButtonList ID="rbPOAholder" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="false" Selected="True"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>

                        <tr>
                            <td style=" padding-top: 7px;vertical-align: top;">9.</td>
                            <td style="width: 450px; padding: 7px; vertical-align: top">Whether copy of resolution is required?
                            </td>
                            <td style="text-align: right; vertical-align: top">
                                <asp:RadioButtonList ID="rbIsResolution" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbIsResolution_SelectedIndexChanged">
                                    <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="false" Selected="True"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr runat="server" id="tr_isscanned" visible="false">

                            <td></td>
                            <td></td>
                            <td style="text-align: right; vertical-align: top">

                                <asp:RadioButtonList ID="rbIsSacnned" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Scanned" Value="true" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Original" Value="false"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>

                        <tr>
                            <td style=" padding-top: 7px;vertical-align: top;">10.</td>
                            <td style="width: 450px; padding-top: 7px;padding-left: 7px; vertical-align: top">Whether physical copy of POA is required ?
                            </td>
                            <td style="text-align: right; vertical-align: top">
                                <asp:RadioButtonList ID="rbPhysicalCopyRequired" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rbPhysicalCopyRequired_SelectedIndexChanged">
                                    <asp:ListItem Text="Yes" Value="true">                                   
                                    </asp:ListItem>
                                    <asp:ListItem Text="No" Value="false" Selected="True"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>

                        </tr>
                        <tr runat="server" id="trshowreason" visible="false">
                            <td style=" padding-top: 7px;vertical-align: top;">11.</td>
                            <td style="width: 450px; padding-top: 7px;padding-left: 7px;vertical-align: top">Reasons for physical copy <strong style="color: red; font-size: smaller">*</strong>
                            </td>
                            <td style="text-align: right; ">
                                <asp:TextBox ID="txtPhisical_copy_request_reason" runat="server" Width="300px" MaxLength="500" TextMode="MultiLine" Rows="2">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator14" runat="server" ErrorMessage="Please Enter Physical Copy Request Reason"
                                    SetFocusOnError="true" Display="Dynamic" ValidationGroup="Save" ControlToValidate="txtPhisical_copy_request_reason">*</asp:RequiredFieldValidator>

                            </td>
                        </tr>
                        <tr id="trterms" runat="server">
                            <td colspan="3">
                                <h4 style="color: #3768AD; font-weight: bold">Terms and conditions</h4>
                                <br />
                                <div id="comments">
                                    <div style="height: 150px; overflow-y: auto">
                                        <ul class="commentlist" style="list-style-type: none">
                                            <li class="comment_even">

                                                <ol type="1">
                                                    <li>I hereby confirm that all the details provided herein are true and correct to the best of my knowledge.</li>
                                                    <li>I agree to preserve the original Power of Attorney (POA) in my safe custody.  In the event of loss of the original POA, I shall promptly inform the Secretarial function about the same.</li>
                                                    <li>I agree to promptly notify the Secretarial function about any change in my current role and return the original POA in my custody to the Secretarial function. </li>
                                                    <li>I agree to return original POA to the Secretarial function upon my resignation or cessation of my employment by any reason whatsoever. </li>
                                                    <li>I agree to promptly intimate the Secretarial function in an event the original POA is submitted with any regulatory /government authority or any other person.</li>
                                                    <li>I agree to act as the lawful attorney to exercise powers contained in this POA and I shall not delegate such powers unless explicitly authorised to do so.</li>
                                                    <li>In the event I delegate one or more powers contained in the POA to any other person, I shall exercise reasonable diligence and care while delegating such powers and agree to make such a delegation only through a letter of authority signed by me.  </li>
                                                    <li>I further agree to share a copy of such letter of authority promptly with the Secretarial function for its record. I understand that a delegatee who is exercising such delegated power cannot further delegate or sub-delegate to other(s) and it shall be my responsibility to ensure the same at any point in time. </li>
                                                </ol>

                                            </li>
                                        </ul>
                                    </div>

                                    <asp:CheckBox ID="chkTerms" Checked="true" runat="server" Text="I have read the above terms and conditions and I accept them." />
                                    <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="Please accept terms and conditions" ForeColor="Red"
                                        ClientValidationFunction="chkterms11" SetFocusOnError="true"
                                        Display="Dynamic" ValidationGroup="Save">*</asp:CustomValidator>

                                </div>
                            </td>
                        </tr>
                        <tr id="TrComments" runat="server" visible="false">
                            <td colspan="3">
                                <h4 style="color: #3768AD; font-weight: bold">Previous comments</h4>
                                <br />
                                <telerik:RadGrid ID="rgComments" runat="server" Width="100%" AllowPaging="false" HeaderStyle-Font-Bold="true"
                                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="rgComments_NeedDataSource">
                                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                                    <MasterTableView PagerStyle-Mode="NextPrevAndNumeric" NoMasterRecordsText="">
                                        <RowIndicatorColumn Visible="False">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn Created="True">
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex+1 %>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="PoaId" UniqueName="PoaId" SortExpression="PoaId"
                                                HeaderText="PoaId" Visible="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="POAStatus" UniqueName="POAStatus" SortExpression="POAStatus" Visible="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="POANo" UniqueName="POANo" SortExpression="POANo" HeaderText="POA No">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Empid" UniqueName="Empid" SortExpression="Empid" HeaderText="Employee ID">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EmpName" UniqueName="EmpName" SortExpression="EmpName" HeaderText="Employee Name">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="role_1" UniqueName="role_1" SortExpression="role_1" HeaderText="Role">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="remarks" UniqueName="remarks" SortExpression="remarks" HeaderText="Comments">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="staus_1" UniqueName="staus_1" SortExpression="staus_1" HeaderText="Status">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Approvaldate" UniqueName="Approvaldate" SortExpression="Approvaldate" HeaderText="Date" DataFormatString="{0:dd-MMM-yyyy}">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>

                                </telerik:RadGrid>
                            </td>
                        </tr>
                        <tr id="trButton" runat="server">
                            <td colspan="3">
                                <table>
                                    <tr style="align-content: center">
                                        <td>
                                            <asp:Button ID="btnDraft" runat="server" Text="Save as Draft" Width="100px" OnClick="btnDraft_Click"  />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="100px" OnClick="btnSubmit_Click"  ValidationGroup="Save" />
                                            <%--OnClientClick="return confirm('Are you sure you want to raise the request?')"--%>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="100px" OnClick="btnCancel_Click" CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr id="TrProcess" runat="server" visible="false">
                <td colspan="2">
                    <h4 style="color: #3768AD; font-weight: bold">Process POA</h4>
                    <table style="width: 100%">
                        <tr>
                            <td style="vertical-align: bottom">Download POA &nbsp; &nbsp; &nbsp; </td>
                            <td><a id="aPOADWnl" runat="server" target="_blank">

                                <asp:Label ID="lblPOATitle" runat="server" Text="Label"></asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td>Date of receipt of the POA</td>
                            <td>
                                <telerik:RadDatePicker ID="Rdpdate" runat="server" Culture="en-IN" Skin="Web20" PopupDirection="TopRight" Font-Bold="false">
                                    <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                        FastNavigationNextText="&amp;lt;&amp;lt;">
                                    </Calendar>

                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator34" runat="server"
                                    ErrorMessage="Please Enter Date of receipt of the POA"
                                    SetFocusOnError="true" Display="Dynamic" ValidationGroup="process" ControlToValidate="Rdpdate">*</asp:RequiredFieldValidator>


                            </td>

                        </tr>
                        <tr id="trrec" runat="server">
                            <td></td>
                            <td>
                                <asp:CheckBox ID="chkreceived" Checked="true" runat="server" Text="I confirm the receipt of Power of Attorney" />
                                <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator" ValidationGroup="process"></asp:CustomValidator>


                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Button ID="btnProcess" runat="server" Text="Confirm" Width="100px" OnClick="btnProcess_Click" ValidationGroup="process" />

                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
        </table>

    </telerik:RadAjaxPanel>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="additional"
        ShowMessageBox="true" ShowSummary="false" />
    <asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="process"
        ShowMessageBox="true" ShowSummary="false" />
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="Save" ShowMessageBox="true" ShowSummary="false" />
</asp:Content>
