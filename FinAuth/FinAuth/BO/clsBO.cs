﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinAuth.BO
{
    public class clsBO
    {
        public string AdditionID { get; set; }
        public string LocationType { get; set; }
        public string LocationDetails { get; set; }
        public string RequiredFor { get; set; }
        public string AccountNO { get; set; }
        public string AdditionDetails { get; set; }
        public string Comments { get; set; }
        public string CreatedBy { get; set; }
        public string SaveDraftFlag { get; set; }
        public string EmpID { get; set; }
        public string Type { get; set; }
        public string DeletionID { get; set; }
        public string EmpName { get; set; }
        public string Designation { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public string TreasuryType { get; set; }
        public string ApproverID { get; set; }
        public string InitiatorID { get; set; }
        public int NextSeq { get; set; }
        public string OriginalFname { get; set; }
        public string ModifiedFname { get; set; }
        public string Bank { get; set; }
        public string BankAddress { get; set; }
        public string Aadhar { get; set; }
        public string Otherdoc { get; set; }
    }
}