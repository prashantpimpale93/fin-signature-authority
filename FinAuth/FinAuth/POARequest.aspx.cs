﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Telerik.Web.UI;

using System.Net.Mail;
using System.IO;
using FinAuth.BLL;
using System.Data;
using System.Data.Entity;


namespace FinAuth
{
    public partial class POARequest : System.Web.UI.Page
    {
        POA objpoa = new POA();
        FinAuthEntities1 fm = new FinAuthEntities1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["EmpID"] + "" == "" || Session["UserRole_Id"] + "" == "")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {

                // ValidationSummary2.Enabled = false;
                if (!IsPostBack)
                {
                    rgRequestAdditional_selected.Visible = false;
                    Label3.Visible = false;
                    if (Request.QueryString["POANo"] + "" != "")
                    {
                        btnCancel.Text = "Cancel";
                        loadData();
                    }
                    else
                    {
                        btnCancel.Text = "Reset";

                        Session["RequestorId"] = Session["EmpID"];
                        Session["RequestorName"] = Session["UserName"];
                        bindDRP();
                        DataTable dt = objpoa.bindAuthorityPower(0).Tables[0];
                        Session["additionalSelected"] = dt;
                        trAdditional.Visible = Convert.ToBoolean(rblistAdded_power.SelectedValue);

                        if (!Convert.ToBoolean(rblistAdded_power.SelectedValue))
                        {
                            if (Rgrequest.Items.Count > 0 && Rgrequest.Visible == true)
                            {
                                tbldesc.Visible = true;
                            }
                            else
                            {
                                tbldesc.Visible = false;
                            }
                        }
                        tblraisenew.Visible = Convert.ToBoolean(rcbRaiseNew.SelectedValue);
                        tbldesc.Visible = !Convert.ToBoolean(rcbRaiseNew.SelectedValue);
                    }
                    // ValidationSummary2.Enabled = false;
                }
            }
        }
        public void hide()
        {
            if (objpoa.usp_view_poa_details(Convert.ToInt32(Request.QueryString["POANo"])).Tables[2].Rows.Count > 0)
            { trAdditional.Visible = true; }
            else
            {
                trAdditional.Visible = false;
            }
            rgRequestAdditional_selected.MasterTableView.GetColumn("delete").Display = false;
            rblistAdded_power.Enabled = false;
            rcbRaiseNew.Enabled = false;
            rcbempId.Enabled = false;
            rcbEmpname.Enabled = false;
            rcbRole.Enabled = false;
            txtAddress.ReadOnly = true;
            txtFatherName.ReadOnly = true;
            txtLocation.ReadOnly = true;
            txtDesignation.ReadOnly = true;
            txtComments.ReadOnly = true;
            txtFunction.ReadOnly = true;
            // txtIOnumber.ReadOnly = true;
            txtPOAReason.ReadOnly = true;
            rbPOAholder.Enabled = false;
            rbIsResolution.Enabled = false;
            rbIsSacnned.Enabled = false;
            rbPhysicalCopyRequired.Enabled = false;
            // Rgrequest.Enabled = false;
            chkreceived.Enabled = false;
            //rgRequestAdditional.Enabled = false;
            // rgRequestAdditional_selected.Enabled = false;
            //tblMainRole.Visible = false;
            rbReqFor.Enabled = false;
            //tblonbehalf.Visible = false;
            //trAdditional.Visible = false;
            rblistAdded_power.Enabled = false;
            //  rgRequestAdditional_selected.Enabled = false;
            txtpowerDesc.ReadOnly = true;
            txtRaisepowerReason.ReadOnly = true;
            rbPOAholder.Enabled = false;
            // rgRequestAdditional_selected.Enabled = false;
            rbSubType.Enabled = false;
            rcbRole.Enabled = false;
            btnview.Visible = false;
            rcbAdditionalRole.Visible = false;
            btnAdd.Visible = false;
            rgRequestAdditional.Visible = false;
            div_rgRequestAdditional.Visible = false;
            btnDraft.Visible = false;
            btnSubmit.Visible = false;
            btnCancel.Visible = false;
            txtPhisical_copy_request_reason.ReadOnly = true;
        }
        private void loadData()
        {

            bindDRP();

            var dtemp = fm.view_poa_details(Convert.ToInt32(Request.QueryString["POANo"])).ToList();


            if (dtemp.Count > 0)
            {
                Object o = new Object();
                EventArgs e1 = new EventArgs();
                RadComboBoxSelectedIndexChangedEventArgs r1 = new RadComboBoxSelectedIndexChangedEventArgs("", "", "", "");
                Session["status"] = dtemp[0].status.ToString();
                Session["str_POANo"] = dtemp[0].POANo.ToString();
                Session["maxid"] = dtemp[0].maxid.ToString();
                rbSubType.SelectedValue = dtemp[0].POATYPE.ToString();
                rbSubType_SelectedIndexChanged(o, e1);
                rbReqFor.SelectedValue = dtemp[0].requestFor.ToString();
                RadioButtonList2_SelectedIndexChanged(o, e1);
                DataTable dt = objpoa.usp_view_poa_details(Convert.ToInt32(Request.QueryString["POANo"])).Tables[2];
                //Session["additionalSelected"] = dt;

                //Rgrequest.DataSource = objpoa.usp_view_poa_details(Convert.ToInt32(Request.QueryString["POANo"])).Tables[1];
                //Rgrequest.DataBind();
                if (dt.Rows.Count > 0)
                {
                    rgRequestAdditional_selected.DataSource = dt;
                    rgRequestAdditional_selected.DataBind();
                    rgRequestAdditional_selected.Visible = true;
                }
                else
                {
                    rgRequestAdditional_selected.Visible = false;
                }
                if (dt.Rows.Count == 0)
                {
                    rblistAdded_power.SelectedValue = "false";
                    trAdditional.Visible = false;
                }
                else
                {
                    rblistAdded_power.SelectedValue = "true";
                    trAdditional.Visible = true;
                }
                // rblistAdded_power_SelectedIndexChanged(o, e1);


                Session["additionalSelected"] = dt;
                rcbRaiseNew.SelectedValue = "false";
                rcbRaiseNew_SelectedIndexChanged(o, e1);
                rcbempId.DataSource = fm.mta_mstemployee.ToList();
                rcbempId.DataTextField = "EmpID";
                rcbempId.DataValueField = "EmpID";
                rcbempId.DataBind();             
                rcbempId.Items.Insert(0, new RadComboBoxItem("-- Select --", "0"));
                rcbempId.SelectedValue = dtemp[0].Initiator_id.ToString();

                rcbEmpname.DataSource = fm.mta_mstemployee.ToList();
                rcbEmpname.DataTextField = "EmpName";
                rcbEmpname.DataValueField = "EmpID";
                rcbEmpname.DataBind();
                rcbEmpname.Items.Insert(0, new RadComboBoxItem("-- Select --", "0"));
                rcbEmpname.SelectedValue = dtemp[0].Initiator_id.ToString();
              
                rcbRole.DataSource = fm.MstRoles.ToList();
                rcbRole.DataTextField = "role";
                rcbRole.DataValueField = "roleid";
                rcbRole.DataBind();
                rcbRole.Items.Insert(0, new RadComboBoxItem("-- Select --", "0"));
                rcbRole.SelectedValue = dtemp[0].RoleId.ToString();
                rcbRole_SelectedIndexChanged(o, r1);
                txtAddress.Text = Convert.ToString(dtemp[0].Address);
                txtFatherName.Text = Convert.ToString(dtemp[0].Fname);
                txtLocation.Text = dtemp[0].Location.ToString();
                txtDesignation.Text = dtemp[0].Designation.ToString();
                txtFunction.Text = dtemp[0].Function.ToString();
                txtComments.Text = Convert.ToString(dtemp[0].Comments);
                // txtIOnumber.Text = dtemp[0].IONo.ToString();
                txtPOAReason.Text = dtemp[0].RequestReson.ToString();
                rbPOAholder.SelectedValue = dtemp[0].POAExist.ToString().ToLower();
                rbIsResolution.SelectedValue = dtemp[0].ISPOA_or_Resolutioncopy.ToString().ToLower();
                if (dtemp[0].ISPOA_or_Resolutioncopy.ToString().ToLower() != "false")
                {
                    tr_isscanned.Visible = true;
                    rbIsSacnned.SelectedValue = dtemp[0].ResolutionCopy_format_isScanned.ToString().ToLower();
                }
                else
                {
                    tr_isscanned.Visible = false;
                }
                rbPhysicalCopyRequired.SelectedValue = dtemp[0].Phisical_copy_required.ToString().ToLower();
                if (dtemp[0].Phisical_copy_required.ToString().ToLower() == "true")
                {
                    trshowreason.Visible = true;
                    // trshowreason.Style.Add("display", "block");
                }
                else
                {
                    trshowreason.Visible = false;
                    // trshowreason.Style.Add("display", "none");
                }
                txtPhisical_copy_request_reason.Text = Convert.ToString(dtemp[0].Phisical_copy_request_reason);
                Rgrequest.DataSource = objpoa.usp_view_poa_details(Convert.ToInt32(Request.QueryString["POANo"])).Tables[1];
                Rgrequest.DataBind();



                btnDraft.Visible = false;
                if (Session["status"] + "" == "7")
                {

                    hide();
                    TrComments.Visible = true;
                    TrProcess.Visible = true;
                    trButton.Visible = false;
                    trterms.Visible = false;
                    if (rbPhysicalCopyRequired.SelectedValue == "true")
                    {
                        chkreceived.Text = "I confirm the receipt of Original Power of Attorney";
                        trrec.Visible = true;
                    }
                    else
                    {
                        chkreceived.Text = "I confirm the receipt of scanned copy of the Power of Attorney.";
                        trrec.Visible = true;
                    }
                    lblPOATitle.Text = dtemp[0].POAtitle.ToString();
                    aPOADWnl.HRef = "~/UploadedFiles/" + dtemp[0].POApdf.ToString();
                    Rdpdate.MaxDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                }
                else
                {
                    btnDraft.Visible = true;
                    TrComments.Visible = false;
                    TrProcess.Visible = false;
                    trButton.Visible = true;
                    trterms.Visible = true;
                    if (Session["status"] + "" != "0" && Session["status"] + "" != "3" && Session["status"] + "" != "4")
                    {
                        hide();
                    }
                    if (Session["status"] + "" != "0" && fm.bindPriviousComments(Convert.ToInt32(Request.QueryString["POANo"])).ToList().Count > 0)
                    {
                        btnDraft.Visible = false;
                        if (Session["status"] + "" != "1")
                        {
                            btnDraft.Visible = false;
                            trterms.Visible = false;
                        }
                        TrComments.Visible = true;
                    }

                }
                Session["RequestorId"] = dtemp[0].Initiator_id.ToString();
                Session["RequestorName"] = dtemp[0].Initiator_Name.ToString();
            }
        }
        private void bindDRP()
        {
            rcbempId.DataSource = fm.bindEmpname();
            rcbempId.DataTextField = "EmpID";
            rcbempId.DataValueField = "EmpID";
            rcbempId.DataBind();
            rcbempId.Items.Insert(0, new RadComboBoxItem("-- Select --", "0"));

            rcbEmpname.DataSource = fm.bindEmpname();
            rcbEmpname.DataTextField = "EmpName";
            rcbEmpname.DataValueField = "EmpID";
            rcbEmpname.DataBind();
            rcbEmpname.Items.Insert(0, new RadComboBoxItem("-- Select --", "0"));

            var dtemp = fm.getEmpDetails(Convert.ToInt32(Session["EmpID"])).ToList();

            if (dtemp.Count > 0)
            {
                txtAddress.Text = "";
                txtFatherName.Text = "";
                txtLocation.Text = dtemp[0].Location.ToString();
                txtDesignation.Text = dtemp[0].Designation.ToString();
                txtFunction.Text = dtemp[0].Function.ToString();
                txtLocation.Attributes.Add("Readonly", "True");
                txtDesignation.Attributes.Add("Readonly", "True");
                txtFunction.Attributes.Add("Readonly", "True");
                txtFatherName.Text = Convert.ToString(dtemp[0].MiddleName);
                txtAddress.Text = Convert.ToString(dtemp[0].addres);
            }

            rcbRole.DataSource = fm.MstRoles.ToList().Where(a => a.Role != "Others");
            rcbRole.DataTextField = "role";
            rcbRole.DataValueField = "roleid";
            rcbRole.DataBind();
            rcbRole.Items.Insert(0, new RadComboBoxItem("-- Select --", "0"));
            Rgrequest.Rebind();

            rgRequestAdditional_selected.Rebind();
            rgRequestAdditional.Rebind();
        }

        protected void RadioButtonList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbReqFor.SelectedValue == "1")
            {
                tblonbehalf.Visible = true;

            }
            else
            {
                tblonbehalf.Visible = false;
                var dtemp = fm.getEmpDetails(Convert.ToInt32(Session["EmpID"])).ToList();

                if (dtemp.Count > 0)
                {
                    Session["RequestorName"] = Session["UserName"];
                    Session["RequestorId"] = Session["EmpID"];
                    txtAddress.Text = "";
                    txtFatherName.Text = "";
                    txtLocation.Text = dtemp[0].Location.ToString();
                    txtDesignation.Text = dtemp[0].Designation.ToString();
                    txtFunction.Text = dtemp[0].Function.ToString();
                    txtFatherName.Text = Convert.ToString(dtemp[0].MiddleName);
                    txtAddress.Text = Convert.ToString(dtemp[0].addres);
                }
            }
        }

        protected void btnview_Click(object sender, EventArgs e)
        {
            if (rbReqFor.SelectedValue == "1")
            {
                Session["RequestorId"] = rcbempId.SelectedValue;
                Session["RequestorName"] = rcbEmpname.SelectedItem.Text;
            }
            else
            {
                Session["RequestorId"] = Session["EmpID"];
                Session["RequestorName"] = Session["UserName"];
            }
            var dtemp = fm.getEmpDetails(Convert.ToInt32(Session["RequestorId"])).ToList();

            if (dtemp.Count > 0)
            {
                txtAddress.Text = "";
                txtFatherName.Text = "";
                txtLocation.Text = dtemp[0].Location.ToString();
                txtDesignation.Text = dtemp[0].Designation.ToString();
                txtFunction.Text = dtemp[0].Function.ToString();
                txtLocation.Attributes.Add("Readonly", "True");
                txtDesignation.Attributes.Add("Readonly", "True");
                txtFunction.Attributes.Add("Readonly", "True");
                txtFatherName.Text = Convert.ToString(dtemp[0].MiddleName);
                txtAddress.Text = Convert.ToString(dtemp[0].addres);
            }

        }

        protected void rblistAdded_power_SelectedIndexChanged(object sender, EventArgs e)
        {
            trAdditional.Visible = Convert.ToBoolean(rblistAdded_power.SelectedValue);
            DataTable dt = objpoa.bindAuthorityPower(0).Tables[0];
            Session["additionalSelected"] = dt;
            rgRequestAdditional_selected.Rebind();
            rgRequestAdditional.Rebind();

        }

        protected void rbSubType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = objpoa.bindAuthorityPower(0).Tables[0];
            Session["additionalSelected"] = dt;

            rgRequestAdditional_selected.Rebind();
            rgRequestAdditional.Rebind();
            if (rbSubType.SelectedValue == "0")
            {
                tblMainRole.Visible = true;
                Rgrequest.DataSource = fm.MstAuthorityPowers.Where(a => a.RoleId.ToString() == rcbRole.SelectedValue).ToList();
                Rgrequest.DataBind();
                rcbAdditionalRole.DataSource = fm.MstRoles.Where(a => a.RoleId.ToString() != rcbRole.SelectedValue).ToList();
                rcbAdditionalRole.DataTextField = "role";
                rcbAdditionalRole.DataValueField = "roleid";
                rcbAdditionalRole.DataBind();
                rcbAdditionalRole.Items.Insert(0, new RadComboBoxItem("-- Select --", "0"));
                //tbldesc.Visible = false;
                rblistAdded_power.SelectedValue = "false";
                trAdditional.Visible = Convert.ToBoolean(rblistAdded_power.SelectedValue);
                rblistAdded_power.Enabled = true;
                //trAdditional.Visible = true;
                trAdditional_req.Visible = true;
            }
            else
            {
                tblMainRole.Visible = false;
                rcbAdditionalRole.DataSource = fm.MstRoles.ToList();
                rcbAdditionalRole.DataTextField = "role";
                rcbAdditionalRole.DataValueField = "roleid";
                rcbAdditionalRole.DataBind();
                rcbAdditionalRole.Items.Insert(0, new RadComboBoxItem("-- Select --", "0"));
                trAdditional.Visible = true;
                tbldesc.Visible = true;
                rblistAdded_power.SelectedValue = "true";
                trAdditional.Visible = Convert.ToBoolean(rblistAdded_power.SelectedValue);
                rblistAdded_power.Enabled = false;
                trAdditional_req.Visible = false;

            }
        }

        protected void rcbRaiseNew_SelectedIndexChanged(object sender, EventArgs e)
        {
            tblraisenew.Visible = Convert.ToBoolean(rcbRaiseNew.SelectedValue);
            tbldesc.Visible = !Convert.ToBoolean(rcbRaiseNew.SelectedValue);
        }

        protected void btnsendMail_Click(object sender, EventArgs e)
        {
            if (fm.MstAuthorityPowers.Where(a => a.powerDesc.ToString().ToLower() == txtpowerDesc.Text.ToLower()).ToList().Count > 0)
            {
                RadAjaxManager1.Alert("Power exists already.");
            }
            else
            {
                var dtemp = fm.generate_PowerRaiseNo().ToList();

                if (dtemp.Count > 0)
                {
                    var reqpower = new Mst_AdditionalPower_request()
                    {
                        PowerReqNo = dtemp[0].requestRefNo.ToString(),
                        intmaxid = Convert.ToInt32(dtemp[0].INTMAXID.ToString()),
                        ReqDate = DateTime.Now,
                        ReqReason = txtRaisepowerReason.Text,
                        // RejectReason = "", 
                        Initiator_name = Session["RequestorName"].ToString(),
                        initiator_id = Convert.ToInt32(Session["RequestorId"].ToString()),
                        RequserId = Convert.ToInt32(Session["EmpID"].ToString()),
                        PowerDesc = txtpowerDesc.Text
                    };
                    fm.Mst_AdditionalPower_request.Add(reqpower);
                    fm.SaveChanges();
                    int id = reqpower.PowerReqId;
                    //add approval flow
                    fm.approval_flowRaisenew(id, 1);
                    // var reqflow1 = new Trn_Approvalflow()
                    // {
                    //     POANO = id,
                    //     status = "P",
                    //     seqno = 1,
                    //     ApproverId = 15949//Convert.ToInt32(fm.getHOD_forApproval(Session["RequestorId"].ToString()))
                    // };
                    // fm.Trn_Approvalflow.Add(reqflow1);
                    //fm.SaveChanges();

                    //send mail
                    ;
                    //mail
                    var dthod = fm.usp_getHOD_forApproval(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
                    var dthodEmail = fm.getEmpDetails(Convert.ToInt32(dthod[0].hod.ToString())).ToList();
                    string hodName = "", hodemailid = "", ccmail = "", iniName = "", OnBehalfEmail="";
                    if (dthodEmail.Count > 0)
                    {
                        hodName = dthodEmail[0].EmpName.ToString();
                        hodemailid = dthodEmail[0].EmailID.ToString();
                    }
                    var dtiniEmail = fm.getEmpDetails(Convert.ToInt32(Session["EmpID"].ToString())).ToList();
                    if (dtiniEmail.Count > 0)
                    {
                        ccmail = dtiniEmail[0].EmailID.ToString();
                        iniName = dtiniEmail[0].EmpName.ToString();
                    }
                    var dtOnBehalfEmail = fm.getEmpDetails(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
                    if (dtOnBehalfEmail.Count > 0)
                    {
                        OnBehalfEmail = dtOnBehalfEmail[0].EmailID.ToString();

                    }
                    objpoa.sendMail_PowerReqRaised(dtemp[0].requestRefNo.ToString(), ccmail, "", iniName, txtpowerDesc.Text, OnBehalfEmail);
                    if (Request.QueryString["POANo"] + "" != "") //changed by Vikas Gage on 23/04/2015
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Mail sent to Secratorial Team for approval of " + dtemp[0].requestRefNo.ToString() + " request.');window.location ='POASubmit.aspx';",
true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Mail sent to Secratorial Team for approval of " + dtemp[0].requestRefNo.ToString() + " request.');window.location ='Home.aspx';",
   true);
                    }

                }
            }
        }

        protected void btnDraft_Click(object sender, EventArgs e)
        {

            if (Request.QueryString["POANo"] + "" != "")
            {
                string requestRefNo = Session["str_POANo"].ToString();


                var reqPOA = new Mst_POArequest()
                {
                    PoaId = Convert.ToInt32(Request.QueryString["POANo"]),
                    POANo = requestRefNo,
                    POATYPE = Convert.ToInt32(rbSubType.SelectedValue),
                    requestFor = Convert.ToInt32(rbReqFor.SelectedValue),

                    Initiator_Name = Session["RequestorName"].ToString(),
                    Initiator_id = Convert.ToInt32(Session["RequestorId"].ToString()),
                    Fname = txtFatherName.Text,
                    Address = txtAddress.Text,
                    Comments = txtComments.Text,
                    //  IONo = txtIOnumber.Text,
                    RequestReson = txtPOAReason.Text,
                    POAExist = Convert.ToBoolean(rbPOAholder.SelectedValue),
                    ISPOA_or_Resolutioncopy = Convert.ToBoolean(rbIsResolution.SelectedValue),
                    Phisical_copy_required = Convert.ToBoolean(rbPhysicalCopyRequired.SelectedValue),
                    issubmitted = false,
                    status = 0,
                    requestedby = Convert.ToInt32(Session["EmpID"].ToString()),
                    requesteddate = DateTime.Now,
                    RoleId = Convert.ToInt32(rcbRole.SelectedValue),
                    maxid = Convert.ToInt32(Session["maxid"]),
                    ResolutionCopy_format_isScanned = Convert.ToBoolean(rbIsSacnned.SelectedValue),
                    Phisical_copy_request_reason = txtPhisical_copy_request_reason.Text
                };
                using (var context = new FinAuthEntities1())
                {
                    context.Mst_POArequest.Attach(reqPOA);
                    context.Entry(reqPOA).State = EntityState.Modified;
                    context.SaveChanges();
                }

                using (var context = new FinAuthEntities1())
                {
                    int poaid = Convert.ToInt32(Request.QueryString["POANo"]);

                    var clubMember = context.trn_POARequest_details.Where(a => a.POAId == poaid).ToList();
                    context.trn_POARequest_details.RemoveRange(clubMember);
                    context.SaveChanges();
                }
                //add details role
                GridDataItemCollection dtCol2 = Rgrequest.Items;

                foreach (GridDataItem item2 in dtCol2)
                {
                    Label lblPowerid2 = (Label)item2.FindControl("lblPowerid2");
                    Label lblRoleid2 = (Label)item2.FindControl("lblRoleid2");
                    var reqPOA_det = new trn_POARequest_details()
                    {
                        POAId = Convert.ToInt32(Request.QueryString["POANo"]),
                        Powerid = Convert.ToInt32(lblPowerid2.Text),
                        RoleId = Convert.ToInt32(lblRoleid2.Text)
                    };
                    fm.trn_POARequest_details.Add(reqPOA_det);
                    fm.SaveChanges();
                }
                GridDataItemCollection dtCol1 = rgRequestAdditional_selected.Items;

                foreach (GridDataItem item1 in dtCol1)
                {
                    Label lblPowerid1 = (Label)item1.FindControl("lblPowerid1");
                    Label lblRoleid1 = (Label)item1.FindControl("lblRoleid1");
                    var reqPOA_det = new trn_POARequest_details()
                    {
                        POAId = Convert.ToInt32(Request.QueryString["POANo"]),
                        Powerid = Convert.ToInt32(lblPowerid1.Text),
                        RoleId = Convert.ToInt32(lblRoleid1.Text)
                    };
                    fm.trn_POARequest_details.Add(reqPOA_det);

                    fm.SaveChanges();
                }
                if (Request.QueryString["POANo"] + "" != "")//Changed by vikas gage on 23/04/2015
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Request for " + requestRefNo + " saved Successfully.');window.location ='POASubmit.aspx';",
    true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Request for " + requestRefNo + " saved Successfully.');window.location ='POARequest.aspx';",
        true);
                }
            }
            else
            {


                var dtemp = fm.generate_POAReqNo().ToList();
                string requestRefNo = "";
                if (dtemp.Count > 0)
                {
                    requestRefNo = dtemp[0].requestRefNo.ToString();
                    var reqPOA = new Mst_POArequest()
                    {

                        POANo = requestRefNo,
                        POATYPE = Convert.ToInt32(rbSubType.SelectedValue),
                        requestFor = Convert.ToInt32(rbReqFor.SelectedValue),

                        Initiator_Name = Session["RequestorName"].ToString(),
                        Initiator_id = Convert.ToInt32(Session["RequestorId"].ToString()),
                        Fname = txtFatherName.Text,
                        Address = txtAddress.Text,
                        Comments = txtComments.Text,
                        //  IONo = txtIOnumber.Text,
                        RequestReson = txtPOAReason.Text,
                        POAExist = Convert.ToBoolean(rbPOAholder.SelectedValue),
                        ISPOA_or_Resolutioncopy = Convert.ToBoolean(rbIsResolution.SelectedValue),
                        Phisical_copy_required = Convert.ToBoolean(rbPhysicalCopyRequired.SelectedValue),
                        issubmitted = false,
                        status = 0,
                        requestedby = Convert.ToInt32(Session["EmpID"].ToString()),
                        requesteddate = DateTime.Now,
                        RoleId = Convert.ToInt32(rcbRole.SelectedValue),
                        maxid = Convert.ToInt32(dtemp[0].INTMAXID.ToString()),
                        ResolutionCopy_format_isScanned = Convert.ToBoolean(rbIsSacnned.SelectedValue),
                        Phisical_copy_request_reason = txtPhisical_copy_request_reason.Text
                    };
                    fm.Mst_POArequest.Add(reqPOA);
                    fm.SaveChanges();
                    int id = reqPOA.PoaId;
                    if (id > 0)
                    { //add details role
                        GridDataItemCollection dtCol2 = Rgrequest.Items;

                        foreach (GridDataItem item2 in dtCol2)
                        {
                            Label lblPowerid2 = (Label)item2.FindControl("lblPowerid2");
                            Label lblRoleid2 = (Label)item2.FindControl("lblRoleid2");
                            var reqPOA_det = new trn_POARequest_details()
                            {
                                POAId = id,
                                Powerid = Convert.ToInt32(lblPowerid2.Text),
                                RoleId = Convert.ToInt32(lblRoleid2.Text)
                            };
                            fm.trn_POARequest_details.Add(reqPOA_det);
                            fm.SaveChanges();
                        }
                        GridDataItemCollection dtCol1 = rgRequestAdditional_selected.Items;

                        foreach (GridDataItem item1 in dtCol1)
                        {
                            Label lblPowerid1 = (Label)item1.FindControl("lblPowerid1");
                            Label lblRoleid1 = (Label)item1.FindControl("lblRoleid1");
                            var reqPOA_det = new trn_POARequest_details()
                            {
                                POAId = id,
                                Powerid = Convert.ToInt32(lblPowerid1.Text),
                                RoleId = Convert.ToInt32(lblRoleid1.Text)
                            };
                            fm.trn_POARequest_details.Add(reqPOA_det);
                            fm.SaveChanges();
                        }
                    }

                }

                if (Request.QueryString["POANo"] + "" != "")//Changed by vikas gage on 23/04/2015
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Request for " + requestRefNo + " saved Successfully.');window.location ='POASubmit.aspx';",
    true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Request for " + requestRefNo + " saved Successfully.');window.location ='POARequest.aspx';",
        true);
                }
            }
            //add approval flow

            //send mail

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            btnSubmit.Enabled = false;
            if (Request.QueryString["POANo"] + "" != "")
            {
                string requestRefNo = Session["str_POANo"].ToString();


                var reqPOA = new Mst_POArequest()
                {
                    PoaId = Convert.ToInt32(Request.QueryString["POANo"]),
                    POANo = requestRefNo,
                    POATYPE = Convert.ToInt32(rbSubType.SelectedValue),
                    requestFor = Convert.ToInt32(rbReqFor.SelectedValue),

                    Initiator_Name = Session["RequestorName"].ToString(),
                    Initiator_id = Convert.ToInt32(Session["RequestorId"].ToString()),
                    Fname = txtFatherName.Text,
                    Address = txtAddress.Text,
                    Comments = txtComments.Text,
                    //  IONo = txtIOnumber.Text,
                    RequestReson = txtPOAReason.Text,
                    POAExist = Convert.ToBoolean(rbPOAholder.SelectedValue),

                    ISPOA_or_Resolutioncopy = Convert.ToBoolean(rbIsResolution.SelectedValue),
                    Phisical_copy_required = Convert.ToBoolean(rbPhysicalCopyRequired.SelectedValue),
                    issubmitted = true,
                    status = 1,
                    requestedby = Convert.ToInt32(Session["EmpID"].ToString()),
                    requesteddate = DateTime.Now,
                    RoleId = Convert.ToInt32(rcbRole.SelectedValue),
                    maxid = Convert.ToInt32(Session["maxid"]),
                    ResolutionCopy_format_isScanned = Convert.ToBoolean(rbIsSacnned.SelectedValue),
                    Phisical_copy_request_reason = txtPhisical_copy_request_reason.Text
                };
                using (var context = new FinAuthEntities1())
                {
                    context.Mst_POArequest.Attach(reqPOA);
                    context.Entry(reqPOA).State = EntityState.Modified;
                    context.SaveChanges();
                }

                using (var context = new FinAuthEntities1())
                {
                    int poaid = Convert.ToInt32(Request.QueryString["POANo"]);

                    var clubMember = context.trn_POARequest_details.Where(a => a.POAId == poaid).ToList();
                    context.trn_POARequest_details.RemoveRange(clubMember);
                    context.SaveChanges();
                }
                //add details role
                GridDataItemCollection dtCol2 = Rgrequest.Items;

                foreach (GridDataItem item2 in dtCol2)
                {
                    Label lblPowerid2 = (Label)item2.FindControl("lblPowerid2");
                    Label lblRoleid2 = (Label)item2.FindControl("lblRoleid2");
                    var reqPOA_det = new trn_POARequest_details()
                    {
                        POAId = Convert.ToInt32(Request.QueryString["POANo"]),
                        Powerid = Convert.ToInt32(lblPowerid2.Text),
                        RoleId = Convert.ToInt32(lblRoleid2.Text)
                    };
                    fm.trn_POARequest_details.Add(reqPOA_det);
                    fm.SaveChanges();
                }
                GridDataItemCollection dtCol1 = rgRequestAdditional_selected.Items;

                foreach (GridDataItem item1 in dtCol1)
                {
                    Label lblPowerid1 = (Label)item1.FindControl("lblPowerid1");
                    Label lblRoleid1 = (Label)item1.FindControl("lblRoleid1");
                    var reqPOA_det = new trn_POARequest_details()
                    {
                        POAId = Convert.ToInt32(Request.QueryString["POANo"]),
                        Powerid = Convert.ToInt32(lblPowerid1.Text),
                        RoleId = Convert.ToInt32(lblRoleid1.Text)
                    };
                    fm.trn_POARequest_details.Add(reqPOA_det);

                    fm.SaveChanges();
                }


                //add approval flow
                if (Session["status"] + "" == "0")
                {
                    var reqflow = new Trn_Approvalflow()
                    {
                        POANO = Convert.ToInt32(Request.QueryString["POANo"]),
                        status = "I",
                        remarks = "",
                        Approvaldate = DateTime.Now,
                        seqno = 0,
                        ApproverId = Convert.ToInt32(Session["EmpID"].ToString()),
                        AppRole = "INI"
                    };
                    fm.Trn_Approvalflow.Add(reqflow);
                    fm.SaveChanges();
                    var dtemp1 = fm.usp_getHOD_forApproval(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
                    int Approver_Id = 0;
                    if (dtemp1.Count > 0)
                    {
                        Approver_Id = Convert.ToInt32(dtemp1[0].hod.ToString());

                        var reqflow1 = new Trn_Approvalflow()
                        {
                            POANO = Convert.ToInt32(Request.QueryString["POANo"]),
                            status = "P",
                            seqno = 1,
                            ApproverId = Approver_Id,
                            AppRole = "HOD"
                        };
                        fm.Trn_Approvalflow.Add(reqflow1);
                        fm.SaveChanges();

                    }
                }
                else
                {
                    fm.POAapprovalflow("Approve", "", Convert.ToInt32(Request.QueryString["POANo"]), Convert.ToInt32(Session["RequestorId"]), "INI");

                }

                var dthod = fm.usp_getHOD_forApproval(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
                var dthodEmail = fm.getEmpDetails(Convert.ToInt32(dthod[0].hod.ToString())).ToList();
                string hodName = "", hodemailid = "", ccmail = "", iniName = "", OnBehalfEmail = "", OnBehalfName="";


                if (dthodEmail.Count > 0)
                {
                    hodName = dthodEmail[0].EmpName.ToString();
                    hodemailid = dthodEmail[0].EmailID.ToString();
                }
                var dtiniEmail = fm.getEmpDetails(Convert.ToInt32(Session["EmpID"].ToString())).ToList();
                if (dtiniEmail.Count > 0)
                {
                    iniName = dtiniEmail[0].EmpName.ToString();
                    ccmail = dtiniEmail[0].EmailID.ToString();
                }
                var dtOnBehalfEmail = fm.getEmpDetails(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
                if (dtOnBehalfEmail.Count > 0)
                {
                    OnBehalfEmail = dtOnBehalfEmail[0].EmailID.ToString();
                    OnBehalfName = dtOnBehalfEmail[0].EmpName.ToString();

                }
                if (Request.QueryString["POANo"] + "" != "")  //Changed by Vikas Gage on 23/04/2015
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('POA request  " + requestRefNo + "  has been successfully raised and sent for approval to HOD," + hodName + ".');window.location ='POASubmit.aspx';",
    true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('POA request  " + requestRefNo + "  has been successfully raised and sent for approval to HOD," + hodName + ".');window.location ='Home.aspx';",
       true);

                }
                //mail
                objpoa.sendMail_POASubmitted(requestRefNo, hodName, hodemailid, ccmail, iniName, OnBehalfEmail, OnBehalfName);
            }
            else
            {



                var dtemp = fm.generate_POAReqNo().ToList();
                string requestRefNo = "";
                if (dtemp.Count > 0)
                {
                    requestRefNo = dtemp[0].requestRefNo.ToString();
                    var reqPOA = new Mst_POArequest()
                    {

                        POANo = requestRefNo,
                        POATYPE = Convert.ToInt32(rbSubType.SelectedValue),
                        requestFor = Convert.ToInt32(rbReqFor.SelectedValue),

                        Initiator_Name = Session["RequestorName"].ToString(),
                        Initiator_id = Convert.ToInt32(Session["RequestorId"].ToString()),
                        Fname = txtFatherName.Text,
                        Address = txtAddress.Text,
                        Comments = txtComments.Text,
                        //     IONo = txtIOnumber.Text,
                        RequestReson = txtPOAReason.Text,
                        POAExist = Convert.ToBoolean(rbPOAholder.SelectedValue),
                        ISPOA_or_Resolutioncopy = Convert.ToBoolean(rbIsResolution.SelectedValue),
                        Phisical_copy_required = Convert.ToBoolean(rbPhysicalCopyRequired.SelectedValue),
                        issubmitted = true,
                        status = 1,
                        requestedby = Convert.ToInt32(Session["EmpID"].ToString()),
                        requesteddate = DateTime.Now,
                        RoleId = Convert.ToInt32(rcbRole.SelectedValue),
                        maxid = Convert.ToInt32(dtemp[0].INTMAXID.ToString()),
                        Phisical_copy_request_reason = txtPhisical_copy_request_reason.Text,
                        ResolutionCopy_format_isScanned = Convert.ToBoolean(rbIsSacnned.SelectedValue)
                    };


                    //  reqPOA.Entry(fm).State = System.Data.Entity.EntityState.Modified;
                    fm.Mst_POArequest.Add(reqPOA);
                    fm.SaveChanges();
                    int id = reqPOA.PoaId;


                    if (id > 0)
                    { //add details role
                        GridDataItemCollection dtCol2 = Rgrequest.Items;

                        foreach (GridDataItem item2 in dtCol2)
                        {
                            Label lblPowerid2 = (Label)item2.FindControl("lblPowerid2");
                            Label lblRoleid2 = (Label)item2.FindControl("lblRoleid2");
                            var reqPOA_det = new trn_POARequest_details()
                            {
                                POAId = id,
                                Powerid = Convert.ToInt32(lblPowerid2.Text),
                                RoleId = Convert.ToInt32(lblRoleid2.Text)
                            };
                            fm.trn_POARequest_details.Add(reqPOA_det);
                            fm.SaveChanges();
                        }
                        GridDataItemCollection dtCol1 = rgRequestAdditional_selected.Items;

                        foreach (GridDataItem item1 in dtCol1)
                        {
                            Label lblPowerid1 = (Label)item1.FindControl("lblPowerid1");
                            Label lblRoleid1 = (Label)item1.FindControl("lblRoleid1");
                            var reqPOA_det = new trn_POARequest_details()
                            {
                                POAId = id,
                                Powerid = Convert.ToInt32(lblPowerid1.Text),
                                RoleId = Convert.ToInt32(lblRoleid1.Text)
                            };
                            fm.trn_POARequest_details.Add(reqPOA_det);

                            fm.SaveChanges();
                        }
                    }

                    //add approval flow
                    var reqflow = new Trn_Approvalflow()
                    {
                        POANO = id,
                        status = "I",
                        remarks = "",
                        Approvaldate = DateTime.Now,
                        seqno = 0,
                        ApproverId = Convert.ToInt32(Session["EmpID"].ToString()),
                        AppRole = "INI"
                    };
                    fm.Trn_Approvalflow.Add(reqflow);
                    fm.SaveChanges();
                    var dtemp1 = fm.usp_getHOD_forApproval(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
                    int Approver_Id = 0;
                    if (dtemp1.Count > 0)
                    {
                        Approver_Id = Convert.ToInt32(dtemp1[0].hod.ToString());

                        var reqflow1 = new Trn_Approvalflow()
                        {
                            POANO = id,
                            status = "P",
                            seqno = 1,
                            ApproverId = Approver_Id,
                            AppRole = "HOD"
                        };
                        fm.Trn_Approvalflow.Add(reqflow1);
                        fm.SaveChanges();

                    }

                    var dthod = fm.usp_getHOD_forApproval(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
                    var dthodEmail = fm.getEmpDetails(Convert.ToInt32(dthod[0].hod.ToString())).ToList();
                    string hodName = "", hodemailid = "", ccmail = "", iniName = "", OnBehalfEmail = "", OnBehalfName="";
                    if (dthodEmail.Count > 0)
                    {
                        hodName = dthodEmail[0].EmpName.ToString();
                        hodemailid = dthodEmail[0].EmailID.ToString();
                    }
                    var dtiniEmail = fm.getEmpDetails(Convert.ToInt32(Session["EmpID"].ToString())).ToList();
                    if (dtiniEmail.Count > 0)
                    {
                        ccmail = dtiniEmail[0].EmailID.ToString();
                        iniName = dtiniEmail[0].EmpName.ToString();
                    }
                    var dtOnBehalfEmail = fm.getEmpDetails(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
                    if (dtOnBehalfEmail.Count > 0)
                    {
                        OnBehalfEmail = dtOnBehalfEmail[0].EmailID.ToString();
                        OnBehalfName = dtOnBehalfEmail[0].EmpName.ToString();
                    }
                    
                    if (Request.QueryString["POANo"] + "" != "") //changed by Vikas Gage on 23/04/2015
                    {

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('POA request  " + requestRefNo + "  has been successfully raised and sent for approval to HOD, " + hodName + "');window.location ='POASubmit.aspx';",
   true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('POA request  " + requestRefNo + "  has been successfully raised and sent for approval to HOD, " + hodName + "');window.location ='Home.aspx';",
      true);
                    }

                    //mail
                    objpoa.sendMail_POASubmitted(requestRefNo, hodName, hodemailid, ccmail, iniName, OnBehalfEmail, OnBehalfName);

                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //  RadAjaxManager1.Alert("T");


            if (Request.QueryString["POANo"] + "" != "")  //Changed by Vikas Gage on 23/04/2015
            {

                RadAjaxManager1.Redirect("POASubmit.aspx");
            }
            else
            {
                RadAjaxManager1.Redirect("POArequest.aspx");

            }
            // Response.Redirect("POArequest.aspx");
        }

        protected void rcbRole_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            Rgrequest.DataSource = fm.MstAuthorityPowers.Where(a => a.RoleId.ToString() == rcbRole.SelectedValue).ToList();
            Rgrequest.DataBind();
            rcbAdditionalRole.DataSource = fm.MstRoles.Where(a => a.RoleId.ToString() != rcbRole.SelectedValue).ToList();
            rcbAdditionalRole.DataTextField = "role";
            rcbAdditionalRole.DataValueField = "roleid";
            rcbAdditionalRole.DataBind();
            rcbAdditionalRole.Items.Insert(0, new RadComboBoxItem("-- Select --", "0"));
            if (File.Exists(Server.MapPath("template_files/" + rcbRole.SelectedItem.Text.Replace("&", "and").Replace(",", "") + ".pdf")))
            {
                role_template.HRef = "template_files/" + rcbRole.SelectedItem.Text.Replace("&", "and").Replace(",", "") + ".pdf";
                tr_Template.Visible = true;

            }
            else
            {

                tr_Template.Visible = false;
            }
            //   role_template.HRef = "template_files/" + rcbRole.SelectedItem.Text.Replace("&", "and").Replace(",", "") + ".pdf";

        }

        protected void rcbAdditionalRole_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            rgRequestAdditional.DataSource = fm.MstAuthorityPowers.Where(a => a.RoleId.ToString() == rcbAdditionalRole.SelectedValue).ToList();
            rgRequestAdditional.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["additionalSelected"];
            GridDataItemCollection dtCol = rgRequestAdditional.Items;
            foreach (GridDataItem item in dtCol)
            {
                CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
                Label lblPowerid = (Label)item.FindControl("lblPowerid");

                if (chkselect.Checked == true)
                {
                    bool flag = true;
                    foreach (DataRow dtrow in dt.Rows)
                    {
                        if (dtrow["PowerId"].ToString() == lblPowerid.Text.Trim())
                        {
                            flag = false;
                            break;
                        }
                        else
                        {
                            flag = true;
                        }
                    }
                    if (flag == true)
                    {
                        DataRow drGrid = null;
                        drGrid = dt.NewRow();

                        drGrid["PowerId"] = lblPowerid.Text.Trim();
                        drGrid["RoleId"] = rcbAdditionalRole.SelectedValue;
                        drGrid["Role"] = rcbAdditionalRole.SelectedItem.Text;
                        drGrid["powerDesc"] = item["powerDesc"].Text;
                        drGrid["AddedOn"] = DateTime.Now;
                        drGrid["IsActive"] = 1;
                        dt.Rows.Add(drGrid);
                        rgRequestAdditional_selected.Visible = true;
                        Label3.Visible = true;
                    }

                }
            }



            Session["additionalSelected"] = dt;

            rgRequestAdditional_selected.DataSource = dt;
            rgRequestAdditional_selected.DataBind();
            rgRequestAdditional.Rebind();
        }

        protected void rgRequestAdditional_selected_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            rgRequestAdditional_selected.DataSource = (DataTable)Session["additionalSelected"];

        }


        protected void rgRequestAdditional_selected_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                DataTable DTTable2 = new DataTable();
                DTTable2 = (DataTable)Session["additionalSelected"];
                GridDataItem i = (GridDataItem)e.Item;
                String id = i.OwnerTableView.DataKeyValues[i.ItemIndex]["PowerId"].ToString();
                foreach (DataRow DR in DTTable2.Rows)
                {
                    if (DR["PowerId"].ToString() == id)
                    {
                        DTTable2.Rows.Remove(DR);
                        break;


                    }
                }
                DTTable2.AcceptChanges();
                Session["additionalSelected"] = DTTable2;
                if (DTTable2.Rows.Count == 0)
                {
                    rgRequestAdditional_selected.Visible = false;
                    Label3.Visible = false;
                }
                else
                {
                    rgRequestAdditional_selected.DataSource = DTTable2;
                    rgRequestAdditional_selected.DataBind();
                    rgRequestAdditional.Rebind();
                }
            }

        }

        protected void Rgrequest_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            Rgrequest.DataSource = fm.MstAuthorityPowers.Where(a => a.RoleId.ToString() == rcbRole.SelectedValue).ToList();
        }

        protected void rgRequestAdditional_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            rgRequestAdditional.DataSource = fm.MstAuthorityPowers.Where(a => a.RoleId.ToString() == rcbAdditionalRole.SelectedValue).ToList();
        }
        //protected void rgRequestAdditional_selected_ItemDataBound(object sender,GridItemEventArgs e)
        //{
        //    if (e.Item is GridDataItem)
        //    {
        //        GridDataItem ditem = (GridDataItem)e.Item;
        //        ImageButton Delete = (ImageButton)ditem["Delete"].Controls[0];
        //        TableCell cell = (TableCell)ditem["ColumnUniqueName"];
        //        if (Session["status"] + "" == "7")
        //        {
        //            Delete.Visible = false;
        //        }

        //    }
        ////}

        protected void rgRequestAdditional_ItemDataBound(object sender, GridItemEventArgs e)
        {
            GridDataItemCollection dtCol = rgRequestAdditional_selected.Items;

            foreach (GridDataItem item in dtCol)
            {

                Label lblPowerid1 = (Label)item.FindControl("lblPowerid1");
                GridDataItemCollection dtCol1 = rgRequestAdditional.Items;
                foreach (GridDataItem item1 in dtCol1)
                {
                    CheckBox chkselect = (CheckBox)item1.FindControl("chkselect");
                    Label lblPowerid = (Label)item1.FindControl("lblPowerid");
                    if (lblPowerid.Text == lblPowerid1.Text)
                    {
                        chkselect.Checked = true;
                        chkselect.Enabled = false;
                    }
                }
            }

        }

        protected void rgComments_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            rgComments.DataSource = fm.bindPriviousComments(Convert.ToInt32(Request.QueryString["POANo"]));
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {

            string requestRefNo = Session["str_POANo"].ToString();
            int POANo = Convert.ToInt32(Request.QueryString["POANo"]);

            var reqPOA = (from o in fm.Mst_POArequest
                          where o.PoaId == POANo
                          select o).First();


            reqPOA.PoaId = Convert.ToInt32(Request.QueryString["POANo"]);
            reqPOA.POANo = requestRefNo;
            reqPOA.POAclosingDate = Rdpdate.SelectedDate.Value;
            reqPOA.status = 8;


            using (var context = new FinAuthEntities1())
            {
                context.Mst_POArequest.Attach(reqPOA);
                context.Entry(reqPOA).State = EntityState.Modified;
                context.SaveChanges();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Request for POA " + requestRefNo + " closed successfully.');window.location ='home.aspx';",
true);
           // send Mail
//mail
            var dthod = fm.usp_getHOD_forApproval(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
            var dthodEmail = fm.getEmpDetails(Convert.ToInt32(dthod[0].hod.ToString())).ToList();
            string hodName = "", hodemailid = "", ccmail = "", iniName = "", OnBehalfEmail="";
            if (dthodEmail.Count > 0)
            {
                hodName = dthodEmail[0].EmpName.ToString();
                hodemailid = dthodEmail[0].EmailID.ToString();
            }
            var dtiniEmail = fm.getEmpDetails(Convert.ToInt32(Session["EmpID"].ToString())).ToList();
            if (dtiniEmail.Count > 0)
            {
                ccmail = dtiniEmail[0].EmailID.ToString();
                iniName = dtiniEmail[0].EmpName.ToString();
            }
            var dtOnBehalfEmail = fm.getEmpDetails(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
            if (dtOnBehalfEmail.Count > 0)
            {
                OnBehalfEmail = dtOnBehalfEmail[0].EmailID.ToString();

            }
            objpoa.SendMail_POAClosed(requestRefNo, Convert.ToDateTime(Rdpdate.SelectedDate.Value).ToString("dd-MMM-yyyy"), hodemailid, ccmail, OnBehalfEmail);

        }

        protected void rbPhysicalCopyRequired_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbPhysicalCopyRequired.SelectedValue == "true")
            {
                trshowreason.Visible = true;
            }
            else
            {
                trshowreason.Visible = false;

            }
        }

        protected void rcbEmpname_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            rcbempId.SelectedValue = rcbEmpname.SelectedValue;
            if (rbReqFor.SelectedValue == "1")
            {
                Session["RequestorId"] = rcbEmpname.SelectedValue;
                Session["RequestorName"] = rcbEmpname.SelectedItem.Text;
            }
            else
            {
                Session["RequestorId"] = Session["EmpID"];
                Session["RequestorName"] = Session["UserName"];
            }
            var dtemp = fm.getEmpDetails(Convert.ToInt32(Session["RequestorId"])).ToList();

            if (dtemp.Count > 0)
            {
                txtAddress.Text = "";
                txtFatherName.Text = "";
                txtLocation.Text = dtemp[0].Location.ToString();
                txtDesignation.Text = dtemp[0].Designation.ToString();
                txtFunction.Text = dtemp[0].Function.ToString();
                txtLocation.Attributes.Add("Readonly", "True");
                txtDesignation.Attributes.Add("Readonly", "True");
                txtFunction.Attributes.Add("Readonly", "True");
                txtFatherName.Text = Convert.ToString(dtemp[0].MiddleName);
                txtAddress.Text = Convert.ToString(dtemp[0].addres);
            }
        }

        protected void rcbempId_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            rcbEmpname.SelectedValue = rcbempId.SelectedValue;
            if (rbReqFor.SelectedValue == "1")
            {
                Session["RequestorId"] = rcbempId.SelectedValue;
                Session["RequestorName"] = rcbEmpname.SelectedItem.Text;
            }
            else
            {
                Session["RequestorId"] = Session["EmpID"];
                Session["RequestorName"] = Session["UserName"];
            }
            var dtemp = fm.getEmpDetails(Convert.ToInt32(Session["RequestorId"])).ToList();

            if (dtemp.Count > 0)
            {
                txtAddress.Text = "";
                txtFatherName.Text = "";
                txtLocation.Text = dtemp[0].Location.ToString();
                txtDesignation.Text = dtemp[0].Designation.ToString();
                txtFunction.Text = dtemp[0].Function.ToString();
                txtLocation.Attributes.Add("Readonly", "True");
                txtDesignation.Attributes.Add("Readonly", "True");
                txtFunction.Attributes.Add("Readonly", "True");
                txtFatherName.Text = Convert.ToString(dtemp[0].MiddleName);
                txtAddress.Text = Convert.ToString(dtemp[0].addres);
            }
        }

        protected void rbIsResolution_SelectedIndexChanged(object sender, EventArgs e)
        {
            tr_isscanned.Visible = Convert.ToBoolean(rbIsResolution.SelectedValue);
        }
    }
}