﻿using FinAuth.BLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinAuth
{
    public partial class angularjs_test : System.Web.UI.Page
    {
        POA objpoa = new POA();
        FinAuthEntities1 fm = new FinAuthEntities1();
        protected void Page_Load(object sender, EventArgs e)
        {
            ConvertDataTabletoString();
        }





        // This method is used to convert datatable to json string
        public string ConvertDataTabletoString()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ToString()))
            {
                using (SqlCommand cmd = new SqlCommand("usp_bindRaisedPowerRequest", con))
                {//cmd.Parameters.AddWithValue("@EmpID",Convert.ToInt32(Session["EmpID"]));
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        rows.Add(row);
                    }
                    return serializer.Serialize(rows);
                }
            }
        }
    }
}