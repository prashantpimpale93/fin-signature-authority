﻿using FinAuth.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinAuth
{
    public partial class home : System.Web.UI.Page
    {
        POA objpoa = new POA();
        FinAuthEntities1 fm = new FinAuthEntities1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["EmpID"] + "" == "" || Session["UserRole_Id"] + "" == "")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {

                if (Session["URole"] + "" == "HOD")
                {
                    poahod.Visible = true;

                }
                else if (Session["URole"] + "" == "SEC")
                {
                    POAreq.Visible = true;
                    POA.Visible = true;
                    h3header.Visible = false;
                    Rgrequest.Visible = false;
                }
                int EmpID = Convert.ToInt32(Session["EmpID"]);
                int? pending_poa = fm.Trn_Approvalflow.Where(a => a.status == "P" && a.ApproverId.Value == EmpID).Select(b => b.id).Count();// && a.ApproverId.Value.ToString() == Session["EmpID"]
                int? pending_poareq = fm.Trn_Approvalflow_raisenew.Where(a => a.status == "P" && a.ApproverId.Value == EmpID).Select(b => b.id).Count();
                if (pending_poa > 0)
                {
                    lblpoacount.Text = pending_poa + " New  POA request(s) Pending for Approval";
                    lblPOAHOD.Text = pending_poa + " New  POA request(s) Pending for Approval";


                    p5.Visible = false;
                    p6.Visible = true;
                    p3.Visible = false;
                    p4.Visible = true;
                }
                else
                {

                    p5.Visible = true;
                    p6.Visible = false;
                    p3.Visible = true;
                    p4.Visible = false;
                }
                if (pending_poareq > 0)
                {
                    lblpoareqcount.Text = pending_poareq + " Request(s) pending for Additional Powers";
                    p1.Visible = false;
                    p2.Visible = true;
                }
                else
                {
                    p1.Visible = true;
                    p2.Visible = false;

                }
                int Initiator_id = Convert.ToInt32(Session["EmpID"]);
                Rgrequest.DataSource = Rgrequest.DataSource = (from m in fm.Mst_POArequest
                                                               from s in fm.Mststatus1
                                                                   // from d in fm.Trn_Approvalflow
                                       .Where(c => c.statusid == m.status && m.Initiator_id == Initiator_id && ((m.status == 8) || (m.status == 9 && m.revoke_eff_date > DateTime.Now))
                                           )
                                                               select new { POANo = m.POANo, s.Status, m.PoaId, m.approveddate, Initiator_Name = m.Initiator_Name, m.Comments, m.RequestReson, m.requesteddate, m.POAtitle, m.POApdf }).Distinct().ToList();
                // fm.Mst_POArequest.Where(a => a.approveddate != null && a.Initiator_id == Initiator_id  && a.status == 8).ToList();
                Rgrequest.DataBind();
            }
        }
    }
}
