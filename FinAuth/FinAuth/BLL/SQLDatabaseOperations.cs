﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace FinAuth.BLL
{
    public class SQLDatabaseOperations
    {


        public static SqlCommand prepareParam(SqlConnection GstCon, SqlCommand GstCommand, string commandText, CommandType CommandType, params object[] parameterValues)
        {
            if (GstCon.State != ConnectionState.Open)
            {
                GstCon.Open();
            }
            int j = 0;
            //associate the connection with the            Command
            GstCommand.Connection = GstCon;

            //set the command text (stored procedure name or SQL statement)
            GstCommand.CommandText = commandText;

            //set the command type
            GstCommand.CommandType = CommandType;
            SqlParameter[] discoveredParameters = null;
            SqlCommandBuilder.DeriveParameters(GstCommand);
            GstCommand.Parameters.RemoveAt(0);
            discoveredParameters = new SqlParameter[GstCommand.Parameters.Count];
            GstCommand.Parameters.CopyTo(discoveredParameters, 0);
            //attach the command parameters if they             are(provided)
            j = discoveredParameters.Length - 1;
            for (int i = 0; i <= j; i++)
            {
                discoveredParameters[i].Value = parameterValues[i];
            }
            GstCommand.Parameters.Clear();
            if ((discoveredParameters != null))
            {
                SqlParameter p1 = null;
                foreach (SqlParameter p1_loopVariable in discoveredParameters)
                {
                    p1 = p1_loopVariable;
                    //check for derived output value with no value assigned
                    if (p1.Direction == ParameterDirection.InputOutput & p1.Value == null)
                    {
                        p1.Value = null;
                    }
                    // GstCommand.Parameters.Add(p1)
                    GstCommand.Parameters.Add(p1);
                }

            }
            return GstCommand;
        }
        public static DataSet ExecuteDataset(string commandText, params object[] parameterValues)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            //create a command and prepare it for execution()
            SqlConnection GstCon = new SqlConnection(connectionString);
            SqlCommand GstCommand = new SqlCommand();
            DataSet GstDS = new DataSet();
            SqlDataAdapter GstDA = null;

            GstCommand.CommandTimeout = 600000;


            GstCommand = prepareParam(GstCon, GstCommand, commandText, CommandType.StoredProcedure, parameterValues);
            //create the DataAdapter & DataSet
            GstDA = new SqlDataAdapter(GstCommand);

            //fill the DataSet using default values for DataTable names, etc.
            GstDA.Fill(GstDS);

            //detach the SqlParameters from the command object, so they can be used again
            GstCommand.Parameters.Clear();

            //return the dataset
            return GstDS;

        }
        //ExecuteDataset
        public static int ExecuteNonQuery(string commandText, params object[] parameterValues)
        {
            string connectionString = ConfigurationManager.AppSettings["Conn"];
            SqlConnection GstCon = new SqlConnection(connectionString);
            SqlCommand GstCommand = new SqlCommand();
            DataSet GstDS = new DataSet();
            int retval = 0;
            GstCommand.CommandTimeout = 600000;
            GstCommand = prepareParam(GstCon, GstCommand, commandText, CommandType.StoredProcedure, parameterValues);

            //finally, execute the command.
            retval = GstCommand.ExecuteNonQuery();

            //detach the SqlParameters from the command object, so they can be used again
            GstCommand.Parameters.Clear();
            return retval;
        }
        //ExecuteNonQuery

        public static object ExecuteScalar(string commandText, params object[] parameterValues)
        {

            string connectionString = ConfigurationManager.AppSettings["Conn"];
            //create a command and prepare it for execution
            SqlConnection gstCon = new SqlConnection(connectionString);
            SqlCommand gstCommand = new SqlCommand();
            object retval = null;

            gstCommand = prepareParam(gstCon, gstCommand, commandText, CommandType.StoredProcedure, parameterValues);
            //execute the command & return the results
            retval = gstCommand.ExecuteScalar();

            //detach the SqlParameters from the command object, so they can be used again
            gstCommand.Parameters.Clear();

            return retval;
        }
        //ExecuteScalar
        public static object ExecuteReader(string commandText, params object[] parameterValues)
        {
            string connectionString = ConfigurationManager.AppSettings["Conn"];

            //create a command and prepare it for execution
            SqlConnection gstCon = new SqlConnection(connectionString);
            SqlCommand gstCommand = new SqlCommand();
            SqlDataReader gstReader = null;
            gstCommand.CommandTimeout = 600000;
            gstCommand = prepareParam(gstCon, gstCommand, commandText, CommandType.StoredProcedure, parameterValues);
            //execute the command & return the results
            gstReader = gstCommand.ExecuteReader();

            //detach the SqlParameters from the command object, so they can be used again
            gstCommand.Parameters.Clear();

            return gstReader;

        }
        //ExecuteReader
    }


}