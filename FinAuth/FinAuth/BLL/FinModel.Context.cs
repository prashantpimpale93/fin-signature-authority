﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinAuth.BLL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class FinAuthEntities1 : DbContext
    {
        public FinAuthEntities1()
            : base("name=FinAuthEntities1")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Mst_POArequest> Mst_POArequest { get; set; }
        public virtual DbSet<Mst_SecTeam> Mst_SecTeam { get; set; }
        public virtual DbSet<MstRole> MstRoles { get; set; }
        public virtual DbSet<Trn_Approvalflow> Trn_Approvalflow { get; set; }
        public virtual DbSet<Trn_Approvalflow_raisenew> Trn_Approvalflow_raisenew { get; set; }
        public virtual DbSet<trn_POARequest_details> trn_POARequest_details { get; set; }
        public virtual DbSet<mta_mstemployee> mta_mstemployee { get; set; }
        public virtual DbSet<Mststatus> Mststatus1 { get; set; }
        public virtual DbSet<Mst_AdditionalPower_request> Mst_AdditionalPower_request { get; set; }
        public virtual DbSet<MstAuthorityPower> MstAuthorityPowers { get; set; }
        public virtual DbSet<AS_Mst_ApprovalFlowTemplate> AS_Mst_ApprovalFlowTemplate { get; set; }
        public virtual DbSet<AS_Mst_Plant> AS_Mst_Plant { get; set; }
        public virtual DbSet<AS_Mst_Reason> AS_Mst_Reason { get; set; }
        public virtual DbSet<AS_Mst_Region> AS_Mst_Region { get; set; }
        public virtual DbSet<AS_Mst_Type> AS_Mst_Type { get; set; }
        public virtual DbSet<AS_Mst_BankDetails> AS_Mst_BankDetails { get; set; }
        public virtual DbSet<AS_Trn_AdditionDetails> AS_Trn_AdditionDetails { get; set; }
        public virtual DbSet<AS_Trn_AdditionMemberDetails> AS_Trn_AdditionMemberDetails { get; set; }
        public virtual DbSet<AS_Trn_ApprovalFlow> AS_Trn_ApprovalFlow { get; set; }
        public virtual DbSet<AS_Trn_BankDetailsLog> AS_Trn_BankDetailsLog { get; set; }
        public virtual DbSet<AS_Trn_DeletionDetails> AS_Trn_DeletionDetails { get; set; }
        public virtual DbSet<AS_Trn_DeletionMemberDetails> AS_Trn_DeletionMemberDetails { get; set; }
    
        public virtual int approval_flowRaisenew(Nullable<int> poaid, Nullable<int> seqno)
        {
            var poaidParameter = poaid.HasValue ?
                new ObjectParameter("poaid", poaid) :
                new ObjectParameter("poaid", typeof(int));
    
            var seqnoParameter = seqno.HasValue ?
                new ObjectParameter("seqno", seqno) :
                new ObjectParameter("seqno", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("approval_flowRaisenew", poaidParameter, seqnoParameter);
        }
    
        public virtual ObjectResult<bindAuthorityPower_Result> bindAuthorityPower(Nullable<int> roleId)
        {
            var roleIdParameter = roleId.HasValue ?
                new ObjectParameter("RoleId", roleId) :
                new ObjectParameter("RoleId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<bindAuthorityPower_Result>("bindAuthorityPower", roleIdParameter);
        }
    
        public virtual ObjectResult<bindEmpname_Result> bindEmpname()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<bindEmpname_Result>("bindEmpname");
        }
    
        public virtual ObjectResult<bindrole_Result> bindrole()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<bindrole_Result>("bindrole");
        }
    
        public virtual ObjectResult<generate_POAReqNo_Result> generate_POAReqNo()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<generate_POAReqNo_Result>("generate_POAReqNo");
        }
    
        public virtual ObjectResult<generate_PowerRaiseNo_Result> generate_PowerRaiseNo()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<generate_PowerRaiseNo_Result>("generate_PowerRaiseNo");
        }
    
        public virtual int POAapprovalflow(string action, string remark, Nullable<int> poaid, Nullable<int> approverid, string role)
        {
            var actionParameter = action != null ?
                new ObjectParameter("action", action) :
                new ObjectParameter("action", typeof(string));
    
            var remarkParameter = remark != null ?
                new ObjectParameter("remark", remark) :
                new ObjectParameter("remark", typeof(string));
    
            var poaidParameter = poaid.HasValue ?
                new ObjectParameter("poaid", poaid) :
                new ObjectParameter("poaid", typeof(int));
    
            var approveridParameter = approverid.HasValue ?
                new ObjectParameter("approverid", approverid) :
                new ObjectParameter("approverid", typeof(int));
    
            var roleParameter = role != null ?
                new ObjectParameter("role", role) :
                new ObjectParameter("role", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("POAapprovalflow", actionParameter, remarkParameter, poaidParameter, approveridParameter, roleParameter);
        }
    
        public virtual ObjectResult<validate_userlogin_Result> validate_userlogin(string username, string password)
        {
            var usernameParameter = username != null ?
                new ObjectParameter("username", username) :
                new ObjectParameter("username", typeof(string));
    
            var passwordParameter = password != null ?
                new ObjectParameter("Password", password) :
                new ObjectParameter("Password", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<validate_userlogin_Result>("validate_userlogin", usernameParameter, passwordParameter);
        }
    
        public virtual ObjectResult<usp_getHOD_forApproval_Result> usp_getHOD_forApproval(Nullable<int> empid)
        {
            var empidParameter = empid.HasValue ?
                new ObjectParameter("empid", empid) :
                new ObjectParameter("empid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<usp_getHOD_forApproval_Result>("usp_getHOD_forApproval", empidParameter);
        }
    
        public virtual ObjectResult<view_poa_details_Result> view_poa_details(Nullable<int> poaid)
        {
            var poaidParameter = poaid.HasValue ?
                new ObjectParameter("poaid", poaid) :
                new ObjectParameter("poaid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<view_poa_details_Result>("view_poa_details", poaidParameter);
        }
    
        public virtual ObjectResult<bindPriviousComments_Result> bindPriviousComments(Nullable<int> poaID)
        {
            var poaIDParameter = poaID.HasValue ?
                new ObjectParameter("PoaID", poaID) :
                new ObjectParameter("PoaID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<bindPriviousComments_Result>("bindPriviousComments", poaIDParameter);
        }
    
        public virtual ObjectResult<usp_bindRaisedPowerRequest_Result> usp_bindRaisedPowerRequest()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<usp_bindRaisedPowerRequest_Result>("usp_bindRaisedPowerRequest");
        }
    
        public virtual ObjectResult<uspBindSerach_Result> uspBindSerach()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<uspBindSerach_Result>("uspBindSerach");
        }
    
        public virtual ObjectResult<getEmpDetails_Result> getEmpDetails(Nullable<int> eMPID)
        {
            var eMPIDParameter = eMPID.HasValue ?
                new ObjectParameter("EMPID", eMPID) :
                new ObjectParameter("EMPID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<getEmpDetails_Result>("getEmpDetails", eMPIDParameter);
        }
    
        public virtual ObjectResult<generate_POASequenceNO_Result> generate_POASequenceNO()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<generate_POASequenceNO_Result>("generate_POASequenceNO");
        }
    
        public virtual ObjectResult<AS_AdditionSignatoryPrint_Result> AS_AdditionSignatoryPrint(string deletionID)
        {
            var deletionIDParameter = deletionID != null ?
                new ObjectParameter("DeletionID", deletionID) :
                new ObjectParameter("DeletionID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_AdditionSignatoryPrint_Result>("AS_AdditionSignatoryPrint", deletionIDParameter);
        }
    
        public virtual int AS_DeleteFinalBankDetails(string deletionID, string empID, string requiredFor, string accountNO, string bank, string bankAddress, string locationDetails, string type)
        {
            var deletionIDParameter = deletionID != null ?
                new ObjectParameter("DeletionID", deletionID) :
                new ObjectParameter("DeletionID", typeof(string));
    
            var empIDParameter = empID != null ?
                new ObjectParameter("EmpID", empID) :
                new ObjectParameter("EmpID", typeof(string));
    
            var requiredForParameter = requiredFor != null ?
                new ObjectParameter("RequiredFor", requiredFor) :
                new ObjectParameter("RequiredFor", typeof(string));
    
            var accountNOParameter = accountNO != null ?
                new ObjectParameter("AccountNO", accountNO) :
                new ObjectParameter("AccountNO", typeof(string));
    
            var bankParameter = bank != null ?
                new ObjectParameter("Bank", bank) :
                new ObjectParameter("Bank", typeof(string));
    
            var bankAddressParameter = bankAddress != null ?
                new ObjectParameter("BankAddress", bankAddress) :
                new ObjectParameter("BankAddress", typeof(string));
    
            var locationDetailsParameter = locationDetails != null ?
                new ObjectParameter("LocationDetails", locationDetails) :
                new ObjectParameter("LocationDetails", typeof(string));
    
            var typeParameter = type != null ?
                new ObjectParameter("Type", type) :
                new ObjectParameter("Type", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AS_DeleteFinalBankDetails", deletionIDParameter, empIDParameter, requiredForParameter, accountNOParameter, bankParameter, bankAddressParameter, locationDetailsParameter, typeParameter);
        }
    
        public virtual ObjectResult<AS_getAdditionReportDetails_Result> AS_getAdditionReportDetails()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_getAdditionReportDetails_Result>("AS_getAdditionReportDetails");
        }
    
        public virtual ObjectResult<AS_getApproverComments_Result> AS_getApproverComments(string requestID)
        {
            var requestIDParameter = requestID != null ?
                new ObjectParameter("RequestID", requestID) :
                new ObjectParameter("RequestID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_getApproverComments_Result>("AS_getApproverComments", requestIDParameter);
        }
    
        public virtual ObjectResult<AS_getDeleExistDetails_Result> AS_getDeleExistDetails(string accountNo)
        {
            var accountNoParameter = accountNo != null ?
                new ObjectParameter("AccountNo", accountNo) :
                new ObjectParameter("AccountNo", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_getDeleExistDetails_Result>("AS_getDeleExistDetails", accountNoParameter);
        }
    
        public virtual ObjectResult<AS_getDeletionDraftDetails_Result> AS_getDeletionDraftDetails()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_getDeletionDraftDetails_Result>("AS_getDeletionDraftDetails");
        }
    
        public virtual ObjectResult<AS_getDeletionReportDetails_Result> AS_getDeletionReportDetails()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_getDeletionReportDetails_Result>("AS_getDeletionReportDetails");
        }
    
        public virtual ObjectResult<AS_getDetailApprovalTrackDetails_Result> AS_getDetailApprovalTrackDetails(string trnID)
        {
            var trnIDParameter = trnID != null ?
                new ObjectParameter("TrnID", trnID) :
                new ObjectParameter("TrnID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_getDetailApprovalTrackDetails_Result>("AS_getDetailApprovalTrackDetails", trnIDParameter);
        }
    
        public virtual ObjectResult<AS_getDraftDetails_Result> AS_getDraftDetails()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_getDraftDetails_Result>("AS_getDraftDetails");
        }
    
        public virtual ObjectResult<AS_getMainApprovalTrackDetails_Result> AS_getMainApprovalTrackDetails()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_getMainApprovalTrackDetails_Result>("AS_getMainApprovalTrackDetails");
        }
    
        public virtual ObjectResult<AS_getPendingAddition_Result> AS_getPendingAddition(string approverID)
        {
            var approverIDParameter = approverID != null ?
                new ObjectParameter("ApproverID", approverID) :
                new ObjectParameter("ApproverID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_getPendingAddition_Result>("AS_getPendingAddition", approverIDParameter);
        }
    
        public virtual ObjectResult<AS_getPendingDeletion_Result> AS_getPendingDeletion(string approverID)
        {
            var approverIDParameter = approverID != null ?
                new ObjectParameter("ApproverID", approverID) :
                new ObjectParameter("ApproverID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_getPendingDeletion_Result>("AS_getPendingDeletion", approverIDParameter);
        }
    
        public virtual ObjectResult<AS_getRejectedRequestsAddition_Result> AS_getRejectedRequestsAddition(string initiatorID)
        {
            var initiatorIDParameter = initiatorID != null ?
                new ObjectParameter("InitiatorID", initiatorID) :
                new ObjectParameter("InitiatorID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_getRejectedRequestsAddition_Result>("AS_getRejectedRequestsAddition", initiatorIDParameter);
        }
    
        public virtual ObjectResult<AS_getRejectedRequestsDeletion_Result> AS_getRejectedRequestsDeletion(string initiatorID)
        {
            var initiatorIDParameter = initiatorID != null ?
                new ObjectParameter("InitiatorID", initiatorID) :
                new ObjectParameter("InitiatorID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_getRejectedRequestsDeletion_Result>("AS_getRejectedRequestsDeletion", initiatorIDParameter);
        }
    
        public virtual int AS_InsertAdditionDetails(string additionID, string locationType, string locationDetails, string requiredFor, string accountNO, string additionDetails, string comments, string createdBy, string saveDraftFlag)
        {
            var additionIDParameter = additionID != null ?
                new ObjectParameter("AdditionID", additionID) :
                new ObjectParameter("AdditionID", typeof(string));
    
            var locationTypeParameter = locationType != null ?
                new ObjectParameter("LocationType", locationType) :
                new ObjectParameter("LocationType", typeof(string));
    
            var locationDetailsParameter = locationDetails != null ?
                new ObjectParameter("LocationDetails", locationDetails) :
                new ObjectParameter("LocationDetails", typeof(string));
    
            var requiredForParameter = requiredFor != null ?
                new ObjectParameter("RequiredFor", requiredFor) :
                new ObjectParameter("RequiredFor", typeof(string));
    
            var accountNOParameter = accountNO != null ?
                new ObjectParameter("AccountNO", accountNO) :
                new ObjectParameter("AccountNO", typeof(string));
    
            var additionDetailsParameter = additionDetails != null ?
                new ObjectParameter("AdditionDetails", additionDetails) :
                new ObjectParameter("AdditionDetails", typeof(string));
    
            var commentsParameter = comments != null ?
                new ObjectParameter("Comments", comments) :
                new ObjectParameter("Comments", typeof(string));
    
            var createdByParameter = createdBy != null ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(string));
    
            var saveDraftFlagParameter = saveDraftFlag != null ?
                new ObjectParameter("SaveDraftFlag", saveDraftFlag) :
                new ObjectParameter("SaveDraftFlag", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AS_InsertAdditionDetails", additionIDParameter, locationTypeParameter, locationDetailsParameter, requiredForParameter, accountNOParameter, additionDetailsParameter, commentsParameter, createdByParameter, saveDraftFlagParameter);
        }
    
        public virtual int AS_InsertAddMemberDetails(string additionID, string empID, string type)
        {
            var additionIDParameter = additionID != null ?
                new ObjectParameter("AdditionID", additionID) :
                new ObjectParameter("AdditionID", typeof(string));
    
            var empIDParameter = empID != null ?
                new ObjectParameter("EmpID", empID) :
                new ObjectParameter("EmpID", typeof(string));
    
            var typeParameter = type != null ?
                new ObjectParameter("Type", type) :
                new ObjectParameter("Type", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AS_InsertAddMemberDetails", additionIDParameter, empIDParameter, typeParameter);
        }
    
        public virtual int AS_InsertDeletionDetails(string deletionID, string locationType, string locationDetails, string accountNO, string deletionDetails, string comments, string createdBy, string saveDraftFlag)
        {
            var deletionIDParameter = deletionID != null ?
                new ObjectParameter("DeletionID", deletionID) :
                new ObjectParameter("DeletionID", typeof(string));
    
            var locationTypeParameter = locationType != null ?
                new ObjectParameter("LocationType", locationType) :
                new ObjectParameter("LocationType", typeof(string));
    
            var locationDetailsParameter = locationDetails != null ?
                new ObjectParameter("LocationDetails", locationDetails) :
                new ObjectParameter("LocationDetails", typeof(string));
    
            var accountNOParameter = accountNO != null ?
                new ObjectParameter("AccountNO", accountNO) :
                new ObjectParameter("AccountNO", typeof(string));
    
            var deletionDetailsParameter = deletionDetails != null ?
                new ObjectParameter("DeletionDetails", deletionDetails) :
                new ObjectParameter("DeletionDetails", typeof(string));
    
            var commentsParameter = comments != null ?
                new ObjectParameter("Comments", comments) :
                new ObjectParameter("Comments", typeof(string));
    
            var createdByParameter = createdBy != null ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(string));
    
            var saveDraftFlagParameter = saveDraftFlag != null ?
                new ObjectParameter("SaveDraftFlag", saveDraftFlag) :
                new ObjectParameter("SaveDraftFlag", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AS_InsertDeletionDetails", deletionIDParameter, locationTypeParameter, locationDetailsParameter, accountNOParameter, deletionDetailsParameter, commentsParameter, createdByParameter, saveDraftFlagParameter);
        }
    
        public virtual int AS_InsertDeletionMemberDetails(string deletionID, string requiredFor, string empID, string empName, string designation, string type)
        {
            var deletionIDParameter = deletionID != null ?
                new ObjectParameter("DeletionID", deletionID) :
                new ObjectParameter("DeletionID", typeof(string));
    
            var requiredForParameter = requiredFor != null ?
                new ObjectParameter("RequiredFor", requiredFor) :
                new ObjectParameter("RequiredFor", typeof(string));
    
            var empIDParameter = empID != null ?
                new ObjectParameter("EmpID", empID) :
                new ObjectParameter("EmpID", typeof(string));
    
            var empNameParameter = empName != null ?
                new ObjectParameter("EmpName", empName) :
                new ObjectParameter("EmpName", typeof(string));
    
            var designationParameter = designation != null ?
                new ObjectParameter("Designation", designation) :
                new ObjectParameter("Designation", typeof(string));
    
            var typeParameter = type != null ?
                new ObjectParameter("Type", type) :
                new ObjectParameter("Type", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AS_InsertDeletionMemberDetails", deletionIDParameter, requiredForParameter, empIDParameter, empNameParameter, designationParameter, typeParameter);
        }
    
        public virtual int AS_InsertFinalBankDetails(string empID, string requiredFor, string accountNO, string bank, string bankAddress, string locationDetails, string type)
        {
            var empIDParameter = empID != null ?
                new ObjectParameter("EmpID", empID) :
                new ObjectParameter("EmpID", typeof(string));
    
            var requiredForParameter = requiredFor != null ?
                new ObjectParameter("RequiredFor", requiredFor) :
                new ObjectParameter("RequiredFor", typeof(string));
    
            var accountNOParameter = accountNO != null ?
                new ObjectParameter("AccountNO", accountNO) :
                new ObjectParameter("AccountNO", typeof(string));
    
            var bankParameter = bank != null ?
                new ObjectParameter("Bank", bank) :
                new ObjectParameter("Bank", typeof(string));
    
            var bankAddressParameter = bankAddress != null ?
                new ObjectParameter("BankAddress", bankAddress) :
                new ObjectParameter("BankAddress", typeof(string));
    
            var locationDetailsParameter = locationDetails != null ?
                new ObjectParameter("LocationDetails", locationDetails) :
                new ObjectParameter("LocationDetails", typeof(string));
    
            var typeParameter = type != null ?
                new ObjectParameter("Type", type) :
                new ObjectParameter("Type", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AS_InsertFinalBankDetails", empIDParameter, requiredForParameter, accountNOParameter, bankParameter, bankAddressParameter, locationDetailsParameter, typeParameter);
        }
    
        public virtual ObjectResult<AS_SP_AdditionSignatoryPrint_Result> AS_SP_AdditionSignatoryPrint(string additionID)
        {
            var additionIDParameter = additionID != null ?
                new ObjectParameter("AdditionID", additionID) :
                new ObjectParameter("AdditionID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_SP_AdditionSignatoryPrint_Result>("AS_SP_AdditionSignatoryPrint", additionIDParameter);
        }
    
        public virtual ObjectResult<AS_SP_DeleteSignatoryPrint_Result> AS_SP_DeleteSignatoryPrint(string deletionID)
        {
            var deletionIDParameter = deletionID != null ?
                new ObjectParameter("DeletionID", deletionID) :
                new ObjectParameter("DeletionID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AS_SP_DeleteSignatoryPrint_Result>("AS_SP_DeleteSignatoryPrint", deletionIDParameter);
        }
    
        public virtual int AS_UpdateAdditionDetails(string additionID, string locationType, string locationDetails, string requiredFor, string accountNO, string additionDetails, string comments, string createdBy, string saveDraftFlag)
        {
            var additionIDParameter = additionID != null ?
                new ObjectParameter("AdditionID", additionID) :
                new ObjectParameter("AdditionID", typeof(string));
    
            var locationTypeParameter = locationType != null ?
                new ObjectParameter("LocationType", locationType) :
                new ObjectParameter("LocationType", typeof(string));
    
            var locationDetailsParameter = locationDetails != null ?
                new ObjectParameter("LocationDetails", locationDetails) :
                new ObjectParameter("LocationDetails", typeof(string));
    
            var requiredForParameter = requiredFor != null ?
                new ObjectParameter("RequiredFor", requiredFor) :
                new ObjectParameter("RequiredFor", typeof(string));
    
            var accountNOParameter = accountNO != null ?
                new ObjectParameter("AccountNO", accountNO) :
                new ObjectParameter("AccountNO", typeof(string));
    
            var additionDetailsParameter = additionDetails != null ?
                new ObjectParameter("AdditionDetails", additionDetails) :
                new ObjectParameter("AdditionDetails", typeof(string));
    
            var commentsParameter = comments != null ?
                new ObjectParameter("Comments", comments) :
                new ObjectParameter("Comments", typeof(string));
    
            var createdByParameter = createdBy != null ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(string));
    
            var saveDraftFlagParameter = saveDraftFlag != null ?
                new ObjectParameter("SaveDraftFlag", saveDraftFlag) :
                new ObjectParameter("SaveDraftFlag", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AS_UpdateAdditionDetails", additionIDParameter, locationTypeParameter, locationDetailsParameter, requiredForParameter, accountNOParameter, additionDetailsParameter, commentsParameter, createdByParameter, saveDraftFlagParameter);
        }
    
        public virtual int AS_UpdateDeletionDetails(string deletionID, string locationType, string locationDetails, string accountNO, string deletionDetails, string comments, string createdBy, string saveDraftFlag)
        {
            var deletionIDParameter = deletionID != null ?
                new ObjectParameter("DeletionID", deletionID) :
                new ObjectParameter("DeletionID", typeof(string));
    
            var locationTypeParameter = locationType != null ?
                new ObjectParameter("LocationType", locationType) :
                new ObjectParameter("LocationType", typeof(string));
    
            var locationDetailsParameter = locationDetails != null ?
                new ObjectParameter("LocationDetails", locationDetails) :
                new ObjectParameter("LocationDetails", typeof(string));
    
            var accountNOParameter = accountNO != null ?
                new ObjectParameter("AccountNO", accountNO) :
                new ObjectParameter("AccountNO", typeof(string));
    
            var deletionDetailsParameter = deletionDetails != null ?
                new ObjectParameter("DeletionDetails", deletionDetails) :
                new ObjectParameter("DeletionDetails", typeof(string));
    
            var commentsParameter = comments != null ?
                new ObjectParameter("Comments", comments) :
                new ObjectParameter("Comments", typeof(string));
    
            var createdByParameter = createdBy != null ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(string));
    
            var saveDraftFlagParameter = saveDraftFlag != null ?
                new ObjectParameter("SaveDraftFlag", saveDraftFlag) :
                new ObjectParameter("SaveDraftFlag", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AS_UpdateDeletionDetails", deletionIDParameter, locationTypeParameter, locationDetailsParameter, accountNOParameter, deletionDetailsParameter, commentsParameter, createdByParameter, saveDraftFlagParameter);
        }
    
        public virtual int AS_UpdatetreasuryDetailsAddition(string additionID, string name, string comments, string date, string treasuryType, string approverID, string initiatorID, Nullable<int> nextSeq, string originalFname, string modifiedFname)
        {
            var additionIDParameter = additionID != null ?
                new ObjectParameter("AdditionID", additionID) :
                new ObjectParameter("AdditionID", typeof(string));
    
            var nameParameter = name != null ?
                new ObjectParameter("Name", name) :
                new ObjectParameter("Name", typeof(string));
    
            var commentsParameter = comments != null ?
                new ObjectParameter("Comments", comments) :
                new ObjectParameter("Comments", typeof(string));
    
            var dateParameter = date != null ?
                new ObjectParameter("Date", date) :
                new ObjectParameter("Date", typeof(string));
    
            var treasuryTypeParameter = treasuryType != null ?
                new ObjectParameter("TreasuryType", treasuryType) :
                new ObjectParameter("TreasuryType", typeof(string));
    
            var approverIDParameter = approverID != null ?
                new ObjectParameter("ApproverID", approverID) :
                new ObjectParameter("ApproverID", typeof(string));
    
            var initiatorIDParameter = initiatorID != null ?
                new ObjectParameter("InitiatorID", initiatorID) :
                new ObjectParameter("InitiatorID", typeof(string));
    
            var nextSeqParameter = nextSeq.HasValue ?
                new ObjectParameter("NextSeq", nextSeq) :
                new ObjectParameter("NextSeq", typeof(int));
    
            var originalFnameParameter = originalFname != null ?
                new ObjectParameter("OriginalFname", originalFname) :
                new ObjectParameter("OriginalFname", typeof(string));
    
            var modifiedFnameParameter = modifiedFname != null ?
                new ObjectParameter("ModifiedFname", modifiedFname) :
                new ObjectParameter("ModifiedFname", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AS_UpdatetreasuryDetailsAddition", additionIDParameter, nameParameter, commentsParameter, dateParameter, treasuryTypeParameter, approverIDParameter, initiatorIDParameter, nextSeqParameter, originalFnameParameter, modifiedFnameParameter);
        }
    
        public virtual int AS_UpdatetreasuryDetailsDeletion(string additionID, string name, string comments, string date, string treasuryType, string approverID, string initiatorID, Nullable<int> nextSeq, string originalFname, string modifiedFname)
        {
            var additionIDParameter = additionID != null ?
                new ObjectParameter("AdditionID", additionID) :
                new ObjectParameter("AdditionID", typeof(string));
    
            var nameParameter = name != null ?
                new ObjectParameter("Name", name) :
                new ObjectParameter("Name", typeof(string));
    
            var commentsParameter = comments != null ?
                new ObjectParameter("Comments", comments) :
                new ObjectParameter("Comments", typeof(string));
    
            var dateParameter = date != null ?
                new ObjectParameter("Date", date) :
                new ObjectParameter("Date", typeof(string));
    
            var treasuryTypeParameter = treasuryType != null ?
                new ObjectParameter("TreasuryType", treasuryType) :
                new ObjectParameter("TreasuryType", typeof(string));
    
            var approverIDParameter = approverID != null ?
                new ObjectParameter("ApproverID", approverID) :
                new ObjectParameter("ApproverID", typeof(string));
    
            var initiatorIDParameter = initiatorID != null ?
                new ObjectParameter("InitiatorID", initiatorID) :
                new ObjectParameter("InitiatorID", typeof(string));
    
            var nextSeqParameter = nextSeq.HasValue ?
                new ObjectParameter("NextSeq", nextSeq) :
                new ObjectParameter("NextSeq", typeof(int));
    
            var originalFnameParameter = originalFname != null ?
                new ObjectParameter("OriginalFname", originalFname) :
                new ObjectParameter("OriginalFname", typeof(string));
    
            var modifiedFnameParameter = modifiedFname != null ?
                new ObjectParameter("ModifiedFname", modifiedFname) :
                new ObjectParameter("ModifiedFname", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AS_UpdatetreasuryDetailsDeletion", additionIDParameter, nameParameter, commentsParameter, dateParameter, treasuryTypeParameter, approverIDParameter, initiatorIDParameter, nextSeqParameter, originalFnameParameter, modifiedFnameParameter);
        }
    }
}
