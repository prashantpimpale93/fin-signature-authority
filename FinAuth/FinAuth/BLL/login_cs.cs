﻿using System.Configuration;
using System.Data;

namespace FinAuth.BLL
{
    public class login_cs
    {
      
        public DataSet validatelogin(string userid,string passwrd)
        {
            DataSet gstDS = SQLDatabaseOperations.ExecuteDataset( "validate_userlogin", userid, passwrd);
            return gstDS;
        }

    }
}