﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Web;

namespace FinAuth.BLL
{
    public class POA
    {
        public SqlCommand com = new SqlCommand();
        public SqlConnection con;
        public string strConnectionString = "";
        private bool handleErrors = false;
        private string strLastError = "";
        // public string stateID = "29";
        public SqlTransaction trans;
        FinAuthEntities1 fm = new FinAuthEntities1();
        SmtpClient Client = new SmtpClient();
        public MailMessage Email;
        public POA()
        {
            if (ConfigurationManager.ConnectionStrings["Conn"] == null)
                throw (new NullReferenceException("ConnectionString configuration is missing from you web.config. It should contain  <connectionStrings> <add name=\"ST\" connectionString=\"Data Source=. ;Initial Catalog = ST;Integrated Security=ST\"  </connectionStrings>"));

            strConnectionString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            con = new SqlConnection(strConnectionString);
            com.Connection = con;
            com.Transaction = trans;

            //Constructor 
            //  this.Client = new SmtpClient("10.121.11.5", 25);
            this.Email = new MailMessage();
            Email.From = new MailAddress("meramilaap@maricoindia.net");

            // this.Email.To.Add(new MailAddress("abhishekd@maricoindia.net"));

            this.Email.Bcc.Add(new MailAddress("urvee@maricoindia.net"));
            //this.Email.Bcc.Add(new MailAddress("deepd@maricoindia.net"));
            this.Email.IsBodyHtml = true;

        }

        public DataSet bindAuthorityPower(int roleid)
        {
            DataSet gstDS = SQLDatabaseOperations.ExecuteDataset("bindAuthorityPower", roleid);
            return gstDS;
        }
        public DataSet usp_view_poa_details(int poaid)
        {
            DataSet gstDS = SQLDatabaseOperations.ExecuteDataset("view_poa_details", poaid);
            return gstDS;
        }

        public void sendMail_POASubmitted(string POAno, string HODName, string toemail, string ccmail, string InitiatorName, string OnBehalf,string OnBehalfName)
        {
            try
            {

                string strHtmlBody;
                //  Email.Subject = "Request Id" + POAno + " requested by Inititator " + ini_name;
                Email.Subject = "Approval of POA Request- " + POAno + "";
                Email.Bcc.Add("urvee@maricoindia.com");

                Email.To.Add(new MailAddress(toemail));
                if (ccmail != "")
                {
                    Email.CC.Add(new MailAddress(ccmail));
                }
                if (ccmail != OnBehalf && OnBehalf != "")
                {
                    using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Mailer_Format/POASubmitted_OnBehalf.html")))
                    {
                        strHtmlBody = reader.ReadToEnd();
                    }
                    strHtmlBody = strHtmlBody.Replace("[@POAno]", POAno.ToString()).Replace("[@IniName]", OnBehalfName.ToString()).Replace("[@HODName]", HODName.ToString());
                    Email.Body = strHtmlBody;
                    Email.CC.Add(new MailAddress(OnBehalf));

                }
                else
                {
                    using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Mailer_Format/POASubmitted.html")))
                    {
                        strHtmlBody = reader.ReadToEnd();
                    }
                    strHtmlBody = strHtmlBody.Replace("[@POAno]", POAno.ToString()).Replace("[@IniName]", InitiatorName.ToString()).Replace("[@HODName]", HODName.ToString());
                    Email.Body = strHtmlBody;
                }

                //foreach (var sec in fm.Mst_SecTeam)
                //{
                //    if (sec.Emailid + "" != "")
                //    {
                //        Email.CC.Add(new MailAddress(sec.Emailid.ToString()));
                //    }
                //}
               //// Email.CC.Add(new MailAddress("secretarialteam@maricoindia.net"));
                Client.Send(Email);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void sendMail_HOD_Rejected(string POAno, string HODname, string toemail, string ccmail, string comments, string InitiatorName, string OnBehalf)
        {
            try
            {
                string strHtmlBody;
                Email.Subject = " Your POA Request " + POAno + " has been rejected/ marked for resubmission";//"Rejection of POA Request- " + POAno;

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Mailer_Format/HOD_rejected.html")))
                {
                    strHtmlBody = reader.ReadToEnd();
                }
                strHtmlBody = strHtmlBody.Replace("[@poano]", POAno.ToString()).Replace("[@iniName]", InitiatorName).Replace("[@Comments]", comments).Replace("[@HODName]", HODname);
                Email.Body = strHtmlBody;
                Email.To.Add(new MailAddress(toemail));
                if (ccmail != "")
                {
                    Email.CC.Add(new MailAddress(ccmail));
                }
                if (toemail != OnBehalf && OnBehalf != "")
                {
                    Email.To.Add(new MailAddress(OnBehalf));
                }
                //foreach (var sec in fm.Mst_SecTeam)
                //{
                //    if (sec.Emailid + "" != "")
                //    {
                //        Email.CC.Add(new MailAddress(sec.Emailid.ToString()));
                //    }
                //}
               // Email.CC.Add(new MailAddress("secretarialteam@maricoindia.net"));
               Client.Send(Email);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void sendMail_HOD_approval(string POAno, string HODname, string toemail, string ccmail, string comments, string OnBehalf)
        {
            try
            {

                string strHtmlBody;
                Email.Subject = "Your POA Request  " + POAno + " has been approved";

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Mailer_Format/HOD_approval.html")))
                {
                    strHtmlBody = reader.ReadToEnd();
                }
                strHtmlBody = strHtmlBody.Replace("[@POAno]", POAno.ToString()).Replace("[@HODName]", HODname).Replace("[@Comments]", comments);
                Email.Body = strHtmlBody;
                Email.To.Add(new MailAddress(toemail));
                if (ccmail != "")
                {
                    Email.CC.Add(new MailAddress(ccmail));
                }
                if (toemail != OnBehalf && OnBehalf != "")
                {
                    Email.To.Add(new MailAddress(OnBehalf));
                }
                //foreach (var sec in fm.Mst_SecTeam)
                //{
                //    if (sec.Emailid + "" != "")
                //    {
                //        Email.To.Add(new MailAddress(sec.Emailid.ToString()));
                //    }
                //}
                Email.To.Add(new MailAddress("secretarialteam@maricoindia.net"));
               Client.Send(Email);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void sendMail_POARevoked(string POAno, string IniName, string toemail, string ccmail)
        {
            try
            {
                string strHtmlBody;
                Email.Subject = "Revocation requested for POA ID" + POAno;

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Mailer_Format/POARevoked.html")))
                {
                    strHtmlBody = reader.ReadToEnd();
                }
                strHtmlBody = strHtmlBody.Replace("[@POAno]", POAno.ToString()).Replace("[@iniName]", IniName);
                Email.Body = strHtmlBody;
                if (ccmail != "")
                {
                    Email.CC.Add(new MailAddress(ccmail));
                }

                //foreach (var sec in fm.Mst_SecTeam)
                //{
                //    if (sec.Emailid + "" != "")
                //    {
                //        Email.CC.Add(new MailAddress(sec.Emailid.ToString()));
                //    }
                //}
               // Email.CC.Add(new MailAddress("secretarialteam@maricoindia.net"));
                Client.Send(Email);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SendMail_POAClosed(string POANo, string closeddt, string toemail, string ccmail, string OnBehalf)
        {
            try
            {
                string strHtmlBody;
                Email.Subject = "Closure of POA Request " + POANo;

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Mailer_Format/POAClosed.html")))
                {
                    strHtmlBody = reader.ReadToEnd();
                }
                strHtmlBody = strHtmlBody.Replace("[@POAno]", POANo.ToString()).Replace("[@closeddt]", closeddt);
                Email.Body = strHtmlBody;
                if (ccmail != "")
                {
                    Email.To.Add(new MailAddress(ccmail));
                }
                if (ccmail != OnBehalf && OnBehalf != "")
                {
                    Email.To.Add(new MailAddress(OnBehalf));
                }
                //foreach (var sec in fm.Mst_SecTeam)
                //{
                //    if (sec.Emailid + "" != "")
                //    {
                //        Email.To.Add(new MailAddress(sec.Emailid.ToString()));
                //    }
                //}
                Email.To.Add(new MailAddress("secretarialteam@maricoindia.net"));
                Client.Send(Email);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void sendMail_SEC_Rejected(string POAno, string iniName, string toemail, string ccmail, string comments, string OnBehalf)
        {
            try
            {
                string strHtmlBody;
                Email.Subject = "POA Request " + POAno + " has been rejected/ marked for resubmission by Secretarial Team.";// "Rejection of POA Request by Secretarial Team - POA ID" + POAno;
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Mailer_Format/SEC_Rejected.html")))
                {
                    strHtmlBody = reader.ReadToEnd();
                }
                strHtmlBody = strHtmlBody.Replace("[@POAno]", POAno.ToString()).Replace("[@iniName]", iniName).Replace("[@Comments]", comments);
                Email.Body = strHtmlBody;
                Email.To.Add(new MailAddress(toemail));
                if (ccmail != "")
                {
                    Email.CC.Add(new MailAddress(ccmail));
                }
                if (toemail != OnBehalf && OnBehalf != "")
                {
                    Email.To.Add(new MailAddress(OnBehalf));
                }
                //foreach (var sec in fm.Mst_SecTeam)
                //{
                //    if (sec.Emailid + "" != "")
                //    {
                //        Email.CC.Add(new MailAddress(sec.Emailid.ToString()));
                //    }
                //}
               // Email.CC.Add(new MailAddress("secretarialteam@maricoindia.net"));
                Client.Send(Email);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void sendMail_SEC_EXECUTED(string POAno, string iniName, string toemail, string ccmail, string OnBehalf)
        {
            try
            {
                string strHtmlBody;
                Email.Subject = "POA Request " + POAno + " has been processed ";
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Mailer_Format/SEC_EXECUTED.html")))
                {
                    strHtmlBody = reader.ReadToEnd();
                }
                strHtmlBody = strHtmlBody.Replace("[@POAno]", POAno.ToString()).Replace("[@iniName]", iniName);
                Email.Body = strHtmlBody;
                Email.To.Add(new MailAddress(toemail));
                if (ccmail != "")
                {
                    Email.CC.Add(new MailAddress(ccmail));
                }
                if (toemail != OnBehalf && OnBehalf != "")
                {
                    Email.To.Add(new MailAddress(OnBehalf));
                }
                //foreach (var sec in fm.Mst_SecTeam)
                //{
                //    if (sec.Emailid + "" != "")
                //    {
                //        Email.CC.Add(new MailAddress(sec.Emailid.ToString()));
                //    }
                //}
               // Email.CC.Add(new MailAddress("secretarialteam@maricoindia.net"));
               Client.Send(Email);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void sendMail_SEC_approval(string POAno, string iniName, string toemail, string ccmail, string OnBehalf)
        {
            try
            {
                string strHtmlBody;
                Email.Subject = " POA Request " + POAno + " has been confirmed by the Secretarial Team";// "Approval of POA Request by Secretarial Team- POA ID" + POAno;
                Email.To.Add(new MailAddress(toemail));
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Mailer_Format/SEC_approval.html")))
                {
                    strHtmlBody = reader.ReadToEnd();
                }
                strHtmlBody = strHtmlBody.Replace("[@POAno]", POAno.ToString()).Replace("[@iniName]", iniName);
                Email.Body = strHtmlBody;

                if (ccmail != "")
                {
                    Email.CC.Add(new MailAddress(ccmail));
                }
                if (toemail != OnBehalf && OnBehalf != "")
                {
                    Email.To.Add(new MailAddress(OnBehalf));
                }
                //foreach (var sec in fm.Mst_SecTeam)
                //{
                //    if (sec.Emailid + "" != "")
                //    {
                //        Email.CC.Add(new MailAddress(sec.Emailid.ToString()));
                //    }

               // Email.CC.Add(new MailAddress("secretarialteam@maricoindia.net"));
               Client.Send(Email);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void sendMail_PowerReqRaised(string POAid, string toemail, string ccmail, string InitiatorName, string Comments, string OnBehalf)
        {
            try
            {

                string strHtmlBody;
                //Email.Subject = "Request for additional powers : " + POAid;
                Email.Subject = "POAR ID " + POAid + " Request for additional powers";
                if (toemail != "")
                {
                    Email.To.Add(new MailAddress(toemail));
                }
                if (toemail != OnBehalf && OnBehalf != "")
                {
                    Email.To.Add(new MailAddress(OnBehalf));
                }
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("Mailer_Format/PowerReqRaised.html")))
                {
                    strHtmlBody = reader.ReadToEnd();
                }
                strHtmlBody = strHtmlBody.Replace("[@POAid]", POAid.ToString()).Replace("[@IniName]", InitiatorName.ToString()).Replace("[@Comments]", Comments.ToString());
                Email.Body = strHtmlBody;
                //foreach (var sec in fm.Mst_SecTeam)
                //{
                //    if (sec.Emailid + "" != "")
                //    {
                //        Email.CC.Add(new MailAddress(sec.Emailid.ToString()));
                //    }
                //}
               // Email.CC.Add(new MailAddress("secretarialteam@maricoindia.net"));
                if (ccmail != "")
                {
                    Email.CC.Add(new MailAddress(ccmail));
                }

               Client.Send(Email);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void sendMail_PowerReqAdded(string POAid, string IniName, string toemail, string ccmail, string Power, string Role, string OnBehalf)
        {
            try
            {
                string strHtmlBody;
                Email.Subject = "POAR ID " + POAid + " Request for additional powers";
                Email.To.Add(new MailAddress(toemail));
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Mailer_Format/PowerReqAdded.html")))
                {
                    strHtmlBody = reader.ReadToEnd();
                }
                strHtmlBody = strHtmlBody.Replace("[@IniName]", IniName.ToString()).Replace("[@Role]", Role).Replace("[@Power]", Power);
                Email.Body = strHtmlBody;
                //foreach (var sec in fm.Mst_SecTeam)
                //{
                //    if (sec.Emailid + "" != "")
                //    {
                //        Email.CC.Add(new MailAddress(sec.Emailid.ToString()));
                //    }
                //}
                if (toemail != OnBehalf && OnBehalf != "")
                {
                    Email.To.Add(new MailAddress(OnBehalf));
                }
               // Email.CC.Add(new MailAddress("secretarialteam@maricoindia.net"));
                if (ccmail != "")
                {
                    // Email.CC.Add(new MailAddress(ccmail));
                }
                Client.Send(Email);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void sendMail_PowerReqRejected(string POAid, string IniName, string toemail, string ccmail, string comments, string OnBehalf)
        {
            try
            {
                string strHtmlBody;
                Email.Subject = "POAR ID " + POAid + " Request for additional powers";
                Email.To.Add(new MailAddress(toemail));
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Mailer_Format/PowerReqRejected.html")))
                {
                    strHtmlBody = reader.ReadToEnd();
                }
                strHtmlBody = strHtmlBody.Replace("[@IniName]", IniName.ToString()).Replace("[@Comments]", comments.ToString());
                Email.Body = strHtmlBody;
                if (ccmail != "")
                {
                    // Email.CC.Add(new MailAddress(ccmail));
                }
                //foreach (var sec in fm.Mst_SecTeam)
                //{
                //    if (sec.Emailid + "" != "")
                //    {
                //        Email.To.Add(new MailAddress(sec.Emailid.ToString()));
                //    }
                //}
               // Email.CC.Add(new MailAddress("secretarialteam@maricoindia.net"));
                Client.Send(Email);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}