﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FinAuth.DAL
{
    public class clsDAL
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn;
        DataSet ds;
        DataTable dt;
        SqlDataAdapter sda;
        public clsDAL()
        {
            if (ConfigurationManager.ConnectionStrings["Conn"] == null)//When Connection string is NULL
                throw (new NullReferenceException("ConnectionString configuration is missing from you web.config. It should contain  <connectionStrings> <add name=\"ST\" connectionString=\"Data Source=. ;Initial Catalog = ST;Integrated Security=ST\"  </connectionStrings>"));
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
            cmd.Connection = conn;
        }
        private void Open()
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();
        }
        private void Close()
        {
            if (conn.State == ConnectionState.Open)
                conn.Close();
        }
        /// <summary>
        /// This Function is used for INSERT,UPDATE,DELETE operations
        /// </summary>
        /// <param name="CommandText"></param>
        /// <param name="comType"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string CommandText, CommandType comType)
        {
            int i = 0;
            try
            {
                this.Open();
                cmd.CommandText = CommandText.Trim();
                cmd.CommandType = comType;
                cmd.CommandTimeout = 9999999;
                i = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR While ExecuteNonQuery: " + ex.Message);
            }
            finally
            {
                this.Close();
            }
            return i;
        }
        /// <summary>
        /// This Function is used for Returning Value/Data from Database
        /// </summary>
        /// <param name="CommandText"></param>
        /// <returns></returns>
        public object ExecuteScalar(string CommandText)
        {
            object obj = null;
            try
            {
                this.Open();
                cmd.CommandText = CommandText.Trim();
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 9999999;
                obj = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR While ExecuteScalar: " + ex.Message);
            }
            finally
            {
                this.Close();
            }
            return obj;
        }
        /// <summary>
        /// This Function is used for Return DataTable(For Text)  
        /// </summary>
        /// <param name="CommandText"></param>
        /// <returns></returns>
        public DataTable ReturnDataTable(string CommandText)
        {
            try
            {
                this.Open();
                cmd.CommandText = CommandText;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 9999999;
                sda = new SqlDataAdapter(cmd);
                dt = new DataTable();
                sda.Fill(dt);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR While ReturnDataTable: " + ex.Message);
            }
            finally
            {
                this.Close();
            }
            return dt;
        }
        /// <summary>
        /// This Function is used for Return DataTable(For StoreProcedure)  
        /// </summary>
        /// <param name="StoredProcedureName"></param>
        /// <returns></returns>
        public DataTable FillDataTable(string StoredProcedureName)
        {
            try
            {
                this.Open();
                cmd.CommandText = StoredProcedureName;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 9999999;
                sda = new SqlDataAdapter(cmd);
                dt = new DataTable();
                sda.Fill(dt);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR While FillDataTable: " + ex.Message);
            }
            finally
            {
                this.Close();
            }
            return dt;
        }
        /// <summary>
        /// This function is used to get data from database using ExecuteReader
        /// </summary>
        /// <param name="commandText"></param>
        /// <returns></returns>
        public IDataReader ExecuteReader(string commandText)
        {
            IDataReader dr = null;
            try
            {
                this.Open();
                cmd.CommandText = commandText;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 9999999;
                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR While ExecuteReader: " + ex.Message);
            }
            finally
            {
                this.Close();
            }
            return dr;
        }
        /// <summary>
        /// This Function set SqlParameter property
        /// </summary> 
        public IDataParameterCollection Parameters
        {
            get
            {
                return cmd.Parameters;
            }

        }
    }
}