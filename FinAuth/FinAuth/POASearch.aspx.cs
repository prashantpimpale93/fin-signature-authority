﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Telerik.Web.UI;

using System.Net.Mail;
using System.IO;
using FinAuth.BLL;
using System.Data;


namespace FinAuth
{
    public partial class POASearch : System.Web.UI.Page
    {
        POA objpoa = new POA();
        FinAuthEntities1 fm = new FinAuthEntities1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["EmpID"] + "" == "" || Session["UserRole_Id"] + "" == "")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    string EMPID = Session["EmpID"] + "";
                    rcbempId.DataSource = fm.bindEmpname();
                    rcbempId.DataTextField = "EmpID";
                    rcbempId.DataValueField = "EmpID";
                    rcbempId.DataBind();
                    rcbempId.Items.Insert(0, new RadComboBoxItem("-- All --", "0"));
                    rcbempId.SelectedValue = EMPID;
                    rcbEmpname.DataSource = fm.bindEmpname();
                    rcbEmpname.DataTextField = "EmpName";
                    rcbEmpname.DataValueField = "EmpID";
                    rcbEmpname.DataBind();
                    rcbEmpname.Items.Insert(0, new RadComboBoxItem("-- All --", "0"));
                    rcbEmpname.SelectedValue = EMPID;
                    Rgrequest.DataSource = fm.uspBindSerach().ToList().Where(a => a.approveddate != null && (a.status == 8 || (a.status == 9 && a.revoke_eff_date > DateTime.Now)) && a.Initiator_id + "" == rcbempId.SelectedValue).OrderByDescending(s => s.requesteddate);
                    Rgrequest.DataBind();
                }
            }
        }

        protected void btnview_Click(object sender, EventArgs e)
        {
            string EMPID = Session["EmpID"] + "";
            if (rcbempId.SelectedValue == "0")
            {
                if (rcbStatus.SelectedValue == "0")
                {
                    Rgrequest.DataSource = fm.uspBindSerach().ToList().Where(a => a.approveddate != null && (a.status == 8 || (a.status == 9))).OrderByDescending(s => s.requesteddate);
                    Rgrequest.DataBind();
                }
                else if (rcbStatus.SelectedValue == "8")
                {
                    Rgrequest.DataSource = fm.uspBindSerach().ToList().Where(a => a.approveddate != null && (a.status == 8 || (a.status == 9 && a.revoke_eff_date > DateTime.Now)));
                    Rgrequest.DataBind();
                }
                else if (rcbStatus.SelectedValue == "9")
                {
                    Rgrequest.DataSource = fm.uspBindSerach().ToList().Where(a => a.approveddate != null && ((a.status == 9 && a.revoke_eff_date <= DateTime.Now)));
                    Rgrequest.DataBind();
                }
            }
            else
            {
                if (rcbStatus.SelectedValue == "0")
                {
                    Rgrequest.DataSource = fm.uspBindSerach().ToList().Where(a => a.approveddate != null && (a.status == 8 || (a.status == 9)) && a.Initiator_id + "" == rcbempId.SelectedValue);
                    Rgrequest.DataBind();
                }
                else if (rcbStatus.SelectedValue == "8")
                {
                    Rgrequest.DataSource = fm.uspBindSerach().ToList().Where(a => a.approveddate != null && (a.status == 8 || (a.status == 9 && a.revoke_eff_date > DateTime.Now)) && a.Initiator_id + "" == rcbempId.SelectedValue);
                    Rgrequest.DataBind();
                }
                else if (rcbStatus.SelectedValue == "9")
                {
                    Rgrequest.DataSource = fm.uspBindSerach().ToList().Where(a => a.approveddate != null && ((a.status == 9 && a.revoke_eff_date <= DateTime.Now)) && a.Initiator_id + "" == rcbempId.SelectedValue);
                    Rgrequest.DataBind();
                }

            }
        }

        protected void Rgrequest_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            string EMPID = Session["EmpID"] + "";
            if (rcbempId.SelectedValue == "0")
            {
                if (rcbStatus.SelectedValue == "0")
                {
                    Rgrequest.DataSource = fm.uspBindSerach().ToList().Where(a => a.approveddate != null && (a.status == 8 || (a.status == 9)));

                }
                else if (rcbStatus.SelectedValue == "8")
                {
                    Rgrequest.DataSource = fm.uspBindSerach().ToList().Where(a => a.approveddate != null && (a.status == 8 || (a.status == 9 && a.revoke_eff_date > DateTime.Now)));

                }
                else if (rcbStatus.SelectedValue == "9")
                {
                    Rgrequest.DataSource = fm.uspBindSerach().ToList().Where(a => a.approveddate != null && ((a.status == 9 && a.revoke_eff_date <= DateTime.Now)) );

                }

            }
            else
            {
                if (rcbStatus.SelectedValue == "0")
                {
                    Rgrequest.DataSource = fm.uspBindSerach().ToList().Where(a => a.approveddate != null && (a.status == 8 || (a.status == 9)) && a.Initiator_id + "" == rcbempId.SelectedValue);

                }
                else if (rcbStatus.SelectedValue == "8")
                {
                    Rgrequest.DataSource = fm.uspBindSerach().ToList().Where(a => a.approveddate != null && (a.status == 8 || (a.status == 9 && a.revoke_eff_date > DateTime.Now)) && a.Initiator_id + "" == rcbempId.SelectedValue);

                }
                else if (rcbStatus.SelectedValue == "9")
                {
                    Rgrequest.DataSource = fm.uspBindSerach().ToList().Where(a => a.approveddate != null && ((a.status == 9 && a.revoke_eff_date <= DateTime.Now)) && a.Initiator_id + "" == rcbempId.SelectedValue);

                }


            }
        }

        protected void Rgrequest_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            Rgrequest.Rebind();
        }

        protected void rcbempId_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            rcbEmpname.SelectedValue = rcbempId.SelectedValue;
        }

        protected void rcbEmpname_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            rcbempId.SelectedValue = rcbEmpname.SelectedValue;
        }
    }
}