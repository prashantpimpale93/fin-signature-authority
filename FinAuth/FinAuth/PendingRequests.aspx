﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.Master" AutoEventWireup="true" CodeBehind="PendingRequests.aspx.cs" Inherits="FinAuth.PendingRequests"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <telerik:RadCodeBlock ID="code1" runat="server">
        <script>
            function OnClientSelectedIndexChanged(sender, eventArgs) {
                <%-- var item = eventArgs.get_item();
                // alert("You selected " + item.get_text() + ":" + item.get_value());
                var dropdownlist = $find("<%= rcbAccept.ClientID %>");
                var item1 = dropdownlist.findItemByValue(item.get_value());
                item1.select();--%>

                var rcbrole = sender.get_attributes().getAttribute("role");
                var reqvd = sender.get_attributes().getAttribute("reqvd");
                if (sender._value == "A") {
                    // rcbrole.style.display = "block";
                    // $find(rcbrole).set_visible(true);
                    $find(rcbrole)._enabled = true;
                    document.getElementById(reqvd).disabled = false;

                    //  $find(rcbrole).disabled = false;
                } else {
                    // rcbrole.style.display = "none";
                    // $find(rcbrole).set_visible(false);
                    $find(rcbrole)._enabled = false;
                    document.getElementById(reqvd).disabled = true;
                    // $find(rcbrole).disabled = true

                }
            }


            function showrole(rcbAccept, rcbrole) {


                if (rcbAccept.value = "Accept") {
                    rcbrole.style.display = "block";
                } else {
                    rcbrole.style.display = "none";

                }
            }
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheading" runat="server">
    Additional Power Requests
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Web20">
    </telerik:RadAjaxManager>

    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
        <div style='overflow: auto; width: 800px; height: 800px;'>
            <table style="width: 100%" align="center">
                <tr>
                    <td colspan="2">
                        <telerik:RadComboBox ID="rcbview" runat="server" AutoPostBack="true" Label="Filter by:"
                            OnSelectedIndexChanged="rcbView_SelectedIndexChanged">
                            <Items>
                                <telerik:RadComboBoxItem Text="--All--" Value="0" />
                                <telerik:RadComboBoxItem Text="Pending" Value="P" Selected="true" />
                                <telerik:RadComboBoxItem Text="Accepted" Value="A" />
                                <telerik:RadComboBoxItem Text="Rejected" Value="R" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="display: none">
                        <telerik:RadFormDecorator Skin="Web20" ID="FormDecorator1" runat="server" DecoratedControls="All"
                            EnableEmbeddedScripts="true" ControlsToSkip="Zone" />
                        <telerik:RadGrid ID="Rgrequest" runat="server" Width="100%" AllowPaging="false" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                            AllowSorting="True" Skin="Web20" AutoGenerateColumns="false"
                            AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="Rgrequest_NeedDataSource" OnItemDataBound="Rgrequest_ItemDataBound" OnUpdateCommand="Rgrequest_UpdateCommand">

                            <HeaderStyle Font-Bold="True"></HeaderStyle>

                            <ItemStyle Font-Bold="False"></ItemStyle>

                            <PagerStyle PageSizes="2,5,10" AlwaysVisible="true" />
                            <MasterTableView PagerStyle-Mode="NextPrevAndNumeric" NoMasterRecordsText="No POAs Exists on your Name">
                                <RowIndicatorColumn Visible="False">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Created="True">
                                </ExpandCollapseColumn>
                                <Columns>

                                    <telerik:GridTemplateColumn HeaderText="Sr No" UniqueName="POANo" SortExpression="POANo" EditFormHeaderTextFormat="Select :">
                                        <ItemTemplate>
                                            <%# Container.DataSetIndex+1 %>
                                            <asp:Label ID="lblstatus" runat="server" Text='<%#Bind("status") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <telerik:RadComboBox ID="rcbAccept" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="rcbAccept_SelectedIndexChanged">
                                                <Items>
                                                    <telerik:RadComboBoxItem Text="Accept" Value="A" />
                                                    <telerik:RadComboBoxItem Text="Reject" Value="R" />
                                                </Items>
                                            </telerik:RadComboBox>
                                            <telerik:RadComboBox ID="rcbrole" runat="server" Label="Select Role :">
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="rcbrole" InitialValue="-- Select --"
                                                Display="Dynamic" ValidationGroup="vgUpdate">* </asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="PowerReqNo" UniqueName="PowerReqNo" SortExpression="PowerReqNo" ReadOnly="true"
                                        HeaderText="Request No">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Initiator_Name" UniqueName="Initiator_Name" SortExpression="Initiator_Name" ReadOnly="true"
                                        HeaderText="Initiator Name">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ReqDate" UniqueName="ReqDate" SortExpression="ReqDate" ReadOnly="true"
                                        HeaderText="Date of Request" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Power Desc" UniqueName="PowerDesc" SortExpression="PowerDesc" EditFormHeaderTextFormat="Power Desc :">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPowerDesc" runat="server" Text='<%#Bind("PowerDesc") %>'></asp:Label>

                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblRequestorId" runat="server" Text='<%#Bind("RequserId") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblInitiator_id" runat="server" Text='<%#Bind("Initiator_id") %>' Visible="false"></asp:Label>
                                            <asp:TextBox ID="txtPowerDesc" runat="server" Width="300px" MaxLength="500" Text='<%#Bind("PowerDesc") %>' TextMode="MultiLine" Rows="2"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtPowerDesc" Display="Dynamic" ValidationGroup="vgUpdate">* </asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%-- <telerik:GridBoundColumn DataField="ReqReason" UniqueName="ReqReason" SortExpression="ReqReason" ReadOnly="true"
                                HeaderText="ReqReason">
                            </telerik:GridBoundColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="Reason" UniqueName="ReqReason" SortExpression="ReqReason" EditFormHeaderTextFormat="Approval/Rejection Comments :">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReqReason" runat="server" Text='<%#Bind("ReqReason") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblPowerReqId" runat="server" Text='<%#Bind("PowerReqId") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPowerReqNO" runat="server" Text='<%#Bind("PowerReqNO") %>' Visible="false"></asp:Label>
                                            <asp:TextBox ID="txtremarks" runat="server" Width="300px" MaxLength="500" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtremarks" Display="Dynamic" ValidationGroup="vgUpdate">* </asp:RequiredFieldValidator>
                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="vgUpdate" ShowMessageBox="true" ShowSummary="false" />
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="remarks" UniqueName="remarks" SortExpression="remarks" ReadOnly="true"
                                        HeaderText="Sec. Team Remarks">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Role" UniqueName="Role" SortExpression="Role" ReadOnly="true"
                                        HeaderText="Role">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn>
                                        <ItemTemplate>
                                            <asp:Button runat="server" Text="SUBMIT" UniqueName="Accept" ID="btnEdit" CommandName="Edit" />
                                            <asp:Label ID="lblstatusPOA" runat="server" Text="" Font-Bold="true"></asp:Label>


                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--  <telerik:GridEditCommandColumn ButtonType="LinkButton" EditText="Accept/Reject" UniqueName="Accept" />
                            <%--<telerik:GridEditCommandColumn ButtonType="LinkButton" EditText="Reject" UniqueName="Reject" />--%>
                                </Columns>

                                <PagerStyle AlwaysVisible="True"></PagerStyle>
                            </MasterTableView>
                        </telerik:RadGrid>


                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadListView Skin="Vista" ID="lvRequest" Width="80%" runat="server" OnItemCommand="lvRequest_ItemCommand"
                            OnNeedDataSource="lvRequest_NeedDataSource" DataKeyNames="POANo" OnItemDataBound="lvRequest_ItemDataBound">
                            <ItemTemplate>
                                <br />
                                <table align="center" width="100%">
                                    <tr>
                                        <td>
                                            <fieldset>
                                                <table align="center">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:Label ID="lblRequestNo" runat="server" Text="Request No"></asp:Label>
                                                        </td>
                                                        <td></td>
                                                        <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                         <asp:Label ID="lblstatus" runat="server" Text='<%#Bind("status") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPowerReqId" runat="server" Text='<%#Bind("PowerReqId") %>' Visible="false" />
                                                            <asp:Label ID="lblPowerReqNO" runat="server" Font-Bold="true" Text='<%#Bind("PowerReqNo") %>'></asp:Label>
                                                        </td>
                                                        <%-- </tr>
                                                <tr>--%>
                                                        <td align="left">
                                                            <asp:Label ID="lblInitiatorName" runat="server" Text="Initiator Name"></asp:Label>
                                                        </td>
                                                        <td></td>
                                                        <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:Label ID="lblInitiator" runat="server" Font-Bold="true" Text='<%#Bind("Initiator_Name") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="white-space: nowrap">
                                                            <asp:Label ID="lblDateRequest" runat="server" Text="Date of Request"></asp:Label>
                                                        </td>
                                                        <td></td>
                                                        <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:Label ID="lblDateReq" runat="server" Font-Bold="true" Text='<%#Bind("ReqDate","{0:dd-MM-yyyy}") %>'></asp:Label>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="white-space: nowrap">
                                                            <asp:Label ID="lblReqReason" runat="server" Text="Request Reason"></asp:Label>
                                                        </td>
                                                        <td></td>
                                                        <td align="left" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:Label ID="lblReqReas" runat="server" Font-Bold="true" Text='<%#Bind("ReqReason") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="white-space: nowrap">
                                                            <asp:Label ID="lblselect" runat="server" Text="Select Accept/Reject"></asp:Label>
                                                        </td>
                                                        <td></td>
                                                        <td align="left" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <telerik:RadComboBox ID="rcbAccept" Font-Bold="true" runat="server" OnSelectedIndexChanged="rcbAccept_SelectedIndexChanged"
                                                            AutoPostBack="true">
                                                            <Items>
                                                                <telerik:RadComboBoxItem Text="Accept" Value="A" />
                                                                <telerik:RadComboBoxItem Text="Reject" Value="R" />
                                                            </Items>
                                                        </telerik:RadComboBox>


                                                        </td>
                                                    </tr>
                                                    <asp:Panel ID="pnlHide" runat="server">
                                                        <tr>
                                                            <td align="left" style="white-space: nowrap">
                                                                <asp:Label ID="lblSelectRole" runat="server" Text="Select Role"></asp:Label>
                                                            </td>
                                                            <td></td>
                                                            <td align="left" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <telerik:RadComboBox ID="rcbrole" Font-Bold="true" runat="server">
                                                        </telerik:RadComboBox>
                                                                <asp:Label ID="lblrole" runat="server" Font-Bold="true" Visible="false" Text='<%#Bind("Roleid") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <%--  <tr>
                                        <td align="left">
                                            <asp:Label ID="lblPowerDes" runat="server" Text="Power Desc"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblPowerD" runat="server" Text='<%#Bind("PowerDesc") %>'></asp:Label>
                                        </td>
                                    </tr>--%>
                                                        <tr>
                                                            <td align="left" style="white-space: nowrap">
                                                                <asp:Label ID="lblPowertxt" runat="server" Text="Requested Power"></asp:Label>
                                                            </td>
                                                            <td></td>
                                                            <td align="left" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                       <asp:Label ID="lblPowertxtDesc" runat="server" Font-Bold="true" Text='<%#Bind("PowerDesc") %>'></asp:Label>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="white-space: nowrap">
                                                                <asp:Label ID="lblPower" runat="server" Text="Power Description"></asp:Label>
                                                            </td>
                                                            <td></td>
                                                            <td align="left" colspan="4">
                                                                <%--   <asp:Label ID="lblPowerD" runat="server" Text='<%#Bind("PowerDesc") %>'></asp:Label>--%>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                          <asp:Label ID="lblRequestorId" runat="server" Text='<%#Bind("RequserId") %>' Visible="false" />
                                                                <asp:Label ID="lblRequestedPowerDesc" runat="server" Text='<%#Bind("RequestedPowerDesc") %>' Visible="false" />
                                                                <asp:TextBox ID="txtPowerDesc" runat="server" Width="300px" MaxLength="500"
                                                                    TextMode="MultiLine" Rows="2"></asp:TextBox>

                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td align="left" style="white-space: nowrap">
                                                                <asp:Label ID="lblSecTeams" runat="server" Text="Sec.Team Remarks"></asp:Label>
                                                            </td>
                                                            <td></td>
                                                            <td align="left" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:TextBox ID="txtremarks" runat="server" Width="300px" MaxLength="500" TextMode="MultiLine"
                                                            Rows="2" />
                                                                <asp:Label ID="lblRemarks" runat="server" Text='<%#Bind("remarks") %>' Visible="false" />
                                                            </td>
                                                        </tr>
                                                        <%--  <tr>
                                        <td align="left">
                                            <asp:Label ID="lblRole1" runat="server" Text="Role"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblrole3" runat="server" Text='<%#Bind("Role") %>'></asp:Label>
                                        </td>
                                    </tr>--%>

                                                        <tr>

                                                            <td align="left" colspan="6">
                                                                <asp:Button runat="server" Text="SUBMIT" UniqueName="Accept"
                                                                    ID="btnEdit" CommandName="save" />
                                                                <asp:Button runat="server" Text="RESET" UniqueName="Reset"
                                                                    ID="btnReset" CommandName="Reset" />
                                                                <asp:Label ID="lblstatusPOA" runat="server" Text="" Font-Underline="true" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblAcc" runat="server" Font-Bold="true" Visible="false" Text="Accepted"></asp:Label>
                                                                <asp:Label ID="lblRej" runat="server" Font-Bold="true" Visible="false" Text="Rejected"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </asp:Panel>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </telerik:RadListView>
                    </td>
                </tr>
                <%--   <asp:Repeater ID="rptRequst" runat="server" >
            <ItemTemplate>
                <asp:Table ID="tbl" runat="server" width="100%">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                        <%# Container.ItemIndex+1 %>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Label ID="lblPOARNo" runat="server" Text='<%#Bind("PowerReqNO") %>' Font-Bold="true"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            Status :  
                            <asp:Label ID="lblstatusPOA" runat="server" Text='<%#Bind("PowerReqNO") %>' Font-Bold="true"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            Role :    
                            <asp:Label ID="Label1" runat="server" Text='<%#Bind("Role") %>' Font-Bold="true"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            Power :    
                            <asp:Label ID="Label2" runat="server" Text='<%#Bind("PowerDesc") %>' Font-Bold="true"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

            </ItemTemplate>
        </asp:Repeater>--%>
            </table>
        </div>
    </telerik:RadAjaxPanel>
</asp:Content>
