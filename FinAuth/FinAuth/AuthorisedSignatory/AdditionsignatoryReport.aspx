﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AS.Master" AutoEventWireup="true" CodeBehind="AdditionsignatoryReport.aspx.cs" Inherits="FinAuth.AuthorisedSignatory.AdditionsignatoryReport" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheading" runat="server">
    Addition Signatory Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RAM" runat="server" SkinID="Web20">
    </telerik:RadAjaxManager>
    <table style="width: 95%; line-height: 15px;" align="center">
        <tr>
            <td style="text-align: left;">
                <div style="overflow: scroll; height: 650px; width: 950px; border: 1px solid red;">
                    <telerik:RadGrid ID="rgAdditionSignatoriesReport" runat="server" Width="100%" AllowPaging="false" PageSize="5" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                        AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="rgAdditionSignatoriesReport_NeedDataSource">

                        <MasterTableView DataKeyNames="AdditionID,Treasury">
                            <RowIndicatorColumn Visible="False">
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Created="True">
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="Sr No.">
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex+1 %>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="AdditionID" UniqueName="AdditionID" SortExpression="AdditionID"
                                    HeaderText="Addition ID">
                                </telerik:GridBoundColumn>
                                 
                                <telerik:GridBoundColumn DataField="RequiredFor" UniqueName="RequiredFor" SortExpression="RequiredFor"
                                    HeaderText="Required Type">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Location" UniqueName="Location" SortExpression="Location"
                                    HeaderText="Location">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LocationDetails" UniqueName="LocationDetails" SortExpression="LocationDetails"
                                    HeaderText="Location Type">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AccountNO" UniqueName="AccountNO" SortExpression="AccountNO"
                                    HeaderText="Account NO">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="BankName" UniqueName="BankName" SortExpression="BankName"
                                    HeaderText="Bank Name">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="BankAddress" UniqueName="BankAddress" SortExpression="BankAddress"
                                    HeaderText="Bank Address">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ReasonDetails" UniqueName="ReasonDetails" SortExpression="ReasonDetails"
                                    HeaderText="Reason Details">
                                </telerik:GridBoundColumn>
                               
                                <telerik:GridBoundColumn DataField="EmpName" UniqueName="EmpName" SortExpression="EmpName"
                                    HeaderText="Created By">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CreatedDate" DataFormatString="{0:dd/MM/yyyy}" UniqueName="CreatedDate" SortExpression="CreatedDate"
                                    HeaderText="Created On">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Download"
                                    UniqueName="Download">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDownload" OnClick="lnkDownload_Click" Font-Bold="true" ToolTip='<%# Eval("AdditionID") %>' 
                                            runat="server" Text="Download" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                 <telerik:GridBoundColumn DataField="Comments" UniqueName="Comments" SortExpression="Comments"
                                    HeaderText="Initiator Comments">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </div>
            </td>
        </tr>
    </table>
    <table>

        <tr>

            <td>

                <rsweb:ReportViewer ID="rptViewer" runat="server" Font-Names="Verdana" Font-Size="8pt"
                    InteractiveDeviceInfos="(Collection)" ProcessingMode="Remote" Style="margin-top: 0px"
                    WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px" Height="1150px"
                    ShowToolBar="false" ShowParameterPrompts="false">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>
