﻿using FinAuth.BAL;
using FinAuth.BO;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;


namespace FinAuth.AuthorisedSignatory
{
    public partial class DeletionsignatoryReport : System.Web.UI.Page
    {
        clsBAL objBAL = new clsBAL();
        clsBO objBO = new clsBO();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void rgAdditionSignatoriesReport_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DataTable dt = new DataTable();
            dt = objBAL.getDeletionReportDetails();
            if (dt.Rows.Count > 0)
            {
                rgAdditionSignatoriesReport.DataSource = dt;
            }
            else
            {
                rgAdditionSignatoriesReport.DataSource = dt;
            }
        }

        public void ReportBind(string AdditionID)
        {
            try
            {

                string Path1 = "";
                string Id = "";
                string FileName = "";
                rptViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                rptViewer.ServerReport.ReportServerUrl = new Uri("http://mlsqlprod01/reportserver");
                while ((rptViewer.ServerReport.IsDrillthroughReport))
                {
                    rptViewer.PerformBack();
                }
                if (AdditionID.Contains("A"))
                {
                    Path1 = "/FinAuthNew/AdditionSignatory";
                    Id = "AdditionID";
                    FileName = "AdditionSignatory";
                }
                else if (AdditionID.Contains("D"))
                {
                    Path1 = "/FinAuthNew/DeletionSignatory";
                    Id = "DeletionID";
                    FileName = "DeletionSignatory";
                }
                //   rptViewer.ServerReport.ReportPath = "/FinAuthNew/DeletionSignatory";


                rptViewer.ServerReport.ReportPath = Path1;
                //Set Parameter value 

                rptViewer.ShowParameterPrompts = false;

                ReportParameter[] rpParameter = new ReportParameter[1];


                rpParameter[0] = new ReportParameter(Id, AdditionID);

                rptViewer.ServerReport.SetParameters(rpParameter);

                if (AdditionID == "")
                {
                    rptViewer.ShowParameterPrompts = true;
                }
                else
                {
                    rptViewer.ShowParameterPrompts = false;
                }
                rptViewer.ShowPrintButton = true;
                rptViewer.ShowZoomControl = true;

                rptViewer.ShowPrintButton = true;
                rptViewer.ServerReport.Refresh();
                List<ReportParameter> list = new List<ReportParameter>();
                list.Add(new ReportParameter(Id, AdditionID));
                rptViewer.ServerReport.SetParameters(list);
                byte[] data = rptViewer.ServerReport.Render("WORD");
                Response.Clear();
                Response.ContentType = "application/msword";
                Response.BinaryWrite(data);
                MemoryStream ms = new MemoryStream(data);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "_" + AdditionID + ".doc");
                Response.Buffer = true;
                ms.WriteTo(Response.OutputStream);
                // Response.End();
            }
            catch (Exception ex)
            {
                RAM.Alert("ERROR GENERATING PDF - " + ex.ToString());
            }

        }

        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            LinkButton lnkCP = (LinkButton)sender;
            string DeletionID = lnkCP.ToolTip;
            ReportBind(DeletionID);
        }

    }
}