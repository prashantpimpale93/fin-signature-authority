﻿using System;
using FinAuth.BAL;
using FinAuth.BO;
using System.Data;
using Telerik.Web.UI;
using System.Configuration;

namespace FinAuth.AuthorisedSignatory
{
    public partial class ApproveDeletionSignatories : System.Web.UI.Page
    {
        SendASmail objMail = new SendASmail();
        clsBAL objBAL = new clsBAL();
        clsBO objBO = new clsBO();
        bool approver = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //ReportBind("DS00000005");
                // return;
                if (!string.IsNullOrEmpty(Request.QueryString["DeletionID"]))
                {

                    FillDetails();
                    if (Request.QueryString["Request"].ToString() == "View")
                    {
                        btnApprove.Visible = false;
                        btnReject.Visible = false;
                        tr_apprej.Visible = false;
                        pnlAttach.Visible = false;
                        pnlFirst.Visible = false;
                    }
                    else
                    {
                        btnApprove.Visible = true;
                        //btnReject.Visible = true;
                        tr_apprej.Visible = true;
                    }
                    //akshay for any TR team can upload final doc
                    if (Request.QueryString["Request"].ToString() != "View")
                    {
                        if (lbltreasuryType.Text == "S")
                        {
                            string trapprover = ConfigurationManager.AppSettings["Approver"].ToString();
                            string[] approver1 = trapprover.Split(',');
                            foreach (var appid in approver1)
                            {
                                if (Session["EmpID"].ToString() == appid || approver == true)
                                {
                                    pnlAttach.Visible = true;
                                    btnReject.Visible = false;
                                    btnApprove.Visible = true;
                                    tr_apprej.Visible = true;
                                    break;
                                }
                                //else
                                //{
                                //    btnApprove.Visible = false;
                                //    btnReject.Visible = false;
                                //    tr_apprej.Visible = false;
                                //    pnlAttach.Visible = false;
                                //    pnlFirst.Visible = false;
                                //}

                            }

                        }
                    }
                }
            }
        }
        private void FillDetails()
        {
            DataTable dt = new DataTable();
            dt = objBAL.returnAllDeletionDetails(Request.QueryString["DeletionID"]);
            if (dt.Rows.Count > 0)
            {
                lblLocation.Text = dt.Rows[0]["Location"].ToString();
                lblLocationDetails.Text = dt.Rows[0]["LocationDetails"].ToString();
                lblAccountNo.Text = dt.Rows[0]["AccountNO"].ToString();
                lblEmpIDCreatedBy.Text = dt.Rows[0]["CreatedBy"].ToString();
                lblCreatedBy.Text = dt.Rows[0]["EmpName"].ToString();
                lblRequestID.Text = dt.Rows[0]["DeletionID"].ToString();
                if ((!string.IsNullOrEmpty(dt.Rows[0]["Date"].ToString()) && (!string.IsNullOrEmpty(dt.Rows[0]["Name"].ToString()))))
                {
                    pnlFirst.Visible = true;
                    rdDate.Enabled = false;
                    rdDate.SelectedDate = Convert.ToDateTime(dt.Rows[0]["Date"].ToString());
                    txtName.ReadOnly = true;
                    txtName.Text = dt.Rows[0]["Name"].ToString();
                }
                DataTable dtAcc = objBAL.getAccountDetails(lblAccountNo.Text);
                if (dtAcc.Rows.Count > 0)
                {
                    lblBankName.Text = dtAcc.Rows[0]["BankName"].ToString();
                    lblBankaddress.Text = dtAcc.Rows[0]["BankAddress"].ToString();
                }
                lblAdditionDetails.Text = dt.Rows[0]["ReasonDetails"].ToString();
                lblComments.Text = dt.Rows[0]["Comments"].ToString();
                bindExtSign(lblAccountNo.Text);
                DataTable dTemp = new DataTable();
                if (lblLocation.Text == "HO")
                {
                    dTemp = objBAL.getApproverTypeHO("HO", Session["EmpID"].ToString());
                }
                else
                {
                    dTemp = objBAL.getApproverType(lblLocation.Text, lblLocationDetails.Text, Session["EmpID"].ToString());
                }

                if (dTemp.Rows.Count > 0)
                {
                    if (dTemp.Rows[0]["Approvertype"].ToString() == "TR")
                    {
                        lblApproverType.Text = dTemp.Rows[0]["Approvertype"].ToString();
                        if (dt.Rows[0]["Treasury"].ToString() == "F")
                        {
                            pnlFirst.Visible = true;
                            lbltreasuryType.Text = "F";
                        }
                        else if (dt.Rows[0]["Treasury"].ToString() == "S")
                        {
                            pnlAttach.Visible = true;
                            lbltreasuryType.Text = "S";
                            btnReject.Visible = false;
                        }
                    }
                }
                //akshay for TR team approval
                if (CheckTRMember() || dTemp.Rows.Count > 0)
                {
                    approver = true;
                    lblApproverType.Text = "TR";
                    if (dt.Rows[0]["Treasury"].ToString() == "F")
                    {
                        pnlFirst.Visible = true;
                        lbltreasuryType.Text = "F";
                    }
                    else if (dt.Rows[0]["Treasury"].ToString() == "S")
                    {
                        pnlAttach.Visible = true;
                        lbltreasuryType.Text = "S";
                        btnReject.Visible = false;
                    }
                }
            }
        }
        private bool CheckTRMember()
        {
            string trapprover = ConfigurationManager.AppSettings["Approver"].ToString();
            string[] approver = trapprover.Split(',');
            foreach (var appid in approver)
            {
                if (Session["EmpID"].ToString() == appid)
                {
                    return true;

                }

            }
            return false;
        }
        private void bindExtSign(string stracccNo)
        {
            string DeletionID = Request.QueryString["DeletionID"];
            DataTable dtEx = new DataTable();
            dtEx = objBAL.getExdetails(DeletionID);
            if (dtEx.Rows.Count > 0)
            {
                rgExistingSignatories.DataSource = dtEx;
                rgExistingSignatories.DataBind();
            }
            else
            {
                rgExistingSignatories.DataSource = dtEx;
                rgExistingSignatories.DataBind();
            }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (radupload.UploadedFiles.Count > 0)
                {
                    foreach (UploadedFile file in radupload.UploadedFiles)
                    {
                        // string path = Server.MapPath("AuthorisedSignatory/Uploads/");
                        string savedfilename = "AS_DEL_" + DateTime.Now.ToString("dd_MMM_yyyy_hh_mm") + file.FileName;

                        file.SaveAs(Server.MapPath("~/AuthorisedSignatory/Uploads/" + savedfilename));
                        objBO.OriginalFname = file.FileName;
                        objBO.ModifiedFname = savedfilename;
                    }
                }
                else
                {
                    objBO.OriginalFname = string.Empty;
                    objBO.ModifiedFname = string.Empty;
                }

                DataTable dtApp = new DataTable();
                int k = 0;
                string AdditionID = Request.QueryString["DeletionID"];
                int seqNo = Convert.ToInt32(Request.QueryString["SeqNo"]);
                int NextseqNo = Convert.ToInt32(Request.QueryString["SeqNo"]) + 1;
                k = objBAL.updateAppFlowAddition(AdditionID, Session["EmpID"].ToString(), txtAppRejComm.Text, seqNo, "DELETION");
                //akshay for Tr approval
                if (k == 0)
                {
                    if (lblLocationDetails.Text == "HO")
                    {
                        k = objBAL.updateAppFlowAdditionTRHO(AdditionID, Session["EmpID"].ToString(), txtAppRejComm.Text, seqNo, "DELETION");
                    }
                    else
                    {
                        k = objBAL.updateAppFlowAdditionTROther(AdditionID, Session["EmpID"].ToString(), txtAppRejComm.Text, seqNo, "DELETION");
                    }

                }
                if (k > 0)
                {
                    DataTable dtTemp = objBAL.getSupStatusDeletion(AdditionID);
                    if (dtTemp.Rows.Count > 0)
                    {
                        if (dtTemp.Rows[0]["Supervisor"].ToString() == "S")
                        {
                            dtApp = objBAL.getFirstAppAddition(AdditionID, lblLocation.Text, lblLocationDetails.Text);
                            objMail.SendNextApproveMailDeletion(Session["EmpID"].ToString(), lblAccountNo.Text, AdditionID, lblLocation.Text, lblLocationDetails.Text, lblEmpIDCreatedBy.Text, txtAppRejComm.Text);
                            int N = objBAL.updateSupervisorStatusDeletion(AdditionID, "A");
                        }
                        else
                        {
                            if (lblLocation.Text == "HO")
                            {
                                if (lblApproverType.Text != "TR")
                                {
                                    dtApp = objBAL.getHOApprover("HO");
                                    objMail.SendTRMailDeletion(Session["EmpID"].ToString(), lblAccountNo.Text, AdditionID, lblLocation.Text, lblLocationDetails.Text, lblEmpIDCreatedBy.Text, txtAppRejComm.Text);
                                }
                            }
                            else
                            {
                                if (lblApproverType.Text != "TR")
                                {
                                    objMail.SendTRMailDeletion(Session["EmpID"].ToString(), lblAccountNo.Text, AdditionID, lblLocation.Text, lblLocationDetails.Text, lblEmpIDCreatedBy.Text, txtAppRejComm.Text);
                                }
                                dtApp = objBAL.getNextApproverAddition(lblLocation.Text, lblLocationDetails.Text, Session["EmpID"].ToString());
                            }
                        }
                    }
                    else
                    {
                        dtApp = objBAL.getNextApproverAddition(lblLocation.Text, lblLocationDetails.Text, Session["EmpID"].ToString());
                    }
                    if (dtApp.Rows.Count > 0)
                    {
                        if (dtApp.Rows[0]["Approvertype"].ToString() == "TR")
                        {
                            int S = objBAL.updateTreasuryStatusDeletion(AdditionID);
                        }
                        int M = objBAL.insertNextApprovalFlowAddition(AdditionID, dtApp.Rows[0]["ApproverID"].ToString(), lblEmpIDCreatedBy.Text, "DELETION", NextseqNo);
                        if (M > 0)
                        {
                            txtAppRejComm.Text = string.Empty;
                            Response.Redirect("Message.aspx?Msg=Request ID " + AdditionID + " has been Approved Successfully.&ID=N&Type=N");

                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(lblApproverType.Text))
                        {
                            if (lblApproverType.Text == "TR")
                            {
                                objBO.AdditionID = AdditionID;
                                objBO.Name = txtName.Text;
                                objBO.Comments = txtAppRejComm.Text;
                                objBO.TreasuryType = lbltreasuryType.Text;
                                objBO.Date = Convert.ToDateTime(rdDate.SelectedDate).ToString("dd-MMM-yyyy");
                                if (objBO.Date == "01-Jan-0001")
                                {
                                    objBO.Date = "";
                                }
                                objBO.ApproverID = Session["EmpID"].ToString();
                                objBO.InitiatorID = lblEmpIDCreatedBy.Text;
                                objBO.NextSeq = NextseqNo;
                                int s = objBAL.UpdatetreasuryDetailsDeletion(objBO);
                            }
                            if (lbltreasuryType.Text == "S")
                            {
                                DataTable dtEx = new DataTable();
                                dtEx = objBAL.getExdetails(AdditionID);
                                if (dtEx.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dtEx.Rows.Count; i++)
                                    {
                                        objBO.DeletionID = AdditionID;
                                        objBO.EmpID = dtEx.Rows[i]["EmpID"].ToString();
                                        objBO.RequiredFor = dtEx.Rows[i]["Requiredtype"].ToString();
                                        objBO.AccountNO = lblAccountNo.Text;
                                        objBO.Bank = lblBankName.Text;
                                        objBO.BankAddress = lblBankaddress.Text;
                                        objBO.LocationDetails = lblLocationDetails.Text;
                                        objBO.Type = dtEx.Rows[i]["Type"].ToString();
                                        int N = objBAL.DeleteFinalBankDetails(objBO);
                                    }
                                }
                            }
                            objMail.SendTRapproveMailDeletion(Session["EmpID"].ToString(), lblAccountNo.Text, AdditionID, lblLocation.Text, lblLocationDetails.Text, lblEmpIDCreatedBy.Text, txtAppRejComm.Text);
                            txtAppRejComm.Text = string.Empty;
                            txtName.Text = string.Empty;
                            rdDate.SelectedDate = null;
                            Response.Redirect("Message.aspx?Msg=Request ID " + AdditionID + " has been Approved Successfully.&ID=" + AdditionID + "&Type=" + lbltreasuryType.Text);

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                RAM.Alert("ERROR-" + ex.ToString());
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtAppRejComm.Text))
                {
                    RAM.Alert("Please Enter Comment");
                    return;
                }
                int k = 0;
                string AdditionID = Request.QueryString["DeletionID"];
                int seqNo = Convert.ToInt32(Request.QueryString["SeqNo"]);
                int NextseqNo = Convert.ToInt32(Request.QueryString["SeqNo"]) + 1;
                k = objBAL.updateRejAppFlowAddition(AdditionID, Session["EmpID"].ToString(), txtAppRejComm.Text, seqNo, "ADDITION");
                if (k > 0)
                {
                    int N = 0;
                    N = objBAL.InsertRejAppFlowAddition(AdditionID, lblEmpIDCreatedBy.Text, "DELETION", NextseqNo);
                    if (N > 0)
                    {
                        txtAppRejComm.Text = string.Empty;
                        txtName.Text = string.Empty;
                        rdDate.SelectedDate = null;
                        objMail.RejectMailDeletion(AdditionID, Session["EmpID"].ToString(), lblEmpIDCreatedBy.Text, lblLocation.Text, lblLocationDetails.Text);
                        Response.Redirect("Message.aspx?Msg=Request ID " + AdditionID + " has been Rejected Successfully.&ID=N&Type=N");

                    }
                }
            }
            catch (Exception ex)
            {
                RAM.Alert("ERROR - " + ex.ToString());
            }
        }
        protected void rgComments_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            string DeletionID = Request.QueryString["DeletionID"];
            DataTable dt = new DataTable();
            dt = objBAL.getApproverComments(DeletionID);
            if (dt.Rows.Count > 0)
            {
                pnlCommnts.Visible = true;
                rgComments.DataSource = dt;
            }
            else
            {
                pnlCommnts.Visible = false;
                rgComments.DataSource = dt;
            }
        }
    }
}