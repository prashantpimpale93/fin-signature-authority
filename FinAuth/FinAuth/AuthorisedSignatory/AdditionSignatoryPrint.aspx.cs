﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;


namespace FinAuth.AuthorisedSignatory
{
    public partial class AdditionSignatoryPrint : System.Web.UI.Page
    {

                protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ////try
                ////{
                ////    // ScriptManager sm = ScriptManager.GetCurrent(Page);
                // sm.RegisterScriptControl(rptViewer);

               
                string GID = Request.QueryString["ID"]; //"PH06/2014/000001";
                if (GID != "" && GID != null)
                {
                    //Response.Redirect("http://mlsqlprod01/ReportServer?%2fFinAuthNew%2fAdditionSignatory&rs:Command=Render&rs:Format=PDF&AdditionID=" + GID);
                  //  return;
                    ReportBind();
                }
                else
                {
                    ReportBind();
                }
                //Thread.Sleep(2000);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Print", "printReport();", true);
                // Response.Write("function myFunction(){window.print();}");
                //}

                //catch (Exception ex)
                //{
                //    ex.Message.ToString();
                //}
            }

        }
        public void ReportBind()
        {

            rptViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;

            rptViewer.ServerReport.ReportServerUrl = new Uri("http://mlsqlprod01/reportserver");


            while ((rptViewer.ServerReport.IsDrillthroughReport))
            {
                rptViewer.PerformBack();

            }

            rptViewer.ServerReport.ReportPath = "/FinAuthNew/AdditionSignatory";

            //Set Parameter value 

            rptViewer.ShowParameterPrompts = false;

            ReportParameter[] rpParameter = new ReportParameter[1];
            string GID = Request.QueryString["ID"]; //"PH06/2014/000001";
            //  string GID = "PH06/2014/0000000016";
            if (GID != "" && GID != null)
            {
                // ddlGatePassNo.FindItemByValue(GID.ToString()).Selected = true;
            }

            rpParameter[0] = new ReportParameter("AdditionID", Request.QueryString["ID"]);




            rptViewer.ServerReport.SetParameters(rpParameter);

            if (GID == "")
            {
                rptViewer.ShowParameterPrompts = true;
            }
            else
            {
                rptViewer.ShowParameterPrompts = false;
            }
            rptViewer.ShowPrintButton = true;
            rptViewer.ShowZoomControl = true;

            rptViewer.ShowPrintButton = true;
            // rptViewer.Reset();
            rptViewer.ServerReport.Refresh();

          //  IReportServerCredentials irsc = DBConnection.NetworkCredentials();
           // rptViewer.ServerReport.ReportServerCredentials = irsc;

            List<ReportParameter> list = new List<ReportParameter>();
            list.Add(new ReportParameter("AdditionID", Request.QueryString["ID"]));
            rptViewer.ServerReport.SetParameters(list);
            byte[] data = rptViewer.ServerReport.Render("pdf");
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "inline;filename=Manifest_Print.pdf");
            Response.BinaryWrite(data);


            //System.IO.StringWriter sw = new System.IO.StringWriter();
            //HtmlTextWriter htw = new HtmlTextWriter(sw);
            //// Response.AddHeader("content-disposition", "attachment; filename=" + strFileName);
            //Response.ClearContent();

            ////Response.AddHeader("content-disposition", attachment);

            //rptViewer.RenderControl(htw);
            //Response.Write(sw.ToString());
            //Response.Write("(function(){window.print();})();");
            //Response.Flush();
            //Response.End();



        }
        public void ss()
        {

            Response.Clear();

            Response.Buffer = true;
            // Response.ContentType = "application/vnd.ms-excel"

            Response.ContentType = "text/html";

            Response.Charset = "";

            System.IO.StringWriter oStringWriter = new System.IO.StringWriter();

            System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);

            oHtmlTextWriter.Write("<html><head>");

            oHtmlTextWriter.Write("<style>");

            oHtmlTextWriter.Write(".dataGridHeader{font-weight: bold;font-size:8pt;color: black;font-family: Verdana;background-color: #ddddee;text-align:left;}");

            oHtmlTextWriter.Write(".dataGridItemStyle{font-size: 8pt;color: black;font-family: Verdana;text-align: left;}");

            oHtmlTextWriter.Write("</style>");

            oHtmlTextWriter.Write("</head><body>");

            oHtmlTextWriter.WriteBeginTag("form runat=server ");

            oHtmlTextWriter.WriteAttribute("target", "_blank");

            oHtmlTextWriter.Write(">");

            rptViewer.RenderControl(oHtmlTextWriter);



            oHtmlTextWriter.Write("</form></body></html>");

            Response.Write(oStringWriter.ToString());

            Response.End();
        }
    
    }
}