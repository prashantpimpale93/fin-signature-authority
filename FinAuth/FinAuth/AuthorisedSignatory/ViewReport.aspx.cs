﻿using FinAuth.BAL;
using FinAuth.BO;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FinAuth.AuthorisedSignatory
{
    public partial class ViewReport : System.Web.UI.Page
    {
        clsBAL objBAL = new clsBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillDropdown();
                rgAdditionSignatoriesReport.Visible = false;
            }

        }
        private void FillDropdown()
        {

            DataTable dtbank = objBAL.GetReportBanklist();
            rcmbBank.DataSource = dtbank;
            rcmbBank.DataTextField = "BankName";
            rcmbBank.DataValueField = "BankName";
            rcmbBank.DataBind();
            rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--All Bank--", "0"));
            rcmbAccountno.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--All Account No--", "0"));
        }

        protected void rcmbBank_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataTable dTemp = objBAL.getAccountNos(rcmbBank.SelectedItem.Text);
            if (dTemp.Rows.Count > 0)
            {
                rcmbAccountno.DataSource = dTemp;
                rcmbAccountno.DataTextField = "AccountNo";
                rcmbAccountno.DataValueField = "AccountNo";
                rcmbAccountno.DataBind();
                rcmbAccountno.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--All Account No--", "0"));
            }
            else
            {
                rcmbAccountno.Items.Clear();
                rcmbAccountno.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--All Account No--", "0"));
            }
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            if (rcmbRequest.SelectedIndex == 0)
            {
                RAM.Alert("Please Select Signatory Type");
                return;
            }
            DataTable dt = new DataTable();
            dt = objBAL.GetViewReport( rcmbBank.SelectedValue, rcmbAccountno.SelectedValue, rcmbtype.SelectedValue,rcmbRequest.SelectedValue);
            rgAdditionSignatoriesReport.DataSource = dt;
            rgAdditionSignatoriesReport.DataBind();
            rgAdditionSignatoriesReport.Visible = true;

        }
        public void ReportBind(string AdditionID)
        {
            try
            {
                string Path1 = "";
                string Id = "";
                string FileName = "";
                rptViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;

                rptViewer.ServerReport.ReportServerUrl = new Uri("http://mlsqlprod01/reportserver");


                while ((rptViewer.ServerReport.IsDrillthroughReport))
                {
                    rptViewer.PerformBack();

                }

                if (AdditionID.Contains("A"))
                {
                    Path1 = "/FinAuthNew/AdditionSignatory";
                    Id = "AdditionID";
                    FileName = "AdditionSignatory";
                }
                else if (AdditionID.Contains("D"))
                {
                    Path1 = "/FinAuthNew/DeletionSignatory";
                    Id = "DeletionID";
                    FileName = "DeletionSignatory";
                }
                //   rptViewer.ServerReport.ReportPath = "/FinAuthNew/DeletionSignatory";
                rptViewer.ServerReport.ReportPath = Path1;
                //Set Parameter value 
                rptViewer.ShowParameterPrompts = false;
                ReportParameter[] rpParameter = new ReportParameter[1];
                rpParameter[0] = new ReportParameter(Id, AdditionID);
                rptViewer.ServerReport.SetParameters(rpParameter);
                if (AdditionID == "")
                {
                    rptViewer.ShowParameterPrompts = true;
                }
                else
                {
                    rptViewer.ShowParameterPrompts = false;
                }
                rptViewer.ShowPrintButton = true;
                rptViewer.ShowZoomControl = true;
                rptViewer.ShowPrintButton = true;
                rptViewer.ServerReport.Refresh();
                List<ReportParameter> list = new List<ReportParameter>();
                list.Add(new ReportParameter(Id, AdditionID));
                rptViewer.ServerReport.SetParameters(list);
                byte[] data = rptViewer.ServerReport.Render("pdf");
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(data);
                MemoryStream ms = new MemoryStream(data);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "_" + AdditionID + ".pdf");
                Response.Buffer = true;
                ms.WriteTo(Response.OutputStream);
                // Response.End();
            }
            catch (Exception ex)
            {
                RAM.Alert("ERROR GENERATING PDF - " + ex.ToString());
            }
        }
        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            LinkButton lnkCP = (LinkButton)sender;
            string AdditionID = lnkCP.ToolTip;
            ReportBind(AdditionID);
        }

        protected void rgAdditionSignatoriesReport_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataTable dt = new DataTable();
            dt = objBAL.GetViewReport(rcmbBank.SelectedValue, rcmbAccountno.SelectedValue, rcmbtype.SelectedValue, rcmbRequest.SelectedValue);
            if (dt.Rows.Count > 0)
            {
                rgAdditionSignatoriesReport.DataSource = dt;
            }
            else
            {
                rgAdditionSignatoriesReport.DataSource = dt;
            }
        }
    }
}