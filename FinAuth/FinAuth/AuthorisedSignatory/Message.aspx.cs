﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinAuth.BAL;
using FinAuth.BO;
using System.Data;
using Telerik.Web.UI;
using Microsoft.Reporting.WebForms;
using System.IO;


namespace FinAuth.AuthorisedSignatory
{
    public partial class Message : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Msg"].ToString()))
                {
                    string[] message = Request.QueryString["Msg"].ToString().Split(';');
                    lblMessage.Text = message[0];//Request.QueryString["Msg"].ToString();
                         if(message.Length>1)
                             lblmsg.Text = message[1];
                    if (!string.IsNullOrEmpty(Request.QueryString["type"].ToString()))
                    {
                        if (Request.QueryString["type"].ToString() == "F")
                        {
                            btnDownload.Visible = true;
                           // ReportBind(AdditionID);
                        }

                    }
                }

            }
        }

        public void ReportBind(string AdditionID)
        {
            try
            {

                string Path1 = "";
                string Id = "";
                string FileName = "";
                rptViewer.Visible = true;
                rptViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;

                rptViewer.ServerReport.ReportServerUrl = new Uri("http://mlsqlprod01/reportserver");


                while ((rptViewer.ServerReport.IsDrillthroughReport))
                {
                    rptViewer.PerformBack();

                }

                if (AdditionID.Contains("A"))
                {
                    Path1 = "/FinAuthNew/AdditionSignatory";
                    Id = "AdditionID";
                    FileName = "AdditionSignatory";
                }
                else if (AdditionID.Contains("D"))
                {
                    Path1 = "/FinAuthNew/DeletionSignatory";
                    Id = "DeletionID";
                    FileName = "DeletionSignatory";
                }
                //   rptViewer.ServerReport.ReportPath = "/FinAuthNew/DeletionSignatory";


                rptViewer.ServerReport.ReportPath = Path1;
                //Set Parameter value 

                rptViewer.ShowParameterPrompts = false;

                ReportParameter[] rpParameter = new ReportParameter[1];


                rpParameter[0] = new ReportParameter(Id, AdditionID);

                rptViewer.ServerReport.SetParameters(rpParameter);

                if (AdditionID == "")
                {
                    rptViewer.ShowParameterPrompts = true;
                }
                else
                {
                    rptViewer.ShowParameterPrompts = false;
                }
                rptViewer.ShowPrintButton = true;
                rptViewer.ShowZoomControl = true;

                rptViewer.ShowPrintButton = true;
                rptViewer.ServerReport.Refresh();
                List<ReportParameter> list = new List<ReportParameter>();
                list.Add(new ReportParameter(Id, AdditionID));
                rptViewer.ServerReport.SetParameters(list);
                byte[] data = rptViewer.ServerReport.Render("pdf");
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(data);
                MemoryStream ms = new MemoryStream(data);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "_" + AdditionID + ".pdf");
                Response.Buffer = true;
                ms.WriteTo(Response.OutputStream);
                // Response.End();
            }
            catch(Exception ex)
            {
                RAM.Alert("ERROR GENERATING PDF - " + ex.ToString());
            }

        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            string AdditionID = Request.QueryString["ID"].ToString();
            if (!string.IsNullOrEmpty(Request.QueryString["type"].ToString()))
            {
                if (Request.QueryString["type"].ToString() == "F")
                {
                    ReportBind(AdditionID);
                }

            }

        }

    }
}