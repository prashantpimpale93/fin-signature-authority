﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinAuth.BAL;
using FinAuth.BO;
using System.Data;

namespace FinAuth.AuthorisedSignatory
{
    public partial class ViewDeletionDraft : System.Web.UI.Page
    {
        clsBAL objBAL = new clsBAL();
        clsBO objBO = new clsBO();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void rgViewDraftDetails_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DataTable dt = new DataTable();
            dt = objBAL.getDeletionDraftDetails();
            if (dt.Rows.Count > 0)
            {
                rgViewDraftDetails.DataSource = dt;
            }
            else
            {
                rgViewDraftDetails.DataSource = dt;
            }
        }
    }
}