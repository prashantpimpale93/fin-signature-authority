﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinAuth.BAL;
using FinAuth.BO;
using System.Data;
using Telerik.Web.UI;

namespace FinAuth.AuthorisedSignatory
{
    public partial class TrackRequest : System.Web.UI.Page
    {
        clsBAL objBAL = new clsBAL();
        clsBO objBO = new clsBO();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void rgTrackRequest_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DataTable dt = new DataTable();
            dt = objBAL.getMainApprovalTrackDetails();
            if (dt.Rows.Count > 0)
            {
                rgTrackRequest.DataSource = dt;
            }
            else
            {
                rgTrackRequest.DataSource = dt;
            }
        }
        protected void rgTrackRequest_DetailTableDataBind(object sender, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            string TrnID = dataItem.GetDataKeyValue("RequestID").ToString();
            DataTable dtTemp = new DataTable();
            dtTemp = objBAL.getDetailApprovalTrackDetails(TrnID);
            if (dtTemp.Rows.Count > 0)
            {
                e.DetailTableView.DataSource = dtTemp;
            }
        }
    }
}