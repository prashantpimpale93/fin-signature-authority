﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinAuth.BAL;
using FinAuth.BO;
using System.Data;


namespace FinAuth.AuthorisedSignatory
{
    public partial class PendingRejectedRequests : System.Web.UI.Page
    {
        clsBAL objBAL = new clsBAL();
        clsBO objBO = new clsBO();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void rgPending_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DataTable dt = new DataTable();
            dt = objBAL.getRejectedRequestsAddition(Session["EmpID"].ToString());
            if (dt.Rows.Count > 0)
            {
                rgPending.DataSource = dt;
            }
            else
            {
                rgPending.DataSource = dt;
            }
        }

        protected void rgPendingDeletion_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DataTable dtDel = new DataTable();
            dtDel = objBAL.getRejectedRequestsDeletion(Session["EmpID"].ToString());
            if (dtDel.Rows.Count > 0)
            {
                rgPendingDeletion.DataSource = dtDel;
            }
            else
            {
                rgPendingDeletion.DataSource = dtDel;
            }
        }
    }
}