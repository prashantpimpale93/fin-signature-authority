﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinAuth.BAL;
using FinAuth.BO;
using System.Data;
using Telerik.Web.UI;
using System.Net;
namespace FinAuth.AuthorisedSignatory
{
    public partial class AdditionSignatories : System.Web.UI.Page
    {
        SendASmail objMail = new SendASmail();
        clsBAL objBAL = new clsBAL();
        clsBO objBO = new clsBO();
        string emailcontent = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            btnSubmit.Attributes.Add("onclick", "javascript:" + btnSubmit.ClientID + ".disabled=true;" + ClientScript.GetPostBackEventReference(btnSubmit, ""));

            if (!IsPostBack)
            {

                if (ViewState["Members"] != null)
                {
                    ViewState["Members"] = null;
                }
                rcmbRegion.Enabled = false;
                rcmbPlant.Enabled = false;
                bindPlant();
                bindRegion();
                bindAddDetails();
                bindEmp();
                bindType();
                if (!string.IsNullOrEmpty(Request.QueryString["AdditionID"]))
                {
                    FillDetails();
                }
            }
        }
        private void FillDetails()
        {
            DataTable dt = objBAL.getAllDetails(Request.QueryString["AdditionID"]);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["Locationtype"].ToString() == "P")
                {
                    rdoPlant.Checked = true;

                    rcmbPlant.SelectedItem.Text = dt.Rows[0]["LocationDetails"].ToString();
                    rcmbRegion.SelectedValue = "0";
                    rdoRegion.Checked = false;
                    rdoHO.Checked = false;
                    rcmbRegion.Enabled = false;
                    rcmbPlant.Enabled = true;
                }
                else if (dt.Rows[0]["Locationtype"].ToString() == "R")
                {
                    rdoRegion.Checked = true;
                    rcmbRegion.SelectedItem.Text = dt.Rows[0]["LocationDetails"].ToString();
                    rcmbPlant.SelectedValue = "0";
                    rdoPlant.Checked = false;
                    rdoHO.Checked = false;
                    rcmbPlant.Enabled = false;
                    rcmbRegion.Enabled = true;
                }
                else if (dt.Rows[0]["Locationtype"].ToString() == "H")
                {
                    rdoHO.Checked = true;
                    rcmbRegion.SelectedValue = "0";
                    rcmbPlant.SelectedValue = "0";
                    rdoRegion.Checked = false;
                    rdoPlant.Checked = false;
                    rcmbPlant.Enabled = false;
                    rcmbRegion.Enabled = false;
                }
                rcmbRequiredFor.SelectedValue = dt.Rows[0]["RequiredFor"].ToString();
                //akshay naik 9-2-18
                //CR bank name drop down
                DataTable dTemp = objBAL.getBanklist(rcmbRequiredFor.SelectedItem.Text, dt.Rows[0]["LocationDetails"].ToString());
                if (dTemp.Rows.Count > 0)
                {
                    rcmbBank.DataSource = dTemp;
                    rcmbBank.DataTextField = "BankName";
                    rcmbBank.DataValueField = "BankName";
                    rcmbBank.DataBind();
                    rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
                }

                DataTable dtAcc = new DataTable();
                dtAcc = objBAL.getAccountDetails(dt.Rows[0]["AccountNO"].ToString());
                if (dtAcc.Rows.Count > 0)
                {
                    rcmbBank.SelectedValue = dtAcc.Rows[0]["BankName"].ToString();
                    lblBankAddValue.Text = dtAcc.Rows[0]["BankAddress"].ToString();
                }
                dTemp = objBAL.getAccountNos(rcmbBank.SelectedItem.Text);
                if (dTemp.Rows.Count > 0)
                {
                    rcmbAccountNo.DataSource = dTemp;
                    rcmbAccountNo.DataTextField = "AccountNo";
                    rcmbAccountNo.DataValueField = "AccountNo";
                    rcmbAccountNo.DataBind();
                    rcmbAccountNo.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Account No--", "0"));
                }
                rcmbAccountNo.SelectedValue = dt.Rows[0]["AccountNO"].ToString();
                rgExistingSignatories.Rebind();
                rcmbDetailAddition.SelectedValue = dt.Rows[0]["AdditionDetails"].ToString();
                txtComments.Text = dt.Rows[0]["Comments"].ToString();
                if (Session["EmpID"].ToString() != dt.Rows[0]["CreatedBy"].ToString())
                {
                    DisablePageControls(false);
                }
            }
            bindMembGrid();

        }
        private void bindMembGrid()
        {
            DataTable dTemp = new DataTable();
            dTemp = objBAL.getNewMembDetails(Request.QueryString["AdditionID"]);
            if (dTemp.Rows.Count > 0)
            {
                rgMemberDetails.DataSource = dTemp;
                rgMemberDetails.DataBind();
                ViewState["Members"] = dTemp;
                pnlMember.Visible = true;
            }
            else
            {
                rgMemberDetails.DataSource = dTemp;
                rgMemberDetails.DataBind();
                pnlMember.Visible = false; ;
            }
        }
        public void DisablePageControls(bool status)
        {
            //foreach (Control c in Page.Controls)
            //{

            foreach (Control ctrl in Master.FindControl("ContentPlaceHolder1").Controls)
            {
                if (ctrl is RadComboBox)
                    ((RadComboBox)ctrl).Enabled = status;
                else if (ctrl is Button)
                    ((Button)ctrl).Enabled = status;
                else if (ctrl is RadButton)
                    ((RadButton)ctrl).Enabled = status;
                else if (ctrl is RadioButtonList)
                    ((RadioButtonList)ctrl).Enabled = status;
                else if (ctrl is ImageButton)
                    ((ImageButton)ctrl).Enabled = status;
                else if (ctrl is CheckBox)
                    ((CheckBox)ctrl).Enabled = status;
                else if (ctrl is CheckBoxList)
                    ((CheckBoxList)ctrl).Enabled = status;
                else if (ctrl is DropDownList)
                    ((DropDownList)ctrl).Enabled = status;
                else if (ctrl is HyperLink)
                    ((HyperLink)ctrl).Enabled = status;
                else if (ctrl is TextBox)
                    ((TextBox)ctrl).Enabled = status;

            }
            rgMemberDetails.MasterTableView.Columns[7].Visible = status;
            lnkdownload.Style.Add("pointer-events", "none");
            // }
        }
        private void bindPlant()
        {
            DataTable dtPlant = new DataTable();
            dtPlant = objBAL.getPlant();
            if (dtPlant.Rows.Count > 0)
            {
                rcmbPlant.DataSource = dtPlant;
                rcmbPlant.DataTextField = "PlantName";
                rcmbPlant.DataValueField = "ID";
                rcmbPlant.DataBind();
                rcmbPlant.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Plant--", "0"));
            }

        }
        private void bindRegion()
        {
            DataTable dtReg = new DataTable();
            dtReg = objBAL.getRegion();
            if (dtReg.Rows.Count > 0)
            {
                rcmbRegion.DataSource = dtReg;
                rcmbRegion.DataTextField = "RegionName";
                rcmbRegion.DataValueField = "ID";
                rcmbRegion.DataBind();
                rcmbRegion.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Region--", "0"));
            }
        }
        private void bindEmp()
        {
            DataTable dtEmp = new DataTable();
            dtEmp = objBAL.getEmpDetails();
            if (dtEmp.Rows.Count > 0)
            {
                rcmbEmpID.DataSource = dtEmp;
                rcmbEmpID.DataTextField = "EmpName";
                rcmbEmpID.DataValueField = "EmpID";
                rcmbEmpID.DataBind();
                rcmbEmpID.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Employee--", "0"));
            }
        }
        private void bindType()
        {
            DataTable dtType = new DataTable();
            dtType = objBAL.getType();
            if (dtType.Rows.Count > 0)
            {
                rcmbType.DataSource = dtType;
                rcmbType.DataTextField = "TypeName";
                rcmbType.DataValueField = "ID";
                rcmbType.DataBind();
                rcmbType.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Type--", "0"));
            }
        }
        private void bindAddDetails()
        {
            DataTable dtAdd = new DataTable();
            dtAdd = objBAL.getAddDetails();
            if (dtAdd.Rows.Count > 0)
            {
                rcmbDetailAddition.DataSource = dtAdd;
                rcmbDetailAddition.DataTextField = "ReasonDetails";
                rcmbDetailAddition.DataValueField = "ID";
                rcmbDetailAddition.DataBind();

            }
        }
        protected void rcmbRequiredFor_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (rdoHO.Checked == false && rdoPlant.Checked == false && rdoRegion.Checked == false)
            {
                RAM.Alert("Please Select Location");
                return;
            }
            else
            {


                if (rdoPlant.Checked)
                {
                    if (rcmbPlant.SelectedValue == "0")
                    {
                        RAM.Alert("Please Select Plant");
                        return;
                    }
                    else
                    {
                        DataTable dtPlant = objBAL.getBanklist(rcmbRequiredFor.SelectedItem.Text.Trim(), rcmbPlant.SelectedItem.Text);
                        if (dtPlant.Rows.Count > 0)
                        {
                            rcmbBank.DataSource = dtPlant;
                            rcmbBank.DataTextField = "BankName";
                            rcmbBank.DataValueField = "BankName";
                            rcmbBank.DataBind();
                            rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
                            rgExistingSignatories.Rebind();
                        }
                        else
                        {
                            lblBankValue.Text = string.Empty;
                            lblBankAddValue.Text = string.Empty;
                            rcmbBank.DataSource = dtPlant;
                            rcmbBank.DataTextField = "BankName";
                            rcmbBank.DataValueField = "BankName";
                            rcmbBank.DataBind();
                            rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
                            rgExistingSignatories.Rebind();
                            RAM.Alert("No Bank For Selected Mapping");
                            return;
                        }
                    }
                }
                else if (rdoRegion.Checked)
                {
                    if (rcmbRegion.SelectedValue == "0")
                    {
                        RAM.Alert("Please Select Region");
                        return;
                    }
                    else
                    {
                        DataTable dtRegion = objBAL.getBanklist(rcmbRequiredFor.SelectedItem.Text.Trim(), rcmbRegion.SelectedItem.Text);
                        if (dtRegion.Rows.Count > 0)
                        {
                            rcmbBank.DataSource = dtRegion;
                            rcmbBank.DataTextField = "BankName";
                            rcmbBank.DataValueField = "BankName";
                            rcmbBank.DataBind();
                            rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
                            rgExistingSignatories.Rebind();
                        }
                        else
                        {
                            lblBankValue.Text = string.Empty;
                            lblBankAddValue.Text = string.Empty;
                            rcmbBank.DataSource = dtRegion;
                            rcmbBank.DataTextField = "BankName";
                            rcmbBank.DataValueField = "BankName";
                            rcmbBank.DataBind();
                            rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
                            rgExistingSignatories.Rebind();
                            RAM.Alert("No Bank For Selected Mapping");
                            return;
                        }

                    }
                }
                else if (rdoHO.Checked)
                {
                    DataTable dtHO = objBAL.getBanklist(rcmbRequiredFor.SelectedItem.Text.Trim(), "HO");
                    if (dtHO.Rows.Count > 0)
                    {
                        rcmbBank.DataSource = dtHO;
                        rcmbBank.DataTextField = "BankName";
                        rcmbBank.DataValueField = "BankName";
                        rcmbBank.DataBind();
                        rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
                        rgExistingSignatories.Rebind();
                    }
                    else
                    {
                        lblBankValue.Text = string.Empty;
                        lblBankAddValue.Text = string.Empty;
                        rcmbBank.DataSource = dtHO;
                        rcmbBank.DataTextField = "BankName";
                        rcmbBank.DataValueField = "BankName";
                        rcmbBank.DataBind();
                        rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
                        rgExistingSignatories.Rebind();
                        RAM.Alert("No Bank For Selected Mapping");
                        return;
                    }
                }
            }

        }
        protected void rcmbAccountNo_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //akshay naik 9-2-18
            //Code commented below
            DataTable dtAcc = new DataTable();
            dtAcc = objBAL.getAccountDetails(rcmbAccountNo.SelectedValue);
            if (dtAcc.Rows.Count > 0)
            {
                //lblBankValue.Text = dtAcc.Rows[0]["BankName"].ToString();
                lblBankAddValue.Text = dtAcc.Rows[0]["BankAddress"].ToString();
            }
            else
            {
                lblBankAddValue.Text = "";
            }
            rgExistingSignatories.Rebind();
        }

        protected void rgExistingSignatories_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DataTable dtEx = new DataTable();
            string strLocation = string.Empty;
            if (rdoPlant.Checked)
            {
                strLocation = rcmbPlant.SelectedItem.Text.Trim();
            }
            else if (rdoRegion.Checked)
            {
                strLocation = rcmbRegion.SelectedItem.Text.Trim();
            }
            else if (rdoHO.Checked)
            {
                strLocation = "HO";
            }
            dtEx = objBAL.getExAuthSignDetails(strLocation, rcmbRequiredFor.SelectedItem.Text.Trim(), rcmbAccountNo.SelectedValue);
            ViewState["dtEx"] = dtEx;
            if (dtEx.Rows.Count > 0)
            {
                rgExistingSignatories.DataSource = dtEx;
            }
            else
            {
                rgExistingSignatories.DataSource = dtEx;
            }
        }
        protected void btnADD_Click(object sender, EventArgs e)
        {
            try
            {
                pnlMember.Visible = true;
                DataTable dt = new DataTable();
                if (ViewState["Members"] == null)
                {
                    dt.Columns.Add("EmpID", typeof(string));
                    dt.Columns.Add("EmpName", typeof(string));
                    dt.Columns.Add("Designation", typeof(string));
                    dt.Columns.Add("Type", typeof(string));
                    dt.Columns.Add("Aadhar", typeof(string));
                    dt.Columns.Add("OtherDoc", typeof(string));
                    if (ViewState["dtEx"] != null)
                    {
                        DataTable dtex = (DataTable)ViewState["dtEx"];

                        for (int i = 0; i < dtex.Rows.Count; i++)
                        {
                            string empid = dtex.Rows[i]["EmpID"].ToString();
                            if (empid == rcmbEmpID.SelectedValue.ToString())
                            {
                                RAM.Alert("Selected Employee Already Exists In Member List");
                                return;
                            }
                        }

                    }
                    DataRow dtrow = dt.NewRow();
                    dtrow["EmpID"] = rcmbEmpID.SelectedValue.ToString();
                    dtrow["EmpName"] = rcmbEmpID.SelectedItem.Text;
                    DataTable dTemp = new DataTable();
                    dTemp = objBAL.getDesignation(rcmbEmpID.SelectedValue.ToString());
                    if (dTemp.Rows.Count > 0)
                    {
                        dtrow["Designation"] = dTemp.Rows[0]["Designation"].ToString();
                    }
                    dtrow["Type"] = rcmbType.SelectedItem.Text;
                    if (chkAadhar.Checked)
                    {
                        dtrow["Aadhar"] = "Yes";
                    }
                    else
                    {
                        dtrow["Aadhar"] = "No";
                    }
                    dtrow["OtherDoc"] = rbtnadress.SelectedItem.Text;
                    dt.Rows.Add(dtrow);
                }
                else
                {
                    dt = (DataTable)ViewState["Members"];
                    if (ViewState["dtEx"] != null)
                    {
                        DataTable dtex = (DataTable)ViewState["dtEx"];

                        for (int i = 0; i < dtex.Rows.Count; i++)
                        {
                            string empid = dtex.Rows[i]["EmpID"].ToString();
                            if (empid == rcmbEmpID.SelectedValue.ToString())
                            {
                                RAM.Alert("Selected Employee Already Exists In Member List");
                                return;
                            }
                        }

                    }
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i]["EmpID"].ToString() == rcmbEmpID.SelectedValue.ToString())
                            {
                                RAM.Alert("Selected Employee Already Exists");
                                return;
                            }
                        }
                        for (int i = 0; i < rgExistingSignatories.Items.Count; i++)
                        {
                            string empid = rgExistingSignatories.Items[i].ToString();
                            if (empid == rcmbEmpID.SelectedValue.ToString())
                            {
                                RAM.Alert("Selected Employee Already Exists In Member List");
                                return;
                            }
                        }
                        DataRow dtrow = dt.NewRow();
                        dtrow["EmpID"] = rcmbEmpID.SelectedValue.ToString();
                        dtrow["EmpName"] = rcmbEmpID.SelectedItem.Text;
                        DataTable dTemp = new DataTable();
                        dTemp = objBAL.getDesignation(rcmbEmpID.SelectedValue.ToString());
                        if (dTemp.Rows.Count > 0)
                        {
                            dtrow["Designation"] = dTemp.Rows[0]["Designation"].ToString();
                        }
                        dtrow["Type"] = rcmbType.SelectedItem.Text;
                        if (chkAadhar.Checked)
                        {
                            dtrow["Aadhar"] = "Yes";
                        }
                        else
                        {
                            dtrow["Aadhar"] = "No";
                        }
                        dtrow["OtherDoc"] = rbtnadress.SelectedItem.Text;
                        dt.Rows.Add(dtrow);
                    }
                }
                ViewState["Members"] = dt;
                rgMemberDetails.DataSource = dt;
                rgMemberDetails.DataBind();
                rcmbEmpID.SelectedValue = "0";
                rcmbType.SelectedValue = "0";
                chkAadhar.Checked = false;
                rbtnadress.ClearSelection();
            }
            catch (Exception ex)
            {
                RAM.Alert("ERROR - " + ex.ToString());
            }
        }
        private string GetUniqueID()
        {
            DataTable dtTemp = new DataTable();
            dtTemp = objBAL.getAddUniqueID();
            string strAdditionID = "";
            string strID = "";
            if (dtTemp.Rows[0]["AdditionID"].ToString().Trim() == "0")
            {
                strAdditionID = "AS00000001";
            }
            else
            {
                strID = string.Concat("00000000", (Convert.ToInt32(dtTemp.Rows[0]["AdditionID"].ToString().Remove(0, 2)) + 1).ToString());
                strAdditionID = "AS" + strID.Substring(strID.Length - 8);
            }
            return strAdditionID;
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Members"] == null)
                {
                    RAM.Alert("Please Add New Member Details");
                    return;
                }
                if (rdoHO.Checked == false && rdoPlant.Checked == false && rdoRegion.Checked == false)
                {
                    RAM.Alert("Please Select Location");
                    return;
                }
                if (hfvalue.Value != "1")
                {
                    RAM.Alert("Please Download Specimen Signature");
                    return;
                }
                string strAdditionID = string.Empty;
                string strLocationType = string.Empty;
                objBO.AdditionID = strAdditionID;
                if (rdoPlant.Checked)
                {
                    objBO.LocationType = "P";
                    strLocationType = "Plant";
                    objBO.LocationDetails = rcmbPlant.SelectedItem.Text.Trim();
                }
                else if (rdoRegion.Checked)
                {
                    objBO.LocationType = "R";
                    strLocationType = "Region";
                    objBO.LocationDetails = rcmbRegion.SelectedItem.Text.Trim();
                }
                else if (rdoHO.Checked)
                {
                    objBO.LocationType = "H";
                    strLocationType = "HO";
                    objBO.LocationDetails = "HO";
                }
                objBO.RequiredFor = rcmbRequiredFor.SelectedItem.Text.Trim();
                objBO.AccountNO = rcmbAccountNo.SelectedValue;
                objBO.AdditionDetails = rcmbDetailAddition.SelectedValue;
                //DataTable dAd = objBAL.getDetailsAdd(rcmbDetailAddition.SelectedItem.Text);
                //if (dAd.Rows.Count > 0)
                //{
                //    objBO.AdditionDetails = dAd.Rows[0]["ReasonDetails"].ToString();
                //}
                //else
                //{
                //    objBO.AdditionDetails = "0";
                //}

                objBO.Comments = txtComments.Text;
                objBO.CreatedBy = Session["EmpID"].ToString();
                objBO.SaveDraftFlag = "N";
                int k = 0;
                int k1 = 0;

                if (!string.IsNullOrEmpty(Request.QueryString["AdditionID"]))
                {
                    strAdditionID = Request.QueryString["AdditionID"];
                    objBO.AdditionID = strAdditionID;
                    k = objBAL.DeleteMemberDetails(Request.QueryString["AdditionID"]);
                    k1 = objBAL.UpdateAdditionDetails(objBO);
                }
                else
                {
                    strAdditionID = GetUniqueID();
                    objBO.AdditionID = strAdditionID;
                    k1 = objBAL.insertAdditionDetails(objBO);
                }

                GenerateApprovalFlow(objBO.LocationDetails, strLocationType, strAdditionID);
                InsertAddMemberDetails(strAdditionID);
                if (k1 > 0)
                {
                    string Finalemail = @"<table class='MsoNormalTable' border='1'><tr><td>Sr. No.</td><td>Bank Name</td><td>Account No.</td><td>Employee code</td><td>Member Name</td><td>Group</td><td>Type</td><td>Location</td></tr>" + emailcontent + "</table>";

                    objMail.SendInitiatorMail(Session["EmpID"].ToString(), rcmbAccountNo.SelectedValue, strAdditionID, strLocationType, objBO.LocationDetails, Finalemail);
                    //ClearALL();
                    if (!string.IsNullOrEmpty(Request.QueryString["Request"]))
                    {
                        if (Request.QueryString["Request"] == "Resubmit")
                        {
                            // RAM.Alert("Details Updated Successfully! Request id is " + strAdditionID);
                            Response.Redirect("Message.aspx?Msg=Details Updated Successfully! Request ID is " + strAdditionID + ".;Please Note: Submit Specimen Signature duly signed along with self attested documents and 3 passport size photograph to Corporate center, Treasury Team within 7 working days.&ID=N&Type=N");
                        }
                        else if (Request.QueryString["Request"] == "Draft")
                        {
                            Response.Redirect("Message.aspx?Msg=Details Inserted Successfully! Request ID is " + strAdditionID + "&ID=N&Type=N");
                        }
                    }
                    else
                    {
                        //RAM.Alert("Details Inserted Successfully! Request id is " + strAdditionID);
                        Response.Redirect("Message.aspx?Msg=Details Inserted Successfully! Request ID is " + strAdditionID + ".;Please Note: Submit Specimen Signature duly signed along with self attested documents and 3 passport size photograph to Corporate center, Treasury Team within 7 working days.&ID=N&Type=N");
                    }

                }
            }
            catch (Exception ex)
            {
                RAM.Alert("ERROR InsertAdditionDetails- " + ex.ToString());
            }
        }
        private void InsertAddMemberDetails(string strAdditionID)
        {
            try
            {
                if (ViewState["Members"] != null)
                {
                    DataTable dt = (DataTable)ViewState["Members"];
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objBO.AdditionID = strAdditionID;
                            objBO.EmpID = dt.Rows[i]["EmpID"].ToString();
                            objBO.Type = dt.Rows[i]["Type"].ToString();
                            objBO.Aadhar = dt.Rows[i]["Aadhar"].ToString();
                            objBO.Otherdoc = dt.Rows[i]["OtherDoc"].ToString();
                            int N = objBAL.InsertAddMemberDetails(objBO);
                            emailcontent = emailcontent + "<tr><td>" + (i + 1).ToString() + "</td><td>" + rcmbBank.SelectedValue + "</td><td>" + rcmbAccountNo.SelectedValue + "</td><td>" + dt.Rows[i]["EmpID"].ToString() + "</td><td>" + dt.Rows[i]["EmpName"].ToString() + "</td><td>" + dt.Rows[i]["Type"].ToString() + "</td><td>" + rcmbRequiredFor.SelectedItem.Text + "</td><td>" + objBO.LocationDetails + "</td></tr>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RAM.Alert("ERROR in InsertAddMemberDetails - " + ex.ToString());
            }

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/AuthorisedSignatory/AdditionSignatories.aspx");
            //ClearALL();
        }
        private void ClearALL()
        {
            rdoPlant.Checked = true;
            rdoRegion.Checked = false;
            rdoHO.Checked = false;
            rcmbPlant.SelectedValue = "0";
            rcmbRegion.SelectedValue = "0";
            rcmbRequiredFor.SelectedValue = "0";
            rcmbAccountNo.ClearSelection();
            lblBankAddValue.Text = string.Empty;
            lblBankValue.Text = string.Empty;
            rgExistingSignatories.DataSource = string.Empty;
            rgExistingSignatories.DataBind();
            pnlMember.Visible = false;
            ViewState["Members"] = null;
            rgMemberDetails.DataSource = string.Empty;
            rgMemberDetails.DataBind();
            // rcmbDetailAddition.SelectedValue = "0";
            txtComments.Text = string.Empty;
        }
        protected void rdoPlant_CheckedChanged(object sender, EventArgs e)
        {
            DataTable dtPlant = new DataTable();
            rcmbRegion.SelectedValue = "0";
            rdoRegion.Checked = false;
            rdoHO.Checked = false;
            rcmbRegion.Enabled = false;
            rcmbPlant.Enabled = true;
            rcmbRequiredFor.SelectedValue = "0";
            rcmbBank.DataSource = dtPlant;
            rcmbBank.DataTextField = "BankName";
            rcmbBank.DataValueField = "BankName";
            rcmbBank.DataBind();
            rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
            lblBankValue.Text = string.Empty;
            lblBankAddValue.Text = string.Empty;
            rgExistingSignatories.Rebind();
        }
        protected void rdoRegion_CheckedChanged(object sender, EventArgs e)
        {
            DataTable dtR = new DataTable();
            rcmbPlant.SelectedValue = "0";
            rdoPlant.Checked = false;
            rdoHO.Checked = false;
            rcmbPlant.Enabled = false;
            rcmbRegion.Enabled = true;
            rcmbRequiredFor.SelectedValue = "0";
            rcmbBank.DataSource = dtR;
            rcmbBank.DataTextField = "BankName";
            rcmbBank.DataValueField = "BankName";
            rcmbBank.DataBind();
            rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
            lblBankValue.Text = string.Empty;
            lblBankAddValue.Text = string.Empty;
            rgExistingSignatories.Rebind();
        }
        protected void rdoHO_CheckedChanged(object sender, EventArgs e)
        {
            DataTable dtHO = new DataTable();
            bindPlant();
            bindRegion();
            rcmbRegion.SelectedValue = "0";
            rcmbPlant.SelectedValue = "0";
            rcmbRequiredFor.SelectedValue = "0";
            rcmbBank.DataSource = dtHO;
            rcmbBank.DataTextField = "BankName";
            rcmbBank.DataValueField = "BankName";
            rcmbBank.DataBind();
            rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
            rdoRegion.Checked = false;
            rdoPlant.Checked = false;
            rcmbPlant.Enabled = false;
            rcmbRegion.Enabled = false;
            lblBankValue.Text = string.Empty;
            lblBankAddValue.Text = string.Empty;
            rgExistingSignatories.Rebind();
        }
        protected void btnDraft_Click(object sender, EventArgs e)
        {
            try
            {
                string strAdditionID = "";
                if (rdoPlant.Checked)
                {
                    objBO.LocationType = "P";
                    objBO.LocationDetails = rcmbPlant.SelectedItem.Text.Trim();
                }
                else if (rdoRegion.Checked)
                {
                    objBO.LocationType = "R";
                    objBO.LocationDetails = rcmbRegion.SelectedItem.Text.Trim();
                }
                else if (rdoHO.Checked)
                {
                    objBO.LocationType = "H";
                    objBO.LocationDetails = "HO";
                }
                objBO.RequiredFor = rcmbRequiredFor.SelectedItem.Text.Trim();
                objBO.AccountNO = rcmbAccountNo.SelectedValue;

                objBO.AdditionDetails = rcmbDetailAddition.SelectedValue;
                objBO.Comments = txtComments.Text;
                objBO.CreatedBy = Session["EmpID"].ToString();
                objBO.SaveDraftFlag = "Y";
                int k = 0;
                int k1 = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["AdditionID"]))
                {
                    strAdditionID = Request.QueryString["AdditionID"];
                    objBO.AdditionID = strAdditionID;
                    k = objBAL.DeleteMemberDetails(Request.QueryString["AdditionID"]);
                    k1 = objBAL.UpdateAdditionDetails(objBO);
                }
                else
                {
                    strAdditionID = GetUniqueID();
                    objBO.AdditionID = strAdditionID;
                    k1 = objBAL.insertAdditionDetails(objBO);
                }
                InsertAddMemberDetails(strAdditionID);
                if (k1 > 0)
                {
                    RAM.Alert("Details Saved successfully ! Request id is " + strAdditionID);
                    //ClearALL();
                }
            }
            catch (Exception ex)
            {
                RAM.Alert("ERROR while Drafting- " + ex.ToString());
            }
        }
        protected void rgMemberDetails_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                DataTable tempDt = new DataTable();
                int s = item.ItemIndex;
                tempDt = (DataTable)ViewState["Members"];
                tempDt.Rows[s].Delete();
                tempDt.AcceptChanges();
                if (tempDt.Rows.Count == 0)
                {
                    ViewState["Members"] = null;
                    pnlMember.Visible = false;
                }
                else
                {
                    ViewState["Members"] = tempDt;
                }
                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["Members"];
                rgMemberDetails.DataSource = dt;
            }
        }
        private void GenerateApprovalFlow(string strLocation, string strLocationtype, string strAdditionID)
        {
            try
            {
                int k = 0;
                DataTable dTemp = new DataTable();
                int seqNo = Convert.ToInt32(Request.QueryString["SeqNo"]);
                int NextseqNo = Convert.ToInt32(Request.QueryString["SeqNo"]) + 1;
                dTemp = objBAL.getInitiatorSupervisor(Session["EmpID"].ToString());
                if (dTemp.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["Request"]))
                    {
                        if (Request.QueryString["Request"] == "Resubmit")
                        {
                            k = objBAL.updateApprovalFlowAdditionResubmit(strAdditionID, Session["EmpID"].ToString(), txtComments.Text, "ADDITION", seqNo);
                            if (k > 0)
                            {
                                int n = objBAL.insertApprovalFlowAdditionNextResubmit(strAdditionID, dTemp.Rows[0]["EmpID"].ToString(), Session["EmpID"].ToString(), "ADDITION", NextseqNo);
                            }
                        }
                        else
                        {
                            k = objBAL.insertApprovalFlowAddition(strAdditionID, Session["EmpID"].ToString(), txtComments.Text, "ADDITION");
                            if (k > 0)
                            {
                                int n = objBAL.insertApprovalFlowAdditionNext(strAdditionID, dTemp.Rows[0]["EmpID"].ToString(), Session["EmpID"].ToString(), "ADDITION");
                            }
                        }
                    }
                    else
                    {
                        k = objBAL.insertApprovalFlowAddition(strAdditionID, Session["EmpID"].ToString(), txtComments.Text, "ADDITION");
                        if (k > 0)
                        {
                            int n = objBAL.insertApprovalFlowAdditionNext(strAdditionID, dTemp.Rows[0]["EmpID"].ToString(), Session["EmpID"].ToString(), "ADDITION");
                        }
                    }
                    int N = objBAL.updateSupervisorStatus(strAdditionID, "S");
                }
            }
            catch (Exception ex)
            {
                RAM.Alert("Error Generating Approval Flow -" + ex.ToString());
            }
        }

        protected void rcmbPlant_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataTable dtPlant = new DataTable();
            rcmbRegion.SelectedValue = "0";
            rdoRegion.Checked = false;
            rdoHO.Checked = false;
            rcmbRegion.Enabled = false;

            rcmbRequiredFor.SelectedValue = "0";
            rcmbBank.DataSource = dtPlant;
            rcmbBank.DataTextField = "BankName";
            rcmbBank.DataValueField = "BankName";
            rcmbBank.DataBind();
            rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
            lblBankValue.Text = string.Empty;
            lblBankAddValue.Text = string.Empty;
            rgExistingSignatories.Rebind();

        }

        protected void rcmbRegion_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataTable dtR = new DataTable();
            rdoHO.Checked = false;
            rdoPlant.Checked = false;
            rcmbPlant.SelectedValue = "0";
            rcmbPlant.Enabled = false;
            rcmbRequiredFor.SelectedValue = "0";
            rcmbBank.DataSource = dtR;
            rcmbBank.DataTextField = "BankName";
            rcmbBank.DataValueField = "BankName";
            rcmbBank.DataBind();
            rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
            lblBankValue.Text = string.Empty;
            lblBankAddValue.Text = string.Empty;
            rgExistingSignatories.Rebind();
        }

        protected void lbtnSpecimenSign_Click(object sender, EventArgs e)
        {
            try
            {

                string strURL = "Specimen_Signature_Photograph_format.doc";
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                response.ClearContent();
                response.Clear();
                response.ContentType = "Application/msword";
                response.AddHeader("Content-Disposition", "attachment; filename=" + strURL + ";");
                response.TransmitFile(Server.MapPath(strURL));
                response.Flush();



            }
            catch (Exception ex)
            {
            }

        }

        protected void rcmbBank_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataTable dTemp = objBAL.getAccountNos(rcmbBank.SelectedItem.Text);
            if (dTemp.Rows.Count > 0)
            {
                rcmbAccountNo.DataSource = dTemp;
                rcmbAccountNo.DataTextField = "AccountNo";
                rcmbAccountNo.DataValueField = "AccountNo";
                rcmbAccountNo.DataBind();
                rcmbAccountNo.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Account No--", "0"));
            }
            //if (rcmbBank.SelectedIndex > 0)
            //{
            //    lblBankAddValue.Text = rcmbBank.SelectedItem.Value;
            //}
            //else
            //{
            //    lblBankAddValue.Text = "";
            //}
        }

        //protected void lbtndownload_Click(object sender, EventArgs e)
        //{
        //    hfvalue.Value = "1";
        //    var filePath = "Specimen_Signature_Photograph_format.doc";
        //    Response.ContentType = "application/msword";
        //    Response.AppendHeader("Content-Disposition", "attachment; filename=Specimen_Signature_Photograph_format.doc");
        //    Response.TransmitFile(filePath);
        //    Response.End();
        //    Response.Flush();

        //}
    }

}