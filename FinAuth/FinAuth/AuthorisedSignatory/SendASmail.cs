﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FinAuth.DAL;
using System.Net.Mail;
using System.IO;
using System.Data;
using FinAuth.BAL;
using System.Net.Configuration;
using System.Configuration;
namespace FinAuth.AuthorisedSignatory
{
    public class SendASmail
    {
        SmtpClient strclient = new SmtpClient("Marico-com.mail.protection.outlook.com", 25);
        clsDAL objDAL = new clsDAL();
        clsBAL objBAL = new clsBAL();
        public void SendInitiatorMail(string InitiatorID, string AccountNo, string AdditionID, string Location, string LocationDetails, string Empdetails)
        {
            DataTable dTemp = new DataTable();
            DataTable dEmp = new DataTable();
            DataTable dtFirst = new DataTable();
            dTemp = objBAL.getInitiatorSupervisor(InitiatorID);
            dEmp = objBAL.getEmpDetails(InitiatorID);
            if (Location == "HO")
            {
                dtFirst = objBAL.getNextApprover(Location, LocationDetails, "TR");
            }
            else
            {
                dtFirst = objBAL.getNextApprover(Location, LocationDetails, "CH");
            }
            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/AuthorisedSignatory/MailerTemplates/Addition.htm"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("[@UserName]", dEmp.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@RequestID]", AdditionID);
            myString = myString.Replace("[@Location]", Location);
            myString = myString.Replace("[@AccountNo]", AccountNo);
            myString = myString.Replace("[@ApproverName]", "-");
            myString = myString.Replace("[@ApproverComment]", "-");
            myString = myString.Replace("[@EmpDetails]", Empdetails);
            MailMessage mailMessage = new MailMessage();
         
            mailMessage.To.Add(dTemp.Rows[0]["EmailID"].ToString());
            mailMessage.CC.Add(dtFirst.Rows[0]["EmailID"].ToString());            
            mailMessage.CC.Add("Treasuryteam@marico.com");
           
            mailMessage.From = new MailAddress("AuthorisedSignatories@maricoindia.net", "Authorised Signatories");
            mailMessage.Subject = "Approval for Addition in Authorised Signatory with Request ID- " + AdditionID;
            mailMessage.Body = myString.ToString();
            mailMessage.IsBodyHtml = true;
            strclient.Send(mailMessage);
        }
        public void SendNextApproveMail(string ApproverID, string AccountNo, string AdditionID, string Location, string LocationDetails, string InitiatorID, string strcomments)
        {
            DataTable dtFirst = new DataTable();
            DataTable dtInit = new DataTable();
            DataTable dSup = new DataTable();
            DataTable dTR = new DataTable();
            if (Location == "HO")
            {
                dtFirst = objBAL.getNextApprover(Location, LocationDetails, "TR");
            }
            else
            {
                dtFirst = objBAL.getNextApprover(Location, LocationDetails, "CH");
                dTR = objBAL.getNextApprover(Location, LocationDetails, "TR");
            }

            dtInit = objBAL.getEmpDetails(InitiatorID);
            dSup = objBAL.getEmpDetails(ApproverID);

            //myString = File.ReadAllText("D:/Finance Authorization/Code/FinAuth/FinAuth/AuthorisedSignatory/MailerTemplates/Addition.htm");
            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/AuthorisedSignatory/MailerTemplates/Addition.htm"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("[@UserName]", dtInit.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@RequestID]", AdditionID);
            myString = myString.Replace("[@Location]", Location);
            myString = myString.Replace("[@AccountNo]", AccountNo);
            myString = myString.Replace("[@ApproverName]", dSup.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@ApproverComment]", strcomments);
            myString = myString.Replace("[@EmpDetails]", "");
            MailMessage mailMessage = new MailMessage();
            string strCC = "";
            if (Location == "HO")
            {
                strCC = dSup.Rows[0]["EmailID"].ToString() + "," + dtInit.Rows[0]["EmailID"].ToString();
            }
            else
            {
                strCC = dTR.Rows[0]["EmailID"].ToString() + "," + dSup.Rows[0]["EmailID"].ToString() + "," + dtInit.Rows[0]["EmailID"].ToString();
            }
           
            mailMessage.CC.Add(strCC);
            mailMessage.To.Add(dtFirst.Rows[0]["EmailID"].ToString());
          
            mailMessage.CC.Add("Treasuryteam@marico.com");
            mailMessage.From = new MailAddress("AuthorisedSignatories@maricoindia.net", "Authorised Signatories");
            mailMessage.Subject = "Approval for Addition in Authorised Signatory with Request ID - " + AdditionID;
            mailMessage.Body = myString.ToString();
            mailMessage.IsBodyHtml = true;
            strclient.Send(mailMessage);
        }
        public void SendTRMail(string ApproverID, string AccountNo, string AdditionID, string Location, string LocationDetails, string InitiatorID, string strcomments)
        {
            DataTable dTR = new DataTable();
            DataTable dTApp = new DataTable();
            DataTable dTSup = new DataTable();
            DataTable dtInit = new DataTable();
            DataTable dtFirst = new DataTable();
            dTR = objBAL.getNextApprover(Location, LocationDetails, "TR");
            dtFirst = objBAL.getEmpDetails(ApproverID);
            dtInit = objBAL.getEmpDetails(InitiatorID);
            dTSup = objBAL.getInitiatorSupervisor(InitiatorID);
            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/AuthorisedSignatory/MailerTemplates/Addition.htm"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("[@UserName]", dtInit.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@RequestID]", AdditionID);
            myString = myString.Replace("[@Location]", Location);
            myString = myString.Replace("[@AccountNo]", AccountNo);
            myString = myString.Replace("[@ApproverName]", dtFirst.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@ApproverComment]", strcomments);
            myString = myString.Replace("[@EmpDetails]", "");
            MailMessage mailMessage = new MailMessage();
            string strCC = "";
            strCC = dTSup.Rows[0]["EmailID"].ToString() + "," + dtInit.Rows[0]["EmailID"].ToString() + "," + dtFirst.Rows[0]["EmailID"].ToString();
         
            mailMessage.CC.Add(strCC);
            mailMessage.To.Add(dTR.Rows[0]["EmailID"].ToString());
           
            mailMessage.CC.Add("Treasuryteam@marico.com");
            mailMessage.From = new MailAddress("AuthorisedSignatories@maricoindia.net", "Authorised Signatories");
            mailMessage.Subject = "Approval for Addition in Authorised Signatory with Request ID - " + AdditionID;
            mailMessage.Body = myString.ToString();
            mailMessage.IsBodyHtml = true;
            strclient.Send(mailMessage);
        }
        public void SendTRapproveMail(string ApproverID, string AccountNo, string AdditionID, string Location, string LocationDetails, string InitiatorID, string strcomments)
        {
            DataTable dTR = new DataTable();
            DataTable dTApp = new DataTable();
            DataTable dTSup = new DataTable();
            DataTable dtInit = new DataTable();
            DataTable dtFirst = new DataTable();
            dTR = objBAL.getEmpDetails(ApproverID);
            dtInit = objBAL.getEmpDetails(InitiatorID);
            dTSup = objBAL.getInitiatorSupervisor(InitiatorID);
            if (Location != "HO")
            {
                dtFirst = objBAL.getNextApprover(Location, LocationDetails, "CH");
            }

            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/AuthorisedSignatory/MailerTemplates/Addition.htm"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("[@UserName]", dtInit.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@RequestID]", AdditionID);
            myString = myString.Replace("[@Location]", Location);
            myString = myString.Replace("[@AccountNo]", AccountNo);
            myString = myString.Replace("[@ApproverName]", dTR.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@ApproverComment]", strcomments);
            myString = myString.Replace("[@EmpDetails]", "");
            MailMessage mailMessage = new MailMessage();
            string strCC = "";
            if (Location != "HO")
            {
                strCC = dTSup.Rows[0]["EmailID"].ToString() + "," + dtFirst.Rows[0]["EmailID"].ToString() + "," + dTR.Rows[0]["EmailID"].ToString();
            }
            else
            {
                strCC = dTSup.Rows[0]["EmailID"].ToString() + "," + dTR.Rows[0]["EmailID"].ToString();
            }
           
            mailMessage.CC.Add(strCC);
            mailMessage.To.Add(dtInit.Rows[0]["EmailID"].ToString());
            
            mailMessage.CC.Add("Treasuryteam@marico.com");
            mailMessage.From = new MailAddress("AuthorisedSignatories@maricoindia.net", "Authorised Signatories");
            mailMessage.Subject = "Approval for Addition in Authorised Signatory with Request ID - " + AdditionID;
            mailMessage.Body = myString.ToString();
            mailMessage.IsBodyHtml = true;
            strclient.Send(mailMessage);
        }
        public void SendInitiatorMailDeletion(string InitiatorID, string AccountNo, string AdditionID, string Location, string LocationDetails, string Empdetails)
        {
            DataTable dTemp = new DataTable();
            DataTable dEmp = new DataTable();
            DataTable dtFirst = new DataTable();
            dTemp = objBAL.getInitiatorSupervisor(InitiatorID);
            dEmp = objBAL.getEmpDetails(InitiatorID);
            if (Location == "HO")
            {
                dtFirst = objBAL.getNextApprover(Location, LocationDetails, "TR");
            }
            else
            {
                dtFirst = objBAL.getNextApprover(Location, LocationDetails, "CH");
            }
            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/AuthorisedSignatory/MailerTemplates/Deletion.htm"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("[@UserName]", dEmp.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@RequestID]", AdditionID);
            myString = myString.Replace("[@Location]", Location);
            myString = myString.Replace("[@AccountNo]", AccountNo);
            myString = myString.Replace("[@ApproverName]", "-");
            myString = myString.Replace("[@ApproverComment]", "-");
            myString = myString.Replace("[@EmpDetails]", Empdetails);
            MailMessage mailMessage = new MailMessage();
            
            mailMessage.To.Add(dTemp.Rows[0]["EmailID"].ToString());
            mailMessage.CC.Add(dtFirst.Rows[0]["EmailID"].ToString());
            mailMessage.CC.Add("Treasuryteam@marico.com");
           
            mailMessage.From = new MailAddress("AuthorisedSignatories@maricoindia.net", "Authorised Signatories");
            mailMessage.Subject = "Approval for Deletion in Authorised Signatory with Request ID - " + AdditionID;
            mailMessage.Body = myString.ToString();
            mailMessage.IsBodyHtml = true;
            strclient.Send(mailMessage);
        }
        public void SendNextApproveMailDeletion(string ApproverID, string AccountNo, string AdditionID, string Location, string LocationDetails, string InitiatorID, string strcomments)
        {
            DataTable dtFirst = new DataTable();
            DataTable dtInit = new DataTable();
            DataTable dSup = new DataTable();
            DataTable dTR = new DataTable();
            if (Location == "HO")
            {
                dtFirst = objBAL.getNextApprover(Location, LocationDetails, "TR");
            }
            else
            {
                dtFirst = objBAL.getNextApprover(Location, LocationDetails, "CH");
                dTR = objBAL.getNextApprover(Location, LocationDetails, "TR");
            }

            dtInit = objBAL.getEmpDetails(InitiatorID);
            dSup = objBAL.getEmpDetails(ApproverID);

            //myString = File.ReadAllText("D:/Finance Authorization/Code/FinAuth/FinAuth/AuthorisedSignatory/MailerTemplates/Addition.htm");
            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/AuthorisedSignatory/MailerTemplates/Deletion.htm"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("[@UserName]", dtInit.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@RequestID]", AdditionID);
            myString = myString.Replace("[@Location]", Location);
            myString = myString.Replace("[@AccountNo]", AccountNo);
            myString = myString.Replace("[@ApproverName]", dSup.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@ApproverComment]", strcomments);
            myString = myString.Replace("[@EmpDetails]", "");
            MailMessage mailMessage = new MailMessage();
            string strCC = "";
            if (Location == "HO")
            {
                strCC = dSup.Rows[0]["EmailID"].ToString() + "," + dtInit.Rows[0]["EmailID"].ToString();
            }
            else
            {
                strCC = dTR.Rows[0]["EmailID"].ToString() + "," + dSup.Rows[0]["EmailID"].ToString() + "," + dtInit.Rows[0]["EmailID"].ToString();
            }
           
            mailMessage.CC.Add(strCC);
            mailMessage.To.Add(dtFirst.Rows[0]["EmailID"].ToString());
          
            mailMessage.CC.Add("Treasuryteam@marico.com");
            mailMessage.From = new MailAddress("AuthorisedSignatories@maricoindia.net", "Authorised Signatories");
            mailMessage.Subject = "Approval for Deletion in Authorised Signatory with Request ID - " + AdditionID;
            mailMessage.Body = myString.ToString();
            mailMessage.IsBodyHtml = true;
            strclient.Send(mailMessage);
        }
        public void SendTRMailDeletion(string ApproverID, string AccountNo, string AdditionID, string Location, string LocationDetails, string InitiatorID, string strcomments)
        {
            DataTable dTR = new DataTable();
            DataTable dTApp = new DataTable();
            DataTable dTSup = new DataTable();
            DataTable dtInit = new DataTable();
            DataTable dtFirst = new DataTable();
            dTR = objBAL.getNextApprover(Location, LocationDetails, "TR");
            dtFirst = objBAL.getEmpDetails(ApproverID);
            dtInit = objBAL.getEmpDetails(InitiatorID);
            dTSup = objBAL.getInitiatorSupervisor(InitiatorID);
            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/AuthorisedSignatory/MailerTemplates/Deletion.htm"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("[@UserName]", dtInit.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@RequestID]", AdditionID);
            myString = myString.Replace("[@Location]", Location);
            myString = myString.Replace("[@AccountNo]", AccountNo);
            myString = myString.Replace("[@ApproverName]", dtFirst.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@ApproverComment]", strcomments);
            myString = myString.Replace("[@EmpDetails]", "");
            MailMessage mailMessage = new MailMessage();
            string strCC = "";
            strCC = dTSup.Rows[0]["EmailID"].ToString() + "," + dtInit.Rows[0]["EmailID"].ToString() + "," + dtFirst.Rows[0]["EmailID"].ToString();
           
            mailMessage.CC.Add(strCC);
            mailMessage.To.Add(dTR.Rows[0]["EmailID"].ToString());
           
            mailMessage.CC.Add("Treasuryteam@marico.com");
            mailMessage.From = new MailAddress("AuthorisedSignatories@maricoindia.net", "Authorised Signatories");
            mailMessage.Subject = "Approval for Deletion in Authorised Signatory with Request ID - " + AdditionID;
            mailMessage.Body = myString.ToString();
            mailMessage.IsBodyHtml = true;
            strclient.Send(mailMessage);
        }
        public void SendTRapproveMailDeletion(string ApproverID, string AccountNo, string AdditionID, string Location, string LocationDetails, string InitiatorID, string strcomments)
        {
            DataTable dTR = new DataTable();
            DataTable dTApp = new DataTable();
            DataTable dTSup = new DataTable();
            DataTable dtInit = new DataTable();
            DataTable dtFirst = new DataTable();
            dTR = objBAL.getEmpDetails(ApproverID);
            dtInit = objBAL.getEmpDetails(InitiatorID);
            dTSup = objBAL.getInitiatorSupervisor(InitiatorID);
            if (Location != "HO")
            {
                dtFirst = objBAL.getNextApprover(Location, LocationDetails, "CH");
            }

            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/AuthorisedSignatory/MailerTemplates/Deletion.htm"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("[@UserName]", dtInit.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@RequestID]", AdditionID);
            myString = myString.Replace("[@Location]", Location);
            myString = myString.Replace("[@AccountNo]", AccountNo);
            myString = myString.Replace("[@ApproverName]", dTR.Rows[0]["EmpName"].ToString());
            myString = myString.Replace("[@ApproverComment]", strcomments);
            myString = myString.Replace("[@EmpDetails]", "");
            MailMessage mailMessage = new MailMessage();
            string strCC = "";
            if (Location != "HO")
            {
                strCC = dTSup.Rows[0]["EmailID"].ToString() + "," + dtFirst.Rows[0]["EmailID"].ToString() + "," + dTR.Rows[0]["EmailID"].ToString();
            }
            else
            {
                strCC = dTSup.Rows[0]["EmailID"].ToString() + "," + dTR.Rows[0]["EmailID"].ToString();
            }
            
            mailMessage.CC.Add(strCC);
            mailMessage.To.Add(dtInit.Rows[0]["EmailID"].ToString());
           
            mailMessage.CC.Add("Treasuryteam@marico.com");
            mailMessage.From = new MailAddress("AuthorisedSignatories@maricoindia.net", "Authorised Signatories");
            mailMessage.Subject = "Approval for Deletion in Authorised Signatory with Request ID - " + AdditionID;
            mailMessage.Body = myString.ToString();
            mailMessage.IsBodyHtml = true;
            strclient.Send(mailMessage);
        }
        public void RejectMailAddition(string AdditionID, string RejectedID, string InitiatorID, string Location, string LocationDetails)
        {
            DataTable dTR = new DataTable();
            DataTable dtInit = new DataTable();
            DataTable dtRej = new DataTable();
            DataTable dtFirst = new DataTable();
            DataTable dTSup = new DataTable();
            dtInit = objBAL.getEmpDetails(InitiatorID);
            dtRej = objBAL.getEmpDetails(RejectedID);
            dTSup = objBAL.getInitiatorSupervisor(InitiatorID);
            dTR = objBAL.getNextApprover(Location, LocationDetails, "TR");
            if (Location != "HO")
            {
                dtFirst = objBAL.getNextApprover(Location, LocationDetails, "CH");
            }
            string strbody = "";
            strbody = "  <html><head><title></title></head><body><table width='100%'><tr><td style='text-align: Left'><font face='Verdana' size='2'>Hi,<br><br>The Authorised Signatory Addition Request No <b>" + AdditionID + "</b>  has been Rejected by<b> " + dtRej.Rows[0]["EmpName"].ToString() + "</b></font></td></tr><tr><td> ";
            strbody = strbody + "</table></td></tr></table><br><font face='Verdana' size='2'><b>Regards,</b></br>Secretarial Team</font></br> </body></html>";
            MailMessage mailMessage = new MailMessage();
            string strCC = "";
            if (Location != "HO")
            {
                strCC = dTSup.Rows[0]["EmailID"].ToString() + "," + dTR.Rows[0]["EmailID"].ToString() + "," + dtFirst.Rows[0]["EmailID"].ToString();
            }
            else
            {
                strCC = dTSup.Rows[0]["EmailID"].ToString() + "," + dTR.Rows[0]["EmailID"].ToString();
            }
           
            mailMessage.CC.Add(strCC);
            mailMessage.To.Add(dtInit.Rows[0]["EmailID"].ToString());
          
            mailMessage.CC.Add("Treasuryteam@marico.com");
            mailMessage.From = new MailAddress("AuthorisedSignatories@maricoindia.net", "Authorised Signatories");
            mailMessage.Subject = "Authorised Signatory Addition Request No :" + AdditionID + " has been Rejected.";
            mailMessage.Body = strbody.ToString();
            mailMessage.IsBodyHtml = true;
             strclient.Send(mailMessage);
        }
        public void RejectMailDeletion(string AdditionID, string RejectedID, string InitiatorID, string Location, string LocationDetails)
        {
            DataTable dTR = new DataTable();
            DataTable dtInit = new DataTable();
            DataTable dtRej = new DataTable();
            DataTable dtFirst = new DataTable();
            DataTable dTSup = new DataTable();
            dtInit = objBAL.getEmpDetails(InitiatorID);
            dtRej = objBAL.getEmpDetails(RejectedID);
            dTSup = objBAL.getInitiatorSupervisor(InitiatorID);
            dTR = objBAL.getNextApprover(Location, LocationDetails, "TR");
            if (Location != "HO")
            {
                dtFirst = objBAL.getNextApprover(Location, LocationDetails, "CH");
            }
            string strbody = "";
            strbody = "  <html><head><title></title></head><body><table width='100%'><tr><td style='text-align: Left'><font face='Verdana' size='2'>Hi,<br><br>The Authorised Signatory Deletion Request No <b>" + AdditionID + "</b>  has been Rejected by<b> " + dtRej.Rows[0]["EmpName"].ToString() + "</b></font></td></tr><tr><td> ";
            strbody = strbody + "</table></td></tr></table><br><font face='Verdana' size='2'><b>Regards,</b></br>Secretarial Team</font></br> </body></html>";
            MailMessage mailMessage = new MailMessage();
            string strCC = "";
            if (Location != "HO")
            {
                strCC = dTSup.Rows[0]["EmailID"].ToString() + "," + dTR.Rows[0]["EmailID"].ToString() + "," + dtFirst.Rows[0]["EmailID"].ToString();
            }
            else
            {
                strCC = dTSup.Rows[0]["EmailID"].ToString() + "," + dTR.Rows[0]["EmailID"].ToString();
            }
           
            mailMessage.CC.Add(strCC);
            mailMessage.To.Add(dtInit.Rows[0]["EmailID"].ToString());
            
            mailMessage.CC.Add("Treasuryteam@marico.com");
            mailMessage.From = new MailAddress("AuthorisedSignatories@maricoindia.net", "Authorised Signatories");
            mailMessage.Subject = "Authorised Signatory Deletion Request No :" + AdditionID + " has been Rejected.";
            mailMessage.Body = strbody.ToString();
            mailMessage.IsBodyHtml = true;
             strclient.Send(mailMessage);
        }
    }
}