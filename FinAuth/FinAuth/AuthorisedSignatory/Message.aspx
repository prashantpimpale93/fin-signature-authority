﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AS.Master" AutoEventWireup="true" CodeBehind="Message.aspx.cs" Inherits="FinAuth.AuthorisedSignatory.Message" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheading" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RAM" runat="server" SkinID="Web20">
    </telerik:RadAjaxManager>
    <table style="width: 100%;">
        <tr>
            <td style="text-align: center;">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <asp:Label ID="lblMessage" Font-Size="14px" ForeColor="Green" Font-Bold="true" runat="server" Text=""></asp:Label>
                <asp:Button ID="btnDownload" runat="server" Text="PDF Download" Visible="false" OnClick="btnDownload_Click" />
                <br/>
                 <br/>
                 <asp:Label ID="lblmsg" Font-Size="18px" ForeColor="red" Font-Bold="true" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>

            <td>

                <rsweb:ReportViewer ID="rptViewer" runat="server" Font-Names="Verdana" Font-Size="8pt"
                    InteractiveDeviceInfos="(Collection)" ProcessingMode="Remote" Style="margin-top: 0px"
                    WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px" Height="1150px" Visible="false"
                    ShowToolBar="false" ShowParameterPrompts="false">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>
