﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinAuth.BAL;
using FinAuth.BO;
using System.Data;
using Telerik.Web.UI;
namespace FinAuth.AuthorisedSignatory
{
    public partial class DeletionSignatories : System.Web.UI.Page
    {
        SendASmail objMail = new SendASmail();
        clsBAL objBAL = new clsBAL();
        clsBO objBO = new clsBO();
        string emailcontent = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            btnSubmit.Attributes.Add("onclick", "javascript:" + btnSubmit.ClientID + ".disabled=true;" + ClientScript.GetPostBackEventReference(btnSubmit, ""));
            if (!IsPostBack)
            {
                DataTable dTemp = new DataTable();
                rcmbBank.DataSource = dTemp;
                rcmbBank.DataTextField = "BankName";
                rcmbBank.DataValueField = "BankName";
                rcmbBank.DataBind();
                rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
                if (Session["Delete"] != null)
                {
                    Session["Delete"] = null;
                }
                if (ViewState["Members"] != null)
                {
                    ViewState["Members"] = null;
                }
                rcmbRegion.Enabled = false;
                rcmbPlant.Enabled = false;
                bindPlant();
                bindRegion();
                bindAddDetails();
                if (!string.IsNullOrEmpty(Request.QueryString["DeletionID"]))
                {
                    FillDetails();
                }
            }
        }
        private void FillDetails()
        {
            DataTable dt = objBAL.getAllDeletionDetails(Request.QueryString["DeletionID"]);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["Locationtype"].ToString() == "P")
                {
                    rdoPlant.Checked = true;
                    rcmbPlant.SelectedItem.Text = dt.Rows[0]["LocationDetails"].ToString();
                    rcmbRegion.SelectedValue = "0";
                    rdoRegion.Checked = false;
                    rdoHO.Checked = false;
                    rcmbRegion.Enabled = false;
                    rcmbPlant.Enabled = true;
                }
                else if (dt.Rows[0]["Locationtype"].ToString() == "R")
                {
                    rdoRegion.Checked = true;
                    rcmbRegion.SelectedItem.Text = dt.Rows[0]["LocationDetails"].ToString();
                    rcmbPlant.SelectedValue = "0";
                    rdoPlant.Checked = false;
                    rdoHO.Checked = false;
                    rcmbPlant.Enabled = false;
                    rcmbRegion.Enabled = true;
                }
                else if (dt.Rows[0]["Locationtype"].ToString() == "H")
                {
                    rdoHO.Checked = true;
                    rcmbRegion.SelectedValue = "0";
                    rcmbPlant.SelectedValue = "0";
                    rdoRegion.Checked = false;
                    rdoPlant.Checked = false;
                    rcmbPlant.Enabled = false;
                    rcmbRegion.Enabled = false;
                }
                DataTable dTemp = objBAL.getBanklist(dt.Rows[0]["LocationDetails"].ToString());
                if (dTemp.Rows.Count > 0)
                {
                    rcmbBank.DataSource = dTemp;
                    rcmbBank.DataTextField = "BankName";
                    rcmbBank.DataValueField = "BankName";
                    rcmbBank.DataBind();
                    rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
                }
               
                DataTable dtAcc = new DataTable();
                dtAcc = objBAL.getAccountDetails(dt.Rows[0]["AccountNO"].ToString());
                if (dtAcc.Rows.Count > 0)
                {
                    lblBankValue.Text = dtAcc.Rows[0]["BankName"].ToString();
                    rcmbBank.SelectedValue = dtAcc.Rows[0]["BankName"].ToString();
                    lblBankAddValue.Text = dtAcc.Rows[0]["BankAddress"].ToString();
                }
                 dTemp = objBAL.getAccountNos(rcmbBank.SelectedItem.Text);
                if (dTemp.Rows.Count > 0)
                {
                    rcmbAccountNo.DataSource = dTemp;
                    rcmbAccountNo.DataTextField = "AccountNo";
                    rcmbAccountNo.DataValueField = "AccountNo";
                    rcmbAccountNo.DataBind();
                    rcmbAccountNo.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Account No--", "0"));
                }

                rcmbAccountNo.SelectedValue = dt.Rows[0]["AccountNO"].ToString();
                rcmbDetailAddition.SelectedValue = dt.Rows[0]["DeletionDetails"].ToString();
                txtComments.Text = dt.Rows[0]["Comments"].ToString();
                if (Session["EmpID"].ToString() != dt.Rows[0]["CreatedBy"].ToString())
                {
                    DisablePageControls(false);
                }
            }
        }
        public void DisablePageControls(bool status)
        {
           
                foreach (Control ctrl in Master.FindControl("ContentPlaceHolder1").Controls)
                {
                    if (ctrl is RadComboBox)
                        ((RadComboBox)ctrl).Enabled = status;
                    else if (ctrl is Button)
                        ((Button)ctrl).Enabled = status;
                    else if (ctrl is RadButton)
                        ((RadButton)ctrl).Enabled = status;
                    else if (ctrl is RadioButtonList)
                        ((RadioButtonList)ctrl).Enabled = status;
                    else if (ctrl is ImageButton)
                        ((ImageButton)ctrl).Enabled = status;
                    else if (ctrl is CheckBox)
                        ((CheckBox)ctrl).Enabled = status;
                    else if (ctrl is CheckBoxList)
                        ((CheckBoxList)ctrl).Enabled = status;
                    else if (ctrl is DropDownList)
                        ((DropDownList)ctrl).Enabled = status;
                    else if (ctrl is HyperLink)
                        ((HyperLink)ctrl).Enabled = status;
                    else if (ctrl is TextBox)
                        ((TextBox)ctrl).Enabled = status;
                }
           
        }  
        private void bindPlant()
        {
            DataTable dtPlant = new DataTable();
            dtPlant = objBAL.getPlant();
            if (dtPlant.Rows.Count > 0)
            {
                rcmbPlant.DataSource = dtPlant;
                rcmbPlant.DataTextField = "PlantName";
                rcmbPlant.DataValueField = "ID";
                rcmbPlant.DataBind();
                rcmbPlant.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Plant--", "0"));
            }
        }
        private void bindRegion()
        {
            DataTable dtReg = new DataTable();
            dtReg = objBAL.getRegion();
            if (dtReg.Rows.Count > 0)
            {
                rcmbRegion.DataSource = dtReg;
                rcmbRegion.DataTextField = "RegionName";
                rcmbRegion.DataValueField = "ID";
                rcmbRegion.DataBind();
                rcmbRegion.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Region--", "0"));
            }
        }
        private void bindAddDetails()
        {
            DataTable dtAdd = new DataTable();
            dtAdd = objBAL.getAddDetails();
            if (dtAdd.Rows.Count > 0)
            {
                rcmbDetailAddition.DataSource = dtAdd;
                rcmbDetailAddition.DataTextField = "ReasonDetails";
                rcmbDetailAddition.DataValueField = "ID";
                rcmbDetailAddition.DataBind();
               
            }
        }
        protected void rcmbAccountNo_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            int k = objBAL.removeDeletionMemberDetails(Request.QueryString["DeletionID"]);
            DataTable dtAcc = new DataTable();
            dtAcc = objBAL.getAccountDetails(rcmbAccountNo.SelectedValue);
            if (dtAcc.Rows.Count > 0)
            {
                lblBankValue.Text = dtAcc.Rows[0]["BankName"].ToString();
                lblBankAddValue.Text = dtAcc.Rows[0]["BankAddress"].ToString();
            }
            rgExistingSignatories.Rebind();
        }
        private string GetUniqueID()
        {
            DataTable dtTemp = new DataTable();
            dtTemp = objBAL.getUniqueID();
            string strAdditionID = "";
            string strID = "";
            if (dtTemp.Rows[0]["DeletionID"].ToString().Trim() == "0")
            {
                strAdditionID = "DS00000001";
            }
            else
            {
                strID = string.Concat("00000000", (Convert.ToInt32(dtTemp.Rows[0]["DeletionID"].ToString().Remove(0, 2)) + 1).ToString());
                strAdditionID = "DS" + strID.Substring(strID.Length - 8);
            }
            return strAdditionID;
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string strAdditionID = "";
                string strLocationType = string.Empty;
                if (rdoPlant.Checked)
                {
                    objBO.LocationType = "P";
                    strLocationType = "Plant";
                    objBO.LocationDetails = rcmbPlant.SelectedItem.Text.Trim();
                }
                else if (rdoRegion.Checked)
                {
                    objBO.LocationType = "R";
                    strLocationType = "Region";
                    objBO.LocationDetails = rcmbRegion.SelectedItem.Text.Trim();
                }
                else if (rdoHO.Checked)
                {
                    objBO.LocationType = "H";
                    strLocationType = "HO";
                    objBO.LocationDetails = "HO";
                }
                objBO.AccountNO = rcmbAccountNo.SelectedValue;
                objBO.AdditionDetails = rcmbDetailAddition.SelectedValue;
                objBO.Comments = txtComments.Text;
                objBO.CreatedBy = Session["EmpID"].ToString();
                objBO.SaveDraftFlag = "N";
                int k = 0;
                int k1 = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["DeletionID"]))
                {
                    strAdditionID = Request.QueryString["DeletionID"];
                    objBO.AdditionID = strAdditionID;
                    k = objBAL.removeDeletionMemberDetails(strAdditionID);
                    InsertAuthorizedSignatoryDetails(strAdditionID);
                    if (!string.IsNullOrEmpty(Session["Delete"].ToString()))
                    {
                        if (Session["Delete"].ToString() == "Delete")
                        {
                            return;
                        }
                    }
                    k1 = objBAL.UpdateDeletionDetails(objBO);
                }
                else
                {
                    strAdditionID = GetUniqueID();
                    objBO.AdditionID = strAdditionID;
                    InsertAuthorizedSignatoryDetails(strAdditionID);
                    if (!string.IsNullOrEmpty(Session["Delete"].ToString()))
                    {
                        if (Session["Delete"].ToString() == "Delete")
                        {
                            return;
                        }
                    }
                    k1 = objBAL.insertDeletionDetails(objBO);
                }
                GenerateApprovalFlow(objBO.LocationDetails, strLocationType, strAdditionID);
                if (k1 > 0)
                {
                    string Finalemail = @"<table class='MsoNormalTable' border='1'><tr><td>Sr. No.</td><td>Bank Name</td><td>Account No.</td><td>Employee code</td><td>Member Name</td><td>Group</td><td>Type</td><td>Location</td></tr>" + emailcontent + "</table>";
                    objMail.SendInitiatorMailDeletion(Session["EmpID"].ToString(), rcmbAccountNo.SelectedValue, strAdditionID, strLocationType, objBO.LocationDetails, Finalemail);
                    if (!string.IsNullOrEmpty(Request.QueryString["Request"]))
                    {
                        if (Request.QueryString["Request"] == "Resubmit")
                        {
                            // RAM.Alert("Details Updated Successfully! Request id is " + strAdditionID);
                            Response.Redirect("Message.aspx?Msg=Details Updated Successfully! Request ID is " + strAdditionID + "&ID=N&Type=N");
                        }
                        else if (Request.QueryString["Request"] == "Draft")
                        {
                            Response.Redirect("Message.aspx?Msg=Details Inserted Successfully! Request ID is " + strAdditionID + "&ID=N&Type=N");
                        }
                    }
                    else
                    {
                        //RAM.Alert("Details Inserted Successfully! Request id is " + strAdditionID);
                        Response.Redirect("Message.aspx?Msg=Details Inserted Successfully! Request ID is " + strAdditionID + "&ID=N&Type=N");
                    }
                }
            }
            catch (Exception ex)
            {
                RAM.Alert("ERROR InsertDeletionDetails- " + ex.ToString());
            }
        }
        private void InsertAddMemberDetails(string strAdditionID)
        {
            try
            {
                if (ViewState["Members"] != null)
                {
                    DataTable dt = (DataTable)ViewState["Members"];
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objBO.AdditionID = strAdditionID;
                            objBO.EmpID = dt.Rows[i]["EmpID"].ToString();
                            objBO.Type = dt.Rows[i]["Type"].ToString();
                            int N = objBAL.InsertAddMemberDetails(objBO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RAM.Alert("ERROR in InsertAddMemberDetails - " + ex.ToString());
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/AuthorisedSignatory/DeletionSignatories.aspx");
            //ClearALL();
        }
        private void ClearALL()
        {
            rdoPlant.Checked = true;
            rdoRegion.Checked = false;
            rdoHO.Checked = false;
            rcmbPlant.SelectedValue = "0";
            rcmbRegion.SelectedValue = "0";
            rcmbAccountNo.ClearSelection();
            lblBankAddValue.Text = string.Empty;
            lblBankValue.Text = string.Empty;
            rcmbDetailAddition.SelectedValue = "0";
            txtComments.Text = string.Empty;
        }
        protected void rdoPlant_CheckedChanged(object sender, EventArgs e)
        {
            int k = objBAL.removeDeletionMemberDetails(Request.QueryString["DeletionID"]);
            rcmbRegion.SelectedValue = "0";
            rdoRegion.Checked = false;
            rdoHO.Checked = false;
            rcmbRegion.Enabled = false;
            rcmbPlant.Enabled = true;
            lblBankValue.Text = "";
            lblBankAddValue.Text = "";
            DataTable dTemp = new DataTable();
            rcmbBank.DataSource = dTemp;
            rcmbBank.DataTextField = "BankName";
            rcmbBank.DataValueField = "BankName";
            rcmbBank.DataBind();
            rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
            rgExistingSignatories.Rebind();
        }
        protected void rdoRegion_CheckedChanged(object sender, EventArgs e)
        {
            int k = objBAL.removeDeletionMemberDetails(Request.QueryString["DeletionID"]);
            DataTable dTemp = new DataTable();
            rcmbPlant.SelectedValue = "0";
            rdoPlant.Checked = false;
            rdoHO.Checked = false;
            rcmbPlant.Enabled = false;
            rcmbRegion.Enabled = true;
            rcmbBank.DataSource = dTemp;
            rcmbBank.DataTextField = "BankName";
            rcmbBank.DataValueField = "BankName";
            rcmbBank.DataBind();
            rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
            lblBankAddValue.Text = string.Empty;
            lblBankValue.Text = string.Empty;
            rgExistingSignatories.Rebind();
        }
        protected void rdoHO_CheckedChanged(object sender, EventArgs e)
        {
            int k = objBAL.removeDeletionMemberDetails(Request.QueryString["DeletionID"]);
            bindPlant();
            bindRegion();
            rcmbRegion.SelectedValue = "0";
            rcmbPlant.SelectedValue = "0";
            rdoRegion.Checked = false;
            rdoPlant.Checked = false;
            rcmbPlant.Enabled = false;
            rcmbRegion.Enabled = false;
            DataTable dTemp = objBAL.getBanklist("HO");
            if (dTemp.Rows.Count > 0)
            {
                rcmbBank.DataSource = dTemp;
                rcmbBank.DataTextField = "BankName";
                rcmbBank.DataValueField = "BankName";
                rcmbBank.DataBind();
                rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
            }
            lblBankAddValue.Text = string.Empty;
            lblBankValue.Text = string.Empty;
            rgExistingSignatories.Rebind();

        }
        protected void btnDraft_Click(object sender, EventArgs e) 
        {
            try
            {
                string strAdditionID = "";
                if (rdoPlant.Checked)
                {
                    objBO.LocationType = "P";
                    objBO.LocationDetails = rcmbPlant.SelectedItem.Text.Trim();
                }
                else if (rdoRegion.Checked)
                {
                    objBO.LocationType = "R";
                    objBO.LocationDetails = rcmbRegion.SelectedItem.Text.Trim();
                }
                else if (rdoHO.Checked)
                {
                    objBO.LocationType = "H";
                    objBO.LocationDetails = "HO";
                }
                objBO.AccountNO = rcmbAccountNo.SelectedValue;
                objBO.AdditionDetails = rcmbDetailAddition.SelectedValue;
                objBO.Comments = txtComments.Text;
                objBO.CreatedBy = Session["EmpID"].ToString();
                objBO.SaveDraftFlag = "Y";
                int k = 0;
                int k1 = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["DeletionID"]))
                {
                    strAdditionID = Request.QueryString["DeletionID"];
                    objBO.AdditionID = strAdditionID;
                    k = objBAL.removeDeletionMemberDetails(Request.QueryString["DeletionID"]);
                    InsertAuthorizedSignatoryDetails(strAdditionID);
                    if (!string.IsNullOrEmpty(Session["Delete"].ToString()))
                    {
                        if (Session["Delete"].ToString() == "Delete")
                        {
                            return;
                        }
                    }
                    k1 = objBAL.UpdateDeletionDetails(objBO);
                }
                else
                {
                    strAdditionID = GetUniqueID();
                    objBO.AdditionID = strAdditionID;
                    InsertAuthorizedSignatoryDetails(strAdditionID);
                    if (!string.IsNullOrEmpty(Session["Delete"].ToString()))
                    {
                        if (Session["Delete"].ToString() == "Delete")
                        {
                            return;
                        }
                    }
                    k1 = objBAL.insertDeletionDetails(objBO);
                }
                InsertAddMemberDetails(strAdditionID);
                if (k1 > 0)
                {
                    RAM.Alert("Details Saved successfully ! Request id is " + strAdditionID);
                    //ClearALL();
                }
            }
            catch (Exception ex)
            {
                RAM.Alert("ERROR while Drafting- " + ex.ToString());
            }
        }
        protected void rcmbPlant_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            int k = objBAL.removeDeletionMemberDetails(Request.QueryString["DeletionID"]);
            DataTable dTemp = objBAL.getBanklist(rcmbPlant.SelectedItem.Text.Trim());
            if (dTemp.Rows.Count > 0)
            {
                rcmbBank.DataSource = dTemp;
                rcmbBank.DataTextField = "BankName";
                rcmbBank.DataValueField = "BankName";
                rcmbBank.DataBind();
                rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
            }
            lblBankAddValue.Text = string.Empty;
            lblBankValue.Text = string.Empty;
            rgExistingSignatories.Rebind();
        }
        protected void rcmbRegion_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            int k = objBAL.removeDeletionMemberDetails(Request.QueryString["DeletionID"]);
            DataTable dTR = objBAL.getBanklist(rcmbRegion.SelectedItem.Text.Trim());
            if (dTR.Rows.Count > 0)
            {
                rcmbBank.DataSource = dTR;
                rcmbBank.DataTextField = "BankName";
                rcmbBank.DataValueField = "BankName";
                rcmbBank.DataBind();
                rcmbBank.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Bank--", "0"));
            }
            lblBankAddValue.Text = string.Empty;
            lblBankValue.Text = string.Empty;
            rgExistingSignatories.Rebind();
        }
        protected void rgExistingSignatories_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataTable dtEx = new DataTable();
            dtEx = objBAL.getExdetailsDeletion(rcmbAccountNo.SelectedValue);
            if (dtEx.Rows.Count > 0)
            {
                rgExistingSignatories.DataSource = dtEx;
            }
            else
            {
                rgExistingSignatories.DataSource = dtEx;
            }
        }
        private void InsertAuthorizedSignatoryDetails(string DeletionID)
        {
            try
            {
                DataTable dTemp = new DataTable();
                int count = 0;
                dTemp.Columns.AddRange(new DataColumn[6] 
                {
                  new DataColumn("DeletionID"),new DataColumn("Requiredtype"), new DataColumn("EmpID"), new DataColumn("EmpName"), new DataColumn("Designation"),new DataColumn("Type") 
                });


                GridDataItemCollection dtCol = rgExistingSignatories.Items;
                foreach (GridDataItem item in dtCol)
                {
                    CheckBox chkitem = (CheckBox)item.FindControl("chkitem");
                    if (chkitem.Checked == true)
                    {
                        count++;
                        string strReqFor = item["RequiredType"].Text.Trim();
                        string strEmpID = item["EmpID"].Text.Trim();
                        string strEmpName = item["EmpName"].Text.Trim();
                        string Designation = item["Designation"].Text.Trim();
                        string Type = item["Type"].Text.Trim();
                        dTemp.Rows.Add(DeletionID, strReqFor, strEmpID, strEmpName, Designation, Type);
                        emailcontent = emailcontent + "<tr><td>" + count.ToString() + "</td><td>" + rcmbBank.SelectedValue + "</td><td>" + rcmbAccountNo.SelectedValue + "</td><td>" + strEmpID + "</td><td>" + strEmpName + "</td><td>" + Type + "</td><td>" + strReqFor + "</td><td>" + objBO.LocationDetails + "</td></tr>";
                    }
                }
                if (count == 0)
                {
                    RAM.Alert("At Least One Checkbox Should Be Selected");
                    Session["Delete"] = "Delete";
                    return;
                }
                else
                {
                    Session["Delete"] = "ADD";
                }
                if (dTemp.Rows.Count > 0)
                {
                    for (int i = 0; i < dTemp.Rows.Count; i++)
                    {
                        objBO.DeletionID = dTemp.Rows[i]["DeletionID"].ToString();
                        objBO.RequiredFor = dTemp.Rows[i]["RequiredType"].ToString();
                        objBO.EmpID = dTemp.Rows[i]["EmpID"].ToString();
                        objBO.EmpName = dTemp.Rows[i]["EmpName"].ToString();
                        objBO.Designation = dTemp.Rows[i]["Designation"].ToString();
                        objBO.Type = dTemp.Rows[i]["Type"].ToString();
                        int N = objBAL.InsertDeletionMemberDetails(objBO);
                    }
                }
            }
            catch (Exception ex)
            {
                RAM.Alert("ERROR - " + ex.ToString());
            }
        }
        private void GenerateApprovalFlow(string strLocation, string strLocationtype, string strAdditionID)
        {
            try
            {
                int k = 0;
                DataTable dTemp = new DataTable();
                int seqNo = Convert.ToInt32(Request.QueryString["SeqNo"]);
                int NextseqNo = Convert.ToInt32(Request.QueryString["SeqNo"]) + 1;
                dTemp = objBAL.getInitiatorSupervisor(Session["EmpID"].ToString());
                if (dTemp.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["Request"]))
                    {
                        if (Request.QueryString["Request"] == "Resubmit")
                        {
                            k = objBAL.updateApprovalFlowAdditionResubmit(strAdditionID, Session["EmpID"].ToString(), txtComments.Text, "DELETION", seqNo);
                            if (k > 0)
                            {
                                int n = objBAL.insertApprovalFlowAdditionNextResubmit(strAdditionID, dTemp.Rows[0]["EmpID"].ToString(), Session["EmpID"].ToString(), "DELETION", NextseqNo);
                            }
                        }
                        else
                        {
                            k = objBAL.insertApprovalFlowAddition(strAdditionID, Session["EmpID"].ToString(), txtComments.Text, "DELETION");
                            if (k > 0)
                            {
                                int n = objBAL.insertApprovalFlowAdditionNext(strAdditionID, dTemp.Rows[0]["EmpID"].ToString(), Session["EmpID"].ToString(), "DELETION");
                            }
                        }
                    }
                    else
                    {
                        k = objBAL.insertApprovalFlowAddition(strAdditionID, Session["EmpID"].ToString(), txtComments.Text, "DELETION");
                        if (k > 0)
                        {
                            int n = objBAL.insertApprovalFlowAdditionNext(strAdditionID, dTemp.Rows[0]["EmpID"].ToString(), Session["EmpID"].ToString(), "DELETION");
                        }
                    }
                    int N = objBAL.updateSupervisorStatusDeletion(strAdditionID, "S");
                }
            }
            catch (Exception ex)
            {
                RAM.Alert("Error Generating Approval Flow -" + ex.ToString());
            }
        }

        protected void rgExistingSignatories_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                int count = 0;
                GridDataItem item = (GridDataItem)e.Item;
                string EmpID = item.GetDataKeyValue("EmpID").ToString();
                CheckBox chkitem = (CheckBox)item.FindControl("chkitem");
                if (!string.IsNullOrEmpty(Request.QueryString["DeletionID"]))
                {
                    count = objBAL.getCheckCount(EmpID, Request.QueryString["DeletionID"]);
                    if (count > 0)
                    {
                        chkitem.Checked = true;
                    }
                }
            }
        }

        protected void rcmbBank_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataTable dTemp = objBAL.getAccountNos(rcmbBank.SelectedItem.Text);
            if (dTemp.Rows.Count > 0)
            {
                rcmbAccountNo.DataSource = dTemp;
                rcmbAccountNo.DataTextField = "AccountNo";
                rcmbAccountNo.DataValueField = "AccountNo";
                rcmbAccountNo.DataBind();
                rcmbAccountNo.Items.Insert(0, new Telerik.Web.UI.RadComboBoxItem("--Select Account No--", "0"));
            }

        }
    }

}