﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AS.Master" AutoEventWireup="true" CodeBehind="ViewReport.aspx.cs" Inherits="FinAuth.AuthorisedSignatory.ViewReport" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheading" runat="server">
    View Signatory Report 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RAM" runat="server" SkinID="Web20">
    </telerik:RadAjaxManager>
    <table style="width: 95%; line-height: 15px;" align="center">
        <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" Font-Bold="True" runat="server" Text="Bank "></asp:Label><telerik:RadComboBox ID="rcmbBank" runat="server" AutoPostBack="true" Filter="StartsWith" MaxHeight="250px" OnSelectedIndexChanged="rcmbBank_SelectedIndexChanged">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <asp:Label ID="lblAccNo" Font-Bold="True" runat="server" Text="Account No. "></asp:Label>
                            <telerik:RadComboBox ID="rcmbAccountno" runat="server" Filter="StartsWith" MaxHeight="250px">
                            </telerik:RadComboBox>
                        </td>
                        <td><asp:Label ID="Label2" Font-Bold="True" runat="server" Text="Account type "></asp:Label>
                            <telerik:RadComboBox ID="rcmbtype" runat="server" Filter="StartsWith" MaxHeight="250px">
                                <Items>
                                    <telerik:RadComboBoxItem Text="All" Value="0" />
                                     <telerik:RadComboBoxItem Text="Offline" Value="Offline" />
                                     <telerik:RadComboBoxItem Text="Online" Value="Online" />
                                </Items>
                            </telerik:RadComboBox></td>
                        <td><asp:Label ID="Label3" Font-Bold="True" runat="server" Text="Signatory type "></asp:Label>
                            <telerik:RadComboBox ID="rcmbRequest" runat="server" Filter="StartsWith" MaxHeight="250px">
                                <Items>
                                    <telerik:RadComboBoxItem Text="--Select--" Value="0" />
                                     <telerik:RadComboBoxItem Text="Addtion" Value="Addtion" />
                                     <telerik:RadComboBoxItem Text="Deletion" Value="Deletion" />
                                </Items>
                            </telerik:RadComboBox></td>
                        <td><asp:Button ID="btnView" runat="server" Text="View" Width="100px" OnClick="btnView_Click"  /></td>
                    </tr>
                </table>


            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <telerik:RadGrid ID="rgAdditionSignatoriesReport" runat="server" Width="100%" AllowPaging="true" PageSize="20" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="rgAdditionSignatoriesReport_NeedDataSource" >
                    <%--  <ClientSettings>
                        <Scrolling AllowScroll="true" UseStaticHeaders="false" />
                    </ClientSettings>--%>
                    <MasterTableView >
                        <RowIndicatorColumn Visible="False">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Created="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                <ItemTemplate>
                                    <%# Container.DataSetIndex+1 %>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                          <%--  <telerik:GridBoundColumn DataField="ID" UniqueName="ID" SortExpression="ID"
                                HeaderText="ID">
                            </telerik:GridBoundColumn>--%>
                             <telerik:GridBoundColumn DataField="BankName" UniqueName="BankName" SortExpression="BankName"
                                HeaderText="Bank Name">
                            </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn DataField="AccountNO" UniqueName="AccountNO" SortExpression="AccountNO"
                                HeaderText="Account NO">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="BankAddress" UniqueName="BankAddress" SortExpression="BankAddress"
                                HeaderText="Bank Address">
                            </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn DataField="Employee" UniqueName="Employee" SortExpression="Employee"
                                    HeaderText="Member Name">
                                </telerik:GridBoundColumn> 
                             <telerik:GridBoundColumn DataField="Empid" UniqueName="Empid" SortExpression="Empid"
                                HeaderText="Emp Id">
                            </telerik:GridBoundColumn>                          
                            <telerik:GridBoundColumn DataField="Type" UniqueName="Type" SortExpression="Type"
                                HeaderText="Group">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RequiredFor" UniqueName="RequiredFor" SortExpression="RequiredFor"
                                    HeaderText="Required For">
                                </telerik:GridBoundColumn> 
                            <telerik:GridBoundColumn DataField="Location" UniqueName="Location" SortExpression="Location"
                                HeaderText="Location">
                            </telerik:GridBoundColumn>
                           
                            <telerik:GridBoundColumn DataField="EntryDate"  UniqueName="EntryDate" SortExpression="EntryDate"
                                HeaderText="Created On">
                            </telerik:GridBoundColumn>
                          <%--  <telerik:GridTemplateColumn HeaderText="Download Slip"
                                UniqueName="Download">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDownload" OnClick="lnkDownload_Click" Font-Bold="true" ToolTip='<%# Eval("ID") %>'
                                        runat="server" Text="Download" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>--%>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
    <table>

        <tr>

            <td>

                <rsweb:ReportViewer ID="rptViewer" runat="server" Font-Names="Verdana" Font-Size="8pt"
                    InteractiveDeviceInfos="(Collection)" ProcessingMode="Remote" Style="margin-top: 0px"
                    WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px" Height="1150px"
                    ShowToolBar="false" ShowParameterPrompts="false" Visible ="false">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>
