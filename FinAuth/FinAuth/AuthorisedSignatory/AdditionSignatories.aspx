﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AS.Master" AutoEventWireup="true" CodeBehind="AdditionSignatories.aspx.cs" Inherits="FinAuth.AuthorisedSignatory.AdditionSignatories" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="../Scripts/jquery-1.10.2.min.js"></script>
        <link rel="stylesheet" href="../styles/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="../styles/responsive.css" type="text/css" media="all" />
        <script type="text/javascript">
            function DisableButtons() {
                var inputs = document.getElementsByTagName("INPUT");
                for (var i in inputs) {
                    if (inputs[i].type == "button" || inputs[i].type == "submit") {
                        //inputs[i].disabled = true;
                    }
                }
            }
            window.onbeforeunload = DisableButtons;
            function CheckBoxValidation(oSrouce, args) {
                var myCheckBox = document.getElementById('<%= chkAadhar.ClientID %>');

                if (myCheckBox.checked) {
                    args.IsValid = true;
                }
                else {
                    args.IsValid = false;
                }


            }
            function FileClick() {

                document.getElementById('<%= hfvalue.ClientID %>').value = "1";

            }

        </script>

    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheading" runat="server">
    Addition of Signatories
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadFormDecorator ID="QsfFromDecorator" Skin="Web20" runat="server" DecoratedControls="All"
        EnableRoundedCorners="true" />
    <telerik:RadAjaxLoadingPanel ID="RAL" Width="10px" Height="10px" runat="server" Skin="Web20" EnableAjaxSkinRendering="true"
        EnableEmbeddedSkins="true">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RAM" runat="server" SkinID="Web20">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rcmbRequiredFor">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rcmbBank" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="rcmbAccountNo" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="rgExistingSignatories" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="lblBankValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="lblBankAddValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="rcmbBank">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgExistingSignatories" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="rcmbAccountNo" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="lblBankAddValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>

                </UpdatedControls>
            </telerik:AjaxSetting>


            <telerik:AjaxSetting AjaxControlID="rcmbAccountNo">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgExistingSignatories" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="lblBankAddValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="rcmbPlant">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rcmbRegion" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="rcmbRequiredFor" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="rcmbBank" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="rcmbAccountNo" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="rgExistingSignatories" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="lblBankValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="lblBankAddValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>

                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rcmbRegion">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rcmbPlant" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="rcmbRequiredFor" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="rcmbBank" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="rcmbAccountNo" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="rgExistingSignatories" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="lblBankValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="lblBankAddValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>

                </UpdatedControls>
            </telerik:AjaxSetting>

        </AjaxSettings>
    </telerik:RadAjaxManager>

    <table style="width: 91%; line-height: 15px; margin-bottom: 0px;" align="center">
        <tr>
            <td style="text-align: center;" colspan="4">
                <asp:ValidationSummary ID="vgSummary" ForeColor="Red" DisplayMode="BulletList" runat="server" ShowMessageBox="false" ShowSummary="true"
                    ValidationGroup="Save" />
                <asp:ValidationSummary ID="ValidationSummary1" ForeColor="Red" DisplayMode="BulletList" runat="server" ShowMessageBox="false" ShowSummary="true"
                    ValidationGroup="ADD" />

            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top;">
                <asp:Label ID="lblRequest" runat="server" Font-Bold="True" Text="Request Initiated by location:"></asp:Label><span style="color: Red">*</span>
            </td>
            <td style="text-align: left; vertical-align: top">
                <asp:RadioButton ID="rdoPlant" Font-Bold="true" Style="margin-bottom: 12px; padding-bottom: 12px;" GroupName="Addition" Text="Plant" runat="server" AutoPostBack="true" OnCheckedChanged="rdoPlant_CheckedChanged" />


            </td>
            <td>&nbsp;&nbsp;&nbsp; &nbsp;</td>
            <td style="text-align: left; vertical-align: top;">
                <telerik:RadComboBox ID="rcmbPlant" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rcmbPlant_SelectedIndexChanged" Filter="StartsWith" MaxHeight="250px" SkinID="Web20">
                </telerik:RadComboBox>
            </td>
            <td>&nbsp;&nbsp;&nbsp; &nbsp;</td>
            <td style="text-align: left; vertical-align: top">
                <asp:RadioButton ID="rdoRegion" GroupName="Addition" Font-Bold="true" Text="Region" runat="server" OnCheckedChanged="rdoRegion_CheckedChanged" AutoPostBack="true" />
            </td>
            <td>&nbsp;&nbsp;&nbsp; &nbsp;</td>
            <td>
                <telerik:RadComboBox ID="rcmbRegion" OnSelectedIndexChanged="rcmbRegion_SelectedIndexChanged" runat="server" AutoPostBack="true" Filter="StartsWith" MaxHeight="250px">
                </telerik:RadComboBox>
            </td>
            <td>&nbsp;&nbsp;&nbsp; &nbsp;</td>
            <td style="text-align: left; vertical-align: top" colspan="2">
                <asp:RadioButton ID="rdoHO" GroupName="Addition" Text="HO" Font-Bold="true" OnCheckedChanged="rdoHO_CheckedChanged" AutoPostBack="true" runat="server" />
            </td>
        </tr>
    </table>
    <table style="width: auto; line-height: 15px;" align="center">
        <tr>
            <td style="text-align: left;">
                <asp:Label ID="lblReq" runat="server" Font-Bold="True" Text="Required for "></asp:Label><span style="color: Red">*</span>
            </td>
            <td style="text-align: left; vertical-align: top">

                <telerik:RadComboBox ID="rcmbRequiredFor" SkinID="Web20" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rcmbRequiredFor_SelectedIndexChanged" Filter="StartsWith" MaxHeight="250px">
                    <Items>
                        <telerik:RadComboBoxItem Text="--Select--" Value="0" />
                        <telerik:RadComboBoxItem Text="Online" Value="Online" />
                        <telerik:RadComboBoxItem Text="Offline" Value="Offline" />
                    </Items>
                </telerik:RadComboBox>
                <asp:Label ID="lblOff" runat="server" Text="(Offline/Online)"></asp:Label>
                <asp:RequiredFieldValidator ID="reqRquiredFor" runat="server" SetFocusOnError="true" ForeColor="Red"
                    ErrorMessage="Select Required For" ControlToValidate="rcmbRequiredFor" InitialValue="--Select--"
                    ValidationGroup="Save">*
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">

                <asp:Label ID="lblBank" runat="server" Font-Bold="True" Text="Bank "></asp:Label><span style="color: Red">*</span>
            </td>
            <td style="text-align: left; vertical-align: top">
                <asp:Label ID="lblBankValue" runat="server" Text=""></asp:Label>
                <telerik:RadComboBox ID="rcmbBank" SkinID="Web20" runat="server" AutoPostBack="true" Filter="StartsWith" MaxHeight="250px" OnSelectedIndexChanged="rcmbBank_SelectedIndexChanged">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="reqbank" runat="server" SetFocusOnError="true" ForeColor="Red"
                    ErrorMessage="Select Bank" ControlToValidate="rcmbBank" InitialValue="--Select Bank--"
                    ValidationGroup="Save">*
                </asp:RequiredFieldValidator>
            </td>
        </tr>

        <tr>
            <td style="text-align: left;">
                <asp:Label ID="lblAccNo" runat="server" Text="Account No. " Font-Bold="True"></asp:Label><span style="color: Red">*</span>
            </td>
            <td style="text-align: left; vertical-align: top">
                <telerik:RadComboBox ID="rcmbAccountNo" SkinID="Web20" runat="server" OnSelectedIndexChanged="rcmbAccountNo_SelectedIndexChanged" AutoPostBack="true" Filter="StartsWith" MaxHeight="250px">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="reqAccNo" runat="server" SetFocusOnError="true" ForeColor="Red"
                    ErrorMessage="Select Account Number" ControlToValidate="rcmbAccountNo" InitialValue="--Select Account No--"
                    ValidationGroup="Save">*
                </asp:RequiredFieldValidator>

            </td>
            <td style="text-align: left;">
                <asp:Label ID="lblBankAdd" Font-Bold="True" runat="server" Text="Branch Address "></asp:Label><span style="color: Red">*</span>
            </td>
            <td style="text-align: left; vertical-align: top">

                <asp:Label ID="lblBankAddValue" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top" colspan="4">
                <br />

            </td>
        </tr>
        <tr>
            <td style="text-align: left;" colspan="4">&nbsp;
                  <h4 style="color: #3768AD; font-weight: bold">Existing Authorised Signatories as per last 'BR Date'":</h4>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">&nbsp;
            </td>
        </tr>
        <%--  <tr>
            <td style="text-align: left; vertical-align: top" colspan="4">
                <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Existing Authorised Signatories as per last 'BR Date'"></asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td style="text-align: left; vertical-align: top" colspan="4">
                <telerik:RadGrid ID="rgExistingSignatories" runat="server" Width="98%" AllowPaging="true" PageSize="5" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                    AllowSorting="True" PagerStyle-AlwaysVisible="true" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="rgExistingSignatories_NeedDataSource">
                    <%--   <ClientSettings>
                        <Scrolling AllowScroll="True" ScrollHeight="150px" UseStaticHeaders="true" />
                    </ClientSettings>--%>
                    <MasterTableView>
                        <RowIndicatorColumn Visible="False">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Created="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                <ItemTemplate>
                                    <%# Container.DataSetIndex+1 %>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="EmpID" UniqueName="EmpID" SortExpression="EmpID"
                                HeaderText="Employee ID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EmpName" UniqueName="EmpName" SortExpression="EmpName"
                                HeaderText="Employee Name">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Designation" UniqueName="Designation" SortExpression="Designation"
                                HeaderText="Designation">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Type" UniqueName="Type" SortExpression="Type"
                                HeaderText="Type">
                            </telerik:GridBoundColumn>
                        </Columns>

                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top" colspan="4">
                <br />

            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <asp:Label ID="lblDet" runat="server" Font-Bold="True" Text="Details for Addition "></asp:Label><span style="color: Red">*</span>
            </td>
            <td style="text-align: left; vertical-align: top">

                <asp:RadioButtonList ID="rcmbDetailAddition" runat="server">
                </asp:RadioButtonList>
                <%-- <asp:DropDownList ID="rcmbDetailAddition" runat="server"></asp:DropDownList>--%>
                <%--  <telerik:RadComboBox ID="rcmbDetailAddition" SkinID="Web20" runat="server" AutoPostBack="false" Filter="StartsWith" MaxHeight="250px">
                </telerik:RadComboBox>--%>
                <asp:RequiredFieldValidator ID="reqDetail" runat="server" SetFocusOnError="true" ForeColor="Red"
                    ErrorMessage="Select Detail Addition" ControlToValidate="rcmbDetailAddition"
                    ValidationGroup="Save">*
                </asp:RequiredFieldValidator>

            </td>
            <td style="text-align: left;">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Specimen Signature"></asp:Label><span style="color: Red">*</span>
            </td>
            <td style="text-align: left; vertical-align: top">
                <asp:HiddenField ID="hfvalue" runat="server" />

                <a href="Specimen_Signature_Photograph_format.doc" id="lnkdownload" runat="server" onclick="FileClick();">Download Specimen Signature</a>

                <%--<asp:LinkButton ID="lbtndownload" runat="server"  OnClick="lbtndownload_Click">Download Specimen Signature</asp:LinkButton>--%>
               
            </td>
        </tr>

        <tr>
            <td style="text-align: left;" colspan="4">&nbsp;
                  <h4 style="color: #3768AD; font-weight: bold">Add New Member Details:</h4>
            </td>
        </tr>
        <%--  <tr>
            <td style="text-align: left; vertical-align: top" colspan="4">
                <asp:Label ID="lblMembers" runat="server" Font-Bold="true" Text="Add New Member Details"></asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td style="text-align: left; vertical-align: top" colspan="4">
                <br />

            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top">
                <asp:Label ID="lblEmp" Font-Bold="true" runat="server" Text="Employee"></asp:Label>
            </td>
            <td>
                <telerik:RadComboBox ID="rcmbEmpID" SkinID="Web20" runat="server" AutoPostBack="false" Filter="StartsWith" MaxHeight="250px">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="reqEmpD" runat="server" SetFocusOnError="true" ForeColor="Red"
                    ErrorMessage="Please Select Employee" ControlToValidate="rcmbEmpID" InitialValue="--Select Employee--"
                    ValidationGroup="ADD">
                </asp:RequiredFieldValidator>
            </td>
            <td style="text-align: left; vertical-align: top">
                <asp:Label ID="lblType" Font-Bold="true" runat="server" Text="Type"></asp:Label>
            </td>
            <td>
                <telerik:RadComboBox ID="rcmbType" SkinID="Web20" runat="server" AutoPostBack="false" Filter="StartsWith" MaxHeight="250px">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="reqType" runat="server" SetFocusOnError="true" ForeColor="Red"
                    ErrorMessage="Please select Type" ControlToValidate="rcmbType" InitialValue="--Select Type--"
                    ValidationGroup="ADD">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" Font-Bold="true" runat="server" Text="Documents provided"></asp:Label></td>
            <td>
                <asp:CheckBox ID="chkAadhar" Text="Aadhar Card" runat="server" />
                <asp:CustomValidator ID="Custom1" ClientValidationFunction="CheckBoxValidation" runat="server" ValidationGroup="ADD" ForeColor="Red"
                    ErrorMessage="Please select Aadhar Card"> </asp:CustomValidator></td>

            <td>
                <asp:RadioButtonList ID="rbtnadress" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem>PAN Card</asp:ListItem>
                    <asp:ListItem>Passport</asp:ListItem>
                    <asp:ListItem>other</asp:ListItem>
                </asp:RadioButtonList></td>

            <td>
                <asp:RequiredFieldValidator ID="ReqiredFieldValidator1" runat="server" ForeColor="Red" ControlToValidate="rbtnadress" ValidationGroup="ADD" ErrorMessage="Please Select Documents">*         </asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="text-align: center; vertical-align: top" colspan="4">
                <asp:Button ID="btnADD" runat="server" Text="ADD" Width="100px" ValidationGroup="ADD" OnClick="btnADD_Click" />
            </td>
        </tr>

        <asp:Panel ID="pnlMember" runat="server" Visible="false">
            <tr>
                <td style="text-align: left; vertical-align: top" colspan="4">
                    <telerik:RadGrid ID="rgMemberDetails" runat="server" Width="98%" AllowPaging="false" PageSize="5" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                        AllowSorting="True" Skin="Web20" OnDeleteCommand="rgMemberDetails_DeleteCommand" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None">
                        <ClientSettings>
                            <Scrolling AllowScroll="True" ScrollHeight="150px" UseStaticHeaders="true" />
                        </ClientSettings>
                        <MasterTableView DataKeyNames="EmpID">
                            <RowIndicatorColumn Visible="False">
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Created="True">
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="Sr No.">
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex+1 %>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="EmpID" UniqueName="EmpID" SortExpression="EmpID"
                                    HeaderText="Employee ID">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="EmpName" UniqueName="EmpName" SortExpression="EmpName"
                                    HeaderText="Employee Name">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Designation" UniqueName="Designation" SortExpression="Designation"
                                    HeaderText="Designation">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Type" UniqueName="Type" SortExpression="Type"
                                    HeaderText="Type">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Aadhar" UniqueName="Aadhar" SortExpression="Aadhar"
                                    HeaderText="Aadhar Card">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OtherDoc" UniqueName="OtherDoc" SortExpression="OtherDoc"
                                    HeaderText="Other Documents">
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn HeaderText="Action" ButtonType="ImageButton" Text="Delete"
                                    CommandName="Delete" UniqueName="Delete" ImageUrl="~/Images/Delete.gif" ConfirmText="Are you sure you want to delete the record ?">
                                </telerik:GridButtonColumn>
                            </Columns>


                        </MasterTableView>
                    </telerik:RadGrid>
                </td>
            </tr>
        </asp:Panel>

        <tr>
            <td style="text-align: left; vertical-align: top" colspan="1">
                <asp:Label ID="lblComments" Font-Bold="true" runat="server" Text="Comments(If Any)"></asp:Label>
            </td>
            <td style="text-align: left; vertical-align: top" colspan="2">
                <asp:TextBox ID="txtComments" TextMode="MultiLine" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top" colspan="4">
                <br />

            </td>
        </tr>
        <tr>
            <td style="text-align: center; vertical-align: top" colspan="4">

                <asp:Button ID="btnDraft" runat="server" Text="Save as Draft" Width="100px" OnClick="btnDraft_Click" />
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="100px" ValidationGroup="Save" OnClick="btnSubmit_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Reset" Width="100px" CausesValidation="false" OnClick="btnCancel_Click" />
            </td>

        </tr>
    </table>
</asp:Content>
