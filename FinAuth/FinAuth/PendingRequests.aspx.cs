﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Net.Mail;
using System.IO;
using FinAuth.BLL;
using System.Data;
using System.Data.Entity;
using System.Drawing;

namespace FinAuth
{
    public partial class PendingRequests : System.Web.UI.Page
    {
        POA objpoa = new POA();
        FinAuthEntities1 fm = new FinAuthEntities1();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["EmpID"] + "" == "" || Session["UserRole_Id"] + "" == "")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    lvRequest.Rebind();
                    int EmpID = Convert.ToInt32(Session["EmpID"]);
                    //Rgrequest.DataSource = (from m in fm.Mst_AdditionalPower_request
                    //                        from d in fm.Trn_Approvalflow_raisenew
                    //                        from emp in fm.mta_mstemployee
                    //                          .Where(f => f.EmpID == m.RequserId && d.POANO == m.PowerReqId && d.status == "P" && d.ApproverId.Value == EmpID)
                    //                        select new { PowerReqNo =m.PowerReqNo, m.PowerReqId, Initiator_Name = emp.EmpName, m.ReqReason, m.ReqDate, m.PowerDesc }).Distinct().ToList();
                    Rgrequest.DataSource = fm.usp_bindRaisedPowerRequest().ToList().Where(a => a.ApproverId.Value == EmpID);
                    Rgrequest.DataBind();
                    if (rcbview.SelectedValue == "0")
                    {
                        lvRequest.DataSource = fm.usp_bindRaisedPowerRequest().ToList().Where(a => a.ApproverId.Value == EmpID);
                    }
                    else
                    {
                        lvRequest.DataSource = fm.usp_bindRaisedPowerRequest().ToList().Where(a => a.ApproverId.Value == EmpID && a.status == rcbview.SelectedValue);
                    }
                    lvRequest.DataBind();

                }
            }


        }

        protected void Rgrequest_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            int EmpID = Convert.ToInt32(Session["EmpID"]);
            if (rcbview.SelectedValue == "0")
            {
                Rgrequest.DataSource = fm.usp_bindRaisedPowerRequest().ToList();
            }
            else
            {
                Rgrequest.DataSource = fm.usp_bindRaisedPowerRequest().Where(a => a.status == rcbview.SelectedValue).ToList();
            }
            //Rgrequest.DataSource = (from m in fm.Mst_AdditionalPower_request
            //                        from d in fm.Trn_Approvalflow_raisenew
            //                        from emp in fm.mta_mstemployee
            //                          .Where(f => f.EmpID == m.RequserId && d.POANO == m.PowerReqId && d.status == "P" && d.ApproverId.Value == EmpID)
            //                        select new { PowerReqNo = m.PowerReqNo, m.PowerReqId, Initiator_Name = emp.EmpName, m.ReqReason, m.ReqDate, m.PowerDesc }).Distinct().ToList();

        }


        protected void Rgrequest_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item.ItemType == GridItemType.Item) || (e.Item.ItemType == GridItemType.AlternatingItem))
            {
                GridDataItem dtitem = e.Item as GridDataItem;
                if (((Label)dtitem.FindControl("lblstatus")).Text == "P")
                {
                    ((Label)dtitem.FindControl("lblstatusPOA")).Visible = false;
                    ((Button)dtitem.FindControl("btnEdit")).Visible = true;
                }
                else
                {
                    ((Button)dtitem.FindControl("btnEdit")).Visible = false;
                    ((Label)dtitem.FindControl("lblstatusPOA")).Visible = true;
                    ((Label)dtitem.FindControl("lblstatusPOA")).Text = ((Label)dtitem.FindControl("lblstatus")).Text == "A" ? "Accepted" : "Rejected";

                }

            }
            if (e.Item.IsInEditMode && e.Item is GridEditableItem)
            {
                GridEditableItem edititem = e.Item as GridEditableItem;
                RadComboBox rcbAccept = (RadComboBox)edititem.FindControl("rcbAccept");
                LinkButton updateButton = (LinkButton)edititem.FindControl("UpdateButton");
                RadComboBox rcbrole = (RadComboBox)edititem.FindControl("rcbrole");
                TextBox TB_PowerDesc = (TextBox)edititem.FindControl("TB_PowerDesc");
                RequiredFieldValidator RequiredFieldValidator3 = (RequiredFieldValidator)edititem.FindControl("RequiredFieldValidator3");
                rcbrole.DataSource = fm.MstRoles.ToList();
                rcbrole.DataTextField = "role";
                rcbrole.DataValueField = "roleid";
                rcbrole.DataBind();
                updateButton.Text = "Submit";
                updateButton.ValidationGroup = "vgUpdate";

                rcbrole.Items.Insert(0, new RadComboBoxItem("-- Select --", "0"));
                rcbAccept.Attributes.Add("Name", rcbAccept.ClientID);
                //   rcbAccept.Attributes.Add("onclick", "showrole(" + rcbAccept.ClientID + "," + rcbrole.ClientID + ");");
                rcbAccept.Attributes.Add("role", rcbrole.ClientID);
                rcbAccept.Attributes.Add("reqvd", RequiredFieldValidator3.ClientID);

            }
        }

        protected void Rgrequest_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if ((e.Item is GridEditFormItem) && (e.Item.IsInEditMode))//editmode=EditForm/InPlace
            {
                GridEditFormItem edititem = (GridEditFormItem)e.Item;

                Label lblPowerReqId = (Label)edititem.FindControl("lblPowerReqId");
                Label lblPowerReqNO = (Label)edititem.FindControl("lblPowerReqNO");
                RadComboBox rcbAccept = (RadComboBox)edititem.FindControl("rcbAccept");
                RadComboBox rcbrole = (RadComboBox)edititem.FindControl("rcbrole");
                TextBox txtremarks = (TextBox)edititem.FindControl("txtremarks");
                TextBox txtPowerDesc = (TextBox)edititem.FindControl("txtPowerDesc");
                Label lblRequestorId = (Label)edititem.FindControl("lblRequestorId");
                var reqPOA = (from o in fm.Mst_AdditionalPower_request
                              where o.PowerReqId + "" == lblPowerReqId.Text
                              select o).First();
                reqPOA.PowerReqId = Convert.ToInt32(lblPowerReqId.Text);
                if (rcbAccept.SelectedValue == "A")
                {
                    reqPOA.Roleid = Convert.ToInt32(rcbrole.SelectedValue);
                    reqPOA.RequestedPowerDesc = txtPowerDesc.Text;
                }
                else
                {
                    reqPOA.Roleid = 0;
                }
                using (var context = new FinAuthEntities1())
                {
                    context.Mst_AdditionalPower_request.Attach(reqPOA);
                    context.Entry(reqPOA).State = EntityState.Modified;
                    context.SaveChanges();
                }


                var reqFlow = (from o in fm.Trn_Approvalflow_raisenew
                               where o.POANO + "" == lblPowerReqId.Text
                               select o).First();

                reqFlow.status = rcbAccept.SelectedValue;
                reqFlow.remarks = txtremarks.Text;
                reqFlow.Approvaldate = DateTime.Now;
                using (var context = new FinAuthEntities1())
                {
                    context.Trn_Approvalflow_raisenew.Attach(reqFlow);
                    context.Entry(reqFlow).State = EntityState.Modified;
                    context.SaveChanges();
                }
                //mail
                var dthod = fm.usp_getHOD_forApproval(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
                var dthodEmail = fm.getEmpDetails(Convert.ToInt32(dthod[0].hod.ToString())).ToList();
                string hodName = "", hodemailid = "", ccmail = "", iniName = "", OnBehalfEmail="";
                if (dthodEmail.Count > 0)
                {
                    hodName = dthodEmail[0].EmpName.ToString();
                    hodemailid = dthodEmail[0].EmailID.ToString();
                }
                var dtiniEmail = fm.getEmpDetails(Convert.ToInt32(lblRequestorId.Text)).ToList();
                if (dtiniEmail.Count > 0)
                {
                    ccmail = dtiniEmail[0].EmailID.ToString();
                    iniName = dtiniEmail[0].EmpName.ToString();
                }
                var dtOnBehalfEmail = fm.getEmpDetails(Convert.ToInt32(Session["RequestorId"].ToString())).ToList();
                if (dtOnBehalfEmail.Count > 0)
                {
                    OnBehalfEmail = dtOnBehalfEmail[0].EmailID.ToString();

                }
                if (rcbAccept.SelectedValue == "A")
                {
                    var MstAuthorityPower1 = new MstAuthorityPower()
                    {

                        AddedOn = DateTime.Now,
                        IsActive = true,
                        powerDesc = txtPowerDesc.Text,
                        RoleId = Convert.ToInt32(rcbrole.SelectedValue)
                    };
                    fm.MstAuthorityPowers.Add(MstAuthorityPower1);
                    fm.SaveChanges();

                    objpoa.sendMail_PowerReqAdded(lblPowerReqNO.Text, iniName, ccmail, hodemailid, txtPowerDesc.Text, rcbrole.SelectedItem.Text, OnBehalfEmail);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Requested  powers in " + lblPowerReqNO.Text + " is added successfully.');",
   true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Requested powers in" + lblPowerReqNO.Text + " rejected successfully.');",
                        true);
                    objpoa.sendMail_PowerReqRejected(lblPowerReqNO.Text, iniName, ccmail, hodemailid, txtremarks.Text, OnBehalfEmail);
                }
                Rgrequest.Rebind();

                //send Mail
                //objpoa.sendMail_PowerReqAdded("");
            }
        }

        //protected void rcbAccept_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        //{
        //    RadComboBox list = sender as RadComboBox;
        //    GridEditableItem editedItem = (sender as RadComboBox).NamingContainer as GridEditableItem;
        //    if (((RadComboBox)sender).SelectedItem.Text == "Accept")
        //    {
        //        ((RadComboBox)editedItem.FindControl("rcbrole")).Visible = true;
        //        ((RequiredFieldValidator)editedItem.FindControl("RequiredFieldValidator3")).Enabled = true;

        //    }
        //    else
        //    {
        //        ((RadComboBox)editedItem.FindControl("rcbrole")).Visible = false;
        //        ((RequiredFieldValidator)editedItem.FindControl("RequiredFieldValidator3")).Enabled = false;
        //    }
        //}

        protected void lvRequest_NeedDataSource(object sender, RadListViewNeedDataSourceEventArgs e)
        {
            int EmpID = Convert.ToInt32(Session["EmpID"]);
            if (rcbview.SelectedValue == "0")
            {
                lvRequest.DataSource = fm.usp_bindRaisedPowerRequest().ToList().Where(a => a.ApproverId.Value == EmpID);
            }
            else
            {
                lvRequest.DataSource = fm.usp_bindRaisedPowerRequest().ToList().Where(a => a.ApproverId.Value == EmpID && a.status == rcbview.SelectedValue);
            }
            //lvRequest.DataBind();
        }

        protected void lvRequest_ItemDataBound(object sender, Telerik.Web.UI.RadListViewItemEventArgs e)
        {
            if ((e.Item.ItemType == RadListViewItemType.DataItem) || (e.Item.ItemType == RadListViewItemType.AlternatingItem))
            {
                RadListViewDataItem dtitem = e.Item as RadListViewDataItem;
                RadComboBox rcbrole = (RadComboBox)e.Item.FindControl("rcbrole");
               
                rcbrole.DataSource = fm.MstRoles.ToList();
                rcbrole.DataTextField = "role";
                rcbrole.DataValueField = "roleid";
                rcbrole.DataBind();
                rcbrole.Items.Insert(0, new RadComboBoxItem("-- Select --", "0"));
                if (((Label)dtitem.FindControl("lblstatus")).Text == "P")
                {
                    ((Label)dtitem.FindControl("lblstatusPOA")).Visible = false;
                    ((Button)dtitem.FindControl("btnEdit")).Visible = true;
                    ((Button)dtitem.FindControl("btnReset")).Visible = true;
                   
                }
                else if (((Label)dtitem.FindControl("lblstatus")).Text == "A")
                {
                    ((Button)dtitem.FindControl("btnEdit")).Visible = false;
                    ((Label)dtitem.FindControl("lblstatusPOA")).Visible = true;
                    ((Label)dtitem.FindControl("lblstatusPOA")).Text = ((Label)dtitem.FindControl("lblstatus")).Text == "A" ? "Accepted" : "Rejected";
                     ((Label)dtitem.FindControl("lblstatusPOA")).ForeColor = Color.Green;
                    RadComboBox rcbAccept = dtitem.FindControl("rcbAccept") as RadComboBox;
                    rcbAccept.FindItemByValue(((Label)dtitem.FindControl("lblstatus")).Text).Selected = true;
                    rcbAccept.Enabled = false;
                    rcbrole.FindItemByValue(((Label)dtitem.FindControl("lblrole")).Text).Selected = true;
                    rcbrole.Enabled = false;
                    string lblRemarks=(dtitem.FindControl("lblRemarks") as Label).Text.Trim();
                    string RequestedPowerDesc = (dtitem.FindControl("lblRequestedPowerDesc") as Label).Text.Trim();
                    TextBox txtremarks = ((TextBox)dtitem.FindControl("txtremarks"));
                    TextBox txtPowerDesc = ((TextBox)dtitem.FindControl("txtPowerDesc"));
                    txtremarks.Text = lblRemarks;
                    txtPowerDesc.Text = RequestedPowerDesc;
                    txtPowerDesc.Enabled = false;
                    txtremarks.Enabled = false;
                    ((Button)dtitem.FindControl("btnReset")).Visible = false;
                   

                }
                else if (((Label)dtitem.FindControl("lblstatus")).Text == "R")
                {
                    ((Button)dtitem.FindControl("btnEdit")).Visible = false;
                    ((Label)dtitem.FindControl("lblstatusPOA")).Visible = true;
                    ((Label)dtitem.FindControl("lblstatusPOA")).Text = ((Label)dtitem.FindControl("lblstatus")).Text == "A" ? "Accepted" : "Rejected";
                    ((Label)dtitem.FindControl("lblstatusPOA")).ForeColor = Color.Red;
                    RadComboBox rcbAccept = dtitem.FindControl("rcbAccept") as RadComboBox;
                    rcbAccept.FindItemByValue(((Label)dtitem.FindControl("lblstatus")).Text).Selected = true;
                    rcbAccept.Enabled = false;
                    rcbrole.Visible = false;
                    string lblRemarks = (dtitem.FindControl("lblRemarks") as Label).Text.Trim();
                    TextBox txtremarks = ((TextBox)dtitem.FindControl("txtremarks"));
                    txtremarks.Text = lblRemarks;
                    txtremarks.Enabled = false;
                    ((TextBox)dtitem.FindControl("txtPowerDesc")).Visible = false;
                    ((Label)dtitem.FindControl("lblSelectRole")).Visible = false;
                    ((Label)dtitem.FindControl("lblPower")).Visible = false;
                    ((Button)dtitem.FindControl("btnReset")).Visible = false;
                   
                }
            }

        }
        protected void lvRequest_ItemCommand(object sender, RadListViewCommandEventArgs e)
        {
            if (e.CommandName == "save")
            {

                RadListViewDataItem item = e.ListViewItem as RadListViewDataItem;
                item.Selected = true;
                Label lblPowerReqId = item.FindControl("lblPowerReqId") as Label;
                Label lblPowerReqNO = item.FindControl("lblPowerReqNO") as Label;
                RadComboBox rcbAccept = item.FindControl("rcbAccept") as RadComboBox;
                RadComboBox rcbrole = item.FindControl("rcbrole") as RadComboBox;
                TextBox txtremarks = item.FindControl("txtremarks") as TextBox;
                TextBox txtPowerDesc = item.FindControl("txtPowerDesc") as TextBox;
                Label lblRequestorId = item.FindControl("lblRequestorId") as Label;
                Label lblInitiator_id = item.FindControl("lblInitiator_id") as Label;                
                Label lblRej = item.FindControl("lblRej") as Label;
                Label lblAcc = item.FindControl("lblAcc") as Label;
                Button btnDelete = item.FindControl("btnDelete") as Button;

                var reqPOA = (from o in fm.Mst_AdditionalPower_request
                              where o.PowerReqId + "" == lblPowerReqId.Text
                              select o).First();
                reqPOA.PowerReqId = Convert.ToInt32(lblPowerReqId.Text);

                if (rcbAccept.SelectedValue == "A")
                {
                    if (rcbrole.SelectedItem.Text.Trim() == "-- Select --")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Please Select Role');", true);
                        return;

                    }
                    if (txtPowerDesc.Text.Trim() == string.Empty)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Please Enter Power Description');", true);
                        return;

                    }
                    reqPOA.Roleid = Convert.ToInt32(rcbrole.SelectedValue);
                    reqPOA.RequestedPowerDesc = txtPowerDesc.Text;

                }
                else
                {
                    reqPOA.Roleid = 0;
                }
                if (txtremarks.Text.Trim() == string.Empty)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Please Enter Remarks');", true);
                    return;

                }
                using (var context = new FinAuthEntities1())
                {
                    context.Mst_AdditionalPower_request.Attach(reqPOA);
                    context.Entry(reqPOA).State = EntityState.Modified;
                    context.SaveChanges();
                }


                var reqFlow = (from o in fm.Trn_Approvalflow_raisenew
                               where o.POANO + "" == lblPowerReqId.Text
                               select o).ToList();
                reqFlow.ForEach(a =>
                      {

                          a.status = rcbAccept.SelectedValue;
                          a.remarks = txtremarks.Text;
                          a.Approvaldate = DateTime.Now;
                          using (var context = new FinAuthEntities1())
                          {
                              context.Trn_Approvalflow_raisenew.Attach(a);
                              context.Entry(a).State = EntityState.Modified;
                              context.SaveChanges();
                          }
                      });
                //mail
                var dthod = fm.usp_getHOD_forApproval(Convert.ToInt32(lblInitiator_id.Text)).ToList();
                var dthodEmail = fm.getEmpDetails(Convert.ToInt32(dthod[0].hod.ToString())).ToList();
                string hodName = "", hodemailid = "", ccmail = "", iniName = "", OnBehalfEmail="";
                if (dthodEmail.Count > 0)
                {
                    hodName = dthodEmail[0].EmpName.ToString();
                    hodemailid = dthodEmail[0].EmailID.ToString();
                }
                var dtiniEmail = fm.getEmpDetails(Convert.ToInt32(lblRequestorId.Text)).ToList();
                if (dtiniEmail.Count > 0)
                {
                    ccmail = dtiniEmail[0].EmailID.ToString();
                    iniName = dtiniEmail[0].EmpName.ToString();
                }
                var dtOnBehalfEmail = fm.getEmpDetails(Convert.ToInt32(lblInitiator_id.ToString())).ToList();
                if (dtOnBehalfEmail.Count > 0)
                {
                    OnBehalfEmail = dtOnBehalfEmail[0].EmailID.ToString();

                }
                if (rcbAccept.SelectedValue == "A")
                {
                    var MstAuthorityPower1 = new MstAuthorityPower()
                    {

                        AddedOn = DateTime.Now,
                        IsActive = true,
                        powerDesc = txtPowerDesc.Text,
                        RoleId = Convert.ToInt32(rcbrole.SelectedValue)
                    };
                    fm.MstAuthorityPowers.Add(MstAuthorityPower1);
                    fm.SaveChanges();

                    objpoa.sendMail_PowerReqAdded(lblPowerReqNO.Text, iniName, ccmail, hodemailid, txtPowerDesc.Text, rcbrole.SelectedItem.Text,"");

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Requested  powers in " + lblPowerReqNO.Text + " is added successfully.');window.location ='PendingRequests.aspx'",
   true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Requested powers in" + lblPowerReqNO.Text + " rejected successfully.');window.location ='PendingRequests.aspx'",
                        true);
                    objpoa.sendMail_PowerReqRejected(lblPowerReqNO.Text, iniName, ccmail, hodemailid, txtremarks.Text,"");
                }
              //  lvRequest.Rebind();

                //send Mail
                //objpoa.sendMail_PowerReqAdded("");
            }
            if (e.CommandName == "Reset")
            {
                lvRequest.Rebind();
             /*   RadListViewDataItem item = e.ListViewItem as RadListViewDataItem;
                item.Selected = true;
                Label lblPowerReqId = item.FindControl("lblPowerReqId") as Label;
                Label lblPowerReqNO = item.FindControl("lblPowerReqNO") as Label;
                RadComboBox rcbAccept = item.FindControl("rcbAccept") as RadComboBox;
                RadComboBox rcbrole = item.FindControl("rcbrole") as RadComboBox;
                TextBox txtremarks = item.FindControl("txtremarks") as TextBox;
                TextBox txtPowerDesc = item.FindControl("txtPowerDesc") as TextBox;
                Label lblRequestorId = item.FindControl("lblRequestorId") as Label;
                Label lblRej = item.FindControl("lblRej") as Label;
                Label lblAcc = item.FindControl("lblAcc") as Label;
                Button btnDelete = item.FindControl("btnDelete") as Button;
                Panel pnlHide = (Panel)item.FindControl("pnlHide");
                txtPowerDesc.Text = string.Empty;
                txtremarks.Text = string.Empty;
                rcbrole.DataSource = fm.MstRoles.ToList();
                rcbrole.DataTextField = "role";
                rcbrole.DataValueField = "roleid";
                rcbrole.DataBind();
                rcbrole.Items.Insert(0, new RadComboBoxItem("-- Select --", "0"));
                rcbAccept.SelectedValue = "A";
                //pnlHide.Visible = false;*/
            }



        }


        protected void rcbAccept_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox list = sender as RadComboBox;
            RadListViewDataItem editedItem = (sender as RadComboBox).NamingContainer as RadListViewDataItem;

            if (((RadComboBox)sender).SelectedItem.Text == "Accept")
            {
                ((RadComboBox)editedItem.FindControl("rcbrole")).Visible = true;
                ((TextBox)editedItem.FindControl("txtPowerDesc")).Visible = true;
                ((Label)editedItem.FindControl("lblSelectRole")).Visible = true;
                ((Label)editedItem.FindControl("lblPower")).Visible = true;
                ((Panel)editedItem.FindControl("pnlHide")).Visible = true;
                //((RequiredFieldValidator)editedItem.FindControl("RequiredFieldValidator3")).Enabled = true;

            }
            else
            {
                ((RadComboBox)editedItem.FindControl("rcbrole")).Visible = false;
                ((TextBox)editedItem.FindControl("txtPowerDesc")).Visible = false;
                ((Label)editedItem.FindControl("lblSelectRole")).Visible = false;
                ((Label)editedItem.FindControl("lblPower")).Visible = false;
                ((Panel)editedItem.FindControl("pnlHide")).Visible = true;
                //((RequiredFieldValidator)editedItem.FindControl("RequiredFieldValidator3")).Enabled = false;
            }
        }

        protected void rcbView_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            lvRequest.Rebind();
        }



    }
}


