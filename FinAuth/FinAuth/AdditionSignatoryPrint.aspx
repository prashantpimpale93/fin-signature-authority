﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdditionSignatoryPrint.aspx.cs" Inherits="FinAuth.AuthorisedSignatory.AdditionSignatoryPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Addition Signatory</title>
</head>
<body style="margin: 0px; width: 600px; height: 90%;">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <rsweb:ReportViewer ID="rptViewer" runat="server" Font-Names="Verdana" Font-Size="8pt"
                InteractiveDeviceInfos="(Collection)" ProcessingMode="Remote" Style="margin-top: 0px"
                WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px" Height="1150px"
                ShowToolBar="false" ShowParameterPrompts="false">
            </rsweb:ReportViewer>
        </div>
    </form>
    <script type="text/javascript">
        function myFunction() {
            window.print();
        }

        function printReport() {
            try {
                var loading = $find("rptViewer").get_isLoading();
                if (!loading) {
                    window.print();
                }
                else {
                    var t = setTimeout(function () { printReport() }, 500);
                }
            }
            catch (e) {
                //console.log(e);
                var s = setTimeout(function () { printReport() }, 500);
            }
        }
        printReport();

    </script>
</body>
</html>
