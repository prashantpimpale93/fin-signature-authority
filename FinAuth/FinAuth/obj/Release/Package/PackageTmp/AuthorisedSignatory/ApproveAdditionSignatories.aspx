﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AS.Master" AutoEventWireup="true" CodeBehind="ApproveAdditionSignatories.aspx.cs" Inherits="FinAuth.AuthorisedSignatory.ApproveAdditionSignatories" %>


<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="../Scripts/jquery-1.10.2.min.js"></script>
        <link rel="stylesheet" href="../styles/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="../styles/responsive.css" type="text/css" media="all" />
        <script type="text/javascript" lang="javascript">
            function funprocess() {
                var flag = false;
                var inputs = $telerik.findControl(document, "radupload");
                if (inputs._selectedFilesCount == 0) {
                    flag = false;
                    alert("Please select valid File.");
                } else {
                    flag = true;
                }
                return flag;
            }

        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheading" runat="server">
    Addition of Signatories
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RAM" runat="server"></telerik:RadAjaxManager>

    <table style="width: 95%; line-height: 15px;" align="center">
        <tr>
            <td style="text-align: left; padding-bottom: 15px;">
                <asp:Label ID="lblReq" runat="server" Font-Bold="true" Text="Request ID:"></asp:Label>
            </td>
            <td style="text-align: left; padding-bottom: 15px; vertical-align: top">
                <asp:Label ID="lblRequestID" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; padding-bottom: 15px;">
                <asp:Label ID="lblBy" runat="server" Font-Bold="true" Text="Initiated By:"></asp:Label>
            </td>
            <td style="text-align: left; padding-bottom: 15px; vertical-align: top">
                <asp:Label ID="lblCreatedBy" runat="server" Text=""></asp:Label>
                <asp:Label ID="lblEmpIDCreatedBy" Visible="false" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; padding-bottom: 15px;">
                <asp:Label ID="lblRequest" runat="server" Font-Bold="true" Text="Request Initiated by location:"></asp:Label>
            </td>
            <td style="text-align: left; padding-bottom: 15px; vertical-align: top">
                <asp:Label ID="lblLocation" runat="server" Text=""></asp:Label>-
                 <asp:Label ID="lblLocationDetails" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; padding-bottom: 15px;">
                <asp:Label ID="lblReqFor" runat="server" Font-Bold="true" Text="Required For:"></asp:Label>
            </td>
            <td style="text-align: left; padding-bottom: 15px; vertical-align: top">
                <asp:Label ID="lblRequiredFor" runat="server" Text=""></asp:Label>

            </td>
        </tr>
        <tr>
            <td style="text-align: left; padding-bottom: 15px;">
                <asp:Label ID="lblAcc" runat="server" Font-Bold="true" Text="Account No:"></asp:Label>
            </td>
            <td style="text-align: left; padding-bottom: 15px; vertical-align: top">
                <asp:Label ID="lblAccountNo" runat="server" Text=""></asp:Label>

            </td>
        </tr>
        <tr>
            <td style="text-align: left; padding-bottom: 15px;">
                <asp:Label ID="lblBank" Font-Bold="true" runat="server" Text="Bank:"></asp:Label>
            </td>
            <td style="text-align: left; padding-bottom: 15px; vertical-align: top">
                <asp:Label ID="lblBankName" runat="server" Text=""></asp:Label>
            </td>
            <td style="text-align: left; padding-bottom: 15px;">
                <asp:Label ID="lblAdd" Font-Bold="true" runat="server" Text="Branch Address:"></asp:Label>
            </td>
            <td style="text-align: left; padding-bottom: 15px; vertical-align: top">
                <asp:Label ID="lblBankaddress" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;" colspan="4">&nbsp;
                  <h4 style="color: #3768AD; font-weight: bold">Existing Authorised Signatories as per last 'BR Date'":</h4>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top" colspan="4">
                <telerik:RadGrid ID="rgExistingSignatories" runat="server" Width="98%" AllowPaging="false" PageSize="5" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None">

                    <ClientSettings>
                        <Scrolling AllowScroll="True" ScrollHeight="150px" UseStaticHeaders="true" />
                    </ClientSettings>
                    <MasterTableView>
                        <RowIndicatorColumn Visible="False">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Created="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                <ItemTemplate>
                                    <%# Container.DataSetIndex+1 %>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="EmpID" UniqueName="EmpID" SortExpression="EmpID"
                                HeaderText="Employee ID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EmpName" UniqueName="EmpName" SortExpression="EmpName"
                                HeaderText="Employee Name">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Designation" UniqueName="Designation" SortExpression="Designation"
                                HeaderText="Designation">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Type" UniqueName="Type" SortExpression="Type"
                                HeaderText="Type">
                            </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn DataField="Aadhar" UniqueName="Aadhar" SortExpression="Aadhar"
                                    HeaderText="Aadhar Card">
                                </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="OtherDoc" UniqueName="OtherDoc" SortExpression="OtherDoc"
                                    HeaderText="Other Documents">
                                </telerik:GridBoundColumn>
                        </Columns>


                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; padding-top: 15px; padding-bottom: 15px; vertical-align: top" colspan="4">
                <asp:Label ID="lblDet" runat="server" Font-Bold="true" Text="Details for Addition : "></asp:Label>

                <asp:Label ID="lblAdditionDetails" runat="server" Text=""></asp:Label>
                <asp:Label ID="lblApproverType" Visible="false" runat="server" Text=""></asp:Label>
                <asp:Label ID="lbltreasuryType" Visible="false" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;" colspan="4">&nbsp;
                  <h4 style="color: #3768AD; font-weight: bold">Add New Member Details:</h4>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; padding-bottom: 15px; vertical-align: top; padding-top: 15px;" colspan="4">
                <telerik:RadGrid ID="rgMemberDetails" runat="server" Width="98%" AllowPaging="false" PageSize="5" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None">

                    <ClientSettings>
                        <Scrolling AllowScroll="True" ScrollHeight="150px" UseStaticHeaders="true" />
                    </ClientSettings>
                    <MasterTableView DataKeyNames="EmpID">
                        <RowIndicatorColumn Visible="False">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Created="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                <ItemTemplate>
                                    <%# Container.DataSetIndex+1 %>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="EmpID" UniqueName="EmpID" SortExpression="EmpID"
                                HeaderText="Employee ID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EmpName" UniqueName="EmpName" SortExpression="EmpName"
                                HeaderText="Employee Name">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Designation" UniqueName="Designation" SortExpression="Designation"
                                HeaderText="Designation">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Type" UniqueName="Type" SortExpression="Type"
                                HeaderText="Type">
                            </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn DataField="Aadhar" UniqueName="Aadhar" SortExpression="Aadhar"
                                    HeaderText="Aadhar Card">
                                </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="OtherDoc" UniqueName="OtherDoc" SortExpression="OtherDoc"
                                    HeaderText="Other Documents">
                                </telerik:GridBoundColumn>
                        </Columns>


                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; padding-bottom: 15px; vertical-align: top; padding-top: 15px;" colspan="1">
                <asp:Label ID="lblCom" Font-Bold="true" runat="server" Text="Comments(If Any):"></asp:Label>
           <%-- </td>
            <td style="text-align: left; padding-bottom: 15px; vertical-align: top; padding-top: 15px;" colspan="2">--%>
                <asp:Label ID="lblComments" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <asp:Panel ID="pnlCommnts" runat="server" Visible="true">
            <tr>
                <td style="text-align: left; padding-bottom: 15px; vertical-align: top; padding-top: 15px;" colspan="4">
                    <h4 style="color: #3768AD; font-weight: bold">Approver Comments:</h4>
                </td>
            </tr>
            <tr>
                <td style="text-align: left; padding-bottom: 15px; vertical-align: top; padding-top: 15px;" colspan="4">
                    <telerik:RadGrid ID="rgComments" runat="server" Width="98%" AllowPaging="false" PageSize="5" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                        AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="rgComments_NeedDataSource">

                        <ClientSettings>
                            <Scrolling AllowScroll="True" ScrollHeight="150px" UseStaticHeaders="true" />
                        </ClientSettings>
                        <MasterTableView>
                            <RowIndicatorColumn Visible="False">
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Created="True">
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridBoundColumn DataField="EmpName" UniqueName="EmpName" SortExpression="EmpName"
                                    HeaderText="Employee Name">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="SeqNo" UniqueName="SeqNo" SortExpression="SeqNo"
                                    HeaderText="Sequence">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ApprovalDatetime" UniqueName="ApprovalDatetime" SortExpression="ApprovalDatetime"
                                    HeaderText="Date">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Status" UniqueName="Status" SortExpression="Status"
                                    HeaderText="Status">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Comments" UniqueName="Comments" SortExpression="Comments"
                                    HeaderText="Comments">
                                </telerik:GridBoundColumn>

                            </Columns>


                        </MasterTableView>
                    </telerik:RadGrid>
                </td>
            </tr>
        </asp:Panel>
        <asp:Panel ID="pnlFirst" runat="server" Visible="false">
            <tr>
                <td style="text-align: left; padding-bottom: 15px; vertical-align: top; padding-top: 15px;" colspan="1">
                    <asp:Label ID="lblDate" Font-Bold="true" runat="server" Text="Board Resolution Date:"></asp:Label>
                </td>
                <td style="text-align: left; padding-bottom: 15px; vertical-align: top; padding-top: 15px;" colspan="2">
                    <telerik:RadDatePicker ID="rdDate" ForeColor="Red" Font-Bold="true" Skin="Web20"
                        Width="175px" runat="server">
                        <DateInput ID="DateInput5" runat="server" DisplayDateFormat="dd/MM/yyyy" ReadOnly="true">
                        </DateInput>
                    </telerik:RadDatePicker>
                    <%--   <telerik:RadDatePicker runat="server" DateInput-ReadOnly="true" Skin="Web20" ID="rdDate">
                        <DateInput ID="dtInput" DisplayDateFormat="dd/MM/yyyy" runat="server" Skin="Web20"
                            DateFormat="dd-MM-yyyy" Width="175px">
                        </DateInput>
                    </telerik:RadDatePicker>--%>
                    <asp:RequiredFieldValidator ID="reqDate" ValidationGroup="Save" runat="server" ErrorMessage="Please Select Date" ControlToValidate="rdDate" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td style="text-align: left; padding-bottom: 15px; vertical-align: top; padding-top: 15px;" colspan="1">
                    <asp:Label ID="lblName" Font-Bold="true" runat="server" Text="Relationship Manager Name:"></asp:Label>
                </td>
                <td style="text-align: left; padding-bottom: 15px; vertical-align: top; padding-top: 15px;" colspan="2">
                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqName" ValidationGroup="Save" runat="server" ErrorMessage="Enter Name" ControlToValidate="txtName" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
            </tr>
        </asp:Panel>
        <asp:Panel ID="pnlAttach" runat="server" Visible="false">
            <tr>
                <td style="text-align: left; padding-bottom: 15px; vertical-align: top; padding-top: 15px;" colspan="1">
                    <asp:Label ID="lblAttch" Font-Bold="true" runat="server" Text="Attachment:"></asp:Label>
                </td>
                <td style="text-align: left; padding-bottom: 15px; vertical-align: top; padding-top: 15px;" colspan="2">
                    <telerik:RadAsyncUpload RenderMode="Lightweight" MaxFileInputsCount="1" MultipleFileSelection="Disabled" runat="server" ID="radupload" Skin="Web20" />
                    <%--   <asp:RequiredFieldValidator ID="reqUpload" ValidationGroup="Save" runat="server" ErrorMessage="Please Select File" ControlToValidate="radupload" ForeColor="Red">*</asp:RequiredFieldValidator>--%>
                </td>
            </tr>
        </asp:Panel>
        <tr id="tr_apprej" runat="server">
            <td style="text-align: left; padding-bottom: 15px; vertical-align: top" colspan="1">
                <asp:Label ID="lblapp" Font-Bold="true" runat="server" Text="Approve/Reject Comments:"></asp:Label>
            </td>
            <td style="text-align: left; padding-bottom: 15px; vertical-align: top" colspan="2">
                <asp:TextBox ID="txtAppRejComm" runat="server" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqAccNo" runat="server" ForeColor="Red"
                    ErrorMessage="Enter Comments" ControlToValidate="txtAppRejComm"
                    ValidationGroup="Save">*
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; padding-bottom: 15px; vertical-align: top" colspan="4">
                <asp:Button ID="btnApprove" runat="server" OnClientClick="return funprocess();" Text="Approve" OnClick="btnApprove_Click" ValidationGroup="Save" Width="100px" />
                &nbsp;
                <asp:Button ID="btnReject" runat="server" Text="Reject" CausesValidation="false" OnClick="btnReject_Click" ValidationGroup="Save" Width="100px" />
            </td>

        </tr>
        <%--  <tr>
            <td colspan="4">
                <rsweb:ReportViewer ID="rptViewer" runat="server" Font-Names="Verdana" Font-Size="8pt"
                    InteractiveDeviceInfos="(Collection)" ProcessingMode="Remote" Style="margin-top: 0px"
                    WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px" Height="1150px"
                    ShowToolBar="false" ShowParameterPrompts="false">
                </rsweb:ReportViewer>
            </td>
        </tr>--%>
    </table>


</asp:Content>
