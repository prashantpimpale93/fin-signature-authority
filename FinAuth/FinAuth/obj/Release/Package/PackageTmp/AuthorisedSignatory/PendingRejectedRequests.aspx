﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AS.Master" AutoEventWireup="true" CodeBehind="PendingRejectedRequests.aspx.cs" Inherits="FinAuth.AuthorisedSignatory.PendingRejectedRequests" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheading" runat="server">
    Pending Rejected Requests
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 95%; line-height: 15px;" align="center">
        <tr>
            <td style="text-align: left;">&nbsp;
                  <h4 style="color: #3768AD; font-weight: bold">Addition Pending Rejected Requests:</h4>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">&nbsp;
                 
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <telerik:RadGrid ID="rgPending" runat="server" Width="100%" AllowPaging="true" PageSize="5" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="rgPending_NeedDataSource">
                    <MasterTableView>
                        <RowIndicatorColumn Visible="False">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Created="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                <ItemTemplate>
                                    <%# Container.DataSetIndex+1 %>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="RequestID" UniqueName="RequestID" SortExpression="RequestID"
                                HeaderText="Request ID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Location" UniqueName="Location" SortExpression="Location"
                                HeaderText="Location">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RequiredFor" UniqueName="RequiredFor" SortExpression="RequiredFor"
                                HeaderText="Required Type">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AccountNo" UniqueName="AccountNo" SortExpression="AccountNo"
                                HeaderText="Account No">
                            </telerik:GridBoundColumn>
                            <telerik:GridHyperLinkColumn UniqueName="Link" HeaderText="Process"
                                DataNavigateUrlFields="Link"
                                Text="Process">
                            </telerik:GridHyperLinkColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">&nbsp;
                  <h4 style="color: #3768AD; font-weight: bold">Deletion Pending Rejected Requests:</h4>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <telerik:RadGrid ID="rgPendingDeletion" runat="server" Width="100%" AllowPaging="true" PageSize="5" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="rgPendingDeletion_NeedDataSource">
                    <MasterTableView>
                        <RowIndicatorColumn Visible="False">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Created="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                <ItemTemplate>
                                    <%# Container.DataSetIndex+1 %>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="RequestID" UniqueName="RequestID" SortExpression="RequestID"
                                HeaderText="Request ID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Location" UniqueName="Location" SortExpression="Location"
                                HeaderText="Location">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AccountNo" UniqueName="AccountNo" SortExpression="AccountNo"
                                HeaderText="Account No">
                            </telerik:GridBoundColumn>
                            <telerik:GridHyperLinkColumn UniqueName="Link" HeaderText="Process"
                                DataNavigateUrlFields="Link"
                                Text="Process">
                            </telerik:GridHyperLinkColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Content>
