﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AS.Master" AutoEventWireup="true" CodeBehind="ViewDraftDetails.aspx.cs" Inherits="FinAuth.AuthorisedSignatory.ViewDraftDetails" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheading" runat="server">
    View Addition Draft Details
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 95%; line-height: 15px;" align="center">
        <tr>
            <td style="text-align: left;">
                <telerik:RadGrid ID="rgViewDraftDetails" runat="server" Width="100%" AllowPaging="true" PageSize="5" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="rgViewDraftDetails_NeedDataSource">


                    <MasterTableView>
                        <RowIndicatorColumn Visible="False">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Created="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                <ItemTemplate>
                                    <%# Container.DataSetIndex+1 %>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="AdditionID" UniqueName="AdditionID" SortExpression="AdditionID"
                                HeaderText="Addition ID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Location" UniqueName="Location" SortExpression="Location"
                                HeaderText="Location">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RequiredFor" UniqueName="RequiredFor" SortExpression="RequiredFor"
                                HeaderText="RequiredFor">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EmpName" UniqueName="EmpName" SortExpression="EmpName"
                                HeaderText="Created By">
                            </telerik:GridBoundColumn>
                            <telerik:GridHyperLinkColumn UniqueName="Link"
                                DataNavigateUrlFields="Link"
                                Text="View">
                            </telerik:GridHyperLinkColumn>

                        </Columns>


                    </MasterTableView>
                </telerik:RadGrid>


            </td>
        </tr>
    </table>
</asp:Content>
