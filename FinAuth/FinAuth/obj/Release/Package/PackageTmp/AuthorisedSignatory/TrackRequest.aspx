﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AS.Master" AutoEventWireup="true" CodeBehind="TrackRequest.aspx.cs" Inherits="FinAuth.AuthorisedSignatory.TrackRequest" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheading" runat="server">
    Track Request
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 95%; line-height: 15px;" align="center">
        <tr>
            <td style="text-align: left;">
                <telerik:RadGrid ID="rgTrackRequest" runat="server" Width="100%" AllowPaging="true" PageSize="20" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                    AllowSorting="false" Skin="Web20" OnNeedDataSource="rgTrackRequest_NeedDataSource" OnDetailTableDataBind="rgTrackRequest_DetailTableDataBind" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None">
                    <MasterTableView DataKeyNames="RequestID">
                        <RowIndicatorColumn Visible="False">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Created="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Request ID" UniqueName="Link" AllowFiltering="false">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hylExp" Font-Underline="true" runat="server" ToolTip='<%#Bind("RequestID") %>' Text='<%#Bind("RequestID") %>' NavigateUrl='<%# Eval("Link") %>'>View</asp:HyperLink>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="Location" UniqueName="Location" SortExpression="Location"
                                HeaderText="Location">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AccountNo" UniqueName="AccountNo" SortExpression="AccountNo"
                                HeaderText="Account No">
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn DataField="EmpName" UniqueName="EmpName" SortExpression="EmpName"
                                HeaderText="Created By">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CreatedDate" UniqueName="CreatedDate" SortExpression="CreatedDate"
                                HeaderText="Created Date">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Request" UniqueName="Request" SortExpression="Request"
                                HeaderText="Request">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Status" UniqueName="Status" SortExpression="Status"
                                HeaderText="Status">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <DetailTables>
                            <telerik:GridTableView runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                GridLines="Both" Name="Child" Width="98%">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="SeqNo" UniqueName="SeqNo" HeaderText="Sequence"
                                        SortExpression="SeqNo" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Approver" UniqueName="ApproverID" HeaderText="Approver"
                                        SortExpression="ApproverID" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ApprovaldateTime" DataFormatString="{0:dd/MM/yyyy hh:mm:ss}"
                                        UniqueName="ApprovaldateTime" HeaderText="Approval Date Time" SortExpression="ApprovaldateTime"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Status" UniqueName="Status" HeaderText="Status"
                                        SortExpression="Status" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Comments" UniqueName="Comments" HeaderText="Comments"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="Comments" HeaderStyle-HorizontalAlign="Left">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </telerik:GridTableView>
                        </DetailTables>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Content>
