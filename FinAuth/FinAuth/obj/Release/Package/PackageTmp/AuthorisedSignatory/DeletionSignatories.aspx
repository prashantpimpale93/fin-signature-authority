﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AS.Master" AutoEventWireup="true" CodeBehind="DeletionSignatories.aspx.cs" Inherits="FinAuth.AuthorisedSignatory.DeletionSignatories" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="../Scripts/jquery-1.10.2.min.js"></script>
        <link rel="stylesheet" href="../styles/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="../styles/responsive.css" type="text/css" media="all" />
        <style>

            .rcbSlide {
            z-index:6 !important;
            }
        </style>
        <script type="text/javascript" language="javascript">

            function SelectAllCheckboxes1(chkheader) {
                var grid = $find("<%=rgExistingSignatories.ClientID %>");
                var masterTable = grid.get_masterTableView();
                for (var i = 0; i < masterTable.get_dataItems().length; i++) {
                    var gridItemElement = masterTable.get_dataItems()[i].findElement("chkitem");
                    gridItemElement.checked = chkheader.checked;

                }
            }

        </script>
        <script type="text/javascript">
            function DisableButtons() {
                var inputs = document.getElementsByTagName("INPUT");
                for (var i in inputs) {
                    if (inputs[i].type == "button" || inputs[i].type == "submit") {
                        inputs[i].disabled = true;
                    }
                }
            }
            window.onbeforeunload = DisableButtons;
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheading" runat="server">
    Deletion of Signatories
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>
                <telerik:RadFormDecorator ID="QsfFromDecorator" Skin="Web20" runat="server" DecoratedControls="All"
                    EnableRoundedCorners="true" />
                <telerik:RadAjaxLoadingPanel ID="RAL" Width="10px" Height="10px" runat="server" Skin="Web20" EnableAjaxSkinRendering="true"
                    EnableEmbeddedSkins="true">
                </telerik:RadAjaxLoadingPanel>
                <telerik:RadAjaxManager ID="RAM" runat="server" SkinID="Web20">
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="rcmbRequiredFor">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="rcmbAccountNo" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rcmbBank" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <%--  <telerik:AjaxSetting AjaxControlID="rdoPlant">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="rcmbAccountNo" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rcmbPlant" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rcmbRegion" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="lblBankAddValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="lblBankValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rgExistingSignatories" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>--%>
                        <telerik:AjaxSetting AjaxControlID="rcmbPlant">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="rcmbAccountNo" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rcmbBank" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rgExistingSignatories" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="lblBankValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="lblBankAddValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="rcmbRegion">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="rcmbAccountNo" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rcmbBank" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rgExistingSignatories" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="lblBankValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="lblBankAddValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <%--  <telerik:AjaxSetting AjaxControlID="rdoRegion">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="rcmbAccountNo" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rcmbPlant" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rcmbRegion" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rgExistingSignatories" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="lblBankValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="lblBankAddValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>--%>
                        <%-- <telerik:AjaxSetting AjaxControlID="rdoHO">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="rcmbPlant" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rcmbRegion" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rcmbAccountNo" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rgExistingSignatories" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="lblBankValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="lblBankAddValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>--%>
                        <telerik:AjaxSetting AjaxControlID="rcmbAccountNo">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="rgExistingSignatories" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="lblBankValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="lblBankAddValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="rcmbBank">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="rgExistingSignatories" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="rcmbAccountNo" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="lblBankAddValue" LoadingPanelID="RAL"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
            </td>
        </tr>
    </table>

    <table style="width: 91%; line-height: 15px; margin-bottom: 0px;" align="center">
        <tr>
            <td style="text-align: center;" colspan="4">
                <asp:ValidationSummary ID="vgSummary" ForeColor="Red" DisplayMode="BulletList" runat="server" ShowMessageBox="false" ShowSummary="true"
                    ValidationGroup="Save" />
            </td>
        </tr>
        <tr>

            <td style="text-align: left;">
                <asp:Label ID="lblRequest" runat="server" Font-Bold="True" Text="Request Initiated by location:"></asp:Label><span style="color: Red">*</span>
            </td>
            <td style="text-align: left; vertical-align: top">

                <table>

                    <tr>

                        <td>
                            <asp:RadioButton ID="rdoPlant" GroupName="Addition" Text="Plant" runat="server" AutoPostBack="true" OnCheckedChanged="rdoPlant_CheckedChanged" /></td>
                        <td>
                            <telerik:RadComboBox ID="rcmbPlant" runat="server" AutoPostBack="true" Filter="StartsWith" MaxHeight="250px" OnSelectedIndexChanged="rcmbPlant_SelectedIndexChanged">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                </table>



            </td>
            <td style="text-align: left; vertical-align: top">
                <table>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rdoRegion" GroupName="Addition" Text="Region" runat="server" OnCheckedChanged="rdoRegion_CheckedChanged" AutoPostBack="true" />
                        </td>
                        <td>
                            <telerik:RadComboBox ID="rcmbRegion" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rcmbRegion_SelectedIndexChanged" Filter="StartsWith" MaxHeight="250px">
                            </telerik:RadComboBox>
                        </td>
                    </tr>

                </table>


            </td>
            <td style="text-align: left; vertical-align: top" colspan="2">
                <asp:RadioButton ID="rdoHO" GroupName="Addition" Text="HO" OnCheckedChanged="rdoHO_CheckedChanged" AutoPostBack="true" runat="server" />

            </td>
        </tr>

        <tr>
            <td>
              <table>
                    <tr>
            <td style="text-align: left;">
                <asp:Label ID="lblBank" Font-Bold="True" runat="server" Text="Bank :"></asp:Label><span style="color: Red">*</span>
            </td>
            <td style="text-align: left; vertical-align: top">
                <telerik:RadComboBox ID="rcmbBank" SkinID="Web20" runat="server" AutoPostBack="true" Filter="StartsWith" MaxHeight="250px" OnSelectedIndexChanged="rcmbBank_SelectedIndexChanged">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="reqbank" runat="server" SetFocusOnError="true" ForeColor="Red"
                    ErrorMessage="Select Bank" ControlToValidate="rcmbBank" InitialValue="--Select Bank--"
                    ValidationGroup="Save">*
                </asp:RequiredFieldValidator>
                 <asp:Label ID="lblBankValue" runat="server" Text="" Visible="false"></asp:Label>
            </td>
                        </tr>
                  </table>
                        </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <table>
                    <tr>
                        <td style="padding-top: 5px;">
                         <asp:Label ID="lblAccNo" Font-Bold="True" runat="server" Text="Account No. "></asp:Label><span style="color: Red">*</span></td>
                        <td><telerik:RadComboBox ID="rcmbAccountNo" runat="server" OnSelectedIndexChanged="rcmbAccountNo_SelectedIndexChanged" AutoPostBack="true" Filter="StartsWith" MaxHeight="250px">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="reqAccNo" runat="server" SetFocusOnError="true" ForeColor="Red"
                    ErrorMessage="Select Account Number" ControlToValidate="rcmbAccountNo" InitialValue="--Select Account No--"
                    ValidationGroup="Save">*
                </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>

                <%--<span style="color: Red">*</span>--%>
           
               
            </td>
            <td style="text-align: left; vertical-align: top"></td>
            <td style="text-align: left;">
                <table>
                    <tr>
                        <td style="padding-top: 5px;">
                            <asp:Label ID="lblBankAdd" runat="server" Font-Bold="True" Text="Branch Address :"></asp:Label>&nbsp;&nbsp;</td>
                        <td>
                            <asp:Label ID="lblBankAddValue" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
                <%--<span style="color: Red">*</span>  
                &nbsp;&nbsp;&nbsp; &nbsp;--%>
               
            </td>
            <td style="text-align: left; vertical-align: top"></td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top" colspan="4">
                <br />

            </td>
        </tr>
        <tr>
            <td style="text-align: left;" colspan="4">&nbsp;
                  <h4 style="color: #3768AD; font-weight: bold">Existing Authorised Signatories as per last 'BR Date'":</h4>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top" colspan="4">
                <telerik:RadGrid ID="rgExistingSignatories" runat="server" Width="100%" AllowPaging="false" PageSize="5" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="false"
                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="rgExistingSignatories_NeedDataSource" OnItemDataBound="rgExistingSignatories_ItemDataBound">
                    <ClientSettings>
                        <Scrolling AllowScroll="True" ScrollHeight="150px" UseStaticHeaders="true" />
                    </ClientSettings>
                    <MasterTableView DataKeyNames="EmpID">

                        <RowIndicatorColumn Visible="False">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Created="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                <ItemTemplate>
                                    <%# Container.DataSetIndex+1 %>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="RequiredFor" UniqueName="RequiredType" SortExpression="RequiredType"
                                HeaderText="Required Type">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EmpID" UniqueName="EmpID" SortExpression="EmpID"
                                HeaderText="Emp ID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EmpName" UniqueName="EmpName" SortExpression="EmpName"
                                HeaderText="Emp Name">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Designation" UniqueName="Designation" SortExpression="Designation"
                                HeaderText="Designation">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Type" UniqueName="Type" SortExpression="Type"
                                HeaderText="Type">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn AllowFiltering="false">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkHeader" runat="server" onclick="javascript:SelectAllCheckboxes1(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkitem" runat="server" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top" colspan="4">
                <br />

            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <asp:Label ID="lblDet" runat="server" Font-Bold="True" Text="Details for Deletion "></asp:Label><span style="color: Red">*</span>
            </td>
            <td style="text-align: left; vertical-align: top">
                <asp:RadioButtonList ID="rcmbDetailAddition" runat="server"></asp:RadioButtonList>
                <%--  <telerik:RadComboBox ID="rcmbDetailAddition" runat="server" AutoPostBack="false" Filter="StartsWith" MaxHeight="250px">
                </telerik:RadComboBox>--%>
                <asp:RequiredFieldValidator ID="reqDetail" runat="server" SetFocusOnError="true" ForeColor="Red"
                    ErrorMessage="Select Detail Addition" ControlToValidate="rcmbDetailAddition"
                    ValidationGroup="Save">*
                </asp:RequiredFieldValidator>

            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top" colspan="1">
                <asp:Label ID="lblComments" runat="server" Font-Bold="True" Text="Comments(If Any)"></asp:Label>
            </td>
            <td style="text-align: left; vertical-align: top" colspan="2">
                <asp:TextBox ID="txtComments" TextMode="MultiLine" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top" colspan="4">
                <br />

            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top" colspan="4">

                <asp:Button ID="btnDraft" runat="server" Text="Save as Draft" Width="100px" OnClick="btnDraft_Click" />
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="100px" ValidationGroup="Save" OnClick="btnSubmit_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Reset" Width="100px" CausesValidation="false" OnClick="btnCancel_Click" />
            </td>

        </tr>
    </table>
</asp:Content>
