﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="login" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Finance Authorisation</title>
    <link href="login-box.css" rel="stylesheet" type="text/css" />
 <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">


            function validatectrl() {
                if (window.document.getElementById("<%=txtUserName.ClientID %>").value == "") {
                    window.document.getElementById("<%=txtUserName.ClientID %>").focus();
                    alert("Please Enter User Name");
                    return false;
                }
                else if (window.document.getElementById("<%=txtPassword.ClientID %>").value == "") {
                    window.document.getElementById("<%=txtPassword.ClientID %>").focus();
                    alert("Please Enter Password");
                    return false;

                }
                return true;
            }


            function clearctrl() {
                window.document.getElementById("<%=txtUserName.ClientID %>").value = "";
                window.document.getElementById("<%=txtPassword.ClientID %>").value = "";
            }
        </script>
    </telerik:RadCodeBlock>
</head>
<body>
    <form id="form1" runat="server">



        <div style="padding: 150px 0 0 370px;">
            <div id="login-box">
                <h2>Login</h2>
                <br />
                <br />
                <div id="login-box-name" style="margin-top: 20px;">
                    User Name:
                </div>
                <div id="login-box-field" style="margin-top: 20px;">
                    <asp:TextBox ID="txtUserName" runat="server" class="form-login" title="Username"
                        value="" size="30" MaxLength="2048"></asp:TextBox>
                    <%-- <input name="q" class="form-login" title="Username" value="" size="30" maxlength="2048" />--%>
                </div>
                <div id="login-box-name">
                    Password:
                </div>
                <div id="login-box-field">
                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" class="form-login"
                        title="Password" value="" size="30" MaxLength="2048"></asp:TextBox>
                    <%-- <input name="q" type="password" class="form-login" title="Password" value="" size="30"
                        maxlength="2048" /></div>--%>
                    <br />
                    <span class="login-box-options">
                        <%-- <input type="checkbox" name="1" value="1">
                    Remember Me <a href="#" style="margin-left: 30px;">Forgot password?</a>--%>
                        <asp:Label ID="lblsms" runat="server" Text=""></asp:Label>
                    </span>
                    <br />
                    <br />
                    <%--    <a href="#">--%>
                    <asp:ImageButton ID="imgbtnLogin" runat="server"  Width="103" ImageUrl="~/images/login-btn.png"
                        Height="42" Style="margin-left: 90px;" OnClick="imgbtnLogin_Click" />
                    <%--                    <img src="images/login-btn.png" width="103" height="42" style="margin-left: 90px;" /></a>--%>
                </div>
            </div>

        </div>
    </form>
</body>
</html>
