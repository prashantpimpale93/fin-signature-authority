<!--

/*
Configure menu styles below
NOTE: To edit the link colors, go to the STYLE tags and edit the ssm2Items colors
*/
YOffset=150; // no quotes!!
XOffset=0;
staticYOffset=30; // no quotes!!
slideSpeed=20 // no quotes!!
waitTime=100; // no quotes!! this sets the time the menu stays out for after the mouse goes off it.
menuBGColor="#3AC0F2";
menuIsStatic="yes"; //this sets whether menu should stay static on the screen
menuWidth=200; // Must be a multiple of 10! no quotes!!
menuCols=2;
hdrFontFamily="verdana";
hdrFontSize="2";
hdrFontColor="white";
hdrBGColor="#A1DCF2";
hdrAlign="left";
hdrVAlign="center";
hdrHeight="20";
linkFontFamily="Verdana";
linkFontSize="2";
linkBGColor="white";
linkOverBGColor="#F6E8FA";
linkTarget="_top";
linkAlign="Left";
barBGColor="#672D91";
barFontFamily="Verdana";
barFontSize="2";
barFontColor="white";
barVAlign="center";
barWidth=15; // no quotes!!
barText="MANAGER"; // <IMG> tag supported. Put exact html for an image to show.

///////////////////////////

// ssmItems[...]=[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
ssmItems[0]=["Menu"] //create header
ssmItems[1]=["DashBoard", "../DashBorad/frmMGRDashBoard.aspx", ""]
ssmItems[2]=["Sales Manger", "../UI/frmSalesManager.aspx",""]/// <reference path="../UI/frmSalesManager.aspx" />
//ssmItems[3]=["Manager", "../UI/frmManager.aspx", ""]/// <reference path="../UI/frmManager.aspx" />
ssmItems[3]=["CP Aprroval", "../UI/frmCPApprove.aspx", ""]/// <reference path="../UI/frmCPApprove.aspx" />

ssmItems[4]=["CP Transfer", "#", ""]
ssmItems[5]=["Area", "../UI/frmArea.aspx", ""]/// <reference path="../UI/frmManager.aspx" />
ssmItems[6]=["Project Details", "../UI/frmProjectDetails.aspx", ""]/// <reference path="../UI/frmManager.aspx" />
ssmItems[7]=["FAQ", "#", ""]
ssmItems[8]=["Feedback & Question", "#", ""]


//ssmItems[7]=["FAQ", "http://www.dynamicdrive.com/faqs.htm", "", 1, "no"] //create two column row
//ssmItems[8]=["Email", "http://www.dynamicdrive.com/contact.htm", "",1]


buildMenu();

//-->