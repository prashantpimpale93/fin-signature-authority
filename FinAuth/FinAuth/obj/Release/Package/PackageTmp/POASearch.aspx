﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.Master" AutoEventWireup="true" CodeBehind="POASearch.aspx.cs" Inherits="FinAuth.POASearch" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <%--   <script type="text/javascript" lang="javascript">

            function OnClientSelectedIndexChanged(sender, eventArgs) {
                var item = eventArgs.get_item();
                // alert("You selected " + item.get_text() + ":" + item.get_value());
                var dropdownlist = $find("<%= rcbEmpname.ClientID %>");
                var item1 = dropdownlist.findItemByValue(item.get_value());
                item1.select();
            }
            function OnClientSelectedIndexChanged1(sender, eventArgs) {
                var item = eventArgs.get_item();
                //   alert("You selected " + item.get_text() + ":" + item.get_value());
                var dropdownlist = $find("<%= rcbempId.ClientID %>");
                var item1 = dropdownlist.findItemByValue(item.get_value());
                item1.select();

            }

        </script>--%>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Contentheading" runat="server">
    POA Search
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Web20">
    </telerik:RadAjaxManager>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
        <table style="width: 100%" align="center">

            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend>Filter by Employee  </legend>



                        <table style="width: 80%" align="center">
                           
                            <tr>
                                <td>Select Employee Name
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="rcbEmpname" runat="server" AllowCustomText="false" Filter="StartsWith" MaxHeight="250px" AutoPostBack="true" OnSelectedIndexChanged="rcbEmpname_SelectedIndexChanged"></telerik:RadComboBox>
                                </td>
                            </tr> <tr>
                                <td>Select Employee ID
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="rcbempId" runat="server" Filter="StartsWith" MaxHeight="250px" AutoPostBack="true" OnSelectedIndexChanged="rcbempId_SelectedIndexChanged"></telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Select Status
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="rcbStatus" runat="server" AllowCustomText="false" Filter="StartsWith" MaxHeight="250px">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="--All--" Value="0" />
                                            <telerik:RadComboBoxItem Selected="true" Text="Active" Value="8" />
                                            <telerik:RadComboBoxItem Text="Revoked" Value="9" />
                                        </Items>
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="align-content: center">
                                    <asp:Button ID="btnview" runat="server" Text="View" SkinID="Web20" Width="50px"
                                        OnClick="btnview_Click" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="Rgrequest" runat="server" Width="100%" AllowPaging="false" HeaderStyle-Font-Bold="true"
                        AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="Rgrequest_NeedDataSource" OnPageIndexChanged="Rgrequest_PageIndexChanged">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <PagerStyle AlwaysVisible="true" />
                        <MasterTableView PagerStyle-Mode="NextPrevAndNumeric" NoMasterRecordsText="No POAs Exists on your Name">
                            <RowIndicatorColumn Visible="False">
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Created="True">
                            </ExpandCollapseColumn>
                            <Columns>
                              <%--  <telerik:GridTemplateColumn HeaderText="Sr No.">
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex+1 %>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>--%>
                                 <telerik:GridBoundColumn DataField="POAseqNo" UniqueName="POAseqNo" SortExpression="POAseqNo"
                                    HeaderText="POA Sequence No">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="approveddate" UniqueName="approveddate" SortExpression="approveddate"
                                    HeaderText="Date of Issue" DataFormatString="{0:dd-MMM-yyyy}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="Initiator_id" UniqueName="Initiator_id" SortExpression="Initiator_id"
                                    HeaderText="Employee Code">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Initiator_Name" UniqueName="Initiator_Name" SortExpression="Initiator_Name"
                                    HeaderText="POA holder's Name">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Comments" UniqueName="Comments" SortExpression="Comments"
                                    HeaderText="Purpose/Powers Conferred" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="RequestReson" UniqueName="RequestReson" SortExpression="RequestReson"
                                    HeaderText="Request Reason" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Role" Visible="false" DataField="POArole" UniqueName="POArole" SortExpression="POArole">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="POA Title" DataField="POAtitle" UniqueName="POAtitle" SortExpression="POAtitle">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="POA status" DataField="POAstatus" UniqueName="POAstatus" SortExpression="POAstatus">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="revoke_eff_date" UniqueName="revoke_eff_date" SortExpression="revoke_eff_date"
                                    HeaderText="Date of Revocation" DataFormatString="{0:dd-MMM-yyyy}">
                                </telerik:GridBoundColumn>

                                <telerik:GridTemplateColumn HeaderText="Download POA">
                                    <ItemTemplate>
                                        <a id="aPOADWnl" runat="server" target="_blank" href='<%# "~/UploadedFiles/"+Eval("POApdf") %>'>View</a>

                                    </ItemTemplate>

                                </telerik:GridTemplateColumn>

                            </Columns>

                            <PagerStyle AlwaysVisible="True"></PagerStyle>
                        </MasterTableView>
                    </telerik:RadGrid>
                </td>

            </tr>
        </table>
    </telerik:RadAjaxPanel>
</asp:Content>
