﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/MasterHome.Master" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="FinAuth.home" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        @-webkit-keyframes fadee {
            from {
                opacity: 1;
            }

            to {
                opacity: 0.5;
            }
        }

        @keyframes fadee {
            from {
                opacity: 1;
            }

            to {
                opacity: 0.5;
            }
        }

        #MainHeader {
            -webkit-animation: fadee 2s 1 alternate;
            -webkit-animation-iteration-count: infinite;
            animation: fadee 2s 1 alternate;
            animation-iteration-count: infinite;
        }




    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Web20">
    </telerik:RadAjaxManager>
    <div id="content">
        <h3 runat="server" id="h3header" style="margin-left: 30px; color: #fff;">List of existing POAs</h3>
        <table style="width: 95%">
            <tr>
                <td>
                    <telerik:RadFormDecorator Skin="Web20" ID="FormDecorator1" runat="server" DecoratedControls="All"
                        EnableEmbeddedScripts="true" ControlsToSkip="Zone" />
                    <telerik:RadGrid ID="Rgrequest" runat="server" Width="100%" AllowPaging="false" HeaderStyle-Font-Bold="true"
                        AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None">

                        <HeaderStyle Font-Bold="True"></HeaderStyle>

                        <PagerStyle PageSizes="2,5,10" AlwaysVisible="true" />
                        <MasterTableView PagerStyle-Mode="NextPrevAndNumeric" NoMasterRecordsText="No POAs exists on your name">
                            <RowIndicatorColumn Visible="False">
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Created="True">
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="Sr. No.">
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex+1 %>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="approveddate" UniqueName="Approvaldate" SortExpression="approveddate"
                                    HeaderText="Date of issue" DataFormatString="{0:dd-MMM-yyyy}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="POA title" DataField="POATitle" UniqueName="POATitle" SortExpression="POATitle">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Download POA">
                                    <ItemTemplate>
                                        <a id="aPOADWnl" runat="server" target="_blank" href='<%# "~/UploadedFiles/"+Eval("POApdf") %>'>View</a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>

                            <PagerStyle AlwaysVisible="True"></PagerStyle>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <%-- <asp:Label ID="Label3" runat="server" Text=" To view in Details "></asp:Label>
                    <a href="POASearch.aspx">
                        <h5 style="margin-left: 100px; color: #fff; border-bottom: 1px solid #fff;">Click Here</h5>
                    </a>--%>
                </td>
            </tr>
        </table>

        <center>


        
     <div class="contentbox wfull" >
            <div class="two_third ">
                <!-- P R O J E C T S -->
             
                <div class="prod-a-b prod-grid-2" >
                    <div class="block fl"  id="POAreq" runat="server" visible="false">
                   <h5 style=" color:#fff;border-bottom:0px solid #fff;"> </h5>  <div class="lh13 relative feature_box_pink" style="align-self:center">
                             <p id="p3" runat="server">
                                <img src="../images/check2.png" alt="" class="float_left" style="height: 30px;">
                                <h1 style="margin-left: 30px;" >No  POA request Pending for Approval </h1>
                           </p>
                            <p id="p4" runat="server"  visible="false">
                               <a href="PendingApproval.aspx?R=S" >   <img src="../images/DrawingPin1_Blue.png" alt="" class="float_left" style="height: 30px;">  <h1 style="margin-left: 30px;"><asp:Label ID="lblpoacount" runat="server" Text=""></asp:Label></h1>
                               <h5 style="margin-left: 100px; color:#fff;border-bottom:1px solid #fff;">View Pending Request(s)</h5>
                            </a></p>
                        </div>
                    </div>
                
                    <div class="block fl" runat="server" id="POA" visible="false">
                    <h5 style=" color:#fff;border-bottom:0px solid #fff;">  </h5>  
                       <div class="lh13 relative feature_box_yellow">
                         <p id="p1" runat="server" >
                                <img src="../images/check2.png" alt="" class="float_left" style="height: 30px;">
                                <h1 style="margin-left: 30px; ">No Request pending for Additional Powers </h1>
                           </p>
                            <p id="p2" runat="server" visible="false">
                               <a href="PendingRequests.aspx"  style="color:white" >   <img src="../images/DrawingPin1_Blue.png" alt="" class="float_left" style="height: 30px;">  <h1 style="margin-left: 30px;"><asp:Label ID="lblpoareqcount" runat="server" Text=""></asp:Label></h1>
                               <h5 style="margin-left: 100px;color:#fff;border-bottom:1px solid #fff;">View Pending Request(s)</h5>
                            </a></p>
                        </div>
                    </div>
                
                </div>
                <!-- /P R O J E C T S -->
              
                 <div class="prod-a-b prod-grid-2" style="align-content:center" >
                    <div class="block"  id="poahod" runat="server" style="float:initial !important"  visible="false">
                   <h5 style=" color:#fff;border-bottom:0px solid #fff;"> </h5>  <div class="lh13 relative feature_box_pink" style="align-self:center">
                             <p id="p5" runat="server">
                                <img src="../images/check2.png" alt="" class="float_left" style="height: 30px;">
                                <h1 style="margin-left: 30px;" >No  POA request Pending for Approval </h1>
                           </p>
                            <p id="p6" runat="server"  visible="false">
                               <a href="PendingApproval.aspx?R=H" style="color:white" >   <img src="../images/DrawingPin1_Blue.png" alt="" class="float_left" style="height: 30px;">  <h1 style="margin-left: 30px;"><asp:Label ID="lblPOAHOD" runat="server" Text=""></asp:Label></h1>
                               <h5 style="margin-left: 100px; color:#fff;border-bottom:1px solid #fff;">View Pending Request(s)</h5>
                            </a></p>
                        </div>
                    </div>
                     </div>  <div class="clear">
                    <!-- -->
                </div>
            </div>
         
        </div></center>
        <!-- end_of_contentbox -->
    </div>
</asp:Content>
