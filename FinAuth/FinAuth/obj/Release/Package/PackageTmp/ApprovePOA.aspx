﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.Master" AutoEventWireup="true" CodeBehind="ApprovePOA.aspx.cs" Inherits="FinAuth.ApprovePOA" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" lang="javascript">
            function funValidateAppr() {
                if (confirm('Are you sure you want to Approve POA Request?')) {


                    if (window.document.getElementById("<%=txtApprComments.ClientID %>").value == "") {
                        window.document.getElementById("<%=txtApprComments.ClientID %>").focus();
                        alert("Please Enter Comments for Approval");
                        return false;
                    }

                    return true;

                }
                else { return false; }
            }
            function funValidateRej() {
                if (confirm('Are you sure you want to Reject POA Request ?')) {


                    if (window.document.getElementById("<%=txtApprComments.ClientID %>").value == "") {
                        window.document.getElementById("<%=txtApprComments.ClientID %>").focus();
                        alert("Please Enter Comments for Rejection");
                        return false;
                    }

                    return true;

                }
                else { return false; }
            }

            function funprocess() {
                if (confirm('Are you sure you want to Process POA Request ?')) {


                    if (window.document.getElementById("<%=txtProComment.ClientID %>").value == "") {
                        alert("Please Enter Comments for Processing");
                        return false;
                    }
                    else if (window.document.getElementById("<%=txtTitle.ClientID %>").value == "") {
                        alert("Please Enter POA Title");
                        return false;
                    }
                    else {
                        var flag = false;

                        var inputs = $telerik.findControl(document, "RadAsyncUpload1");
                        // for (var i = 0; i < inputs.length; i++) {
                        //check for empty string or invalid extension
                        if (inputs._selectedFilesCount == 0) {
                            flag = false;
                            alert("Please select valid File. Only .pdf files are allowed.");
                        } else {

                            //if (inputs.validateExtensions()) {

                            flag = true;
                            //}
                            //else {
                            //    flag = false;
                            //    alert("Please select valid File. Only .pdf files are allowed.");
                            //}
                        }
                        //  arguments.IsValid = flag;
                        //    return true;
                        return flag;

                    }
                }
                else { return false; }

            }



        </script>
    </telerik:RadCodeBlock>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contentheading" runat="server">
    APPROVE POA 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Web20">
    </telerik:RadAjaxManager>
    <table style="width: 100%" align="center">
        <tr>
            <td colspan="2">
                <h4 style="color: #3768AD; font-weight: bold">POA type</h4>
                <table style="width: 50%; line-height: 15px;" align="center">
                    <tr>
                        <td></td>
                        <td>
                            <asp:Label ID="lblSubType" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>Requested for :
                        </td>
                        <td>
                            <asp:Label ID="lblReqFor" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>Employee name :
                        </td>
                        <td>
                            <telerik:RadComboBox ID="rcbEmpname" runat="server" Visible="false" AllowCustomText="false" Filter="StartsWith" MaxHeight="250px"></telerik:RadComboBox>
                            <asp:Label ID="LabelEmpname" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>Employee id :
                        </td>
                        <td>
                            <telerik:RadComboBox ID="rcbempId" runat="server" Visible="false" Filter="StartsWith" MaxHeight="250px"></telerik:RadComboBox>
                            <asp:Label ID="LabelempId" runat="server"></asp:Label>
                        </td>
                    </tr>

                    <tr runat="server" id="trreqname" visible="false">
                        <td>Requestor name :
                        </td>
                        <td>

                            <asp:Label ID="LabelRequestorname" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="trreqid" visible="false">
                        <td>Requestor id :
                        </td>
                        <td>

                            <asp:Label ID="LabelRequestorid" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h4 style="color: #3768AD; font-weight: bold">Powers</h4>
                <table style="width: 100%" align="center" id="tblMainRole" runat="server" visible="true">
                    <tr>

                        <td>Role :&nbsp;&nbsp;
                            <telerik:RadComboBox ID="rcbRole" runat="server"></telerik:RadComboBox>
                            <asp:Label ID="LabelRole" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="height: 150px; overflow-y: auto; width: 100%">
                                <telerik:RadGrid ID="Rgrequest" runat="server" Width="98%" AllowPaging="false" HeaderStyle-Font-Bold="true"
                                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="Rgrequest_NeedDataSource">
                                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                                    <ItemStyle Font-Bold="false" />
                                    <MasterTableView PagerStyle-Mode="NextPrevAndNumeric" NoMasterRecordsText="No POAs exists for this role">
                                        <RowIndicatorColumn Visible="False">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn Created="True">
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex+1 %>
                                                    <asp:Label ID="lblPowerid2" runat="server" Text='<%#Bind("PowerId") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblRoleid2" runat="server" Text='<%#Bind("RoleId") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="PowerId" UniqueName="PowerId" SortExpression="PowerId" Visible="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="powerDesc" UniqueName="powerDesc" SortExpression="powerDesc"
                                                HeaderText="Description of Power">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr runat="server" id="tradditonal_pwr" style="width: 100%" align="center">
            <td colspan="2">
                <table style="width: 100%">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label3" runat="server" Text="List of additional powers to be added in your POA :"></asp:Label>
                            <div style="height: 150px; overflow-x: auto; width: 100%">

                                <telerik:RadGrid ID="rgRequestAdditional_selected" runat="server" Width="98%" AllowPaging="false" HeaderStyle-Font-Bold="true"
                                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None"
                                    OnNeedDataSource="rgRequestAdditional_selected_NeedDataSource">
                                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                                    <MasterTableView PagerStyle-Mode="NextPrevAndNumeric" DataKeyNames="PowerId" NoMasterRecordsText="No additional powers selected">
                                        <RowIndicatorColumn Visible="False">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn Created="True">
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex+1 %>
                                                    <asp:Label ID="lblPowerid1" runat="server" Text='<%#Bind("PowerId") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblRoleid1" runat="server" Text='<%#Bind("RoleId") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="Role" UniqueName="Role" SortExpression="Role"
                                                HeaderText="Role">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="powerDesc" UniqueName="powerDesc" SortExpression="powerDesc"
                                                HeaderText="Description of Additional Powers">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PowerId" UniqueName="PowerId" SortExpression="PowerId" Visible="false">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </div>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h4 style="color: #3768AD; font-weight: bold">POA details</h4>
                <table style="width: 100%" align="center">
                    <tr>
                        <td>1.</td>
                        <td>Designation</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtDesignation" ReadOnly="true" runat="server" Width="300px" MaxLength="50"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td nowrap="nowrap">Father's Name/Husband's Name</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtFatherName" ReadOnly="true" runat="server" Width="300px" MaxLength="50"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td>Present address</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtAddress" ReadOnly="true" runat="server" Width="300px" TextMode="MultiLine" Rows="2"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>4.</td>
                        <td>Function</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtFunction" ReadOnly="true" runat="server" Width="300px" MaxLength="50"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>5.</td>
                        <td>Location</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtLocation" ReadOnly="true" runat="server" Width="300px" MaxLength="50"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>6.</td>
                        <td>Comments </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtComments" ReadOnly="true" runat="server" Width="300px" MaxLength="50" TextMode="MultiLine" Rows="2"></asp:TextBox></td>
                        <asp:TextBox ID="txtIOnumber" ReadOnly="true" runat="server" Width="300px" MaxLength="50" Visible="false"></asp:TextBox>
                    </tr>
                    <%-- <tr>
                        <td>IO number</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtIOnumber" ReadOnly="true" runat="server" Width="300px" MaxLength="50"></asp:TextBox></td>
                    </tr>--%>
                    <tr>
                        <td>7.</td>
                        <td>Reason for POA</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtPOAReason" SkinID="Metro" ReadOnly="true" runat="server" Width="300px" MaxLength="50" TextMode="MultiLine" Rows="2"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>8.</td>
                        <td style="width: 450px">Whether you have any active POA issued in your name?
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblPOAholder" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>9.</td>
                        <td style="width: 450px">Whether copy of resolution is required?
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblIsResolution" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="width: 450px"></td>
                        <td style="text-align: left">
                            <asp:Label ID="lblisScanned" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>10.</td>
                        <td>Whether physical Copy will be Required ?
                        </td>
                        <td style="text-align: left;">
                            <asp:Label ID="lblPhysicalCopyRequired" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="trshowreason">
                        <td>11.</td>
                        <td style="width: 450px">Reasons for physical copy
                        </td>

                        <td style="text-align: right; align-self: center">
                            <asp:TextBox ID="txtPhisical_copy_request_reason" ReadOnly="true" runat="server" Width="300px" MaxLength="500" TextMode="MultiLine"
                                Rows="2"></asp:TextBox></td>


                    </tr>
                    <tr id="TrComments" runat="server">
                        <td colspan="3">
                            <h4 style="color: #3768AD; font-weight: bold">Previous comments</h4>
                            <br />
                            <telerik:RadGrid ID="rgComments" runat="server" Width="100%" AllowPaging="false" HeaderStyle-Font-Bold="true" MasterTableView-ShowHeadersWhenNoRecords="false"
                                AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="rgComments_NeedDataSource">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <MasterTableView PagerStyle-Mode="NextPrevAndNumeric" NoMasterRecordsText="">
                                    <RowIndicatorColumn Visible="False">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn Created="True">
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridTemplateColumn HeaderText="Sr No.">
                                            <ItemTemplate>
                                                <%# Container.DataSetIndex+1 %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="PoaId" UniqueName="PoaId" SortExpression="PoaId"
                                            HeaderText="PoaId" Visible="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="POAStatus" UniqueName="POAStatus" SortExpression="POAStatus" Visible="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="POANo" UniqueName="POANo" SortExpression="POANo" HeaderText="POA No">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Empid" UniqueName="Empid" SortExpression="Empid" HeaderText="Employee ID">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="EmpName" UniqueName="EmpName" SortExpression="EmpName" HeaderText="Employee Name">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="role_1" UniqueName="role_1" SortExpression="role_1" HeaderText="Role">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="remarks" UniqueName="remarks" SortExpression="remarks" HeaderText="Comments">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="staus_1" UniqueName="staus_1" SortExpression="staus_1" HeaderText="Status">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Approvaldate" UniqueName="Approvaldate" SortExpression="Approvaldate" HeaderText="Date" DataFormatString="{0:dd-MMM-yyyy}">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>

                            </telerik:RadGrid>
                        </td>
                    </tr>
                    <tr id="TrNewComment_header" runat="server">
                        <td colspan="3">
                            <h4 style="color: #3768AD; font-weight: bold">Approval/Rejection comments</h4>
                            <br />
                        </td>
                    </tr>
                    <tr id="TrNewComment" runat="server">


                        <td style="text-align: left; align-self: center" colspan="3">
                            <asp:TextBox ID="txtApprComments" runat="server" Width="500px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="TrButton" runat="server">
                        <td style="align-content: center" colspan="3">

                            <table width="100%">
                                <tr style="align-content: center">
                                    <td>
                                        <asp:Button ID="btnReject" runat="server" Text="Reject" Width="100px" OnClientClick="return funValidateRej();" OnClick="btnReject_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnApprove" runat="server" Text="Approve" Width="100px" OnClientClick="return funValidateAppr();" OnClick="btnApprove_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="100px" OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="TrProcess" runat="server" visible="true">
                        <td colspan="3">
                            <h4 style="color: #3768AD; font-weight: bold">Process POA</h4>
                            <table style="width: 100%">
                                <tr>
                                    <td>Processing comments 
                                    </td>
                                    <td style="text-align: right; align-self: center">
                                        <asp:TextBox ID="txtProComment" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>POA title 
                                    </td>
                                    <td style="text-align: right; align-self: center">
                                        <asp:TextBox ID="txtTitle" runat="server" MaxLength="250" Width="300px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>POA Sequence No 
                                    </td>
                                    <td style="text-align: right; align-self: center">
                                        <asp:Label ID="lblSeqNo" runat="server" MaxLength="250" Width="300px" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Upload POA</td>
                                    <td>

                                        <telerik:RadAsyncUpload runat="server" ID="RadAsyncUpload1"
                                            HideFileInput="true"
                                            Skin="Web20" MaxFileInputsCount="1"
                                            MultipleFileSelection="Disabled"
                                            AllowedFileExtensions=".pdf" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="btnProcess" runat="server" Text="Process" Width="100px" OnClientClick="return funprocess();" OnClick="btnProcess_Click" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
