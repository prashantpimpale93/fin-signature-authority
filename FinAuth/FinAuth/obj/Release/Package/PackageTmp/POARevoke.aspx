﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.Master" AutoEventWireup="true" CodeBehind="POARevoke.aspx.cs" Inherits="FinAuth.POARevoke" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" lang="javascript">

          <%--  function OnClientSelectedIndexChanged(sender, eventArgs) {
                var item = eventArgs.get_item();
                // alert("You selected " + item.get_text() + ":" + item.get_value());
                var dropdownlist = $find("<%= rcbEmpname.ClientID %>");
                var item1 = dropdownlist.findItemByValue(item.get_value());
                item1.select();
            }
            function OnClientSelectedIndexChanged1(sender, eventArgs) {
                var item = eventArgs.get_item();
                //   alert("You selected " + item.get_text() + ":" + item.get_value());
                var dropdownlist = $find("<%= rcbempId.ClientID %>");
                var item1 = dropdownlist.findItemByValue(item.get_value());
                item1.select();

            }--%>
            function funValidateReveko() {
               <%-- var IsValid = false;
                var gridView = document.getElementById("<%=Rgrequest.ClientID %>");
                //----------------------------------------------
             var grid = $find("<%=Rgrequest.ClientID %>");
                var masterTable = grid.get_masterTableView();
                for (var i = 0; i < masterTable.get_dataItems().length; i++) {
                    var gridItemElement = masterTable.get_dataItems()[i].findElement("chkselect");

                    if (gridItemElement.checked == true && gridItemElement.disabled == false) {

                        IsValid = true;
                        break;
                    }
                }
                if (IsValid == false) {
                    alert("Please select atleat one Power to revoke");
                    return false;
                }
--%>


                if (window.document.getElementById("<%=txtRevokeReason.ClientID %>").value == "") {
                    alert("Please Enter Revokation Reason");
                    return false;
                }
                if (window.document.getElementById("<%=Rdpdate.ClientID %>").value == "") {
                    alert("Please Enter Effiective date of Revocation");
                    return false;
                }
                if (confirm('Are you sure you want to revoke the POA ?')) {
                    return true;
                }

                else { return false; }
            }
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Contentheading" runat="server">
    POA Revoke
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Web20">
    </telerik:RadAjaxManager>
    <%-- <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">--%>
    <table style="width: 100%" align="center">
        <asp:Panel ID="pnlselEMP" runat="server" Visible="true">
            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend>Filter by Employee  </legend>
                        <table style="width: 80%" align="center">

                            <tr>
                                <td>Select Employee Name
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="rcbEmpname" runat="server" AllowCustomText="false" AutoPostBack="true" Filter="StartsWith" MaxHeight="250px" OnSelectedIndexChanged="rcbEmpname_SelectedIndexChanged"></telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Select Employee ID
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="rcbempId" runat="server" Filter="StartsWith" AutoPostBack="true" MaxHeight="250px" OnSelectedIndexChanged="rcbempId_SelectedIndexChanged"></telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="align-content: center">
                                    <asp:Button ID="btnview" runat="server" Text="View" SkinID="Web20" Width="50px"
                                        OnClick="btnview_Click" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </asp:Panel>
        <tr>
            <td colspan="2">
                <telerik:RadGrid ID="Rgrequest" runat="server" Width="100%" AllowPaging="false" HeaderStyle-Font-Bold="true"
                    AllowSorting="True" Skin="Web20" AutoGenerateColumns="false" AllowFilteringByColumn="False" CellSpacing="0" GridLines="None" OnNeedDataSource="Rgrequest_NeedDataSource">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <PagerStyle PageSizes="2,5,10" AlwaysVisible="true" />
                    <MasterTableView PagerStyle-Mode="NextPrevAndNumeric" NoMasterRecordsText="No POAs Exists on your Name">
                        <RowIndicatorColumn Visible="False">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Created="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Sr No.">
                                <ItemTemplate>
                                    <%# Container.DataSetIndex+1 %>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="approveddate" UniqueName="approveddate" SortExpression="approveddate"
                                HeaderText="Date of Issue" DataFormatString="{0:dd-MMM-yyyy}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="POA Title" DataField="POATitle" UniqueName="POATitle" SortExpression="POATitle">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="POA Owner" DataField="Initiator_Name" UniqueName="Initiator_Name" SortExpression="Initiator_Name">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Download POA">
                                <ItemTemplate>
                                    <a id="aPOADWnl" runat="server" target="_blank" href='<%# "~/UploadedFiles/"+Eval("POApdf") %>'>View</a>
                                    <asp:Label ID="lblPOAid" runat="server" Text='<%#Bind("POAId") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblPOAno" runat="server" Text='<%#Bind("POAno") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblInitiator_id" runat="server" Text='<%#Bind("Initiator_id") %>' Visible="false"></asp:Label>

                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Select">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkselect" runat="server" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <PagerStyle AlwaysVisible="True"></PagerStyle>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td>Effective Date of Revocation</td>
            <td>
                <telerik:RadDatePicker ID="Rdpdate" runat="server" Culture="en-IN" Skin="Web20" PopupDirection="TopRight" Font-Bold="false">
                    <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                        FastNavigationNextText="&amp;lt;&amp;lt;">
                    </Calendar>
                </telerik:RadDatePicker>
            </td>
        </tr>
        <tr>
            <td>Reason for revocation
            </td>
            <td style="text-align: left; align-self: center">
                <asp:TextBox ID="txtRevokeReason" runat="server" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>

        <tr style="align-content: center">
            <td></td>
            <td>
                <asp:Button ID="btnRevoke" runat="server" Text="Revoke" Width="100px" OnClientClick="return funValidateReveko();" OnClick="btnRevoke_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Reset" Width="100px" OnClick="btnCancel_Click" />
            </td>

        </tr>
    </table>
    <%-- </telerik:RadAjaxPanel>--%>
</asp:Content>
